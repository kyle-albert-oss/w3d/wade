module.exports = {
	plugins: ["import"],
	extends: ["react-app", "plugin:import/errors", "plugin:import/warnings", "plugin:import/typescript", "prettier"],
	parserOptions: {
		project: "./tsconfig.eslint.json",
	},
	rules: {
		"import/default": "off", // not working with TS right now for export = (https://github.com/benmosher/eslint-plugin-import/issues/1527)
		"import/named": "off", // not needed with TS.
		"import/namespace": "off", // not needed with TS.
		"import/no-cycle": "off", // takes long, using madge for this
		"import/no-internal-modules": "off",
		"import/no-useless-path-segments": [
			"warn",
			{
				noUselessIndex: true,
			},
		],
		"import/order": [
			"warn",
			{
				"newlines-between": "always",
			},
		],
		"no-shadow": ["error"],
		"prefer-const": [
			"warn",
			{
				destructuring: "all",
			},
		],
		"require-await": "off",
	},
	overrides: [
		{
			files: ["**/*.ts?(x)"],
			rules: {
				"@typescript-eslint/explicit-function-return-type": "off",
				"@typescript-eslint/no-inferrable-types": "off",
				"@typescript-eslint/require-await": "warn",
			},
		},
	],
	settings: {
		"import/extesions": [".js", ".ts", ".tsx"],
		"import/parsers": {
			"@typescript-eslint/parser": [".ts", ".tsx"],
		},
	},
};
