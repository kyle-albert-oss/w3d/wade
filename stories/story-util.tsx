import * as React from "react";
import { useMount } from "react-use";
import { connect, Provider, useSelector } from "react-redux";
import { ResizableBox, ResizableBoxProps } from "react-resizable";
import CssBaseline from "@material-ui/core/CssBaseline";
import { withKnobs } from "@storybook/addon-knobs";
import { StoryApi } from "@storybook/addons";
import { storiesOf } from "@storybook/react";
import * as History from "history";
import * as PIXI from "pixi.js";
import { DndProvider } from "react-dnd";
import { HTML5Backend as HTML5DndBackend } from "react-dnd-html5-backend";
import { DeepReadonly } from "ts-essentials";
import tsDefaults from "ts-defaults";
import { ConnectedRouter } from "connected-react-router";

import { configureStore } from "../src/store/redux";
import { formsDisabledSelector, graphicChunksSelector } from "../src/store/redux/selectors";
import { AppRootState, PartialAppRootState } from "../src/store/redux/types/state";
import { FormDisableContext } from "../src/ui/components/context/form-disable";
import { ConnectedMaterialThemeProvider } from "../src/ui/routes/shared/MaterialThemeProvider";
import "./story.scss";
import "typeface-roboto";
import { MaterialProps, withMaterial } from "../src/ui/components/core/material";
import { getDispatchActionMapper, IActionProps } from "../src/store/redux/actions";
import { AddRemove, TRACKED_KEYS } from "../src/models/wade";
import { attachSharedStoreListeners, attachAppStoreListeners } from "../src/store/redux/listeners";
import { DEFAULT_MAP_TEXTURE_OPTS } from "../src/models/pixi";

import { getDefaultInitialState } from "./store";

type MarkOptional<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;
type ResizeProps = MarkOptional<ResizableBoxProps, "height" | "width">;

export interface IStoryOpts {
	knobs?: boolean;
	reduxStore?: boolean | PartialAppRootState;
	resizable?: ResizeProps | boolean;
	router?: boolean | History.MemoryHistoryBuildOptions;
	style?: React.CSSProperties;
}

export const defaultStoryOpts: DeepReadonly<IStoryOpts> = {
	knobs: true,
	reduxStore: true,
	resizable: true,
};

const StoryApp = connect(
	null,
	getDispatchActionMapper()
)(
	withMaterial(((props) => {
		const { actions, children, classes } = props;

		const disabled = useSelector<AppRootState, boolean>((state) => formsDisabledSelector(state));
		useMount(() => {
			document.addEventListener("keydown", (e) => {
				const key = e.key;
				if (!TRACKED_KEYS.has(key)) {
					return;
				}
				actions.updateKeysDown(key, AddRemove.ADD);
			});
			document.addEventListener("keyup", (e) => {
				const key = e.key;
				if (!TRACKED_KEYS.has(key)) {
					return;
				}
				actions.updateKeysDown(key, AddRemove.REMOVE);
			});
		});

		return (
			<div className={classes.root}>
				<CssBaseline />
				<FormDisableContext.Provider value={{ disabled }}>{children}</FormDisableContext.Provider>
			</div>
		);
	}) as React.FC<IActionProps & MaterialProps>)
);

export const createStory = (name: string, storyModule: NodeModule, opts: DeepReadonly<IStoryOpts> = defaultStoryOpts): StoryApi<any> => {
	let sb = storiesOf(name, storyModule);
	opts = tsDefaults(opts, defaultStoryOpts);

	const { knobs, style, reduxStore, resizable, router } = opts;

	if (knobs) {
		sb = sb.addDecorator(withKnobs);
	}
	sb = sb.addDecorator((storyFn) => <DndProvider backend={HTML5DndBackend}>{storyFn()}</DndProvider>);
	if (style) {
		sb = sb.addDecorator((storyFn) => <div style={style}>{storyFn()}</div>);
	}
	if (resizable) {
		sb = sb.addDecorator((storyFn) => (
			<ResizableBox
				className="story-resize"
				height={1000}
				minConstraints={[100, 100]}
				width={1000}
				{...(typeof resizable === "object" ? (resizable as ResizeProps) : {})}
			>
				<div className="story-resize-inner">{storyFn()}</div>
			</ResizableBox>
		));
	}

	if (reduxStore) {
		const initialState: PartialAppRootState = typeof reduxStore === "boolean" ? getDefaultInitialState() : reduxStore;
		// @ts-expect-error
		const historyOpts: History.MemoryHistoryBuildOptions = {
			...(typeof router === "object" ? router : undefined),
		};
		const history = History.createMemoryHistory(historyOpts);
		const store = attachAppStoreListeners(
			attachSharedStoreListeners(
				configureStore<AppRootState>({ history, initialState })
			)
		);

		// TODO: refactor as store listener
		const graphicChunks = graphicChunksSelector(store.getState());
		for (const gc of graphicChunks) {
			if (gc.url) {
				PIXI.Texture.from(gc.url, DEFAULT_MAP_TEXTURE_OPTS);
			}
		}

		sb = sb.addDecorator((storyFn) => (
			<ConnectedMaterialThemeProvider>
				<StoryApp>{storyFn()}</StoryApp>
			</ConnectedMaterialThemeProvider>
		));
		if (router) {
			sb = sb.addDecorator((storyFn) => <ConnectedRouter history={history}>{storyFn()}</ConnectedRouter>);
		}
		sb = sb.addDecorator((storyFn) => <Provider store={store}>{storyFn()}</Provider>);
	}

	return sb;
};
