import _ from "lodash";

import { defaultTheme } from "../../src/models/material";
import {
	graphicInitialState,
	mapDefInitialState,
	mapInitialState,
	PartialAppRootState,
	projectInitialState,
	uiInitialState,
} from "../../src/store/redux/types/state";
import wl6MapDef from "../../assets/map-defs/2e30a080-bea9-11e9-a91c-f1f09fb2dd09.json";
import wl6Palette from "../../assets/palettes/eb9e1e60-c1f4-11e9-a7d2-13e2d0a6422c.json";
import maps from "../../assets-test/stories/maps.json";
import vswap from "../../assets-test/stories/vswap.json";
import { GameProjectType } from "../../src/models/project";

// NOTE: This does NOT do a recursive merge, so reducer defaults may not apply here
export const getDefaultInitialState = (): PartialAppRootState => {
	const theme = _.cloneDeep(defaultTheme);

	// @ts-ignore
	return {
		graphic: {
			...graphicInitialState,
			activePalette: wl6Palette as any,
			availablePaletteRefs: {
				"eb9e1e60-c1f4-11e9-a7d2-13e2d0a6422c": { id: "eb9e1e60-c1f4-11e9-a7d2-13e2d0a6422c", name: "WL6" },
				PAL1: { id: "PAL1", name: "Palette C" },
				PAL2: { id: "PAL2", name: "Palette A" },
				PAL3: { id: "PAL3", name: "Palette B" },
				PAL4: { id: "PAL4", name: "Palette F" },
				PAL5: { id: "PAL5", name: "Palette E" },
				PAL6: { id: "PAL6", name: "Palette D" },
			},
			...(vswap as any),
		},
		map: {
			...mapInitialState,
			maps: maps,
		},
		mapDef: {
			...mapDefInitialState,
			activeMapDef: wl6MapDef as any,
			availableMapDefsRefs: {
				"2e30a080-bea9-11e9-a91c-f1f09fb2dd09": {
					id: "2e30a080-bea9-11e9-a91c-f1f09fb2dd09",
					name: "WL6",
					extensions: ["WL6"],
					isTemplate: true,
				},
				MAPDEF1: { id: "MAPDEF1", name: "Map Def B" },
				MAPDEF2: { id: "MAPDEF2", name: "Map Def A" },
				MAPDEF3: { id: "MAPDEF3", name: "Map Def C" },
			},
		},
		project: {
			...projectInitialState,
			activeProject: {
				id: "ID1",
				name: "Active Project",
				type: GameProjectType.ORIGINAL,
				mapDefinitionId: "2e30a080-bea9-11e9-a91c-f1f09fb2dd09",
				mapHeight: 64,
				mapWidth: 64,
				paletteId: "eb9e1e60-c1f4-11e9-a7d2-13e2d0a6422c",
			},
			availableProjectRefs: {
				ID1: {
					id: "ID1",
					name: "Active Project",
				},
				ID2: {
					id: "ID2",
					name: "Project 2",
				},
				ID3: {
					id: "ID3",
					name: "Project 3",
				},
			},
			recentProjectIds: ["ID2"],
		},
		ui: {
			...uiInitialState,
		},
		theme,
	};
};
