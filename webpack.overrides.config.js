const path = require("path");

const webpack = require("webpack");
const { paths } = require("react-app-rewired");
const { appendWebpackPlugin, editWebpackPlugin } = require("@rescripts/utilities");
const ThreadsPlugin = require("threads-plugin");

const addAlias = (config, aliasObj) => {
	config.resolve = {
		...config.resolve,
		alias: {
			...((config.resolve && config.resolve.alias) || {}),
			...aliasObj,
		},
	};
	return config;
};

const baseWebpack = (config) => {
	config = editWebpackPlugin(
		(p) => {
			p.memoryLimit = p.options.memoryLimit = 4096;
			p.useTypescriptIncrementalApi = p.options.useTypescriptIncrementalApi = false;
			return p;
		},
		"ForkTsCheckerWebpackPlugin",
		config
	);
	config = applyPixiJSHack(config);
	config = applyWorkerHack(config);
	return config;
};

const applyWorkerHack = (config) => {
	config = appendWebpackPlugin(new ThreadsPlugin({ target: "electron-node-worker" }), config);
	return config;
};

const applyPixiJSHack = (config) => {
	config = addAlias(config, {
		"pixi.js": path.resolve(__dirname, "./src/pixi.js"),
		"pixi.js-stable": path.resolve(__dirname, "./node_modules/pixi.js"),
	});
	return config;
};

const applyElectronHack = (config) => {
	config.node = {
		global: true,
		fs: true,
	};
	config.target = "electron-renderer";
	return config;
};

// noinspection WebpackConfigHighlighting
module.exports = {
	storybookWebpack: (config) => {
		config = baseWebpack(config);
		config = editWebpackPlugin(
			(p) => {
				p.tsconfig = p.options.tsconfig = path.resolve(__dirname, "tsconfig.stories.json");
				return p;
			},
			"ForkTsCheckerWebpackPlugin",
			config
		);

		// add stories dir
		const babelLoader = config.module.rules.find((r) => r && r.loader && r.loader.endsWith("node_modules/babel-loader/lib/index.js"));
		if (!babelLoader) {
			throw new Error(`Could not find babel-loader.`);
		}
		babelLoader.include.push(path.resolve(__dirname, "stories"));

		// remove eslint
		const eslintIndex = config.module.rules.findIndex(
			(r) => r && r.use && r.use[0] && r.use[0].loader && r.use[0].loader.includes("eslint-loader")
		);
		if (eslintIndex === -1) {
			console.warn("Warning: could not find eslint loader");
		} else {
			config.module.rules.splice(eslintIndex, 1);
		}

		config.node = { ...(config.node || {}), fs: "empty" };
		config = addAlias(config, {
			"fs-extra": path.resolve(__dirname, "./.storybook/fsExtraMock.js"),
		});

		config = appendWebpackPlugin(new webpack.DefinePlugin({ "process.env.STORYBOOK": true }), config);

		return config;
	},
	webpack: (config) => {
		config = baseWebpack(config);
		config = applyElectronHack(config);
		return config;
	},
};
