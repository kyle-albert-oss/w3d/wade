import * as os from "os";
import * as path from "path";

import * as fs from "fs-extra";
import * as gulp from "gulp";

import { inlineTask, rmTask, execTask, GulpTask, cmd, inlineSyncTask } from "./gulp-util";

const { dest, parallel, series, src, task } = gulp;

const srcDir = "src";

// using CI2 env because cra env.CI makes linting warnings errors for some reason.
const jestTask = (name: string, opts?: { ci?: boolean; coverage?: boolean }) =>
	execTask(name, cmd("rescripts test", { conditionalParams: { "--coverage": opts?.coverage }, params: ["--watchAll=false"] }));
const eslintCmd = (opts?: { fix?: boolean }): string => {
	return cmd(`eslint '${srcDir}/**/*.{js,jsx,ts,tsx}'`, { conditionalParams: { "--fix": opts?.fix } });
};
const electronBuilderCmd = (opts: { linux?: boolean; mac?: boolean; win?: boolean; sign?: boolean }) => {
	if (!opts.linux && !opts.mac && !opts.win) {
		throw new Error(`No build target specified.`);
	}
	return cmd("electron-builder build", {
		params: [`-${opts?.mac ? "m" : ""}${opts?.win ? "w" : ""}${opts?.linux ? "l" : ""}`],
		conditionalParams: {
			"-c.mac.identity=null": !opts?.sign,
		},
	});
};

let wadeResourceDir: string;
if (os.platform() === "darwin") {
	wadeResourceDir = `${os.homedir()}/Library/Application Support/WolfensteinADE/app-resources`;
} else if (os.platform() === "win32") {
	// TODO
}

//
// DEV
//

task(
	"dev:prepare-app-resources",
	series(
		rmTask("dev:delete-app-resources", (d) => d(wadeResourceDir, { force: true })),
		inlineSyncTask("dev:copy-app-resources", () => {
			fs.copySync(path.resolve(process.cwd(), "assets"), wadeResourceDir, { overwrite: true, recursive: true });
		})
	)
);

//
// CLEAN
//
rmTask("clean:rest", (d) => d(["./build", "./coverage", "./out"])); // we don't need to clean dist because electron-builder creates this and says it's automatically cleaned: https://www.electron.build/multi-platform-build
task("clean", parallel("clean:rest"));

//
// LINT
//

execTask("lint", eslintCmd());
execTask("lint:fix", eslintCmd({ fix: true }));

//
// TEST
//

execTask("test:cyclic", `madge -c --warning --ts-config ./tsconfig.json --extensions js,jsx,ts,tsx --no-spinner ./${srcDir}`);
execTask("test:lint", eslintCmd());
jestTask("test:unit", { ci: true });
jestTask("test:unit-with-coverage", { ci: true, coverage: true });

const baseTestTasks: GulpTask[] = ["test:cyclic", "test:lint"];
const getTestTasks = (opts?: { coverage?: boolean }): GulpTask[] => {
	const tasks = [...baseTestTasks];
	tasks.push(opts?.coverage ? "test:unit-with-coverage" : "test:unit");
	return tasks;
};

//
// BUILD
//

const buildTasks: GulpTask[] = [
	execTask("build:dep", "npm rebuild --update-binary > /dev/null"),
	execTask("build:electron:main", "tsc -p ./tsconfig.electron.json"),
	execTask("build:react", "rescripts build"),
];

// these are separate since we are generally only building one platform at a time (separate CI Job).
execTask("build:electron:linux", electronBuilderCmd({ linux: true }));
execTask("build:electron:mac", electronBuilderCmd({ mac: true, sign: true }));
execTask("build:electron:win", electronBuilderCmd({ win: true }));

execTask("ci:build:electron:linux", electronBuilderCmd({ linux: true }));
execTask("ci:build:electron:mac", electronBuilderCmd({ mac: true })); // this only works when using my custom gitlab ci mac runner. Will have to do until gitlab has mac runners later this year.
execTask("ci:build:electron:win", electronBuilderCmd({ win: true }));

//
// PREPACK
//

task("prepack:clean", parallel(rmTask("clean:mac-scripts-resources", (d) => d("build-resources/mac/scripts/resources"))));

// can't figure out how to bundle files external to the .app in a pkg right now, so bundling the base assets with the scripts
task(
	"prepack:mac",
	series(inlineTask("prepack:mac:copy-scripts", () => src("assets/**").pipe(dest("build-resources/mac/scripts/resources"))))
);

//
// DIST
//

const distTasks: GulpTask[] = [];

//
// TOP-LEVEL TASKS
//

const getTopLevelBuildTasks = (opts: {
	ci?: boolean;
	test?: boolean;
	coverage?: boolean;
	linux?: boolean;
	mac?: boolean;
	win?: boolean;
}): GulpTask[] => {
	if (!opts.linux && !opts.mac && !opts.win) {
		throw new Error("No build target specified.");
	}
	const tasks: GulpTask[] = ["clean"];
	if (opts.test) {
		tasks.push(opts.coverage ? "build:test-with-coverage" : "build:test");
	}
	tasks.push(...buildTasks);
	if (opts.linux) {
		tasks.push(`${opts.ci ? "ci:" : ""}build:electron:linux`);
	}
	if (opts.mac) {
		tasks.push("prepack:clean");
		tasks.push("prepack:mac");
		tasks.push(`${opts.ci ? "ci:" : ""}build:electron:mac`);
	}
	if (opts.win) {
		tasks.push(`${opts.ci ? "ci:" : ""}build:electron:win`);
	}
	tasks.push(...distTasks);
	return tasks;
};

task("test", parallel(...getTestTasks()));
task("build:test", parallel(...getTestTasks()));
task("build:test-with-coverage", parallel(...getTestTasks({ coverage: true })));

task("build:linux", series(...getTopLevelBuildTasks({ test: true, linux: true })));
task("build:mac", series(...getTopLevelBuildTasks({ test: true, mac: true })));
task("build:win", series(...getTopLevelBuildTasks({ test: true, win: true })));

//
// CI TOP-LEVEL TASKS, we want to be as granular as possible to split into pipeline stages
// Adding build:dep to ensure the stage box doesn't run into this annoying dep binary issue.
//

task("ci:test", series("build:dep", parallel(...getTestTasks({ coverage: true }))));

task("ci:build:linux", series("build:dep", ...getTopLevelBuildTasks({ ci: true, linux: true })));
task("ci:build:mac", series("build:dep", ...getTopLevelBuildTasks({ ci: true, mac: true })));
task("ci:build:win", series("build:dep", ...getTopLevelBuildTasks({ ci: true, win: true })));
