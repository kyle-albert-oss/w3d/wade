# WADE

An open source Electron-based Wolfenstein 3D map editor.\
**This is project is an alpha state.**

## Demo

See https://kyle-albert-oss.gitlab.io/w3d/wade-storybook. It takes long to load because the process for loading the game resources into storybook converts the maps and
graphics to redux state JSON. This JSON is bundled in the storybook bundle. In the desktop application the data is read from the filesystem.

## Screenshots

<img src="./docs/screenshots/map-editor-1.png" height="500" alt="Map Editor" />
<img src="./docs/screenshots/map-def-edit-1.png" height="500" alt="Map Definition Editor" />

## Features

- Create and edit projects for Wolfenstein 3D mods, both original game file formats and ECWolf formats.
- Edit and save Wolfenstein 3D maps for the following game file formats: (other formats untested or unsupported)
  - WL6
  - ECWolf PK3
- Map drawing tools (pencil, eraser, rectangular fill/outline, fill, dropper)
- Map select/deselection
  - TODO: copy/paste support
  - TODO: undo/redo support
- Create and edit map definitions.

## Development

**NOTICE: WADE is currently untested in Windows and Linux environments (both binary and development workflows). `electron-builder` is capable of producing Windows and Linux builds but likely code changes will need to be made to get them to work.**

### Requirements

- Node >= 12.14.1

### Tools Used

- TypeScript
- React (create-react-app)
- Electron
- Material UI
- Redux
- Storybook

### First-time setup

#### Dev Mode

##### Install/Update Dependencies

```shell script
npm i
```

##### Base Resource Setup

In order to run the application in development mode, you'll need execute the below command in the terminal to copy the minimal data the app needs to run.\
This includes map definitions, palettes, and other base resources. It will copy it to your OS's app data folder under the folder `WolfensteinADE`.

```shell script
npm run dev:setup
```

#### Storybook

Storybook is the recommended way to develop new functionality unless native APIs are needed (file system, etc) as it will quickly become cumbersome to do repititous workflows with usage of electron.\
In order to use storybook, you will need the original WL6 game files to provide game data. The WL6 files should be copied to `assets-test/game-files/wl6`. After copying the files, run

```shell script
npm run script -- dev-scripts/create-story-data.ts
```

### Electron Dev Mode

```shell script
npm run dev
```

### Build

```shell script
npm run build:<platform>
```

### Storybook

```shell script
npm run storybook
```

### Workflows/Reminders

#### Upgrading Electron

Sentry symbols need to be regenerated:

```shell script
npm run gen:sentry-symbols
```

### Directories

- `assets` houses baseline resources for the app to function. This data will be installed by the app installer.
- `assets-test` houses resources used by tests and stories. Because the original game assets still require a license they must be manually copied to this directory in order for certain things to work.
  - `assets-test/game-files/wl6` WL6 game resources should be copied here for story/test data purposes.
- `build` the electron build folder
- `build-resources` install scripts and anything to be bundled with the app installer.
- `dev-scripts` scripts to facilitate development (code gen, story data generation, etc)
- `docs` additional documentation
- `public` static assets to be served by electron
- `src` main source directory containing the electron app
- `stories` anything specific to stories

### Troubleshooting

#### Compiled against a different Node.js version

```
Error: The module '...'
was compiled against a different Node.js version using
NODE_MODULE_VERSION x. This version of Node.js requires
NODE_MODULE_VERSION y.
```

##### 🔧 Fix

Run

```shell script
npm rebuild --update-binary
```
