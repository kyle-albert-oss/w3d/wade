import { Plugin, Viewport } from "pixi-viewport";
import "jest-extended";

declare global {
	interface ArrayConstructor {
		isArray<T>(arg: T): arg is unknown extends T ? Extract<any[], T> : T extends readonly any[] ? T : T & any[];
	}
}

declare module "pixi-viewport" {
	interface Plugin {
		active?: boolean;
		parent: Viewport;
	}

	interface Viewport {
		input: any;
	}
}
