import "./errors";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import { HTML5Backend as HTML5DndBackend } from "react-dnd-html5-backend";
import { DndProvider } from "react-dnd";

import "typeface-roboto";

import "./index.css";
import { AppHistory, AppStore } from "./app-data";
import App from "./ui/routes/App";
import { ConnectedMaterialThemeProvider } from "./ui/routes/shared/MaterialThemeProvider";
import { ErrorPage } from "./ui/components/core/ErrorPage";

ReactDOM.render(
	<ErrorPage>
		<Provider store={AppStore}>
			<DndProvider backend={HTML5DndBackend}>
				<ConnectedRouter history={AppHistory}>
					<ConnectedMaterialThemeProvider>
						<App />
					</ConnectedMaterialThemeProvider>
				</ConnectedRouter>
			</DndProvider>
		</Provider>
	</ErrorPage>,
	document.getElementById("root")
);
