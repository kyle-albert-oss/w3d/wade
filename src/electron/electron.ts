/* eslint-disable import/first */
import * as path from "path";

import * as Sentry from "@sentry/electron";
import * as fs from "fs-extra";
import { app, BrowserWindow, dialog } from "electron";
import isDev from "electron-is-dev";
import chalk from "chalk";

// Sentry.init({
// 	dsn: "https://0a950dab04d94d47adac8b8f73daaf03@sentry.io/5176838",
// });

let mainWindow: Electron.BrowserWindow | undefined;

if (app.isReady()) {
	app.quit();
	throw new Error("Ready too soon");
}

if (process.platform === "darwin") {
	app.commandLine.appendArgument("--enable-features=Metal");
}

const configureAppLocalStoragePath = () => {
	const appFolderName = "WolfensteinADE"; // less likely to clash with anything else
	// by default, electron maps to %APPDATA% which is the roaming folder. Roaming should only contain files that can be transferred painlessly across computers (ex. cookie for a website).
	// we need to map to %LOCALAPPDATA% instead.
	if (process.platform === "win32" && process.env.LOCALAPPDATA) {
		app.setPath("appData", process.env.LOCALAPPDATA);
	}
	const appFolderPath = path.resolve(app.getPath("appData"), appFolderName);
	const appResourcesPath = path.join(appFolderPath, "app-resources"); // need a subfolder because electron app stores stuff in the root directory and we don't want to clash.
	fs.mkdirpSync(appResourcesPath);
	app.setPath("userData", appFolderPath);
};

configureAppLocalStoragePath();

function createWindow() {
	mainWindow = new BrowserWindow({
		width: 1440,
		height: 1440,
		webPreferences: {
			nodeIntegration: true,
			nodeIntegrationInWorker: true,
			preload: path.join(__dirname, "sentry.js"),
		},
	});
	const mw = mainWindow;
	mw.loadURL(isDev ? "http://localhost:3000" : `file://${path.join(__dirname, "../build/index.html")}`);
	if (isDev) {
		const { default: installExtension, REACT_DEVELOPER_TOOLS, REDUX_DEVTOOLS } = require("electron-devtools-installer");

		mw.webContents.once("dom-ready", () => {
			mw.webContents.once("devtools-opened", () => mw.focus());
			mw.webContents.openDevTools();
		});

		Promise.all([installExtension(REACT_DEVELOPER_TOOLS), installExtension(REDUX_DEVTOOLS)])
			.then((extensions) => console.debug(`Installed extra extensions: ${extensions.join(", ")}`))
			.catch(() => console.error("Failed to install extra dev extensions."));
	}
	mw.on("closed", () => (mainWindow = undefined));
}

app.on("ready", createWindow);

app.on("window-all-closed", () => {
	if (process.platform !== "darwin") {
		app.quit();
	}
});

app.on("activate", () => {
	if (mainWindow == null) {
		createWindow();
	}
});

export const selectDirectory = () => {
	return dialog.showOpenDialog(mainWindow!, {
		buttonLabel: "Select Folder",
		properties: ["openDirectory"],
	});
};

export const execFile = (cmd: string, parameters?: readonly string[]) => {
	const childProcesses = require("child_process");
	console.debug(chalk`👟 {blue Executing {bold ${[cmd, ...(parameters ?? [])].join(" ")}}}`);
	const childProcess = childProcesses.execFile(cmd, parameters ?? null);
};
