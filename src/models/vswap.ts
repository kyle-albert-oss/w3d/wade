import { StrictOmit } from "ts-essentials";
import { IVswapGraphicChunk } from "wolf3d-data";

export interface IWadeVswapGraphicChunk extends StrictOmit<IVswapGraphicChunk, "rawData"> {
	rawData?: ImageBitmap;
	url?: string;
}
