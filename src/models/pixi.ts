import * as PIXI from "pixi.js";
import { DeepReadonly } from "ts-essentials";

export type PixiTextureOpts = ConstructorParameters<typeof PIXI.BaseTexture>[1];
export const DEFAULT_MAP_TEXTURE_OPTS: DeepReadonly<PixiTextureOpts> = {
	scaleMode: PIXI.SCALE_MODES.NEAREST,
};
