import { AppTreeItemData } from "./wade";

export interface ISimpleTreeNode {
	label: string;
}

export interface ISettingsGroupTreeNode extends ISimpleTreeNode {
	subRoute: string;
}

export type SettingsGroupTreeItemData = AppTreeItemData<ISettingsGroupTreeNode>;
