import { EnumValues } from "enum-values";
import * as t from "io-ts";
import { IGameMap } from "wolf3d-data";
import { dictionary } from "./wade";

export enum TileDirection {
	NORTH = 2,
	NORTH_EAST = 1,
	EAST = 0,
	SOUTH_EAST = 7,
	SOUTH = 6,
	SOUTH_WEST = 5,
	WEST = 4,
	NORTH_WEST = 3,
	// for doors
	NORTH_SOUTH = 8,
	EAST_WEST = 9,
}
const tileDirectionLiterals = EnumValues.getValues<TileDirection>(TileDirection).map((d) => t.literal(d));
const TileDirectionValues = t.union([tileDirectionLiterals[0], tileDirectionLiterals[1], ...tileDirectionLiterals.slice(2)]);

export enum TileType {
	ACTOR = "Actor",
	BONUS = "Bonus",
	BOSS = "Boss",
	DEAD = "Dead",
	DEAF = "Deaf",
	DOOR = "Door",
	END_LEVEL = "End Level",
	ELEVATOR = "Elevator",
	FLOOR_CODE = "Floor Code",
	KEY = "Key",
	LOCKED_DOOR = "Locked Door",
	MOVING = "Moving",
	PATROL = "Patrol",
	PLAYER_START = "Start",
	SECRET = "Secret",
	SOLID = "Solid",
	STATIC = "Static",
	WALL = "Wall",
}
const tileTypeLiterals = EnumValues.getValues<TileType>(TileType).map((type) => t.literal(type));
const TileTypeValues = t.union([tileTypeLiterals[0], tileTypeLiterals[1], ...tileTypeLiterals.slice(2)]);

export enum GraphicMethod {
	CUSTOM_IMAGE = "CUSTOM_IMAGE",
	GRID = "GRID",
	VSWAP = "VSWAP",
}
const graphicMethodLiterals = EnumValues.getValues<GraphicMethod>(GraphicMethod).map((type) => t.literal(type));
const GraphicMethodValues = t.union([graphicMethodLiterals[0], graphicMethodLiterals[1], ...graphicMethodLiterals.slice(2)]);

export const MapTileJson = t.intersection([
	t.type({
		code: t.number,
		label: t.string,
		plane: t.number,
	}),
	t.partial({
		customImage: t.string,
		difficulty: t.number,
		direction: TileDirectionValues,
		graphicIndex: t.number,
		graphicMethod: GraphicMethodValues,
		gridImage: t.array(t.union([t.string, t.null])),
		keyId: t.string, // key for locked doors
		tags: t.array(t.string),
		types: dictionary(TileTypeValues, t.literal(1)),
		variants: t.array(t.number),
	}),
]);
export type MapTileData = t.TypeOf<typeof MapTileJson>;

export enum PlaneType {
	DEFAULT = "DEFAULT",
	OBJECT = "OBJECT",
	WALL = "WALL",
}
const planeTypeLiterals = EnumValues.getValues<PlaneType>(PlaneType).map((type) => t.literal(type));
const PlaneTypeValues = t.union([planeTypeLiterals[0], planeTypeLiterals[1], ...planeTypeLiterals.slice(2)]);

export const MapPlaneDefinitionJson = t.intersection([
	t.type({
		index: t.number,
		name: t.string,
		tiles: t.record(t.string, MapTileJson), // tileCode -> tileData
		tilesOrdering: t.array(t.number),
		type: PlaneTypeValues,
	}),
	t.partial({
		defaultToHighLowByteMode: t.boolean,
		keys: t.record(t.string, t.string), // keyId -> tileCode
		tagGroups: t.array(t.array(t.string)),
		zIndex: t.number,
	}),
]);
export type MapPlaneDefinition = t.TypeOf<typeof MapPlaneDefinitionJson>;

export interface IEditableMap {
	data: IGameMap;
	fileName?: string; // for pk3 writes (<fileName>.wad)
	hasUnsavedChanges?: boolean;
	index: number;
}

export interface IMapPlaneXYData {
	code?: number;
	key: string;
	map: number;
	plane: number;
	x: number;
	y: number;
}
