enum MapCanvasTool {
	DROPPER = "DROPPER",
	ERASER = "ERASER",
	FILL = "FILL",
	PENCIL = "PENCIL",
	RECTANGLE_FILL = "RECTANGLE_FILL",
	RECTANGLE_OUTLINE = "RECTANGLE_OUTLINE",
	SELECTION_RECTANGLE = "SELECTION_RECTANGLE",
}

// each group can only have one selected at a time
export const brushTools: readonly MapCanvasTool[] = [
	MapCanvasTool.PENCIL,
	MapCanvasTool.RECTANGLE_FILL,
	MapCanvasTool.RECTANGLE_OUTLINE,
	MapCanvasTool.FILL,
];
export const selectionTools: readonly MapCanvasTool[] = [MapCanvasTool.SELECTION_RECTANGLE];

export interface ISelectedMapCanvasTools {
	[key: string]: boolean;
}

export default MapCanvasTool;
