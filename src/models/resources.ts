import { EnumValues } from "enum-values";
import * as iots from "io-ts";
import { StrictOmit } from "ts-essentials";
import { IGameMap } from "wolf3d-data";

export enum GameResourceFilename {
	AUDIOT = "AUDIOT",
	AUDIOHED = "AUDIOHED",
	GAMEMAPS = "GAMEMAPS",
	MAPHEAD = "MAPHEAD",
	VGADICT = "VGADICT",
	VGAGRAPH = "VGAGRAPH",
	VSWAP = "VSWAP",
}

export const gameResourceFilenameRegex: RegExp = new RegExp(`^(${EnumValues.getValues(GameResourceFilename).join("|")}).*`, "i");

// Game Resources
export const GameResourceJson = iots.type({
	id: iots.string,
	name: iots.string,
	wadeVersion: iots.string,
});
export type GameResource = iots.TypeOf<typeof GameResourceJson>;

export type GameResourceRef = Pick<GameResource, "id" | "name">;
export type GameResourceId = GameResourceRef["id"];
export type NewGameResource<T extends GameResource> = StrictOmit<T, "id" | "wadeVersion">;

export interface IGameResourceReqPayload {
	type: GameResourceFilename;
	/**
	 * Path(s) of resource(s).
	 * Some resources need other resources to load.
	 *
	 * Conventions:
	 *  GAMEMAPS: type = GAMEMAP, path = [GAMEMAPS path, MAPHEAD path]
	 *  VSWAP: type = VSWAP, path = [VSWAP path, PaletteId]
	 */
	path: string | string[]; // some resources need other resources to load
}

export type WadeResourceType = "map-def" | "palette" | "project";
export interface IWadeResourceReqPayload {
	id: GameResourceId;
	type: WadeResourceType;
}

export interface IWadeAvailableResourcesReqPayload {
	type: WadeResourceType;
}

export interface IWadeDeleteResourcePayload {
	ids: readonly GameResourceId[];
	type: WadeResourceType;
}

export type ResourceReqPayload = IGameResourceReqPayload | IWadeResourceReqPayload | IWadeAvailableResourcesReqPayload;

export interface IWadeResourcesAvailableRespPayload extends IWadeAvailableResourcesReqPayload {
	resourceRefs: GameResourceRef[];
}

export interface IWadeResourceWriteReqPayload<D extends GameResource = GameResource> {
	resourceData: D;
	type: WadeResourceType;
}

export interface IWadeResourceWriteRespPayload {
	resourceRef: GameResourceRef;
	type: IWadeResourceWriteReqPayload["type"];
}

export type ResourceDataPayload<D = GameResource, R extends ResourceReqPayload = ResourceReqPayload> = R & {
	resourceData: D;
	resourceRef?: GameResourceRef;
};

export interface IWriteGamemapsReqPayload {
	maps: IGameMap[];
	type: GameResourceFilename.GAMEMAPS;
}
