import * as t from "io-ts";

import { MapPlaneDefinitionJson } from "./map";
import { GameResourceJson, GameResourceRef } from "./resources";

export const WL1_MAP_DEF_ID = "af50d520-72ec-11ea-940d-d3f33367aed1";
export const WL6_MAP_DEF_ID = "2e30a080-bea9-11e9-a91c-f1f09fb2dd09";
export const SOD_MAP_DEF_ID = "101e9730-72ec-11ea-bd7d-6b7fd442ec9e";

export const MapDefinitionDifficultyJson = t.type({
	name: t.string,
});
export type MapDefinitionDifficulty = t.TypeOf<typeof MapDefinitionDifficultyJson>;

export const WadeMapDefinitionJson = t.intersection([
	GameResourceJson,
	t.type({
		extensions: t.array(t.string),
		planes: t.record(t.string, MapPlaneDefinitionJson),
	}),
	t.partial({
		difficulties: t.array(MapDefinitionDifficultyJson),
		isTemplate: t.boolean,
	}),
]);
export type WadeMapDefinition = t.TypeOf<typeof WadeMapDefinitionJson>;

export interface IWadeMapDefRef extends GameResourceRef, Pick<WadeMapDefinition, "extensions" | "isTemplate"> {}
