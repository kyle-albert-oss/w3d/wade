# Models

This directory is intended for TypeScript types, constants, and other building blocks. It should have very limited if not zero algorithmic code.
It was created to clean things up and prevent circular dependencies.

**To prevent circular dependencies, code in this directory should not depend on anything in any other directory except node_modules.**
