import { red } from "@material-ui/core/colors";
import { ThemeOptions } from "@material-ui/core/styles/createMuiTheme";
import { ComponentsProps } from "@material-ui/core/styles/props";
import { DeepReadonly } from "ts-essentials";

const defaultProps: DeepReadonly<ComponentsProps> = {
	MuiButtonBase: {
		disableRipple: true,
	},
	MuiButton: {
		disableRipple: true,
	},
	MuiButtonGroup: {
		disableRipple: true,
	},
	MuiCircularProgress: {
		disableShrink: true,
	},
	MuiDialog: {
		disableBackdropClick: true,
	},
	MuiIconButton: {
		disableRipple: true,
	},
	MuiInputLabel: {
		shrink: true,
	},
	MuiOutlinedInput: {
		notched: true,
	},
	MuiPopover: {
		transitionDuration: 0,
	},
	MuiSlider: {
		valueLabelDisplay: "auto",
	},
	MuiTextField: {
		size: "small",
		variant: "outlined",
	},
};

export const defaultTheme: DeepReadonly<ThemeOptions> = {
	palette: {
		type: "dark",
		primary: {
			main: red[900],
		},
		secondary: {
			main: "#AAAAAA",
		},
	},
	props: defaultProps,
};
