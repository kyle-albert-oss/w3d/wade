import * as t from "io-ts";

import { GameResourceJson, GameResourceRef } from "./resources";

export const MAX_COLORS = 256;
export const WL6_PAL_ID = "eb9e1e60-c1f4-11e9-a7d2-13e2d0a6422c";
export const SOD_PAL_ID = "beb2cac0-72ef-11ea-9477-a3267326f0a3";

const RGBTupleJson = t.tuple([t.number, t.number, t.number]);
export type RGBTuple = t.TypeOf<typeof RGBTupleJson>;

const GamePaletteDataJson = t.type({
	colors: t.array(RGBTupleJson),
});
export type IGamePaletteData = t.TypeOf<typeof GamePaletteDataJson>;

export const GamePaletteJson = t.intersection([
	GameResourceJson,
	t.type({
		data: GamePaletteDataJson,
		extensions: t.array(t.string),
	}),
	t.partial({
		isTemplate: t.boolean,
	}),
]);

export type IGamePalette = t.TypeOf<typeof GamePaletteJson>;

export interface IGamePaletteRef extends GameResourceRef, Pick<IGamePalette, "extensions"> {}
