import { GetTreeItemChildrenFn, TreeItem } from "react-sortable-tree";
import * as t from "io-ts";
import { unsafeCoerce } from "fp-ts/function";

export const MAX_RECENT_PROJECTS = 5;
export enum AddRemove {
	ADD = "ADD",
	REMOVE = "REMOVE",
}
export const TRACKED_KEYS = new Set<string>(["Control", "Shift", "Meta", "A", "C", "V", "Z"]);
export type ImgSrc = string | undefined;
export type ImgSrcs = ImgSrc | ReadonlyArray<string | null>;

export type AppTreeItemData<T = any> = Pick<TreeItem, "title" | "subtitle" | "expanded"> & {
	children?: AppTreeItemData<T>[] | GetTreeItemChildrenFn;
	item: T;
};

export const LEFT_NAV_WIDTH_PX = 240;

export const MAX_COLOR_PRESETS = 16;
export const DEFAULT_COLOR_PRESETS = [
	"#000000",
	"#2472C8",
	"#0DBC79",
	"#11A8CD",
	"#CD3131",
	"#BC3FBC",
	"#E5E510",
	"#E5E5E5",
	"#666666",
	"#3B8EEA",
	"#23D18B",
	"#29B8DB",
	"#F14C4C",
	"#D670D6",
	"#F5F543",
	"#E5E5E5",
];

export const MAP_EDIT_COLOR_PRESETS = [
	"#000000",
	"#0000AA",
	"#00AA00",
	"#00AAAA",
	"#AA0000",
	"#AA00AA",
	"#AA5500",
	"#AAAAAA",
	"#555555",
	"#5555FF",
	"#55FF55",
	"#55FFFF",
	"#FF5555",
	"#FF55FF",
	"#FFFF55",
	"#FFFFFF",
];

export interface DictionaryC<D extends t.Mixed, C extends t.Mixed>
	extends t.DictionaryType<
		D,
		C,
		{
			[K in t.TypeOf<D>]?: t.TypeOf<C>;
		},
		{
			[K in t.OutputOf<D>]?: t.OutputOf<C>;
		},
		unknown
	> {}

export const dictionary = <D extends t.Mixed, C extends t.Mixed>(domain: D, codomain: C, name?: string): DictionaryC<D, C> => {
	return unsafeCoerce(t.record(t.union([domain, t.undefined]), codomain, name));
};
