import { EnumValues } from "enum-values";
import * as t from "io-ts";
import { DeepReadonly } from "ts-essentials";

import { GameResourceJson, IWriteGamemapsReqPayload, NewGameResource, GameResourceFilename } from "./resources";

export enum GameProjectType {
	ECWOLF = "ECWolf",
	ORIGINAL = "Original",
}

const gameProjectTypeLiterals = EnumValues.getValues<GameProjectType>(GameProjectType).map((type) => t.literal(type));
const GameProjectTypeValues = t.union([gameProjectTypeLiterals[0], gameProjectTypeLiterals[1]]);

export const GameProjectJson = t.intersection([
	GameResourceJson,
	t.type({
		type: GameProjectTypeValues,
	}),
	t.partial({
		gameResourceDir: t.string, // for original game resources (*.WL6, *.SOD, etc)
		gameResourceExt: t.string, // for original game resources
		mapDefinitionId: t.string,
		mapHeight: t.number,
		mapWidth: t.number,
		maxDoors: t.number,
		maxActors: t.number,
		maxStatics: t.number,
		paletteId: t.string,
		path: t.string, // path to project file containing this data. Calculated when project is loaded (not persisted)
		pk3FilePath: t.string, // for EC wolf based resources,
		playArgs: t.array(t.string),
		playCmd: t.string,
	}),
]);
export type GameProject = t.TypeOf<typeof GameProjectJson>;
export type NewGameProject = NewGameResource<GameProject>;

export interface IGameProjectExtra {
	// to help with loading of projects
	projectAction?: "open";
	gamemapsFilePath?: string;
	mapheadFilePath?: string;
	pk3StagingDir?: string;
	vswapFilePath?: string;
}

export interface ICompileProjectResult {
	complete: boolean;
	maps?: boolean | number;
	pk3?: boolean; // pk3 rewritten
}

export type WriteProjectExtResourceReqPayload = { project: DeepReadonly<GameProject> } & IWriteGamemapsReqPayload;

export type ResourceSrc = "pk3" | GameResourceFilename;
