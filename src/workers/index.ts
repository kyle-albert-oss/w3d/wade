import { spawn, Worker, Thread, ModuleThread } from "threads";

import { ResourceWorker } from "./resource.worker";

/**
 * Workers for processing resource requests in the background to avoid clogging the UI thread and making the app jittery.
 * A UI listener is registered in each worker instance and in the UI a listener is also registered to listen to worker messages. Multiple
 */

const getNewResourceWorker = async (): Promise<ModuleThread<ResourceWorker>> => spawn<ResourceWorker>(new Worker("./resource.worker"));

/**
 * Utility methods for the UI process to communicate with workers.
 */
export const withResourceWorker = async <O>(fn: (worker: ResourceWorker) => Promise<O>): Promise<O> => {
	try {
		const worker = await getNewResourceWorker();
		const result = await fn(worker);
		await Thread.terminate(worker);
		return result;
	} catch (e) {
		console.error("Worker error:", e);
		throw e;
	}
};
