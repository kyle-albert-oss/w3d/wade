import through from "through2";

import { GameResourceRef, GameResourceFilename, WadeResourceType } from "../models/resources";

/**
 * Things shared by workers and the main/ui threads.
 */

export type ReadResourceReturnType<D = any, E = any> = {
	extra?: E;
	resourceData: D;
	resourceRef?: GameResourceRef;
	type: GameResourceFilename | WadeResourceType;
};

type WorkerizedObj<T> = {
	[K in keyof T]: WorkerSafe<T[K]>;
};
export type WorkerSafe<T> = T extends Function ? never : T extends object ? WorkerizedObj<T> : T;

export type WorkerListener<I = any, O = any> = (msg: I) => Promise<O>;

export const thruFilter = <T = any>(filterFn: (item: T) => boolean) =>
	through.obj(function (this: any, item: T, enc: unknown, next: () => void) {
		if (filterFn(item)) {
			this.push(item);
		}
		next();
	});
