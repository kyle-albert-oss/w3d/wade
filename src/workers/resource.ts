import * as path from "path";

import * as filenamify from "filenamify";
import { getOrElse } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/pipeable";
import fs from "fs-extra";
import * as t from "io-ts";
import klaw from "klaw";
import chalk from "chalk";
import { unzipToFolder } from "wolf3d-data";
import { DeepReadonly } from "ts-essentials";

import { WadeMapDefinition, WadeMapDefinitionJson } from "../models/map-definition";
import { GamePaletteJson, IGamePalette } from "../models/palette";
import { GameProject, GameProjectJson, IGameProjectExtra } from "../models/project";
import {
	GameResource,
	GameResourceFilename,
	gameResourceFilenameRegex,
	GameResourceId,
	GameResourceRef,
	WadeResourceType,
} from "../models/resources";
import { getWadeMapDefPath, getWadePalettePath, getWadeProjectPath, getWadeZipStagingPath } from "../store/local/resources";
import { getAppVersion } from "../util/env";
import { generateUUID } from "../util/strings";
import { getNextDedupedName } from "../util/resources";
import { getJSONPropsFromFile, getJSONFilePaths, updateIdOrNameInRootJSON } from "../util/files";

import { thruFilter } from "./shared";

/**
 * A bunch of utility methods to read and write app/game resources. Intended for use in the worker processes but should also be usable in other processes.
 */

const defaultErrorHandler = <T>(filePath: string) =>
	getOrElse<t.Errors, T>((errors) => {
		throw new Error(
			`Failed to parse file at ${filePath}\n${errors
				.sort(({ message: m1 }, { message: m2 }) => {
					if ((m1 && m2) || m1 == m2) {
						return 0;
					}
					return m1 ? -1 : 1;
				})
				.map(
					(e) =>
						`<<MSG>> ${e.message}; <<VALUE>> ${e.value}; <<CONTEXT>> ${e.context.map(
							(c, i, arr) => `${c.key}${i === arr.length - 1 ? ` = ${c.actual}` : ""}`
						)}.`
				)
				.join("\n")}`
		);
	});

export const autoDetectGameResourceExts = (dir: string): Promise<string[]> =>
	new Promise((r, rj) => {
		const exts = new Set<string>();
		klaw(dir)
			.pipe(thruFilter((item) => !item.stats.isDirectory() && gameResourceFilenameRegex.test(path.basename(item.path))))
			.on("data", (item) => {
				const itemPath = item.path as string;
				const match = path.basename(itemPath).match(gameResourceFilenameRegex);
				if (match == null) {
					throw new Error(
						`autoDetectedGameResourceExts: ${path.basename(itemPath)} unexpectedly did not match ${gameResourceFilenameRegex}.`
					);
				}
				const ext = path.extname(match[0]);
				exts.add(ext);
			})
			.on("end", () => r([...exts]))
			.on("error", (err: any, item: any) => rj(err));
	});

export const findGameResources = async (basePath: string): Promise<IGameProjectExtra> =>
	new Promise((r, rj) => {
		const extra = {} as IGameProjectExtra;
		klaw(basePath)
			.pipe(thruFilter((item) => !item.stats.isDirectory() && gameResourceFilenameRegex.test(path.basename(item.path))))
			.on("data", (item) => {
				const itemPath = item.path as string;
				const match = path.basename(itemPath).match(gameResourceFilenameRegex);
				if (match == null) {
					throw new Error(
						`Error finding game resources: ${path.basename(itemPath)} unexpectedly did not match ${gameResourceFilenameRegex}.`
					);
				}
				const filename = match[1].toLocaleUpperCase() as GameResourceFilename;
				switch (filename) {
					case GameResourceFilename.GAMEMAPS:
						extra.gamemapsFilePath = itemPath;
						break;
					case GameResourceFilename.MAPHEAD:
						extra.mapheadFilePath = itemPath;
						break;
					case GameResourceFilename.VSWAP:
						extra.vswapFilePath = itemPath;
						break;
					default:
						break;
				}
			})
			.on("end", () => r(extra))
			.on("error", (err: any, item: any) => rj(err));
	});

export const readGameResource = async <T extends GameResource>(decoder: t.Decode<string, T>, filePath: string): Promise<T> => {
	filePath = filenamify.path(filePath);
	const json = await fs.readJSON(filePath);
	return pipe(decoder(json), defaultErrorHandler(filePath));
};

export const getGameResourceRef = <R extends GameResourceRef = GameResourceRef>(filePath: string): Promise<R> =>
	getJSONPropsFromFile<R>(filePath, /^(id|name|extensions|isTemplate)\b/);

export const readAvailableResourcesInDir = async <R extends GameResourceRef = GameResourceRef>(basePath: string): Promise<R[]> => {
	const filePaths = await getJSONFilePaths(basePath);

	const promises = [] as Array<Promise<R>>;
	for (const filePath of filePaths) {
		promises.push(getGameResourceRef<R>(filePath));
	}

	return Promise.all(promises);
};

export const deleteGameResources = async (resourceIds: readonly GameResourceId[], basePath: string): Promise<GameResourceId[]> => {
	for (const resourceId of resourceIds) {
		const resourcePath = path.resolve(basePath, `${resourceId}.json`);
		fs.unlinkSync(resourcePath);
		console.debug(chalk`💥 {yellow Deleted resource {bold ${resourcePath}}.}`);
	}
	return Promise.resolve(resourceIds as Array<GameResourceRef["id"]>);
};

type WriteGameResourceMorph<T> = Omit<T, "wadeVersion"> & { wadeVersion?: string };
export const writeGameResource = async <T extends DeepReadonly<GameResource>, E extends T = T>(
	encoder: t.Encode<E, E>,
	data: WriteGameResourceMorph<T>,
	filePath: string
): Promise<GameResourceRef> => {
	filePath = filenamify.path(filePath, { replacement: "_" });

	if (!data.id) {
		throw new Error(`Cannot write resource because it is missing an id.`);
	}

	console.debug(chalk`💿 {blue Writing resource file {bold ${filePath}}...}`);

	await fs.mkdirp(path.dirname(filePath));
	const resourceData = encoder({ ...data, wadeVersion: getAppVersion() } as E);
	await fs.writeJSON(filePath, resourceData, { spaces: "\t" });

	console.debug(chalk`✅️ {green Wrote resource file {bold ${filePath}}.}`);

	return {
		id: resourceData.id,
		name: resourceData.name,
	};
};

export const copyGameResource = async <R extends GameResourceRef = GameResourceRef>(opts: {
	basePath: string;
	destId?: string;
	destName?: string;
	srcId: string;
	type: WadeResourceType;
}): Promise<R> => {
	let { basePath, destId, destName, srcId, type } = opts;

	let commonPath: string;
	if (type === "map-def") {
		commonPath = getWadeMapDefPath(basePath);
	} else if (type === "palette") {
		commonPath = getWadePalettePath(basePath);
	} else {
		throw new Error(`Copy is not supported for resources of type '${type}'`);
	}

	const srcPath = path.join(commonPath, `${srcId}.json`);
	if (!destId) {
		destId = generateUUID();
	}
	const destPath = path.join(commonPath, `${destId}.json`);
	if (!destName) {
		const { name: srcName } = await getGameResourceRef(srcPath);
		const existingNames = (await readAvailableResourcesInDir(commonPath)).map((r) => r.name);
		destName = getNextDedupedName(srcName, existingNames);
	}

	console.debug(chalk`💿 {blue Copying resource file {bold ${srcPath}} to {bold ${destPath}}...}`);
	fs.copyFileSync(srcPath, destPath);
	await updateIdOrNameInRootJSON(destPath, { newId: destName, newName: destName });
	console.debug(chalk`✅️ {green Wrote resource file {bold ${destPath}}.}`);

	return getGameResourceRef<R>(destPath);
};

export const createZipStagingFolderForProject = async (
	baseResourcePath: string,
	zipFilePath: string,
	projectId: string
): Promise<string> => {
	const zipFileName = path.basename(zipFilePath);
	const nowMs = Date.now();
	const stagingPath = getWadeZipStagingPath(baseResourcePath, `${projectId}/${zipFileName}-${nowMs}`);
	console.debug(chalk`💿 {blue Creating staging directory {bold ${stagingPath}}...}`);
	return unzipToFolder(zipFilePath, stagingPath); // returns the staging dir
};

export const cleanZipStagingFolder = (stagingDirPath: string) => {
	if (fs.existsSync(stagingDirPath)) {
		fs.removeSync(stagingDirPath);
	}
};

export const deleteMapDefinitions = async (mapDefinitionIds: readonly GameResourceId[], basePath?: string): Promise<GameResourceId[]> =>
	deleteGameResources(mapDefinitionIds, getWadeMapDefPath(basePath));
export const readMapDefinition = async (mapDefinitionId: string, basePath?: string): Promise<WadeMapDefinition> =>
	readGameResource(WadeMapDefinitionJson.decode, getWadeMapDefPath(basePath, `${mapDefinitionId}.json`));
export const writeMapDefinition = async (data: WadeMapDefinition, basePath?: string): Promise<GameResourceRef> =>
	writeGameResource(WadeMapDefinitionJson.encode, data, getWadeMapDefPath(basePath, `${data.id}.json`));

export const deletePalettes = async (paletteIds: readonly GameResourceId[], basePath?: string): Promise<GameResourceId[]> =>
	deleteGameResources(paletteIds, getWadePalettePath(basePath));
export const readPalette = async (paletteId: string, basePath?: string): Promise<IGamePalette> =>
	readGameResource(GamePaletteJson.decode, getWadePalettePath(basePath, `${paletteId}.json`));
export const writePalette = async (data: DeepReadonly<WriteGameResourceMorph<IGamePalette>>, basePath?: string): Promise<GameResourceRef> =>
	writeGameResource(GamePaletteJson.encode, data, getWadePalettePath(basePath, `${data.id}.json`));

export const deleteProjects = async (projectIds: readonly GameResourceId[], basePath?: string): Promise<GameResourceId[]> =>
	deleteGameResources(projectIds, getWadeProjectPath(basePath));
export const readProject = async (projectId: string, basePath?: string): Promise<GameProject> =>
	readGameResource(GameProjectJson.decode, getWadeProjectPath(basePath, `${projectId}.json`));
export const writeProject = async (data: DeepReadonly<GameProject>, basePath?: string): Promise<GameResourceRef> =>
	writeGameResource(GameProjectJson.encode, data as GameProject, getWadeProjectPath(basePath, `${data.id}.json`));
