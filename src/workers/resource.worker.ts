import * as path from "path";

import archiver from "archiver";
import * as fs from "fs-extra";
import {
	GameMapsReader,
	GameMapsWriter,
	VswapFileReader,
	IGameMap,
	readECWolfMapWad,
	writeECWolfMapWad,
	WAD_FILE_NAME_ERROR_HINT,
	isValidWadFileName,
	zipFolder,
	ECWolfMap,
} from "wolf3d-data";
import chalk from "chalk";
import { expose } from "threads/worker";
import Conf from "conf";
import { DeepReadonly } from "ts-essentials";

import { VswapWorkerCanvasIngester } from "../game/data/vswap/VswapCanvasIngester";
import { IEditableMap } from "../models/map";
import { IWadeMapDefRef, WadeMapDefinition } from "../models/map-definition";
import { IGamePalette, IGamePaletteRef } from "../models/palette";
import { GameProject, IGameProjectExtra } from "../models/project";
import {
	GameResource,
	GameResourceFilename,
	GameResourceId,
	GameResourceRef,
	IGameResourceReqPayload,
	IWadeDeleteResourcePayload,
	IWadeResourceReqPayload,
	IWadeResourceWriteReqPayload,
	WadeResourceType,
} from "../models/resources";
import { AppConfigKeys, fetchAppConfigKeys, getLocalConfigOptions, IAppConfig } from "../store/local/config";
import { getWadeMapDefPath, getWadePalettePath, getWadeProjectPath } from "../store/local/resources";
import { GraphicState } from "../store/redux/types/state";
import { arrayToRODictionary, safeAssign } from "../util/objects";
import { logPreFsWrite } from "../util/log";
import { allChildFiles } from "../util/files";
import { WDCMapDefinitionFileConverter, IWDCToWadeConversionOpts } from "../game/data/maps/MapDefinitionFiles";
import { ICopyWadeResourceOpts, ICopyWadeResourceResult } from "../store/redux/types/action";

import {
	autoDetectGameResourceExts,
	deleteMapDefinitions,
	deletePalettes,
	deleteProjects,
	findGameResources,
	readAvailableResourcesInDir,
	readMapDefinition,
	readPalette,
	readProject,
	writeMapDefinition,
	writePalette,
	writeProject,
	createZipStagingFolderForProject,
	copyGameResource,
} from "./resource";
import { WorkerListener, WorkerSafe } from "./shared";

const onDeleteResource: WorkerListener<
	{ baseResourcePath?: string; request: IWadeDeleteResourcePayload },
	IWadeDeleteResourcePayload
> = async ({ baseResourcePath, request }) => {
	const { ids, type } = request;
	let deletedIds: GameResourceId[];
	if (type === "map-def") {
		deletedIds = await deleteMapDefinitions(ids, baseResourcePath);
	} else if (type === "palette") {
		deletedIds = await deletePalettes(ids, baseResourcePath);
	} else if (type === "project") {
		deletedIds = await deleteProjects(ids, baseResourcePath);
	} else {
		throw new Error(`Delete resources: resource type ${type} is unsupported.`);
	}

	return {
		ids: deletedIds,
		type,
	};
};

const onGetBootstrapData: WorkerListener<{ resourceBasePath: string }> = async ({ resourceBasePath }) => {
	const appConfig = new Conf<IAppConfig>(getLocalConfigOptions(resourceBasePath));
	const [config, mapDefs, palettes, projects] = await Promise.all([
		fetchAppConfigKeys([AppConfigKeys.Projects.Recent, AppConfigKeys.ColorPresets], appConfig),
		readAvailableResourcesInDir<IWadeMapDefRef>(getWadeMapDefPath(resourceBasePath)),
		readAvailableResourcesInDir<IGamePaletteRef>(getWadePalettePath(resourceBasePath)),
		readAvailableResourcesInDir(getWadeProjectPath(resourceBasePath)),
	]);

	const recentProjectIds = config[AppConfigKeys.Projects.Recent] || [];
	return {
		graphic: {
			availablePaletteRefs: arrayToRODictionary(palettes, "id"),
		},
		mapDef: {
			availableMapDefsRefs: arrayToRODictionary(mapDefs, "id"),
		},
		project: {
			availableProjectRefs: arrayToRODictionary(projects, "id"),
			recentProjectIds,
		},
		ui: {
			colorPresets: config[AppConfigKeys.ColorPresets] ?? [],
			isProjectDialogOpen: true,
			selectedProjectId: recentProjectIds?.[0],
		},
	};
};

const onReadAvailableResources: WorkerListener<
	{ resourceBasePath?: string; type: WadeResourceType },
	{ resourceRefs: GameResourceRef[]; type: WadeResourceType }
> = async ({ resourceBasePath, type }) => {
	let resourceRefs: GameResourceRef[];

	if (type === "map-def") {
		resourceRefs = await readAvailableResourcesInDir(getWadeMapDefPath(resourceBasePath));
	} else if (type === "palette") {
		resourceRefs = await readAvailableResourcesInDir(getWadePalettePath(resourceBasePath));
	} else if (type === "project") {
		resourceRefs = await readAvailableResourcesInDir(getWadeProjectPath(resourceBasePath));
	} else {
		resourceRefs = [];
	}

	return { resourceRefs, type };
};

const onReadResource: WorkerListener<
	{ baseResourcePath: string; extra?: any; request: IWadeResourceReqPayload | IGameResourceReqPayload },
	{ extra?: any; resourceData: any; resourceRef?: GameResourceRef; type: GameResourceFilename | WadeResourceType }
> = async ({ baseResourcePath, extra, request }) => {
	if ("id" in request) {
		const { id, type } = request;
		let resourceData: GameResource;

		if (type === "map-def") {
			resourceData = await readMapDefinition(id, baseResourcePath);
		} else if (type === "palette") {
			resourceData = await readPalette(id, baseResourcePath);
		} else if (type === "project") {
			resourceData = await readProject(id, baseResourcePath);
			const { gameResourceDir, pk3FilePath } = resourceData as GameProject;
			const projectExtra = extra as IGameProjectExtra;
			if (projectExtra?.projectAction === "open" && gameResourceDir) {
				if (gameResourceDir) {
					safeAssign(projectExtra, await findGameResources(gameResourceDir));
				}
				if (pk3FilePath) {
					projectExtra.pk3StagingDir = await createZipStagingFolderForProject(baseResourcePath, pk3FilePath, id);
				}
			}
		} else {
			throw new Error(`Error in read resource: resource type ${type} is unsupported.`);
		}

		const resourceRef = { id, name: resourceData?.name ?? "Unknown" } as GameResourceRef;

		return { extra, resourceData, resourceRef, type };
	} else if ("path" in request) {
		const { path: rsrcPaths, type } = request;
		let resourceData: any;
		let transferables: any[] | undefined;

		if (type === GameResourceFilename.GAMEMAPS) {
			const gmr = new GameMapsReader(rsrcPaths[1], rsrcPaths[0], {
				numPlanes: 3,
			});
			resourceData = await gmr.readAllMaps();
		} else if (type === GameResourceFilename.VSWAP) {
			const palette = await readPalette(rsrcPaths[1], baseResourcePath);
			const vfr = new VswapFileReader(rsrcPaths[0]);
			const vswap = await vfr.getData(new VswapWorkerCanvasIngester(palette.data.colors));
			transferables = vswap.transferables;
			resourceData = {
				graphicChunks: vswap.chunks,
				spriteStartIndex: vswap.spritePageOffset,
			} as Partial<GraphicState>;
		} else {
			throw new Error(`Error in read resource: resource type ${type} is unsupported.`);
		}

		// TODO: transferables
		return {
			extra,
			resourceData,
			path: rsrcPaths,
			type,
		};
	}
	throw new Error(`Unsupported.`);
};

const onWriteWadeResource: WorkerListener<
	{ baseResourcePath?: string; request: IWadeResourceWriteReqPayload },
	{ resourceRef: GameResourceRef; type: WadeResourceType }
> = async ({ baseResourcePath, request }) => {
	const { resourceData, type } = request;
	let resourceRef: GameResourceRef;

	if (type === "map-def") {
		resourceRef = await writeMapDefinition(resourceData as WadeMapDefinition, baseResourcePath);
	} else if (type === "palette") {
		resourceRef = await writePalette(resourceData as IGamePalette, baseResourcePath);
	} else {
		throw new Error(`Error writing resource: resource type ${type} is unsupported.`);
	}

	return { resourceRef, type };
};

const onDetectGameResourceExt: WorkerListener<{ dir: string }, string[]> = async ({ dir }) => autoDetectGameResourceExts(dir);

const writeGameMaps: WorkerListener<{ gameResourceDir: string; gameResourceExt: string; maps: NonNullable<IGameMap[]> }, void> = async (
	data
) => {
	const { gameResourceDir, gameResourceExt, maps } = data;

	let { gamemapsFilePath, mapheadFilePath } = await findGameResources(gameResourceDir);
	gamemapsFilePath = gamemapsFilePath || path.resolve(gameResourceDir, `GAMEMAPS.${gameResourceExt}`);
	mapheadFilePath = mapheadFilePath || path.resolve(gameResourceDir, `MAPHEAD.${gameResourceExt}`);
	const gmw = new GameMapsWriter(mapheadFilePath, gamemapsFilePath, {
		mapHeaderSuffix: true,
		numPlanes: 3,
	});
	console.debug(chalk`💿️ {blue Writing gamemaps {bold ${gamemapsFilePath}} and maphead {bold ${mapheadFilePath}}...}`);
	await gmw.writeGameMaps(maps);
	console.debug(chalk`✅ {green Done writing gamemaps {bold ${gamemapsFilePath}} and maphead {bold ${mapheadFilePath}}.}`);
};

const readPk3StagingMaps: WorkerListener<{ pk3StagingDir: string }, ECWolfMap[]> = async ({ pk3StagingDir }) => {
	if (!fs.existsSync(pk3StagingDir)) {
		throw new Error(`Staging dir ${pk3StagingDir} doesn't exist.`);
	}

	const mapsFolder = path.join(pk3StagingDir, "maps");
	if (!fs.existsSync(mapsFolder)) {
		return [];
	}

	const maps: ECWolfMap[] = [];
	const mapFiles = fs.readdirSync(mapsFolder);
	for (const mapFile of mapFiles) {
		const mapFilePath = path.resolve(mapsFolder, mapFile);
		maps.push((await readECWolfMapWad(mapFilePath))[0]);
	}

	return maps.sort((m1, m2) => m1.fileName.localeCompare(m2.fileName));
};

const writePk3StagingMaps: WorkerListener<
	{ maps: DeepReadonly<IEditableMap[]>; pk3StagingDir: string; toDelete?: string[] },
	void
> = async ({ maps, pk3StagingDir, toDelete }) => {
	if (!fs.existsSync(pk3StagingDir)) {
		throw new Error(`Staging dir ${pk3StagingDir} doesn't exist.`);
	}
	const mapsFolder = path.join(pk3StagingDir, "maps");
	fs.mkdirpSync(mapsFolder);

	if (toDelete) {
		for (const toDel of toDelete) {
			const mapPath = path.join(mapsFolder, toDel);
			if (fs.existsSync(mapPath)) {
				console.debug(chalk`💥 {red Deleting {bold ${mapPath}}}`);
				fs.unlinkSync(mapPath);
			} else {
				console.warn(chalk`⚠️ {yellow Skipping delete of path {bold ${mapPath}} as it doesn't exist.}`);
			}
			fs.unlinkSync(path.join(mapsFolder));
		}
	}

	const mapsToSave = maps.filter((m) => m.hasUnsavedChanges);

	for (const map of mapsToSave) {
		if (!map.fileName) {
			throw new Error(`Map at index ${map.index} has no wad file name.`);
		}
		if (!isValidWadFileName(map.fileName)) {
			throw new Error(`${map.fileName} is an invalid WAD file name. ${WAD_FILE_NAME_ERROR_HINT}`);
		}
	}

	for (const map of mapsToSave) {
		const mapPath = path.join(mapsFolder, `${map.fileName!}.wad`);
		console.debug(chalk`💿 {blue Writing {bold ${mapPath}}...}`);
		await writeECWolfMapWad(mapPath, map.data as IGameMap);
	}
};

const saveStagingPk3: WorkerListener<{ pk3StagingDir: string; pk3FilePath: string }, void> = async ({ pk3FilePath, pk3StagingDir }) => {
	// create a backup of the old one
	if (fs.existsSync(pk3FilePath)) {
		const parentDir = path.dirname(pk3FilePath);
		const backupDir = path.join(parentDir, "wade");
		fs.mkdirpSync(backupDir);
		const backupPath = path.join(backupDir, `${path.basename(pk3FilePath)}.bak`);

		console.debug(chalk`💿 {blue Backing up {bold ${pk3FilePath}} to {bold ${backupPath}}...}`);
		fs.moveSync(pk3FilePath, backupPath, { overwrite: true });
	}
	logPreFsWrite(pk3FilePath);
	// this works around "async stack got corrupted" errors that are caused by archiver.directory / archiver.file in electron worker env
	// https://github.com/microsoft/vscode/issues/85601#issuecomment-558917205
	// https://github.com/electron/electron/pull/12109#pullrequestreview-252043639
	const archiveFn = async (a: archiver.Archiver, srcPath: string): Promise<void> => {
		const childFiles = allChildFiles(srcPath).filter((fi) => !fi.isDirectory);
		for (const childFile of childFiles) {
			a.append(fs.createReadStream(childFile.absPath, { autoClose: true }), { name: childFile.relPath });
		}
		return Promise.resolve();
	};
	await zipFolder({ archiver: archiveFn, srcPath: pk3StagingDir, destPath: pk3FilePath });
};

const writeProjectFile = async (baseResourcePath: string, project: DeepReadonly<GameProject>) => writeProject(project, baseResourcePath);

const fsDelete = async (filePath: string): Promise<void> => {
	if (!fs.existsSync(filePath)) {
		return;
	}
	const stat = fs.statSync(filePath);
	if (stat.isDirectory()) {
		fs.removeSync(filePath);
	} else {
		fs.unlinkSync(filePath);
	}
	return Promise.resolve();
};

const convertWdcMapDefToWade: WorkerListener<
	{ convertOpts?: WorkerSafe<IWDCToWadeConversionOpts>; filePath: string },
	WadeMapDefinition
> = async ({ convertOpts, filePath }) => new WDCMapDefinitionFileConverter().convertToWadeFormat(filePath, convertOpts);

const copyMapDef: WorkerListener<ICopyWadeResourceOpts, ICopyWadeResourceResult<WadeMapDefinition, IWadeMapDefRef>> = async ({
	baseResourcePath,
	destId,
	destName,
	srcId,
}) => {
	const ref = await copyGameResource<IWadeMapDefRef>({
		basePath: baseResourcePath,
		destId,
		destName,
		srcId,
		type: "map-def",
	});
	return { ref, resource: await readMapDefinition(ref.id, baseResourcePath) };
};

const resourceWorkerFacade = {
	convertWdcMapDefToWade,
	copyMapDef,
	detectGameResourceExt: onDetectGameResourceExt,
	deleteResource: onDeleteResource,
	fsDelete,
	getBootstrapData: onGetBootstrapData,
	readAvailableResources: onReadAvailableResources,
	readPk3StagingMaps,
	readResource: onReadResource,
	saveStagingPk3,
	writeGameMaps,
	writePk3StagingMaps,
	writeProjectFile,
	writeWadeResource: onWriteWadeResource,
};

export type ResourceWorker = typeof resourceWorkerFacade;

expose(resourceWorkerFacade);
