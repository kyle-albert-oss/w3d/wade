import Conf from "conf";
import * as History from "history";

import { getLocalConfigOptions, IAppConfig } from "./store/local/config";
import { getRootWadeResourceDir } from "./store/local/resources";
import { configureStore } from "./store/redux";
import { AppStoreType } from "./store/redux/types/action";
import { attachSharedStoreListeners, attachAppStoreListeners } from "./store/redux/listeners";

/**
 * This file serves the singleton instances of data sources for the app. They are put on the window to reduce chances of circular dependencies between source files that cause hard-to-trace errors.
 */

export const AppHistory = History.createBrowserHistory();
export const AppStore = attachAppStoreListeners(attachSharedStoreListeners(configureStore({ history: AppHistory })));
export const AppConfig = new Conf<IAppConfig>(getLocalConfigOptions(getRootWadeResourceDir()));

declare global {
	interface Window {
		appConfig: typeof AppConfig;
		appStore: AppStoreType;
	}
}

window.appConfig = AppConfig;
window.appStore = AppStore;
