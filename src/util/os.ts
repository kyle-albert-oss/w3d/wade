import { onlyInElectronEnv } from "./env";

export const showFile = (absolutePath: string) => onlyInElectronEnv((el) => el.shell.showItemInFolder(absolutePath));
export const getFileProgramLabel = (prefix: string = "") =>
	onlyInElectronEnv(() => {
		if (prefix !== "") {
			prefix = `${prefix} `;
		}
		const os = require("os");
		switch (os.platform()) {
			case "darwin":
				return `${prefix}Finder`;
			case "win32":
				return `${prefix}Explorer`;
			default:
				return `${prefix}File Browser`;
		}
	});
