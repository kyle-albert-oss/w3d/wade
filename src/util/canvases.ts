import { Canvas, createCanvas } from "canvas";

export type AppCanvas = OffscreenCanvas | Canvas;

export const canUseOffscreenCanvas = () =>
	process.env.NODE_ENV !== "test" && process.env.USE_NODE_CANVAS !== "true" && typeof OffscreenCanvas !== "undefined";

export const newCanvas = (width: number, height: number): AppCanvas => {
	return canUseOffscreenCanvas() ? new OffscreenCanvas(width, height) : createCanvas(width, height);
};

export const toDataUrl = async (canvas: AppCanvas): Promise<string> => {
	if (canvas instanceof Canvas) {
		return canvas.toDataURL();
	}
	const blob = await canvas.convertToBlob();
	const fr = new FileReader();
	return new Promise((resolve, reject) => {
		fr.addEventListener("load", () => {
			resolve(fr.result as string);
		});
		fr.readAsDataURL(blob);
	});
};

export const toBlob = async (canvas: HTMLCanvasElement): Promise<Blob> =>
	new Promise((r, rj) => {
		canvas.toBlob((blob) => {
			if (!blob) {
				rj("Error");
				return;
			}
			r(blob);
		});
	});

export const convertBitmapToObjUrl = async (bitmap: ImageBitmap): Promise<string> => {
	const canvas = document.getElementById("utility-canvas") as HTMLCanvasElement;
	canvas.width = bitmap.width;
	canvas.height = bitmap.height;
	const ctx = (canvas.getContext("bitmaprenderer") as unknown) as ImageBitmapRenderingContext;
	ctx.transferFromImageBitmap(bitmap);
	const blob = await toBlob(canvas);
	return URL.createObjectURL(blob);
};
