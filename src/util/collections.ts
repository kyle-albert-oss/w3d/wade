import { Dictionary } from "ts-essentials";

import { toNumber } from "./numbers";

const arrAnyMatch = <T>(array: readonly T[], matchFn: (item: T) => boolean | undefined): boolean => {
	for (const item of array) {
		if (matchFn(item)) {
			return true;
		}
	}
	return false;
};

const arrExpandToLength = <T>(array: readonly T[], desiredLength: number, itemFn: (index: number) => T): T[] => {
	if (array.length <= desiredLength) {
		return array as T[];
	}
	const result = [...array];
	const fill = desiredLength - array.length;
	for (let i = 0; i < fill; i++) {
		result.push(itemFn(array.length + i));
	}
	return result;
};

const arrMoveTo = <T>(array: T[], fromIndex: number, toIndex: number): T[] => {
	const [item] = array.splice(fromIndex, 1);
	array.splice(toIndex, 0, item);
	return array;
};

const arrMoveToIndex = <T>(array: T[], from: number, to: number, valModifier: (val: T, newIndex: number, oldIndex: number) => T): T[] => {
	if (from < 0 || from > array.length - 1) {
		throw new Error(`from parameter out of range. Expected 0 <= ${from} <= ${array.length - 1}.`);
	} else if (to < 0 || to > array.length - 1) {
		throw new Error(`to parameter out of range. Expected 0 <= ${to} <= ${array.length - 1}.`);
	}
	let movingVal: T;
	let assigned = false;

	const result = array.reduce<T[]>(
		(acc, val, idx) => {
			let newIdx: number = idx;
			if (idx === from) {
				assigned = true;
				movingVal = val;
				return acc;
			} else if (to > from && idx > from && idx <= to) {
				newIdx = idx - 1;
			} else if (to <= from && idx < from && idx >= to) {
				// from >= to
				newIdx = idx + 1;
			}
			if (newIdx === idx) {
				acc[newIdx] = val;
			} else {
				acc[newIdx] = valModifier ? valModifier(val, newIdx, idx) : val;
			}
			return acc;
		},
		[...array]
	);

	if (assigned) {
		result[to] = valModifier ? valModifier(movingVal!, to, from) : movingVal!;
	}

	return result;
};

const arrReplace = <T>(array: T[], find: T, replacement: T): T[] => {
	for (let i = 0; i < array.length; i++) {
		if (array[i] === find) {
			array[i] = replacement;
		}
	}
	return array;
};

const arrReplaceFill2D = <T>(array: T[], idx: number, numCols: number, replacement: T): T[] => {
	const val = array[idx];
	if (val === replacement) {
		return array;
	}
	return arrReplaceFill2DRecurse(array, val, idx, numCols, replacement);
};

const arrReplaceFill2DRecurse = <T>(array: T[], val: T, idx: number, numCols: number, replacement: T): T[] => {
	if (array[idx] !== val) {
		return array;
	}

	array[idx] = replacement;

	const colIdx = idx % numCols;

	// check north
	let checkIdx = idx - numCols;
	if (checkIdx >= 0) {
		array = arrReplaceFill2DRecurse(array, val, checkIdx, numCols, replacement);
	}

	// check east
	checkIdx = idx + 1;
	if (checkIdx % numCols !== 0) {
		array = arrReplaceFill2DRecurse(array, val, checkIdx, numCols, replacement);
	}

	// check west
	if (colIdx !== 0) {
		array = arrReplaceFill2DRecurse(array, val, idx - 1, numCols, replacement);
	}

	// check south
	checkIdx = idx + numCols;
	if (checkIdx < array.length) {
		array = arrReplaceFill2DRecurse(array, val, checkIdx, numCols, replacement);
	}

	return array;
};

/**
 * @return new dictionary with index deleted and indexes greater than it subtracted by 1
 */
const dictDeleteIndexed = <T>(
	dictionary: Dictionary<T>,
	indexToDelete: number,
	valModifier?: (val: T, newIndex: number, oldIndex: number) => T
): Dictionary<T> => {
	return Object.entries(dictionary)
		.sort(([k1], [k2]) => toNumber(k1) - toNumber(k2))
		.reduce<Dictionary<T>>((acc, [_idx, val]) => {
			const idx = toNumber(_idx);
			if (idx === indexToDelete) {
				return acc;
			} else if (idx > indexToDelete) {
				acc[idx - 1] = valModifier ? valModifier(val, idx - 1, idx) : val;
			} else {
				acc[idx] = val;
			}
			return acc;
		}, {});
};

const dictKeysIndexed = (dictionary: {}): number[] | undefined => {
	const numbers = Object.keys(dictionary)
		.map((k) => toNumber(k))
		.filter((n) => !isNaN(n));
	return numbers.length > 0 ? numbers : undefined;
};

const dictMoveIndexed = <T>(
	dictionary: Dictionary<T>,
	from: number,
	to: number,
	valModifier?: (val: T, newIndex: number, oldIndex: number) => T
): Dictionary<T> => {
	if (from === to) {
		return dictionary;
	}

	let movingVal: T;
	let assigned = false;
	const result = Object.entries(dictionary)
		.sort(([k1], [k2]) => toNumber(k1) - toNumber(k2))
		.reduce<Dictionary<T>>((acc, [_idx, val]) => {
			const idx = toNumber(_idx);
			let newIdx: number = idx;
			if (idx === from) {
				assigned = true;
				movingVal = val;
				return acc;
			} else if (to > from && idx > from && idx <= to) {
				newIdx = idx - 1;
			} else if (to <= from && idx < from && idx >= to) {
				// from >= to
				newIdx = idx + 1;
			}
			if (newIdx === idx) {
				acc[newIdx] = val;
			} else {
				acc[newIdx] = valModifier ? valModifier(val, newIdx, idx) : val;
			}
			return acc;
		}, {});

	if (assigned) {
		result[to] = valModifier ? valModifier(movingVal!, to, from) : movingVal!;
	}

	return result;
};

const setAddAll = <T>(set: Set<T>, items: Iterable<any>): Set<T> => {
	if (items == null) {
		return set;
	}
	for (const item of items) {
		set.add(item);
	}
	return set;
};

const setDeleteAll = <T>(set: Set<T>, items: Iterable<any>): Set<T> => {
	if (items == null) {
		return set;
	}
	for (const item of items) {
		set.delete(item);
	}
	return set;
};

const setDifferenceWithIterable = <T, U>(c1: ReadonlySet<T>, c2: Iterable<U>): Set<T | U> => {
	const result = new Set<T | U>(c1);
	if (c2 == null) {
		return result;
	}
	for (const e2 of c2) {
		if (c1.has(e2 as any)) {
			result.delete(e2);
		} else {
			result.add(e2);
		}
	}
	return result;
};

const setHasAny = <T>(set: ReadonlySet<T>, items: Iterable<any>): boolean | undefined => {
	if (items == null) {
		return undefined;
	}
	for (const item of items) {
		if (set.has(item)) {
			return true;
		}
	}
	return false;
};

const Arrays = {
	anyMatch: arrAnyMatch,
	expandToLength: arrExpandToLength,
	moveTo: arrMoveTo,
	moveToIndex: arrMoveToIndex,
	replace: arrReplace,
	replaceFill2D: arrReplaceFill2D,
} as const;

const Dictionaries = {
	deleteIndexed: dictDeleteIndexed,
	keysIndexed: dictKeysIndexed,
	moveIndexed: dictMoveIndexed,
};

const Sets = {
	addAll: setAddAll,
	deleteAll: setDeleteAll,
	differenceWithIterable: setDifferenceWithIterable,
	hasAny: setHasAny,
} as const;

export { Arrays, Dictionaries, Sets };
