import * as path from "path";

import * as fs from "fs-extra";
import klaw from "klaw";
import { chain } from "stream-chain";
import { parser } from "stream-json";
import { filter } from "stream-json/filters/Filter";
import Assembler from "stream-json/Assembler";
import { replace } from "stream-json/filters/Replace";
import { stringer } from "stream-json/Stringer";
import FilterBase from "stream-json/filters/FilterBase";

import { thruFilter } from "../workers/shared";

import * as Strings from "./strings";

export const readFileAsDataUrl = (blob: Blob): Promise<string> =>
	new Promise((r, rj) => {
		const fr = new FileReader();
		fr.addEventListener("load", () => r(fr.result as string));
		fr.addEventListener("abort", (e) => rj(e));
		fr.addEventListener("error", (e) => rj(e));
		fr.readAsDataURL(blob);
	});

export interface IFileInfo {
	absPath: string;
	ext?: string;
	isDirectory: boolean;
	relPath: string;
}

const allChildFilesRecurse = (startDir: string, dir: string, filterFn?: (fi: IFileInfo) => any): IFileInfo[] => {
	const files: IFileInfo[] = [];

	const childFileNames = fs.readdirSync(dir);
	const childFiles = childFileNames
		.map(
			(n): IFileInfo => {
				const childFileAbsPath = path.resolve(path.join(dir, n));
				const childFileRelPath = path.relative(startDir, childFileAbsPath);
				const childStat = fs.statSync(childFileAbsPath);
				const isDirectory = childStat.isDirectory();
				const ext = (isDirectory ? undefined : path.extname(childFileAbsPath).substring(1)) || undefined;
				return {
					absPath: childFileAbsPath,
					ext,
					isDirectory,
					relPath: childFileRelPath,
				};
			}
		)
		.filter(filterFn ?? (() => true))
		.sort((fi1, fi2) => {
			if ((fi1.isDirectory && fi2.isDirectory) || (!fi1.isDirectory && !fi2.isDirectory)) {
				return path.basename(fi1.absPath).localeCompare(path.basename(fi2.absPath));
			} else if (fi1.isDirectory) {
				return 1;
			}
			return -1;
		});

	for (const childFile of childFiles) {
		files.push(childFile);
		if (childFile.isDirectory) {
			files.push(...allChildFilesRecurse(startDir, childFile.absPath));
		}
	}

	return files;
};

export const allChildFiles = (dir: string, filterFn?: (fi: IFileInfo) => any): IFileInfo[] => allChildFilesRecurse(dir, dir, filterFn);

export const getJSONFilePaths = async (basePath: string): Promise<string[]> =>
	new Promise<string[]>((r, rj) => {
		const paths = [] as string[];
		klaw(basePath)
			.pipe(thruFilter((item) => !item.stats.isDirectory() && Strings.equalsIgnoreCase(path.extname(item.path), ".json")))
			.on("data", (item) => paths.push(item.path))
			.on("end", () => r(paths))
			.on("error", (err: any, item: any) => rj(err));
	});

export const getJSONPropsFromFile = async <T extends object = any>(filePath: string, keyFilter: string | RegExp): Promise<T> =>
	new Promise<T>((r, rj) => {
		const pipeline = chain([fs.createReadStream(filePath, { encoding: "utf8" }), parser(), filter({ filter: keyFilter })]);
		const asm = Assembler.connectTo(pipeline);
		pipeline.on("error", rj).on("end", () => {
			r(asm.current as T);
		});
	});

const jsonString = (value: string): FilterBase.Token[] => [
	{ name: "startString" },
	{ name: "stringChunk", value },
	{ name: "endString" },
	{ name: "stringValue", value },
];

// only update, not delete
export const updateIdOrNameInRootJSON = async (filePath: string, opts: { newId?: string; newName?: string }): Promise<void> => {
	const tmpFile = `${filePath}.tmp`;
	await new Promise((r, rj) => {
		const pipeline = chain([
			fs.createReadStream(filePath),
			parser(),
			replace({
				filter: /^(id|name)$/,
				replacement: (stack, token) => {
					if (stack?.[0] === "id" && opts.newId) {
						return jsonString(opts.newId);
					} else if (stack?.[0] === "name" && opts.newName) {
						return jsonString(opts.newName);
					}
					return [token];
				},
			}),
			stringer(),
			fs.createWriteStream(tmpFile),
		]);
		pipeline.on("error", rj).on("end", () => r());
	});
	await fs.move(tmpFile, filePath, { overwrite: true });
};
