export const wrap = <T, U extends T[], V>(fn: (...args: U) => V) => {
	return (...args: U): V => fn(...args);
};
