import chalk from "chalk";

export const LOG_ICON_IO = "💿";
export const LOG_ICON_SUCCESS = "✅";
export const LOG_ICON_WARN = "⚠️";

export const logPreFsWrite = (filePath: string) => console.log(chalk`${LOG_ICON_IO} {blue Writing {bold ${filePath}}...}`);
export const logSuccess = (msg: string) => console.log(chalk`${LOG_ICON_SUCCESS} {green ${msg}}`);
export const logWarn = (msg: string) => console.warn(chalk`${LOG_ICON_WARN}️ {yellow ${msg}}`);
