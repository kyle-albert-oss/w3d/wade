import { Dictionary, NonNever, PickProperties } from "ts-essentials";

import { toNumber } from "./numbers";

type ObjectKey = string | number;

type AssignPartial<T> = T extends Partial<infer U> ? Partial<U> : Partial<T>;
export const safeAssign = <T extends object>(target: T, ...sources: Array<AssignPartial<T>>) => Object.assign(target, ...sources);

export const typedKeysOf = <T>(obj: T): Array<keyof T> => Object.keys(obj) as Array<keyof T>;
export const numberKeysOf = (obj: {}): number[] =>
	Object.keys(obj)
		.map((k) => toNumber(k))
		.filter((n) => !isNaN(n));

export type KeysOfExactType<O, T> = Exclude<keyof PickProperties<O, T>, undefined>;
export type KeysOfLooseType<O, T> = Exclude<keyof NonNever<{ [K in keyof O]: O[K] extends T ? O[K] : never }>, undefined>;

type KeyMapper<T> = (T extends object ? KeysOfLooseType<T, string | number> : never) | ((item: T, i: number) => string | number);
export const arrayToRODictionary = <T>(array: Maybe<readonly T[]>, keyMapper: KeyMapper<T>): Dictionary<T> => {
	if (!array) {
		return {};
	}

	return array.reduce((acc, item, i) => {
		if (typeof keyMapper === "string" || typeof keyMapper === "number") {
			const id = (item[keyMapper] as unknown) as string | number; // TODO(👃): find a better way
			acc[id] = item;
		} else if (typeof keyMapper === "function") {
			const id = keyMapper(item, i);
			acc[id] = item;
		}
		return acc;
	}, {} as Dictionary<T>);
};

export const dictionaryToROArray = <T>(
	dictionary: Maybe<Dictionary<T>>,
	keys?: readonly ObjectKey[],
	greedy: boolean = true
): readonly T[] => {
	if (!dictionary) {
		return [];
	}
	const numKeys: number = (keys as ObjectKey[])?.length ?? 0;
	if (numKeys === 0 && !greedy) {
		return [];
	} else if (numKeys > 0) {
		return keys!.reduce((acc, key) => {
			if (dictionary[key] != null) {
				acc.push(dictionary[key]);
			}
			return acc;
		}, [] as T[]);
	}
	return Object.values(dictionary);
};

type SortFn<T> = Parameters<T[]["sort"]>[0];

export const dictionaryToROArrayAll = <T>(dictionary: Maybe<Dictionary<T>>): readonly T[] =>
	dictionaryToROArray(dictionary, undefined, true);
export const dictionaryToROArrayAllSorted = <T>(sortFn?: SortFn<T>): ((d: Maybe<Dictionary<T>>) => readonly T[]) => (
	dictionary: Maybe<Dictionary<T>>
) => (dictionaryToROArray(dictionary, undefined, true) as T[]).sort(sortFn);

export const dictionaryToROArrayByKeys = <T>(dictionary: Maybe<Dictionary<T>>, keys?: readonly ObjectKey[]): readonly T[] =>
	dictionaryToROArray(dictionary, keys, false);

export const dictionaryLookup = <T>(dictionary: Maybe<Dictionary<T>>, key: Maybe<ObjectKey>): Maybe<T> =>
	dictionary == null || key == null ? undefined : dictionary[key];

export const throwIfNil = <T>(value: T, throwFn: () => Error): NonNullable<T> => {
	if (value == null) {
		throw throwFn();
	}
	return value as NonNullable<T>;
};

export const isBlank = <T>(arg: T): arg is Exclude<T, boolean | number | symbol | Function> => !isPresent(arg);
export const isPresent = <T>(arg: T): arg is NonNullable<T> => {
	if (arg == null) {
		return false;
	}
	switch (typeof arg) {
		case "string":
			return arg !== "";
		case "object":
			return Array.isArray(arg) ? arg.length > 0 : Object.keys(arg).length > 0;
	}
	return true;
};
export const undefIfBlank = <T>(arg: T): Exclude<T, null> | undefined => (isBlank(arg) ? undefined : (arg as Exclude<T, null>));
export const nullIfBlank = <T>(arg: T): Exclude<T, undefined> | null => (isBlank(arg) ? null : (arg as Exclude<T, undefined>));
// TODO: correct return type
export const deleteNullAndUndefined = <T extends { [k: string]: any }>(arg: T): T => {
	for (const [k, v] of Object.entries(arg)) {
		if (v == null) {
			delete arg[k];
		}
	}
	return arg;
};
