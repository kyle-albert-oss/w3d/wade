import { Dictionary } from "ts-essentials";

import { toNumber } from "./numbers";

const COPY_NUM_REGEXP = /(.*?)\s\((\d+)\)$/;
export const getNextDedupedName = (targetName: string, existingNames: readonly string[]) => {
	const targetNameMatch = COPY_NUM_REGEXP.exec(targetName);
	const targetBaseName = targetNameMatch?.[1] ?? targetName;

	const existingNamesMaxNum = existingNames.reduce<Dictionary<number>>((acc, existingName) => {
		const existingMatch = COPY_NUM_REGEXP.exec(existingName);
		const existingBaseName = existingMatch?.[1] ?? existingName;
		const copyNum = toNumber(existingMatch?.[2], { min: 1, defaultValue: 1, outOfRangeOrNaN: 1 });
		acc[existingBaseName] = Math.max(acc[existingBaseName] ?? 1, copyNum);
		return acc;
	}, {});

	if (existingNamesMaxNum[targetBaseName]) {
		return `${targetBaseName} (${existingNamesMaxNum[targetBaseName] + 1})`;
	}
	return targetName;
};
