import { Dictionary } from "ts-essentials";

import { Dictionaries, Arrays } from "../collections";

describe("Arrays", () => {
	let arr: number[];

	beforeEach(() => {
		arr = [1, 2, 3, 4, 5];
	});

	describe("moveTo", () => {
		it("moves correctly when from < to", () => {
			Arrays.moveTo(arr, 1, 3);
			expect(arr).toEqual([1, 3, 4, 2, 5]);

			Arrays.moveTo(arr, 0, 4);
			expect(arr).toEqual([3, 4, 2, 5, 1]);
		});

		it("moves correctly when from > to", () => {
			Arrays.moveTo(arr, 3, 1);
			expect(arr).toEqual([1, 4, 2, 3, 5]);

			Arrays.moveTo(arr, 4, 0);
			expect(arr).toEqual([5, 1, 4, 2, 3]);
		});

		it("is the same when from === to", () => {
			Arrays.moveTo(arr, 2, 2);
			expect(arr).toEqual([1, 2, 3, 4, 5]);
		});
	});
});

describe("Dictionaries", () => {
	describe("moveIndexed", () => {
		let dict: Dictionary<string>;

		beforeEach(() => {
			dict = {
				0: "a",
				1: "b",
				2: "c",
				3: "d",
				4: "e",
				5: "f",
			};
		});

		it("Moves item correctly when from < to", () => {
			const result = Dictionaries.moveIndexed(dict, 1, 4);
			expect(result[0]).toBe("a");
			expect(result[1]).toBe("c");
			expect(result[2]).toBe("d");
			expect(result[3]).toBe("e");
			expect(result[4]).toBe("b");
			expect(result[5]).toBe("f");
		});

		it("Moves item correctly when from > to", () => {
			const result = Dictionaries.moveIndexed(dict, 3, 1);
			expect(result[0]).toBe("a");
			expect(result[1]).toBe("d");
			expect(result[2]).toBe("b");
			expect(result[3]).toBe("c");
			expect(result[4]).toBe("e");
			expect(result[5]).toBe("f");
		});

		it("Modifies altered values if provided a value modifier", () => {
			const result = Dictionaries.moveIndexed(dict, 3, 1, (str, newIndex) => `${str}${newIndex}`);
			expect(result[0]).toBe("a");
			expect(result[1]).toBe("d1");
			expect(result[2]).toBe("b2");
			expect(result[3]).toBe("c3");
			expect(result[4]).toBe("e");
			expect(result[5]).toBe("f");
		});

		it("Returns same dictionary if from === to", () => {
			const result = Dictionaries.moveIndexed(dict, 2, 2);
			expect(result).toBe(dict);
		});
	});
});
