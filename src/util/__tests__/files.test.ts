import * as path from "path";

import * as fs from "fs-extra";

import { allChildFiles, getJSONPropsFromFile, updateIdOrNameInRootJSON } from "../files";

describe("allChildFiles", () => {
	it("Lists all child files.", () => {
		const wd = path.join(process.cwd(), "src");
		const childFiles = allChildFiles(wd);

		expect(childFiles.find((fi) => fi.relPath === "index.tsx")).toBeTruthy();
		expect(childFiles.find((fi) => fi.relPath === "index.tsx")?.isDirectory).toEqual(false);
		expect(childFiles.find((fi) => fi.relPath === "util/__tests__")).toBeTruthy();
		expect(childFiles.find((fi) => fi.relPath === "util/__tests__")?.isDirectory).toEqual(true);
		expect(childFiles.find((fi) => fi.relPath === "util/__tests__/files.test.ts")).toBeTruthy();
	});

	it("Lists all child files with relative dir input.", () => {
		const wd = "src";
		const childFiles = allChildFiles(wd);

		expect(childFiles.find((fi) => fi.relPath === "index.tsx")).toBeTruthy();
		expect(childFiles.find((fi) => fi.relPath === "index.tsx")?.isDirectory).toEqual(false);
		expect(childFiles.find((fi) => fi.relPath === "util/__tests__")).toBeTruthy();
		expect(childFiles.find((fi) => fi.relPath === "util/__tests__")?.isDirectory).toEqual(true);
		expect(childFiles.find((fi) => fi.relPath === "util/__tests__/files.test.ts")).toBeTruthy();
	});
});

describe("getJSONPropsFromFile", () => {
	it("Gets only the specified part of the object based on the prop filter.", async () => {
		const result = await getJSONPropsFromFile(path.resolve(__dirname, "test.json"), /^(id|name)$/);
		expect(JSON.stringify(result ?? {})).toEqual('{"id":"ABcD-eFG-123","name":"Some Name"}');
	});

	it("Gets only the specified part of the object based on the prop filter (nested key).", async () => {
		const result = await getJSONPropsFromFile(path.resolve(__dirname, "test.json"), /\bnestedNumber\b/);
		expect(JSON.stringify(result ?? {})).toEqual('{"obj":{"nestedNumber":5}}');
	});

	it("Get entire sub object.", async () => {
		const result = await getJSONPropsFromFile(path.resolve(__dirname, "test.json"), /^obj\b/);
		expect(JSON.stringify(result ?? {})).toEqual('{"obj":{"nestedNumber":5,"name":"innerName","someNestedArr":["name","1"]}}');
	});
});

describe("updateIdNameJSON", () => {
	const testJsonFilePath = path.resolve(process.cwd(), "tmp/output-test.json");

	beforeAll(() => {
		fs.mkdirpSync(path.resolve(process.cwd(), "tmp"));
		if (fs.existsSync(testJsonFilePath)) {
			fs.unlinkSync(testJsonFilePath);
		}
		fs.copyFileSync(path.resolve(__dirname, "test.json"), testJsonFilePath);
	});

	it("Updates id and name", async () => {
		await updateIdOrNameInRootJSON(testJsonFilePath, { newId: "NewID", newName: "New Name" });
		const json = fs.readJSONSync(testJsonFilePath);
		expect(Object.keys(json).length).toEqual(4);
		expect(json.id).toEqual("NewID");
		expect(json.name).toEqual("New Name");
		expect(json.obj?.nestedNumber).toEqual(5);
		expect(json.obj?.name).toEqual("innerName");
		expect(json.obj?.someNestedArr).toEqual(["name", "1"]);
	});
});
