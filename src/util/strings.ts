import _ from "lodash";
import { v1 as uuid } from "uuid";

export const compareIgnoreCase = (s1: Maybe<string>, s2: Maybe<string>, coerceNulls: boolean = false): number => {
	if (coerceNulls) {
		s1 = s1 || "";
		s2 = s2 || "";
	}
	// eslint-disable-next-line eqeqeq
	if (s1 == s2) {
		return 0;
	} else if (s1 == null) {
		return 1;
	} else if (s2 == null) {
		return -1;
	}
	return s1.localeCompare(s2, undefined, { sensitivity: "accent" });
};

export const containsIgnoreCase = (str: Maybe<string>, contains: Maybe<string>): boolean => {
	if (str == null || contains == null) {
		return false;
	}
	return new RegExp(_.escapeRegExp(contains), "i").test(str);
};

export const containsAnyIgnoreCase = (str: Maybe<string>, contains: Maybe<readonly string[]>): boolean => {
	if (str == null || contains == null || contains.length === 0) {
		return false;
	}
	for (const contain of contains) {
		if (containsIgnoreCase(str, contain)) {
			return true;
		}
	}
	return false;
};

export const anyContainsIgnoreCase = (strs: Maybe<readonly string[]>, contains: Maybe<string>): boolean => {
	if (strs == null || contains == null || strs.length === 0) {
		return false;
	}
	for (const str of strs) {
		if (containsIgnoreCase(str, contains)) {
			return true;
		}
	}
	return false;
};

export const equalsIgnoreCase = (s1: Maybe<string>, s2: Maybe<string>, coerceNulls: boolean = false): boolean =>
	compareIgnoreCase(s1, s2, coerceNulls) === 0;

export const generateUUID = uuid;

export const Strings = {
	anyContainsIgnoreCase,
	equalsIgnoreCase,
	compareIgnoreCase,
	containsAnyIgnoreCase,
} as const;
