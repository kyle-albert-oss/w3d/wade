export type OnOutOfRange = (num: number) => number | never;

export interface IToNumberOpts {
	allowNaN?: boolean;
	defaultValue?: number;
	max?: number;
	min?: number;
	outOfRangeOrNaN?: number | "error" | keyof Pick<IToNumberOpts, "defaultValue" | "max" | "min"> | OnOutOfRange;
	radix?: number;
}

export const toNumber = (value: string | number | null | undefined, opts?: IToNumberOpts): number => {
	if (!opts) {
		if (typeof value === "string") {
			return parseFloat(value);
		} else if (typeof value === "number") {
			return value;
		}
		return Number.NaN;
	}

	const { allowNaN, defaultValue, max, min, outOfRangeOrNaN, radix } = opts;
	let result;
	if (typeof value === "string") {
		result = radix != null ? parseInt(value, radix) : parseFloat(value);
	} else {
		result = value;
	}

	if (result == null) {
		result = Number.NaN;
	}
	if (isNaN(result)) {
		if (allowNaN) {
			return result;
		} else if (outOfRangeOrNaN === "error") {
			throw new Error(`${value} is NaN.`);
		} else if (typeof outOfRangeOrNaN === "function") {
			return outOfRangeOrNaN(result);
		} else if (defaultValue != null) {
			return defaultValue;
		}
		return Number.NaN;
	} else if (min != null && result < min) {
		if (outOfRangeOrNaN == null) {
			return min;
		} else if (typeof outOfRangeOrNaN === "number") {
			return outOfRangeOrNaN;
		} else if (outOfRangeOrNaN === "error") {
			throw new Error(`${result} is less than ${min}.`);
		} else if (typeof outOfRangeOrNaN === "function") {
			return outOfRangeOrNaN(result);
		} else if (opts[outOfRangeOrNaN] != null) {
			return opts[outOfRangeOrNaN]!;
		}
		return Number.NaN;
	} else if (max != null && result > max) {
		if (outOfRangeOrNaN == null) {
			return max;
		} else if (typeof outOfRangeOrNaN === "number") {
			return outOfRangeOrNaN;
		} else if (outOfRangeOrNaN === "error") {
			throw new Error(`${result} is greater than ${max}.`);
		} else if (typeof outOfRangeOrNaN === "function") {
			return outOfRangeOrNaN(result);
		} else if (opts[outOfRangeOrNaN] != null) {
			return opts[outOfRangeOrNaN]!;
		}
		return Number.NaN;
	}
	return result;
};
