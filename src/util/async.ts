export type AwaitAllInputType = {
	[key: string]: any;
};
type NormalizePromise<T> = T extends PromiseLike<any> ? T : Promise<T>;
type PromisifyObj<T> = {
	[K in keyof T]: NormalizePromise<T[K]>;
};

type PromiseElement<T> = T extends PromiseLike<infer U> ? U : T;
type AwaitAllResult<T> = {
	[K in keyof T]: PromiseElement<T[K]>;
};

export const allKeyed = async <T extends AwaitAllInputType>(obj: PromisifyObj<T>): Promise<AwaitAllResult<PromisifyObj<T>>> => {
	const entries = Object.entries(obj);
	if (!entries) {
		return {} as AwaitAllResult<T>;
	}
	const results = await Promise.all(entries.map((e) => e[1]));
	const result: AwaitAllResult<any> = {};
	for (let i = 0; i < entries.length; i++) {
		const entry = entries[i];
		const [key] = entry;
		result[key] = results[i];
	}
	return result as AwaitAllResult<PromisifyObj<T>>;
};
