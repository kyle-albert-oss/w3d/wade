import * as React from "react";

// eslint-disable-next-line @typescript-eslint/interface-name-prefix
export interface IdentityFunction {
	<T>(fn: T): T;
}
export const typedMemo: IdentityFunction = React.memo;
export const typedForwardRef = React.forwardRef as IdentityFunction;
