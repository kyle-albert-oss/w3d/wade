/**
 * Electron is not available in the worker threads and code is likely to be imported by workers that contains electron-only pieces.
 * Most of the time, the code that does this is not directly used by the worker however runtime errors will occur due to the absence of electron.
 * This file provides utility methods to work around this and also other environmental related helpers.
 */
import { ElectronMainHandle } from "../electron-main";

type AppElectron = import("electron").AllElectron;

export const getAppVersion = (): string => "alpha";
export const isElectronEnv = (): boolean =>
	process.type !== "worker" && !process.env.SCRIPT && !isStorybookEnv() && process.env.NODE_ENV !== "test";
export const isStorybookEnv = (): boolean => !!process.env.STORYBOOK;

type NoElectronFn<T> = () => T;
type ElectronFn<T, E> = (electron: E) => T;

export const onlyInElectronEnv = <T = void, E = AppElectron>(fn: ElectronFn<T, E>): T | undefined => {
	if (!isElectronEnv()) {
		return undefined;
	}
	const electron = (require("electron") as unknown) as E;
	return fn(electron);
};

export const onlyInNonElectronEnv = <T = void>(fn: NoElectronFn<T>): T | undefined => (isElectronEnv() ? undefined : fn());

export const maybeInElectronEnv = <T = void, E = AppElectron>(inElectronFn: ElectronFn<T, E>, noElectronFn: NoElectronFn<T>): T =>
	(isElectronEnv() ? onlyInElectronEnv(inElectronFn) : onlyInNonElectronEnv(noElectronFn))!;

export const ElectronHandle: ElectronMainHandle = onlyInElectronEnv((el) => el.remote.require("./electron"));

export class RemoteElectronBuilderFactory {
	protected el: Electron.AllElectron;

	constructor(el: Electron.AllElectron) {
		this.el = el;
	}

	newMenuBuilder = () => new RemoteElectronMenuBuilder(this.el.remote);
}

export class RemoteElectronMenuBuilder {
	protected el: Electron.Remote;
	protected menu: Electron.Menu;

	constructor(electron: Electron.Remote) {
		this.el = electron;
		this.menu = new this.el.Menu();
	}

	item = (opts: Electron.MenuItemConstructorOptions): this => {
		const { el } = this;
		this.menu.append(new el.MenuItem(opts));
		return this;
	};

	itemIf = (condition: any, opts: Electron.MenuItemConstructorOptions): this => {
		if (condition) {
			return this.item(opts);
		}
		return this;
	};

	separator = (): this => {
		const { el } = this;
		this.menu.append(new el.MenuItem({ type: "separator" }));
		return this;
	};

	submenu = (label: string, menuBuilderFn: (b: RemoteElectronMenuBuilder) => RemoteElectronMenuBuilder): this => {
		const { el } = this;
		const submenuBuilder = new RemoteElectronMenuBuilder(this.el);
		const submenu = menuBuilderFn(submenuBuilder);
		this.menu.append(new el.MenuItem({ label, submenu: submenu.menu }));
		return this;
	};

	build = (): Electron.Menu => this.menu;
}
