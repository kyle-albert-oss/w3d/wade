import * as React from "react";
import { connect } from "react-redux";
import { Graphics, Sprite, Stage } from "@inlet/react-pixi";
import * as PIXI from "pixi.js";
import { DeepReadonly } from "ts-essentials";

import { ImgSrcs } from "../../../../models/wade";
import {
	activeMapDefTileFnSelector,
	mapCanvasTileSizeSelector,
	mapTileGraphicUrlForEditorSelector,
	selectedMapDataSelector,
	uiStateSelector,
	surroundingMapTileCodeFnSelector,
} from "../../../../store/redux/selectors";
import { toNumber } from "../../../../util/numbers";
import * as MoreObjects from "../../../../util/objects";
import { MapTileData, TileType } from "../../../../models/map";
import { AppRootState } from "../../../../store/redux/types/state";
import { getDispatchActionMapper } from "../../../../store/redux/actions";

interface IMapTileProps {
	isolated?: boolean;
	map?: number;
	plane?: number;
	renderMode: "basic" | "webgl";
	renderWhenBlank?: boolean;
	size?: number;
	tileCode?: number;
	x?: number;
	y?: number;
}

type MappedProps = DeepReadonly<{
	imgSrc: ImgSrcs | undefined;
	opacity: number;
	mappedSize: number;
}>;

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IMapTileProps>): MappedProps => {
	const { isolated, map, plane, size, x, y } = ownProps;
	let { tileCode } = ownProps;
	let imgSrc: ImgSrcs;
	let tile: DeepReadonly<MapTileData> | undefined;
	let opacity = 1;

	if (plane != null) {
		if (map != null && x != null && y != null && tileCode == null) {
			tileCode = selectedMapDataSelector(state)?.planes?.[plane]?.data?.[y]?.[x];
		}
		if (tileCode != null) {
			const mapTileFn = activeMapDefTileFnSelector(state);
			tile = mapTileFn(plane, tileCode);
			imgSrc = mapTileGraphicUrlForEditorSelector(state)(tile);

			if (!isolated && tile?.types) {
				const isFloorCode = !!tile.types[TileType.FLOOR_CODE];
				const isWall = !!tile.types[TileType.WALL];
				if (isFloorCode) {
					const { mapCanvasFloorCodeOpacity } = uiStateSelector(state);
					opacity = mapCanvasFloorCodeOpacity;
				} else if (isWall && x != null && y != null) {
					const surroundingTileCodesFn = surroundingMapTileCodeFnSelector(state);
					const surroundingTileCodes = surroundingTileCodesFn(plane, x, y);
					if (surroundingTileCodes) {
						const { e, n, ne, nw, s, se, sw, w } = surroundingTileCodes;
						if (
							(e == null || mapTileFn(plane, e)?.types?.[TileType.WALL]) &&
							(n == null || mapTileFn(plane, n)?.types?.[TileType.WALL]) &&
							(s == null || mapTileFn(plane, s)?.types?.[TileType.WALL]) &&
							(w == null || mapTileFn(plane, w)?.types?.[TileType.WALL]) &&
							(ne == null || mapTileFn(plane, ne)?.types?.[TileType.WALL]) &&
							(nw == null || mapTileFn(plane, nw)?.types?.[TileType.WALL]) &&
							(sw == null || mapTileFn(plane, sw)?.types?.[TileType.WALL]) &&
							(se == null || mapTileFn(plane, se)?.types?.[TileType.WALL])
						) {
							// opacity = 0;
						}
					}
				}
			}
		}
	}

	return {
		imgSrc,
		mappedSize: size ?? mapCanvasTileSizeSelector(state),
		opacity,
	};
};

const drawTile = (
	g: PIXI.Graphics,
	canvasDimension: number,
	x: number,
	y: number,
	colors: Readonly<string[]> | undefined,
	opacity: number = 1
): void => {
	g.clear();
	if (!colors) {
		return;
	}
	const scaleFactor = canvasDimension / 7;
	for (let i = 0; i < 49; i++) {
		const col = i % 7;
		const row = Math.floor(i / 7);
		const color = colors[i];
		if (color) {
			const colorAsNum = toNumber(colors[i].replace("#", ""), { radix: 16 });
			g.beginFill(colorAsNum, opacity);
			g.lineStyle(0.5, colorAsNum, opacity); // smooth out the gaps
			g.drawRect(x + col * scaleFactor, y + row * scaleFactor, scaleFactor, scaleFactor);
			g.endFill();
		}
	}
};

interface IProps extends IMapTileProps, MappedProps {}

const MapTileFC: React.FC<IProps> = (props) => {
	const { imgSrc, isolated, mappedSize: size, opacity = 1, renderMode, renderWhenBlank, x, y } = props;
	const xCoord = x != null ? x * size : 0;
	const yCoord = y != null ? y * size : 0;

	const drawCb = React.useCallback((g: PIXI.Graphics) => drawTile(g, size, xCoord, yCoord, imgSrc as string[], isolated ? opacity : 1), [
		imgSrc,
		isolated,
		opacity,
		size,
		xCoord,
		yCoord,
	]);

	if (opacity === 0) {
		return null;
	}

	if (renderMode === "webgl") {
		let webglContents: React.ReactNode;
		if (typeof imgSrc === "string") {
			const texture = PIXI.Texture.from(imgSrc);
			if (texture) {
				webglContents = <Sprite height={size} texture={texture} width={size} x={xCoord} y={yCoord} />;
			} else {
				webglContents = <Sprite height={size} image={imgSrc} width={size} x={xCoord} y={yCoord} />;
			}
		} else if (Array.isArray(imgSrc)) {
			webglContents = <Graphics draw={drawCb} />;
		}

		if (!isolated) {
			return <>{webglContents}</>;
		}

		return (
			<Stage width={size} height={size} options={{ backgroundColor: 0, forceCanvas: true, resolution: 1 }}>
				{webglContents}
			</Stage>
		);
	}

	if (!imgSrc) {
		return <>{renderWhenBlank ? <div style={{ width: size, height: size, backgroundColor: "black" }} /> : null}</>;
	}

	const styles: React.CSSProperties = {
		width: size,
		height: size,
		userSelect: "none",
	};

	if (x != null || y != null) {
		MoreObjects.safeAssign(styles, {
			position: "absolute",
			top: yCoord,
			left: xCoord,
		});
	}

	if (typeof imgSrc === "string") {
		return <img src={imgSrc} draggable={false} style={styles} />;
	} else {
		return null;
		// return (
		// 	<svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" baseProfile="full" height={size} style={styles} viewBox="0 0 7 7" width={size}>
		// 		<defs />
		// 		{imgSrc.map((c, i) => (c != null ? <rect fill={c} height={1} key={i} width={1} x={i % 7} y={Math.floor(i / 7)} /> : null))}
		// 	</svg>
		// );
	}
};

export const MapTile = connect(mapStateToProps, getDispatchActionMapper())(React.memo(MapTileFC));
