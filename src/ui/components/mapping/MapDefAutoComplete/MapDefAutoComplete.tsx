import * as React from "react";
import { useSelector } from "react-redux";

import { IWadeMapDefRef } from "../../../../models/map-definition";
import { availableMapDefRefsArraySelector } from "../../../../store/redux/selectors";
import * as Strings from "../../../../util/strings";
import {
	AutoComplete,
	IAutoCompleteProps,
	AutoCompleteFilterOption,
	AutoCompleteOpt,
	AutoCompleteGroups,
	AutoCompleteOptionsProp,
} from "../../core/AutoComplete";

export interface IMapDefAutoCompleteProps extends IAutoCompleteProps<IWadeMapDefRef> {
	grouped?: boolean;
	templatesOnly?: boolean; // only applies if grouped = false
}

const MapDefAutoCompleteFC: React.FC<IMapDefAutoCompleteProps> = (props) => {
	const { grouped, templatesOnly, ...rest } = props;
	const mapDefs = useSelector(availableMapDefRefsArraySelector);

	const options = React.useMemo<AutoCompleteOptionsProp<IWadeMapDefRef>>(() => {
		if (grouped) {
			const result: AutoCompleteGroups<IWadeMapDefRef> = [];

			const nonTemplates = mapDefs
				.filter((md) => !md.isTemplate)
				.map(
					(md): AutoCompleteOpt<IWadeMapDefRef> => ({
						label: md.name,
						value: md,
					})
				);
			if (nonTemplates.length) {
				result.push({
					label: "Templates",
					options: nonTemplates,
				});
			}

			const templates = mapDefs
				.filter((md) => md.isTemplate)
				.map(
					(md): AutoCompleteOpt<IWadeMapDefRef> => ({
						label: md.name,
						value: md,
					})
				);
			if (templates.length) {
				result.push({
					options: nonTemplates,
				});
			}

			return result;
		}
		return mapDefs
			.filter((md) => (templatesOnly ? md.isTemplate : true))
			.map((md) => ({
				label: md.name,
				value: md,
			}));
	}, [grouped, mapDefs, templatesOnly]);

	const filterOptionCb = React.useCallback((option: AutoCompleteFilterOption<IWadeMapDefRef>, rawInput: string) => {
		const {
			label,
			data: { value: mapDef },
		} = option;
		return Strings.anyContainsIgnoreCase([label, ...(mapDef.extensions || [])], rawInput);
	}, []);

	return (
		<AutoComplete<IWadeMapDefRef>
			filterOption={filterOptionCb}
			isClearable
			isSearchable
			label="Map Definition"
			options={options}
			{...rest}
			isMulti={false}
		/>
	);
};

export const MapDefAutoComplete = React.memo(MapDefAutoCompleteFC);
