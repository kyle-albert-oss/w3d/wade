import * as React from "react";
import { connect } from "react-redux";
import { Graphics, Sprite } from "@inlet/react-pixi";
import { DeepReadonly } from "ts-essentials";

import { IMapPlaneXYData } from "../../../../models/map";
import { ImgSrcs } from "../../../../models/wade";
import {
	mapCanvasTileSizeSelector,
	mapTileGraphicUrlForEditorSelector,
	activeMapDefTileFnSelector,
} from "../../../../store/redux/selectors";
import { AppRootState } from "../../../../store/redux/types/state";

export interface IPendingMapTileProps extends Omit<IMapPlaneXYData, "key"> {}

type MappedProps = DeepReadonly<{
	imgSrc?: ImgSrcs;
	mapCanvasTileSize: number;
}>;

interface IProps extends IPendingMapTileProps, MappedProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IPendingMapTileProps>): MappedProps => {
	const { code, plane } = ownProps;
	const tile = activeMapDefTileFnSelector(state)(plane, code);

	return {
		imgSrc: mapTileGraphicUrlForEditorSelector(state)(tile),
		mapCanvasTileSize: mapCanvasTileSizeSelector(state),
	};
};

const PendingMapTileFC: React.FC<IProps> = (props) => {
	const { code, imgSrc, mapCanvasTileSize: size, x, y } = props;

	const xCoord = x * size;
	const yCoord = y * size;

	if (code === 0) {
		return (
			<Graphics
				draw={(g: PIXI.Graphics) => {
					g.beginFill(0, 0.4);
					g.drawRect(xCoord, yCoord, size, size);
					g.endFill();
				}}
			/>
		);
	} else if (imgSrc == null || Array.isArray(imgSrc)) {
		return null;
	}
	// tslint:enable:jsx-no-lambda

	return <Sprite alpha={0.8} height={size} image={imgSrc as string} width={size} x={xCoord} y={yCoord} />;
};

export const PendingMapTile = connect(mapStateToProps)(PendingMapTileFC);
