import * as React from "react";
import { connect } from "react-redux";
import { PixiComponent, withPixiApp } from "@inlet/react-pixi";
import { ClampZoomOptions, ClickEventData, Viewport, ZoomedEventData } from "pixi-viewport";
import { DeepReadonly, Writable } from "ts-essentials";

import MapCanvasTool from "../../../../models/map-canvas-tool";
import { AppActionsType, getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { uiUpdate } from "../../../../store/redux/actions/ui";
import {
	keysDownSelector,
	mapCanvasTileSizeSelector,
	mapTileZoomSelector,
	selectedBrushMapToolSelector,
	selectedMapCanvasSizeSelector,
	selectedMapHeightSelector,
	selectedMapIndexSelector,
	selectedMapToolsSetSelector,
	selectedMapWidthSelector,
	selectedPlaneNumSelector,
	selectedSelectionMapToolSelector,
	selectedTileMouse1Selector,
	selectedTileMouse2Selector,
	uiStateSelector,
} from "../../../../store/redux/selectors";
import {
	IMapFillOperation,
	IMapSetOperation,
	IPlaneBasedMapOperation,
	ITileSelection,
	IXYCoord,
	MapOpCodeOrEnum,
	MapTileSelectState,
} from "../../../../store/redux/types/map-ops";
import { AppRootState } from "../../../../store/redux/types/state";

import { IPaintEventData, PaintPlugin } from "./paint-plugin";

export interface IMapPixiViewportProps {
	children?: React.ReactNode;
	sizeTo: any;
}

type MappedProps = DeepReadonly<{
	canvasHeight: number;
	canvasWidth: number;
	isMouseOverMapCanvas?: boolean;
	keysDown: Set<string>;
	mapCanvasTileSize: number;
	mapHeight: number;
	mapTileZoom: number;
	mapWidth: number;
	selectedBrushMapTool?: MapCanvasTool;
	selectedMapIndex: number;
	selectedMapToolsSet: Set<MapCanvasTool>;
	selectedPlaneNum: number;
	selectedSelectionMapTool?: MapCanvasTool;
	selectedTileM1?: ITileSelection;
	selectedTileM2?: ITileSelection;
}>;

interface IMapPixiViewportInternalProps extends IMapPixiViewportProps, MappedProps, IActionProps {
	app: PIXI.Application;
}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IMapPixiViewportProps>): MappedProps => {
	const selectedMapCanvasSize = selectedMapCanvasSizeSelector(state);
	return {
		canvasHeight: selectedMapCanvasSize,
		canvasWidth: selectedMapCanvasSize,
		isMouseOverMapCanvas: uiStateSelector(state).isMouseOverMapCanvas,
		keysDown: keysDownSelector(state),
		mapCanvasTileSize: mapCanvasTileSizeSelector(state),
		mapHeight: selectedMapHeightSelector(state) ?? 0,
		mapTileZoom: mapTileZoomSelector(state),
		mapWidth: selectedMapWidthSelector(state) ?? 0,
		selectedBrushMapTool: selectedBrushMapToolSelector(state),
		selectedMapIndex: selectedMapIndexSelector(state),
		selectedMapToolsSet: selectedMapToolsSetSelector(state),
		selectedPlaneNum: selectedPlaneNumSelector(state),
		selectedSelectionMapTool: selectedSelectionMapToolSelector(state),
		selectedTileM1: selectedTileMouse1Selector(state),
		selectedTileM2: selectedTileMouse2Selector(state),
	};
};

interface IMapPixiViewport extends Viewport {
	___app: PIXI.Application;
	___props: Writable<MappedProps>;
	___resizeTo: HTMLElement;
	___setZoomConstraints: typeof uiUpdate;
	___worldHeight: number;
	___worldWidth: number;
	___resizeHandler?(): void;
}

const MapPixiViewportBase = PixiComponent<IMapPixiViewportInternalProps, IMapPixiViewport>("MapPixiViewport", {
	create: (props) => {
		const { actions, app, sizeTo, ...rest } = props;
		const { canvasHeight, canvasWidth } = rest;

		let viewport = new Viewport({
			interaction: app.renderer.plugins.interaction,
			screenWidth: sizeTo?.clientWidth ?? 100,
			screenHeight: sizeTo?.clientHeight ?? 100,
			worldWidth: canvasWidth,
			worldHeight: canvasHeight,
		})
			.bounce()
			.clamp({ direction: "all", underflow: "center" })
			.clampZoom({ maxHeight: canvasHeight, maxWidth: canvasWidth })
			.decelerate()
			.drag({ mouseButtons: "middle" })
			.pinch()
			.wheel({ smooth: 5 }) as IMapPixiViewport;

		viewport.plugins.add("paint", new PaintPlugin(viewport, { mouseButtons: "left-right" }), 3);

		viewport = viewport
			.on("clicked", (cd: ClickEventData) => onClick(cd, viewport, actions))
			.on("paint-end", (dd: IPaintEventData) => onPaintEnd(dd, viewport, actions))
			.on("painted", (dd: IPaintEventData) => onPaint(dd, viewport, actions))
			.on("paint-start", (dd: IPaintEventData) => onPaintStart(dd, viewport, actions))
			.on("zoomed", (ed: ZoomedEventData) => onZoom(ed, actions))
			.on("mousemove", (e: PIXI.InteractionEvent) => onMouseMove(e, viewport, actions))
			.on("mouseout", (e: PIXI.InteractionEvent) => onMouseOut(e, viewport, actions))
			.on("mouseover", (e: PIXI.InteractionEvent) => onMouseOver(e, viewport, actions));

		viewport.___app = app;
		viewport.___props = { ...rest };
		viewport.___resizeTo = sizeTo;
		viewport.___worldWidth = canvasWidth;
		viewport.___worldHeight = canvasHeight;
		viewport.___setZoomConstraints = actions.uiUpdate;

		return viewport;
	},

	didMount(instance: IMapPixiViewport, parent: PIXI.Container): void {
		parent.addChild(instance);

		instance.___resizeHandler = () => {
			resize(instance, instance.___resizeTo, instance.___worldWidth, instance.___worldHeight);
		};

		window.addEventListener("resize", instance.___resizeHandler);
	},

	willUnmount(instance: IMapPixiViewport, parent: PIXI.Container): void {
		if (instance.___resizeHandler) {
			window.removeEventListener("resize", instance.___resizeHandler);
		}
	},

	applyProps(
		instance: IMapPixiViewport,
		oldProps: Readonly<IMapPixiViewportInternalProps>,
		newProps: Readonly<IMapPixiViewportInternalProps>
	): void {
		const { mapTileZoom: oldMapTileZoom } = oldProps;
		const { actions, app, sizeTo, ...rest } = newProps;
		const { canvasHeight, mapTileZoom, canvasWidth } = rest;

		Object.assign(instance.___props, rest);
		if (oldMapTileZoom !== mapTileZoom) {
			instance.setZoom(mapTileZoom);
		}

		resize(instance, sizeTo, canvasWidth, canvasHeight);
	},
});

const resize = (instance: IMapPixiViewport, sizeTo: HTMLElement, worldWidth: number, worldHeight: number) => {
	instance.___resizeTo = sizeTo;

	const { height: oldViewportHeight, width: oldViewportWidth } = instance.___app.renderer;
	const { clientWidth: viewportWidth, clientHeight: viewportHeight } = sizeTo || { clientWidth: 100, clientHeight: 100 };

	if (oldViewportWidth !== viewportWidth || oldViewportHeight !== viewportHeight) {
		const clampZoomOpts: ClampZoomOptions = {};
		let zoomMin;
		if (viewportWidth > viewportHeight) {
			clampZoomOpts.maxHeight = worldHeight;
			zoomMin = viewportHeight / worldHeight;
		} else {
			clampZoomOpts.maxWidth = worldWidth;
			zoomMin = viewportWidth / worldWidth;
		}
		instance.clampZoom(clampZoomOpts);
		instance.___setZoomConstraints({ mapTileZoomMin: zoomMin });
	}

	instance.___app.renderer.resize(viewportWidth, viewportHeight);
	instance.___worldHeight = worldHeight;
	instance.___worldWidth = worldWidth;
	instance.resize(viewportWidth, viewportHeight, worldWidth, worldHeight);
};

const isRightClick = (event: PIXI.InteractionEvent | IPaintEventData) =>
	"realButton" in event ? event.realButton === 2 : event.data.button === 2;

const onZoom = (eventData: ZoomedEventData, actions: AppActionsType) => {
	actions.uiUpdate({ mapTileZoom: eventData.viewport.scale.x });
};

const onClick = (eventData: ClickEventData, vp: IMapPixiViewport, actions: AppActionsType) => {
	const {
		mapCanvasTileSize,
		mapHeight,
		mapWidth,
		selectedBrushMapTool,
		selectedMapIndex,
		selectedMapToolsSet,
		selectedPlaneNum,
		selectedSelectionMapTool,
		selectedTileM1,
		selectedTileM2,
	} = vp.___props;

	const [x, y] = toTileCoords(eventData.world.x, eventData.world.y, mapCanvasTileSize);
	if (x < 0 || y < 0 || x >= mapWidth || y >= mapHeight) {
		return;
	}

	const isRightClk = isRightClick(eventData.event);
	const button = isRightClk ? "MOUSE2" : "MOUSE1";

	if (selectedMapToolsSet.has(MapCanvasTool.DROPPER)) {
		actions.doDropperSiphon({ button, plane: selectedPlaneNum, x, y });
		return;
	} else if (selectedSelectionMapTool) {
		actions.doPendingMapOperation({
			maps: selectedMapIndex,
			points: [],
			subOperation: MapTileSelectState.SET,
			type: "select",
		});
	} else if (selectedBrushMapTool) {
		const type: IMapSetOperation["type"] | IMapFillOperation["type"] = selectedBrushMapTool === MapCanvasTool.FILL ? "fill" : "set";
		let planes: IMapSetOperation["planes"];
		let code: IMapSetOperation["code"];
		if (selectedMapToolsSet.has(MapCanvasTool.ERASER)) {
			planes = selectedPlaneNum;
			code = 0;
		} else {
			const tilePlane = (isRightClk ? selectedTileM2 : selectedTileM1)?.plane;
			if (tilePlane == null) {
				return;
			}
			planes = tilePlane;
			code = button;
		}

		actions.doPendingMapOperation({
			code,
			maps: selectedMapIndex,
			planes,
			points: { x, y },
			type,
		});
	}
};

let lastMouseMoveX: number;
let lastMouseMoveY: number;

const onMouseMove = (e: PIXI.InteractionEvent, vp: IMapPixiViewport, actions: AppActionsType) => {
	const { isMouseOverMapCanvas, mapCanvasTileSize, mapHeight, mapWidth } = vp.___props;
	if (!isMouseOverMapCanvas) {
		return;
	}
	const worldPoint = vp.toWorld(e.data.global);
	const [x, y] = toTileCoords(worldPoint.x, worldPoint.y, mapCanvasTileSize);

	if ((x === lastMouseMoveX && y === lastMouseMoveY) || x < 0 || y < 0 || x >= mapWidth || y >= mapHeight) {
		return;
	}

	lastMouseMoveX = x;
	lastMouseMoveY = y;

	actions.uiUpdate({
		mapPointerXCoord: x,
		mapPointerYCoord: y,
	});
};

const onMouseOut = (e: PIXI.InteractionEvent, vp: IMapPixiViewport, actions: AppActionsType) => {
	actions.uiToggle({ isMouseOverMapCanvas: false });
};
const onMouseOver = (e: PIXI.InteractionEvent, vp: IMapPixiViewport, actions: AppActionsType) => {
	actions.uiToggle({ isMouseOverMapCanvas: true });
};

const toTileCoords = (worldX: number, worldY: number, scale: number): [number, number] => [
	Math.floor(worldX / scale),
	Math.floor(worldY / scale),
];

const onPaintStart = (eventData: IPaintEventData, vp: IMapPixiViewport, actions: AppActionsType) => {};

let lastPaintX: number | undefined;
let lastPaintY: number | undefined;

type PlanesType = IPlaneBasedMapOperation<any>["planes"];
const onPaint = (eventData: IPaintEventData, vp: IMapPixiViewport, actions: AppActionsType) => {
	const {
		mapCanvasTileSize,
		mapHeight,
		mapWidth,
		selectedBrushMapTool,
		selectedMapIndex,
		selectedMapToolsSet,
		selectedSelectionMapTool,
		selectedTileM1,
		selectedTileM2,
	} = vp.___props;

	const [x, y] = toTileCoords(eventData.world.x, eventData.world.y, mapCanvasTileSize);
	if ((x === lastPaintX && y === lastPaintY) || x < 0 || y < 0 || x >= mapWidth || y >= mapHeight) {
		return;
	}

	lastPaintX = x;
	lastPaintY = y;

	const isM2 = isRightClick(eventData);

	let code: MapOpCodeOrEnum;
	let planes: PlanesType;

	if (selectedBrushMapTool) {
		if (selectedMapToolsSet.has(MapCanvasTool.ERASER)) {
			code = 0;
			planes = "all";
		} else {
			const tilePlane = (isM2 ? selectedTileM2 : selectedTileM1)?.plane;
			if (tilePlane == null) {
				return;
			}
			planes = tilePlane;
			code = isM2 ? "MOUSE2" : "MOUSE1";
		}

		if (selectedBrushMapTool === MapCanvasTool.PENCIL) {
			actions.doPendingMapOperation(
				{
					code,
					maps: selectedMapIndex,
					planes,
					points: { x, y },
					type: "set",
				},
				{
					commit: false,
				}
			);
		} else if (selectedBrushMapTool === MapCanvasTool.RECTANGLE_FILL || selectedBrushMapTool === MapCanvasTool.RECTANGLE_OUTLINE) {
			const points = getPendingRectanglePoints(eventData, x, y, selectedBrushMapTool === MapCanvasTool.RECTANGLE_OUTLINE, vp);
			actions.doPendingMapOperation(
				{
					code,
					maps: selectedMapIndex,
					planes,
					points,
					type: "set",
				},
				{
					clearPending: true,
					commit: false,
				}
			);
		}
	} else if (selectedSelectionMapTool) {
		const points = getPendingRectanglePoints(eventData, x, y, false, vp);
		let subOperation: MapTileSelectState;
		if (vp.___props.keysDown.has("Shift")) {
			subOperation = MapTileSelectState.ADD;
		} else if (vp.___props.keysDown.has("Control")) {
			subOperation = MapTileSelectState.SUBTRACT;
		} else {
			subOperation = MapTileSelectState.SET;
		}

		actions.doPendingMapOperation(
			{
				maps: selectedMapIndex,
				points,
				subOperation,
				type: "select",
			},
			{
				clearPending: true,
				commit: false,
			}
		);
	}
};

const getPendingRectanglePoints = (eventData: IPaintEventData, x: number, y: number, outline: boolean, vp: IMapPixiViewport) => {
	const { mapCanvasTileSize } = vp.___props;

	let [startX, startY] = toTileCoords(eventData.worldStart.x, eventData.worldStart.y, mapCanvasTileSize);
	if (startX > x) {
		[x, startX] = [startX, x];
	}
	if (startY > y) {
		[y, startY] = [startY, y];
	}
	const points: IXYCoord[] = [];
	for (let curX = startX; curX <= x; curX++) {
		for (let curY = startY; curY <= y; curY++) {
			if (!outline || curX === startX || curX === x || curY === startY || curY === y) {
				points.push({ x: curX, y: curY });
			}
		}
	}

	return points;
};

const onPaintEnd = (eventData: IPaintEventData, vp: IMapPixiViewport, actions: AppActionsType) => {
	actions.commitPendingMapOperations();

	lastPaintX = lastPaintY = undefined;
};

// @ts-expect-error
export const MapPixiViewport = connect(mapStateToProps, getDispatchActionMapper())(withPixiApp(MapPixiViewportBase));
