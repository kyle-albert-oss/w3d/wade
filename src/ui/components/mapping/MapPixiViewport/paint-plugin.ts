import * as PIXI from "pixi.js";
import { Plugin, Viewport } from "pixi-viewport";
import tsDefaults, { WithEnforcedDefaults } from "ts-defaults";

export interface IPaintPluginOpts {
	mouseButtons?: "all" | "left-right";
}

type PaintPluginOptionsInternal = WithEnforcedDefaults<IPaintPluginOpts, "mouseButtons">;

export class PaintPlugin extends Plugin {
	current?: number;
	last?: { x: number; y: number; button: number } | null;
	mouse?: [boolean, boolean, boolean];
	options: PaintPluginOptionsInternal;
	realButton: number = 0;
	painted: boolean;
	paintStart?: PIXI.Point;
	paintStartWorld?: PIXI.Point;
	paused?: boolean;

	constructor(parent: Viewport, options: IPaintPluginOpts = {}) {
		super(parent);
		this.options = tsDefaults(
			options,
			{
				mouseButtons: "all",
			},
			["mouseButtons"]
		)!;
		this.painted = false;

		const { mouseButtons: buttons } = this.options;

		if (!buttons || buttons === "all") {
			this.mouse = [true, true, true];
		} else {
			this.mouse = [buttons.indexOf("left") !== -1, buttons.indexOf("middle") !== -1, buttons.indexOf("right") !== -1];
		}
		parent.on("rightdown", (e) => (this.realButton = 2));
		parent.on("rightup", (e) => (this.realButton = 0));
	}

	getButton(event?: PIXI.InteractionEvent) {
		return event && event.data.button !== -1 ? event.data.button : this.realButton;
	}

	checkButtons(event: PIXI.InteractionEvent) {
		const isMouse = event.data.pointerType === "mouse";
		const count = this.parent.input.count();
		if (count === 1 || (count > 1 && !this.parent.plugins.get("pinch"))) {
			if (!isMouse || (this.mouse && this.mouse[this.getButton(event)])) {
				return true;
			}
		}
		return false;
	}

	down(event: PIXI.InteractionEvent) {
		if (this.paused) {
			return;
		}
		if (this.checkButtons(event)) {
			this.last = { x: event.data.global.x, y: event.data.global.y, button: this.getButton(event) };
			this.current = event.data.pointerId;
			return true;
		} else {
			this.last = null;
		}
	}

	get active() {
		return this.painted;
	}

	move(event: PIXI.InteractionEvent) {
		if (this.paused) {
			return;
		}
		if (this.last && this.current === event.data.pointerId) {
			const x = event.data.global.x;
			const y = event.data.global.y;
			const count = this.parent.input.count();
			if (count === 1 || (count > 1 && !this.parent.plugins.get("pinch"))) {
				const distX = x - this.last.x;
				const distY = y - this.last.y;
				if (
					this.painted ||
					((this.parent.input.checkThreshold(distX) || this.parent.input.checkThreshold(distY)) &&
						(!this.parent.plugins.get("drag") || !this.parent.plugins.get("drag").active))
				) {
					this.last = { x, y, button: this.getButton(event) };
					if (!this.painted) {
						const newScreen = new PIXI.Point(this.last.x, this.last.y);
						const world = this.parent.toWorld(newScreen);
						this.paintStart = newScreen;
						this.paintStartWorld = this.parent.toWorld(this.paintStart);

						this.parent.emit("paint-start", {
							interactionEvent: event,
							realButton: this.getButton(event),
							screen: newScreen,
							screenStart: newScreen,
							viewport: this.parent,
							world,
							worldStart: world,
						});
					}

					this.painted = true;

					const screen = event.data.global.clone();
					this.parent.emit("painted", {
						interactionEvent: event,
						realButton: this.getButton(event),
						screen,
						screenStart: this.paintStart,
						viewport: this.parent,
						world: this.parent.toWorld(screen),
						worldStart: this.paintStartWorld,
					});

					return true;
				}
			} else {
				this.painted = false;
			}
		}
	}

	up(event: PIXI.InteractionEvent) {
		const touches = this.parent.input.touches;
		if (touches.length === 1) {
			const pointer = touches[0];
			if (pointer.last) {
				this.last = { x: pointer.last.x, y: pointer.last.y, button: this.getButton(event) };
				this.current = pointer.id;
			}
			this.painted = false;
			return true;
		} else if (this.last) {
			if (this.painted) {
				const screen = event.data.global.clone();

				this.parent.emit("paint-end", {
					interactionEvent: event,
					realButton: this.getButton(event),
					screen,
					screenStart: this.paintStart,
					viewport: this.parent,
					world: this.parent.toWorld(screen),
					worldStart: this.paintStartWorld,
				});

				this.last = null;
				this.painted = false;
				delete this.paintStart;
				delete this.paintStartWorld;
				return true;
			}
		}
	}

	resume() {
		delete this.last;
		this.paused = false;
	}
}

export interface IPaintEventData {
	interactionEvent: PIXI.InteractionEvent;
	realButton: number;
	screen: PIXI.Point;
	screenStart: PIXI.Point;
	viewport: Viewport;
	world: PIXI.Point;
	worldStart: PIXI.Point;
}

declare module "pixi-viewport" {
	interface Viewport {
		on(event: "painted", handler: (ed: IPaintEventData) => void): this;
		on(event: "paint-end", handler: (ed: IPaintEventData) => void): this;
		on(event: "paint-start", handler: (ed: IPaintEventData) => void): this;
	}
}
