import * as React from "react";
import { connect, Provider, ReactReduxContext } from "react-redux";
import { Container, Stage } from "@inlet/react-pixi";
import { DeepReadonly, Dictionary } from "ts-essentials";
import { IGameMapPlane } from "wolf3d-data";

import { MapPlaneDefinition } from "../../../../models/map";
import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import {
	selectedMapCanvasSizeSelector,
	selectedMapIndexSelector,
	selectedMapPlanesSelector,
	activeMapDefPlanesSelector,
	uiStateSelector,
} from "../../../../store/redux/selectors";
import { AppRootState } from "../../../../store/redux/types/state";
import { MapPixiViewport } from "../MapPixiViewport";
import { MapPlane } from "../MapPlane";
import { MapSelectionPlane } from "../MapSelectionPlane";
import { PendingMapDataPlane } from "../PendingMapDataPlane";

import "./MapCanvas.css";

export interface IMapCanvasProps {
	renderMode: "basic" | "webgl";
}

type MappedProps = DeepReadonly<{
	height: number | undefined;
	hiddenPlanes: number[] | undefined;
	mapIndex: number | undefined;
	planes: IGameMapPlane[] | undefined; // for map
	planeDefs: Dictionary<MapPlaneDefinition> | undefined;
	width: number | undefined;
	opacity: number | undefined;
}>;

interface IProps extends IMapCanvasProps, MappedProps, IActionProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IMapCanvasProps>): MappedProps => {
	const mapIndex = selectedMapIndexSelector(state);
	const selectedMapPlanes = selectedMapPlanesSelector(state);
	const selectedMapCanvasSize = selectedMapCanvasSizeSelector(state);
	const planeDefs = activeMapDefPlanesSelector(state);
	const uiState = uiStateSelector(state);
	return {
		height: selectedMapCanvasSize,
		hiddenPlanes: uiState.mapCanvasHiddenPlanes,
		mapIndex,
		planes: selectedMapPlanes,
		planeDefs,
		width: selectedMapCanvasSize,
		opacity: uiState.mapCanvasFloorCodeOpacity,
	};
};

export const MapCanvasFC: React.FC<IProps> = (props) => {
	const { height, hiddenPlanes, mapIndex, opacity, planes, planeDefs, renderMode, width } = props;

	const [containerRef, setContainerRef] = React.useState<HTMLDivElement>();
	const onRefInit = React.useCallback((el: HTMLDivElement) => {
		setContainerRef(el);
	}, []);
	const onContextMenu = React.useCallback((e: React.MouseEvent) => {
		e.preventDefault();
	}, []);

	const zIndexSortedPlanes = React.useMemo(() => {
		if (!planes || !planeDefs) {
			return planes;
		}
		// sort by zIndex, fallback to plane ordinal index; for nullish zIndex, sort zIndex-less planes before the ones with zIndex
		return [...planes]
			.filter((p) => !hiddenPlanes?.includes(p.index))
			.sort((p1, p2) => {
				const p1ZIndex = planeDefs?.[p1.index]?.zIndex;
				const p2ZIndex = planeDefs?.[p2.index]?.zIndex;
				if (p1ZIndex == null && p2ZIndex == null) {
					return p1.index - p2.index;
				} else if (p1ZIndex == null) {
					return -1;
				} else if (p2ZIndex == null) {
					return 1;
				}
				return p1ZIndex - p2ZIndex;
			});
	}, [hiddenPlanes, planeDefs, planes]);

	if (mapIndex == null || !zIndexSortedPlanes || !width || !height || !planeDefs) {
		return <span>No Map</span>;
	}

	const planesJSX = (
		<Container sortableChildren>
			{zIndexSortedPlanes.map((plane) => (
				<Container key={plane.index}>
					<MapPlane mapIndex={mapIndex} plane={plane.index} renderMode={renderMode} />
				</Container>
			))}
			<PendingMapDataPlane />
			<MapSelectionPlane />
		</Container>
	);

	let content: React.ReactNode = planesJSX;
	if (renderMode === "webgl") {
		content = containerRef && (
			<ReactReduxContext.Consumer>
				{({ store }) => (
					<Stage width={width} height={height} options={{ antialias: false, backgroundColor: 0, resolution: 1 }}>
						<Provider store={store}>
							<MapPixiViewport sizeTo={containerRef}>{planesJSX}</MapPixiViewport>
						</Provider>
					</Stage>
				)}
			</ReactReduxContext.Consumer>
		);
	}

	return (
		<div className="app-map-canvas" onContextMenu={onContextMenu} ref={onRefInit}>
			{content}
		</div>
	);
};

export const MapCanvas = connect(mapStateToProps, getDispatchActionMapper())(React.memo(MapCanvasFC));
