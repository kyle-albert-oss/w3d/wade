import * as React from "react";
import { useDispatch } from "react-redux";
import { button } from "@storybook/addon-knobs";

import { createStory } from "../../../../../stories/story-util";
import { doPendingMapOperation } from "../../../../store/redux/actions/map";
import { MapTileSelectState } from "../../../../store/redux/types/map-ops";

import { IMapCanvasProps, MapCanvas } from "./MapCanvas";

const xy = (x: number, y: number): { x: number; y: number } => ({ x, y });

let selectionActive = false;
const DefaultMapCanvas: React.FC<IMapCanvasProps> = (props) => {
	const dispatch = useDispatch();

	button("Toggle Selection", () => {
		selectionActive = !selectionActive;
		if (selectionActive) {
			dispatch(
				doPendingMapOperation({
					maps: 0,
					points: [
						xy(1, 1),
						xy(1, 2),
						xy(1, 3),
						xy(2, 3),
						xy(3, 3),
						xy(3, 2),
						xy(3, 1),
						xy(2, 1),
						xy(1, 5),
						xy(6, 1),
						xy(5, 2),
						xy(6, 2),
						xy(7, 3),
						xy(6, 3),
					],
					subOperation: MapTileSelectState.SET,
					type: "select",
				})
			);
		} else {
			dispatch(
				doPendingMapOperation({
					maps: 0,
					points: [],
					subOperation: MapTileSelectState.SET,
					type: "select",
				})
			);
		}
	});

	return <MapCanvas {...props} />;
};

createStory("Map|MapCanvas", module).add("Default", () => <DefaultMapCanvas renderMode="webgl" />);
