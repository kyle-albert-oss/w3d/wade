import * as React from "react";

import { AppTooltip, IAppTooltipProps } from "../../core/AppTooltip";
import { MapTileDetail } from "../MapTileDetail";

interface IProps extends Omit<IAppTooltipProps, "title"> {
	plane?: number;
	tileCode?: number;
}

const MapTileTooltip: React.FC<IProps> = (props) => {
	const { children, plane, tileCode, ...rest } = props;

	if (plane == null || tileCode == null) {
		return children;
	}

	return (
		<AppTooltip arrow pad={false} {...rest} title={<MapTileDetail plane={plane} tileCode={tileCode} />}>
			{children}
		</AppTooltip>
	);
};

export default React.memo(MapTileTooltip);
