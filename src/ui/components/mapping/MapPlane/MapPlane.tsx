import * as React from "react";
import { connect } from "react-redux";
import { DeepReadonly } from "ts-essentials";

import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { selectedMapPlanesSelector } from "../../../../store/redux/selectors";
import { AppRootState } from "../../../../store/redux/types/state";

import { MapPlaneRow } from "./MapPlaneRow";

export interface IMapPlaneProps {
	mapIndex: number;
	plane: number;
	renderMode: "basic" | "webgl";
}

type MappedProps = DeepReadonly<{
	height: number | undefined;
	width: number | undefined;
}>;

interface IProps extends IMapPlaneProps, MappedProps, IActionProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IMapPlaneProps>): MappedProps => {
	const mapPlane = selectedMapPlanesSelector(state)?.[ownProps.plane];
	return {
		height: mapPlane?.height,
		width: mapPlane?.width,
	};
};

const MapPlaneFC: React.FC<IProps> = (props) => {
	const { height, mapIndex, plane, renderMode, width } = props;

	if (width == null || height == null) {
		return <></>;
	}

	const rows: React.ReactNode[] = [];
	for (let row = 0; row < height; row++) {
		rows.push(<MapPlaneRow key={row} map={mapIndex} plane={plane} renderMode={renderMode} row={row} />);
	}

	return <>{rows}</>;
};

export const MapPlane = connect(mapStateToProps, getDispatchActionMapper())(React.memo(MapPlaneFC));
