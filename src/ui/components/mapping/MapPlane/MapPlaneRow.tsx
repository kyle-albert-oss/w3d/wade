import * as React from "react";
import { useMemo } from "react";
import { connect } from "react-redux";
import { DeepReadonly } from "ts-essentials";
import { Container } from "@inlet/react-pixi";

import { IActionProps, getDispatchActionMapper } from "../../../../store/redux/actions";
import { AppRootState } from "../../../../store/redux/types/state";
import { uiStateSelector, selectedMapPlanesSelector, activeMapDefTileFnSelector } from "../../../../store/redux/selectors";
import { TileType } from "../../../../models/map";
import { MapTile } from "../MapTile";
import { PixiAlphaFilter } from "../../pixi/PixiAlphaFilter";

export interface IMapPlaneRowProps {
	map: number;
	plane: number;
	renderMode: "webgl" | "basic";
	row: number;
}

type MappedProps = DeepReadonly<{
	tileCodes: number[] | undefined;
	tileProvider: ReturnType<typeof activeMapDefTileFnSelector>;
	floorCodeOpacity: number;
}>;

interface IProps extends IMapPlaneRowProps, MappedProps, IActionProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IMapPlaneRowProps>): MappedProps => {
	const { plane, row } = ownProps;
	const uiState = uiStateSelector(state);
	const planes = selectedMapPlanesSelector(state);
	const planeData = planes?.[plane];
	const rowData = planeData?.data?.[row];

	return {
		tileCodes: rowData,
		tileProvider: activeMapDefTileFnSelector(state),
		floorCodeOpacity: uiState.mapCanvasFloorCodeOpacity,
	};
};

const MapPlaneRowFC: React.FC<IProps> = (props) => {
	const { floorCodeOpacity, map, plane, renderMode, row, tileCodes, tileProvider } = props;

	const { tileNodes, floorCodeNodes } = useMemo(() => {
		const newTileNodes: React.ReactNode[] = [];
		const newFloorCodeNodes: React.ReactNode[] = [];

		for (let col = 0; tileCodes && col < tileCodes.length; col++) {
			const tileCode = tileCodes[col];
			const tile = tileProvider(plane, tileCode);
			if (!tile) {
				continue;
			}
			const tileNode = <MapTile key={col} map={map} plane={plane} renderMode={renderMode} x={col} y={row} />;
			if (tile?.types?.[TileType.FLOOR_CODE]) {
				newFloorCodeNodes.push(tileNode);
			} else {
				newTileNodes.push(tileNode);
			}
		}

		return {
			tileNodes: newTileNodes,
			floorCodeNodes: newFloorCodeNodes,
		};
	}, [map, plane, renderMode, row, tileCodes, tileProvider]);

	return (
		<>
			{floorCodeNodes.length > 0 && <PixiAlphaFilter alpha={floorCodeOpacity}>{floorCodeNodes}</PixiAlphaFilter>}
			{tileNodes.length > 0 && <Container>{tileNodes}</Container>}
		</>
	);
};

export const MapPlaneRow = connect(mapStateToProps, getDispatchActionMapper())(React.memo(MapPlaneRowFC));
