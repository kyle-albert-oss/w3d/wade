import * as React from "react";
import { number } from "@storybook/addon-knobs";

import { createStory } from "../../../../../stories/story-util";

import { MapTileDetail } from "./MapTileDetail";

createStory("Map|MapTileDetail", module, { style: { padding: 20 } }).add("Default", () => (
	<MapTileDetail plane={number("Plane", 0, { max: 2, min: 0, step: 1 })} tileCode={number("Tile Code", 1, { min: 0, step: 1 })} />
));
