import { CardMedia } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import * as React from "react";
import { connect } from "react-redux";
import { DeepReadonly } from "ts-essentials";

import { MapTileData } from "../../../../models/map";
import { ImgSrcs } from "../../../../models/wade";
import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { mapTileGraphicUrlsSelector, activeMapDefTileFnSelector } from "../../../../store/redux/selectors";
import { AppRootState } from "../../../../store/redux/types/state";
import { SpaceBetween } from "../../core/SpaceBetween";
import { AppChip } from "../../core/AppChip";
import { compareIgnoreCase } from "../../../../util/strings";
import { MapTile } from "../MapTile";

export interface IMapTileDetailProps {
	plane: number;
	tileCode: number;
}

type MappedProps = DeepReadonly<{
	imgSrcs: Exclude<ImgSrcs, string>;
	tile: MapTileData | undefined;
}>;

interface IProps extends IMapTileDetailProps, MappedProps, IActionProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IMapTileDetailProps>): MappedProps => {
	const { plane, tileCode } = ownProps;
	const tile = activeMapDefTileFnSelector(state)(plane, tileCode);
	const srcs = mapTileGraphicUrlsSelector(state)(tile);
	return {
		imgSrcs: (typeof srcs === "string" ? [srcs] : srcs) || [],
		tile,
	};
};

const MapTileDetailFC: React.FC<IProps> = (props) => {
	const { imgSrcs, tile } = props;

	if (!tile) {
		return null;
	}

	const { code, label, gridImage, plane, tags = [], types = {} } = tile;

	return (
		<Card style={{ width: 300 }}>
			<div style={{ position: "relative", height: 100, backgroundColor: "rgba(0, 0, 0, .5)" }}>
				{imgSrcs && imgSrcs.length > 0 ? (
					<>
						{imgSrcs.length > 1 && (
							<CardMedia component="img" src={imgSrcs[0] ?? undefined} style={{ position: "absolute", height: 100, opacity: 0.2 }} />
						)}
						<Grid container justify="center" style={{ position: "absolute" }}>
							{imgSrcs.map((src) => (
								<Grid key={src ?? undefined} item style={{ position: "relative" }}>
									<img src={src ?? undefined} style={{ height: 100 }} />
								</Grid>
							))}
						</Grid>
					</>
				) : gridImage ? (
					<Box display="flex" justifyContent="center" alignItems="center" height="100%">
						<MapTile isolated plane={plane} renderMode="webgl" renderWhenBlank tileCode={code} />
					</Box>
				) : null}
			</div>
			<CardContent>
				<Typography gutterBottom variant="h6">
					{label}
				</Typography>
				<Grid item container spacing={2} wrap="nowrap">
					<Grid item xs>
						<SpaceBetween>
							<Box color="text.hint">Tile Code</Box>
							<Box fontFamily="Monospace" fontSize={16}>
								{code}
							</Box>
						</SpaceBetween>
						<Box pt={1} pb={1}>
							<Divider />
						</Box>
						<SpaceBetween>
							<Box color="text.hint">Types</Box>
							<div>
								{Object.keys(types)
									.sort(compareIgnoreCase)
									.map((t) => (
										<Box key={t} display="inline-block" ml={1}>
											<AppChip label={t} size="small" square />
										</Box>
									))}
							</div>
						</SpaceBetween>
						<Box pt={1} pb={1}>
							<Divider />
						</Box>
						<SpaceBetween>
							<Box color="text.hint">Tags</Box>
							<div>
								{tags.map((t) => (
									<Box key={t} display="inline-block" ml={1}>
										<AppChip label={t} size="small" square />
									</Box>
								))}
							</div>
						</SpaceBetween>
					</Grid>
				</Grid>
			</CardContent>
		</Card>
	);
};

export const MapTileDetail = connect(mapStateToProps, getDispatchActionMapper())(React.memo(MapTileDetailFC));
