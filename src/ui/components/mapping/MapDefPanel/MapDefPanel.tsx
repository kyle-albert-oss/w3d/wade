import * as React from "react";
import { useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import clsx from "clsx";

import { useActions } from "../../../../store/redux/actions";
import { activeMapDefPlaneIndexNameSelector, selectedPlaneNumSelector } from "../../../../store/redux/selectors";
import { MapDefList } from "../MapDefList";

const MapDefPanelFC: React.FC = () => {
	const actions = useActions();
	const planes = useSelector(activeMapDefPlaneIndexNameSelector);
	const selectedPlaneNum = useSelector(selectedPlaneNumSelector);

	const onPlaneTabChangeCb = React.useCallback(
		(event: React.ChangeEvent<{}>, value: number) => {
			actions.uiUpdate({ selectedPlaneNum: value || 0 });
		},
		[actions]
	);

	return (
		<Grid container direction="column" className="app-height-full">
			<Grid item className="app-width-full">
				<Tabs onChange={onPlaneTabChangeCb} scrollButtons="auto" value={selectedPlaneNum} variant="scrollable">
					{planes.map((p) => (
						<Tab className="app-tabs-auto-width" key={p.index} label={p.name} value={p.index} />
					))}
				</Tabs>
			</Grid>
			<Grid item className="app-overflow-x-hidden app-overflow-y-auto app-grow">
				<div className={clsx("app-embedded-panel", "app-height-full")}>
					<MapDefList />
				</div>
			</Grid>
		</Grid>
	);
};

export const MapDefPanel = React.memo(MapDefPanelFC);
