import * as React from "react";
import { useSelector } from "react-redux";
import AutoSizer from "react-virtualized-auto-sizer";
import { FixedSizeList } from "react-window";

import { mapDefTileSizeSelector, selectedPlaneAllItemsSelector } from "../../../../store/redux/selectors";
import { MapDefListItem } from "../MapDefListItem";

const MapDefListFC: React.FC = () => {
	const items = useSelector(selectedPlaneAllItemsSelector);
	const mapDefTileSize = useSelector(mapDefTileSizeSelector);

	const itemKeyFn = React.useCallback((index: number, data: any) => {
		return data[index].code;
	}, []);

	return (
		<AutoSizer>
			{({ height, width }) => (
				<FixedSizeList
					className="app-overflow-x-hidden"
					height={height}
					itemCount={items.length}
					itemData={items}
					itemKey={itemKeyFn}
					itemSize={mapDefTileSize}
					overscanCount={10}
					width={width}
				>
					{MapDefListItem}
				</FixedSizeList>
			)}
		</AutoSizer>
	);
};

export const MapDefList = React.memo(MapDefListFC);
