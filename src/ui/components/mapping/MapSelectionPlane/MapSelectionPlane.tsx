import * as PIXI from "pixi.js";
import * as React from "react";
import { useMemo } from "react";
import { connect } from "react-redux";
import { Graphics } from "@inlet/react-pixi";
import { DeepReadonly, Dictionary } from "ts-essentials";

import { getMapPlaneXYData, getMapPlaneXYKey } from "../../../../store/redux/reducers/map";
import {
	mapCanvasTileSizeSelector,
	mapSelectionDataSelector,
	pendingMapSelectionDataSelector,
	uiStateSelector,
	mapTileZoomSelector,
} from "../../../../store/redux/selectors";
import { MapTileSelectState } from "../../../../store/redux/types/map-ops";
import { AppRootState } from "../../../../store/redux/types/state";
import { drawDashedLine } from "../../pixi/draw";
import { IMapPlaneXYData } from "../../../../models/map";
import { PixiAlphaFilter } from "../../pixi/PixiAlphaFilter";

export interface IMapSelectionPlaneProps {}

type MappedProps = DeepReadonly<{
	isMouseOverMapCanvas: boolean;
	mapCanvasTileSize: number;
	mapPointerXCoord: number | undefined;
	mapPointerYCoord: number | undefined;
	mapSelectionData: Dictionary<MapTileSelectState>;
	mapTileZoom: number;
	pendingMapSelectionData: Dictionary<MapTileSelectState>;
}>;

interface IProps extends IMapSelectionPlaneProps, MappedProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IMapSelectionPlaneProps>): MappedProps => {
	const { isMouseOverMapCanvas, mapPointerXCoord, mapPointerYCoord } = uiStateSelector(state);
	return {
		isMouseOverMapCanvas,
		mapCanvasTileSize: mapCanvasTileSizeSelector(state),
		mapPointerXCoord,
		mapPointerYCoord,
		mapTileZoom: mapTileZoomSelector(state),
		mapSelectionData: mapSelectionDataSelector(state),
		pendingMapSelectionData: pendingMapSelectionDataSelector(state),
	};
};

interface IMapTileSelectionData {
	selectState: MapTileSelectState;
	xyData: IMapPlaneXYData;
}

type MapTileSelectionDataMap = Dictionary<IMapTileSelectionData>;

const WHITE_COLOR = 0xffffff;
const ADD_COLOR = 0x00ff00;
const SUBTRACT_COLOR = 0xff0000;
const DASH = 4;
const GAP = 4;
const HIGHLIGHT_WEIGHT = 2;

const MapSelectionPlaneC: React.FC<IProps> = (props) => {
	const {
		isMouseOverMapCanvas,
		mapCanvasTileSize: size,
		mapPointerXCoord,
		mapPointerYCoord,
		mapSelectionData,
		mapTileZoom,
		pendingMapSelectionData,
	} = props;

	const selectionData = useMemo(() => {
		const result: MapTileSelectionDataMap = {};
		let onlyRenderPending = false;

		for (const [pendingKey, selectState] of Object.entries(pendingMapSelectionData)) {
			if (selectState === MapTileSelectState.SET) {
				onlyRenderPending = true;
			}
			result[pendingKey] = {
				selectState,
				xyData: getMapPlaneXYData(pendingKey),
			};
		}
		if (!onlyRenderPending) {
			for (const [selectionKey, selectState] of Object.entries(mapSelectionData)) {
				if (result[selectionKey]) {
					continue;
				}
				result[selectionKey] = {
					selectState,
					xyData: getMapPlaneXYData(selectionKey),
				};
			}
		}

		return result;
	}, [mapSelectionData, pendingMapSelectionData]);

	const selectionNodes = useMemo(() => {
		const nodes: React.ReactNode[] = [];

		for (const [selectionKey, tileState] of Object.entries(selectionData)) {
			const {
				xyData: { x, y, map, plane },
				selectState,
			} = tileState;
			const drawX = x * size;
			const drawY = y * size;

			const drawOverlay = (g: PIXI.Graphics) => {
				g.clear();
				let color = WHITE_COLOR;
				if (selectState === MapTileSelectState.ADD) {
					color = ADD_COLOR;
				} else if (selectState === MapTileSelectState.SUBTRACT) {
					color = SUBTRACT_COLOR;
				}
				g.beginFill(color, 0.2);
				g.drawRect(drawX, drawY, size, size);
				g.endFill();
			};

			const drawTop = selectionData[getMapPlaneXYKey(map, plane, x, y - 1)] == null;
			const drawRight = selectionData[getMapPlaneXYKey(map, plane, x + 1, y)] == null;
			const drawBottom = selectionData[getMapPlaneXYKey(map, plane, x, y + 1)] == null;
			const drawLeft = selectionData[getMapPlaneXYKey(map, plane, x - 1, y)] == null;

			const drawSelectionLines = (g: PIXI.Graphics) => {
				g.clear();

				// draw the lines
				const lineWidth = 4;
				let x1, x2, y1, y2;
				if (drawTop) {
					x1 = drawX;
					y1 = y2 = drawY;
					x2 = drawX + size;
					g.lineStyle(lineWidth, 0xffffff, 1, 1);
					drawDashedLine(g, x1, y1, x2, y2, DASH, GAP);
					g.endFill();
				}
				if (drawRight) {
					x1 = x2 = drawX + size;
					y1 = drawY;
					y2 = drawY + size;
					g.lineStyle(lineWidth, 0xffffff, 1, 1);
					drawDashedLine(g, x1, y1, x2, y2, DASH, GAP);
					g.endFill();
				}
				if (drawBottom) {
					x1 = drawX;
					y1 = y2 = drawY + size;
					x2 = drawX + size;
					g.lineStyle(lineWidth, 0xffffff, 1, 0);
					drawDashedLine(g, x1, y1, x2, y2, DASH, GAP);
					g.endFill();
				}
				if (drawLeft) {
					x1 = x2 = drawX;
					y1 = drawY;
					y2 = drawY + size;
					g.lineStyle(lineWidth, 0xffffff, 1, 0);
					drawDashedLine(g, x1, y1, x2, y2, DASH, GAP);
					g.endFill();
				}

				g.line.reset();

				// fill in corner gaps
				g.beginFill(0xffffff, 1);
				if (drawTop) {
					if (drawLeft) {
						x1 = drawX - lineWidth;
						y1 = drawY - lineWidth;
						g.drawRect(x1, y1, lineWidth, lineWidth);
					}
					if (drawRight) {
						x1 = drawX + size;
						y1 = drawY - lineWidth;
						g.drawRect(x1, y1, lineWidth, lineWidth);
					}
				}
				if (drawBottom) {
					if (drawLeft) {
						x1 = drawX - lineWidth;
						y1 = drawY + size;
						g.drawRect(x1, y1, lineWidth, lineWidth);
					}
					if (drawRight) {
						x1 = drawX + size;
						y1 = drawY + size;
						g.drawRect(x1, y1, lineWidth, lineWidth);
					}
				}
				g.endFill();
			};

			nodes.push(
				<React.Fragment key={selectionKey}>
					<Graphics draw={drawOverlay} zIndex={3000} />
					<Graphics draw={drawSelectionLines} zIndex={4000} />
				</React.Fragment>
			);
		}

		return nodes;
	}, [selectionData, size]);

	const highlight = useMemo(
		() => (
			<Graphics
				draw={(g) => {
					g.clear();
					if (!isMouseOverMapCanvas || mapPointerXCoord == null || mapPointerYCoord == null) {
						return;
					}
					g.beginFill(0x0, 0);
					g.lineStyle(HIGHLIGHT_WEIGHT / mapTileZoom, 0xffff00);
					g.drawRect(mapPointerXCoord * size, mapPointerYCoord * size, size, size);
					g.endFill();
				}}
				zIndex={5000}
			/>
		),
		[isMouseOverMapCanvas, mapPointerXCoord, mapPointerYCoord, mapTileZoom, size]
	);

	return (
		<>
			{selectionNodes}
			<PixiAlphaFilter alpha={0.7}>{highlight}</PixiAlphaFilter>
		</>
	);
};

export const MapSelectionPlane = connect(mapStateToProps)(React.memo(MapSelectionPlaneC));
