import * as React from "react";
import { connect } from "react-redux";
import { DeepReadonly, Dictionary } from "ts-essentials";

import { getMapPlaneXYData } from "../../../../store/redux/reducers/map";
import { pendingMapDataSelector } from "../../../../store/redux/selectors";
import { AppRootState, IPendingMapData } from "../../../../store/redux/types/state";
import { PendingMapTile } from "../PendingMapTile";

export interface IPendingMapDataPlaneProps {}

type MappedProps = DeepReadonly<{
	pendingData: Dictionary<IPendingMapData>;
}>;

interface IProps extends IPendingMapDataPlaneProps, MappedProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IPendingMapDataPlaneProps>): MappedProps => {
	return {
		pendingData: pendingMapDataSelector(state),
	};
};

const PendingMapDataPlaneFC: React.FC<IProps> = (props) => {
	const { pendingData } = props;

	const nodes = Object.keys(pendingData).map((k) => {
		const { key, ...rest } = getMapPlaneXYData(k, pendingData);
		return <PendingMapTile key={key} {...rest} />;
	});

	return <>{nodes}</>;
};

export const PendingMapDataPlane = connect(mapStateToProps)(React.memo(PendingMapDataPlaneFC));
