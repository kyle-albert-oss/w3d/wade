import * as React from "react";
import { useSelector } from "react-redux";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { createStyles, Theme } from "@material-ui/core";

import { MapTileData } from "../../../../models/map";
import { useActions } from "../../../../store/redux/actions";
import {
	mapDefTileLabelSizeSelector,
	selectedEditTileSelector,
	selectedMapTilesTuplesSelector,
	selectedPlaneNumSelector,
} from "../../../../store/redux/selectors";
import { MapTileSelectionChange } from "../../../../store/redux/types/ui";
import { MapTile } from "../MapTile";
import MapTileTooltip from "../MapTileTooltip";
import { FormMode, FormModeContext } from "../../context/form-mode";
import { AppRootState } from "../../../../store/redux/types/state";
import { ITileSelection } from "../../../../store/redux/types/map-ops";
import { makeAppStyles } from "../../core/material";
import { DraggableListItem, AppListItem } from "../../core/AppListItem";
import { useDragProps } from "../../hooks";

export interface IMapDefListItemProps {
	data: MapTileData[];
	index: number;
	style: any;
}

const useStyles = makeAppStyles((theme: Theme) =>
	createStyles({
		mapDefListItemDrag: {
			minWidth: "unset !important",
			width: 36,
		},
		mapDefListItem: {
			paddingBottom: 0,
			paddingLeft: 0,
			paddingTop: 0,
		},
		mapDefListItemImg: {
			minWidth: "auto",
			paddingRight: theme.spacing(2),
		},
	})
);

const MapDefListItemFC: React.FC<IMapDefListItemProps> = (props) => {
	const { data, index, style = {} } = props;
	const tile = data[index];

	const actions = useActions();
	const classes = useStyles();
	const { formMode } = React.useContext(FormModeContext);

	const selectedTiles = useSelector<AppRootState, Maybe<OneOrMany<ITileSelection>>>(
		formMode === FormMode.EDIT ? selectedEditTileSelector : selectedMapTilesTuplesSelector
	);
	const selectedPlane = useSelector(selectedPlaneNumSelector);
	const labelFontSize = useSelector(mapDefTileLabelSizeSelector);
	const { code, label, plane } = tile;
	const selected = React.useMemo(() => {
		if (selectedTiles == null) {
			return undefined;
		} else if (Array.isArray(selectedTiles)) {
			return selectedTiles.findIndex((t) => t.tileCode === code && t.plane === selectedPlane) !== -1;
		} else {
			return selectedTiles.tileCode === code && selectedTiles.plane === selectedPlane;
		}
	}, [selectedPlane, selectedTiles, code]);

	const onListItemClickCb = React.useCallback(
		(e: React.MouseEvent) => {
			if (e.button === 2) {
				e.preventDefault();
			}
			if (formMode === FormMode.EDIT) {
				actions.uiUpdate({ selectedEditTile: { plane: selectedPlane, tileCode: code } });
			} else {
				let button: MapTileSelectionChange["button"];
				if (e.button === 0) {
					button = "MOUSE1";
				} else if (e.button === 2) {
					button = "MOUSE2";
				} else {
					return;
				}

				actions.updateMapTileSelection({
					button,
					operation: "set",
					plane: selectedPlane,
					tileCode: code,
				});
			}
		},
		[actions, code, selectedPlane, formMode]
	);

	const displayLabel = formMode === FormMode.EDIT ? `${code} - ${label}` : label;
	const dragProps = useDragProps({
		dragDisabled: code === 0,
		dragId: code,
		dragIndex: index,
		move: (fromIndex, toIndex) => actions.moveActiveMapDefTile({ fromIndex, toIndex }, { plane }),
	});

	// noinspection JSSuspiciousNameCombination
	const iconStyle: React.CSSProperties = { minWidth: style.height };

	// secondary action rendered in div due to rendering issue
	const listItemContents = (
		<React.Fragment>
			<ListItemIcon className={classes.mapDefListItemImg} style={iconStyle}>
				<MapTile isolated plane={plane} renderMode="webgl" renderWhenBlank tileCode={code} />
			</ListItemIcon>
			<ListItemText primary={displayLabel} primaryTypographyProps={{ display: "block", noWrap: true, variant: "body2" }} />
		</React.Fragment>
	);

	if (formMode === FormMode.EDIT) {
		return (
			<DraggableListItem
				button
				className={classes.mapDefListItem}
				handleClassName={classes.mapDefListItemDrag}
				onClick={onListItemClickCb}
				onContextMenu={onListItemClickCb}
				selected={selected}
				style={style}
				{...dragProps}
			>
				{listItemContents}
			</DraggableListItem>
		);
	}

	return (
		<MapTileTooltip enterDelay={1500} placement="left" plane={plane} tileCode={code}>
			<AppListItem
				button
				className={classes.mapDefListItem}
				onClick={onListItemClickCb}
				onContextMenu={onListItemClickCb}
				selected={selected}
				style={style}
			>
				{listItemContents}
			</AppListItem>
		</MapTileTooltip>
	);
};

export const MapDefListItem = React.memo(MapDefListItemFC);
