import * as React from "react";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import Drawer from "@material-ui/core/Drawer";
import { DeepReadonly } from "ts-essentials";

import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { uiStateSelector } from "../../../../store/redux/selectors";
import { AppRootState } from "../../../../store/redux/types/state";

type MappedProps = DeepReadonly<{
	isDevConsoleOpen?: boolean;
}>;

interface IProps extends MappedProps, IActionProps {}

const mapStateToProps = (state: AppRootState): MappedProps => {
	return {
		isDevConsoleOpen: uiStateSelector(state).isDevConsoleOpen,
	};
};

class DevDrawer extends React.Component<IProps> {
	vswapCanvasRef: React.RefObject<HTMLCanvasElement>;
	imageRef: React.RefObject<HTMLImageElement>;

	constructor(props: Readonly<IProps>) {
		super(props);
		this.vswapCanvasRef = React.createRef();
		this.imageRef = React.createRef();
	}

	render() {
		const { actions, isDevConsoleOpen } = this.props;
		return (
			<Drawer anchor="bottom" open={isDevConsoleOpen} onClose={() => actions.uiToggle({ isDevConsoleOpen: false })}>
				<Button onClick={() => actions.updateTheme({ palette: { type: "light" } })}>Light Theme</Button>
				<Button
					onClick={() =>
						actions.updateTheme({
							palette: {
								type: "dark",
								primary: {
									main: "#b71c1c",
								},
								secondary: {
									main: "#AAAAAA",
								},
							},
						})
					}
				>
					Dark Theme
				</Button>
				<Button onClick={() => actions.uiToggle("useLegacyTileGraphics")}>Toggle Legacy Mode</Button>
			</Drawer>
		);
	}
}

export default connect(mapStateToProps, getDispatchActionMapper())(DevDrawer);
