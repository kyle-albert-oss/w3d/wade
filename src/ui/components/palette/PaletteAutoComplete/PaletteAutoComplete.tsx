import * as React from "react";
import { useSelector } from "react-redux";

import { IGamePaletteRef } from "../../../../models/palette";
import * as Strings from "../../../../util/strings";
import { AutoComplete, IAutoCompleteProps, AutoCompleteOptArray, AutoCompleteFilterOption } from "../../core/AutoComplete";
import { availablePaletteRefsArraySelector } from "../../../../store/redux/selectors";

export interface IGamePaletteAutoCompleteProps extends IAutoCompleteProps<IGamePaletteRef> {}

const PaletteAutoCompleteFC: React.FC<IGamePaletteAutoCompleteProps> = (props) => {
	const { templatesOnly, ...rest } = props;
	const palettes = useSelector(availablePaletteRefsArraySelector);

	const options = React.useMemo<AutoCompleteOptArray<IGamePaletteRef>>(
		() =>
			palettes.map((pal) => ({
				label: pal.name,
				value: pal,
			})),
		[palettes]
	);

	const filterOptionCb = React.useCallback((option: AutoCompleteFilterOption<IGamePaletteRef>, rawInput: string) => {
		const {
			label,
			data: { value: palette },
		} = option;
		return Strings.anyContainsIgnoreCase([label, ...(palette.extensions || [])], rawInput);
	}, []);

	return (
		<AutoComplete<IGamePaletteRef>
			filterOption={filterOptionCb}
			isClearable
			isSearchable
			label="Palette"
			options={options}
			{...rest}
			isMulti={false}
		/>
	);
};

export const PaletteAutoComplete = React.memo(PaletteAutoCompleteFC);
