import * as React from "react";
import { Grid } from "@material-ui/core";
import { DeepReadonly } from "ts-essentials";

import { RGBTuple } from "../../../../models/palette";

export interface IPaletteGridProps {
	colors?: DeepReadonly<RGBTuple[]>;
	maxColumns?: number;
	size?: number;
}

export class PaletteGrid extends React.PureComponent<IPaletteGridProps> {
	static readonly defaultProps: Partial<IPaletteGridProps> = {
		maxColumns: 16,
		size: 32,
	};

	render() {
		const { colors = [], maxColumns, size = PaletteGrid.defaultProps.size! } = this.props;
		const maxWidth = maxColumns ? size * maxColumns : undefined;
		return (
			<Grid container style={{ maxWidth: maxWidth ? `${maxWidth}px` : undefined }}>
				{colors.map(([r, g, b], i) => (
					<Grid key={i} item>
						<div
							style={{
								background: `rgb(${r},${g},${b})`,
								height: size,
								width: size,
							}}
						/>
					</Grid>
				))}
			</Grid>
		);
	}
}
