import * as React from "react";
import Grid, { GridProps } from "@material-ui/core/Grid";
import { StrictOmit } from "ts-essentials";
import clsx from "clsx";

import { makeAppStyles } from "../material";

export interface ISpaceBetweenProps extends StrictOmit<GridProps, "justify"> {
	autoHeight?: boolean;
}

const useSpaceBetweenStyles = makeAppStyles({
	autoHeight: {
		height: "auto !important",
	},
});

const SpaceBetweenFC: React.FC<ISpaceBetweenProps> = (props) => {
	const { autoHeight, className, children, ...rest } = props;
	const sbClasses = useSpaceBetweenStyles();
	return (
		<Grid container justify="space-between" wrap="nowrap" spacing={1} className={clsx(sbClasses.autoHeight, className)} {...rest}>
			{React.Children.map(children, (child, i) => (
				<React.Fragment>
					{child && (
						<Grid key={i} item>
							{child}
						</Grid>
					)}
				</React.Fragment>
			))}
		</Grid>
	);
};

export const SpaceBetween = React.memo(SpaceBetweenFC);
