import * as React from "react";
import { connect } from "react-redux";
import { DeepReadonly } from "ts-essentials";
import escapeRegExp from "lodash.escaperegexp";
import clsx from "clsx";
import { createStyles, ListItemIcon, ListItemText, Theme } from "@material-ui/core";

import { AppRootState } from "../../../../../store/redux/types/state";
import { getDispatchActionMapper } from "../../../../../store/redux/actions";
import { AppTreeItemData } from "../../../../../models/wade";
import { ISimpleTreeNode } from "../../../../../models/settings";
import { TreeContext } from "../shared/context";
import { AppListItem } from "../../AppListItem";
import { makeAppStyles } from "../../material";

export interface IAppTreeNodeProps {
	node: AppTreeItemData<ISimpleTreeNode>;
}

type MappedProps = DeepReadonly<{
	isDisplayingSearchResults?: boolean;
	isChildrenLoading?: boolean;
	searchQuery?: string;
}>;

interface IProps extends IAppTreeNodeProps, MappedProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IAppTreeNodeProps>): MappedProps => {
	const { node } = ownProps;
	return {
		searchQuery: undefined, // TODO
	};
};

const useNodeLiStyles = makeAppStyles((theme: Theme) =>
	createStyles({
		dense: {
			marginBottom: 0,
			marginTop: 0,
			paddingBottom: 0,
			paddingTop: 0,
		},
	})
);

const SimpleTreeNodeFC: React.FC<IProps> = (props) => {
	const { isDisplayingSearchResults, node, searchQuery } = props;
	const { item } = node;
	const { dense, onNodeClick, selectedNodes } = React.useContext(TreeContext);
	const selected = React.useMemo(() => selectedNodes && selectedNodes.find((n) => n === item) != null, [item, selectedNodes]);
	const onClickCb = React.useCallback(() => {
		if (onNodeClick) {
			onNodeClick(item);
		}
	}, [item, onNodeClick]);
	const liStyles = useNodeLiStyles();

	let icon: React.ReactNode;
	let nodeTextJSX: React.ReactNode;

	if (isDisplayingSearchResults && searchQuery && searchQuery.length > 0) {
		const { label: nodeLabel } = item;
		const tokens = nodeLabel.split(new RegExp(`(${escapeRegExp(searchQuery)})`, "i"));
		const nodeTextJSXArr = [] as React.ReactNode[];
		for (let i = 0; i < tokens.length; i++) {
			const token = tokens[i];
			const classes = clsx({
				highlighted: searchQuery.localeCompare(token, undefined, { sensitivity: "base" }) === 0, // TODO: highlight styling
			});
			nodeTextJSXArr.push(
				<span key={i} className={classes}>
					{token}
				</span>
			);
		}
		nodeTextJSX = nodeTextJSXArr;
	} else {
		nodeTextJSX = <span>{node.title ?? item.label}</span>;
	}

	return (
		<AppListItem button classes={{ dense: liStyles.dense }} dense={dense} onClick={onClickCb} selected={selected}>
			{icon && <ListItemIcon>{icon as React.ReactElement}</ListItemIcon>}
			<ListItemText classes={{ dense: liStyles.dense }} primary={nodeTextJSX} primaryTypographyProps={{ variant: "button" }} />
		</AppListItem>
	);
};

export const SimpleTreeNode = connect(mapStateToProps, getDispatchActionMapper())(SimpleTreeNodeFC);
