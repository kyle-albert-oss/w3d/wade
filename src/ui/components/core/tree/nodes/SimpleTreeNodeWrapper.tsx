import * as React from "react";
import { connect } from "react-redux";
import { NodeRendererProps } from "react-sortable-tree";
import { DeepReadonly } from "ts-essentials";
import { ArrowRight, ArrowDropDown } from "@material-ui/icons";
import clsx from "clsx";
import { Theme } from "@material-ui/core";

import { AppRootState } from "../../../../../store/redux/types/state";
import { IActionProps, getDispatchActionMapper } from "../../../../../store/redux/actions";
import { AppTreeItemData } from "../../../../../models/wade";
import { AppIconButton } from "../../AppIconButton";
import { useTreeStyles } from "../shared/styles";
import { IMaterialThemeProps } from "../../material";
import { ISimpleTreeNode } from "../../../../../models/settings";
import { TreeContext } from "../shared/context";

import { SimpleTreeNode } from "./SimpleTreeNode";

type MappedProps = DeepReadonly<{
	isDisplayingSearchResults?: boolean;
}>;

const mapStateToProps = (state: AppRootState): MappedProps => {
	return {
		isDisplayingSearchResults: false, // TODO
	};
};

interface IProps extends NodeRendererProps, MappedProps, IActionProps, IMaterialThemeProps {}

const isDescendant = (older: AppTreeItemData, younger: AppTreeItemData): boolean => {
	return (
		!!older.children &&
		typeof older.children !== "function" &&
		older.children.some((child) => child === younger || isDescendant(child as AppTreeItemData, younger))
	);
};

export const createTreeNodeWrapper = <T extends object = ISimpleTreeNode>(Node: React.FC<{ node: AppTreeItemData<T> }>) => {
	const SimpleTreeNodeWrapperFC: React.FC<IProps> = (props) => {
		const {
			actions,
			scaffoldBlockPxWidth,
			toggleChildrenVisibility,
			connectDragPreview,
			connectDragSource,
			isDragging,
			canDrop,
			canDrag,
			node: inputNode,
			title,
			subtitle,
			draggedNode,
			path,
			treeIndex,
			isSearchMatch,
			isSearchFocus,
			icons,
			buttons,
			className,
			style,
			didDrop,
			lowerSiblingCounts,
			listIndex,
			swapFrom,
			swapLength,
			swapDepth,
			treeId, // Not needed, but preserved for other renderers
			isOver, // Not needed, but preserved for other renderers
			parentNode, // Needed for dndManager
			rowDirection,
			isDisplayingSearchResults,
			theme,
			...otherProps
		} = props;

		const { dense } = React.useContext(TreeContext);
		const styles = useTreeStyles({
			rowPadding(thm: Theme): number | undefined {
				return dense ? 0 : undefined; // undef = default
			},
		});

		const node = inputNode as AppTreeItemData;
		const { expanded, item } = node;
		const isDraggedDescendant = draggedNode && isDescendant(draggedNode as AppTreeItemData, node);
		const isLandingPadActive = !didDrop && isDragging;
		const hasChildren = node.children != null && node.children.length > 0; // TODO:

		const onNodeExpandToggleClickCb = React.useCallback(() => {
			const { expanded: _expanded } = node;

			if (_expanded) {
				// TODO: set collapsed ( async loaded tree )
			} else {
				// TODO: set expanded ( async loaded tree )
			}

			// use this for non async tree
			toggleChildrenVisibility!({
				node,
				path,
				treeIndex,
			});
		}, [node, toggleChildrenVisibility, path, treeIndex]);

		const nodeContent = connectDragPreview(
			<div
				className={clsx(
					styles.row,
					{
						[styles.rowLandingPad]: isLandingPadActive,
						[styles.rowCancelPad]: isLandingPadActive && !canDrop,
					},
					className
				)}
				style={{
					opacity: isDraggedDescendant ? 0.5 : 1,
					...style,
				}}
			>
				<div
					className={clsx(styles.rowContents, {
						// [styles.rowContentsDragDisabled]: !canDrag,
					})}
				>
					<div className={styles.rowLabel}>
						<Node node={node} />
					</div>
				</div>
			</div>
		);

		return (
			<div className={styles.appTreeNode} {...otherProps}>
				{hasChildren ? (
					<div>
						<AppIconButton
							className={styles.expandToggleButton}
							onClick={onNodeExpandToggleClickCb}
							style={{ left: -0.5 * scaffoldBlockPxWidth }}
						>
							{expanded ? <ArrowDropDown /> : <ArrowRight />}
						</AppIconButton>
						{expanded && !isDragging && <div style={{ width: scaffoldBlockPxWidth }} className={styles.lineChildren} />}
					</div>
				) : (
					<div className={styles.appLeafNodeBranch} />
				)}
				<div className={styles.rowWrapper + (!canDrag ? ` ${styles.rowWrapperDragDisabled}` : "")}>
					{canDrag ? connectDragSource(nodeContent, { dropEffect: "copy" }) : nodeContent}
				</div>
			</div>
		);
	};

	return SimpleTreeNodeWrapperFC;
};

export const SimpleTreeNodeWrapper = connect(mapStateToProps, getDispatchActionMapper())(createTreeNodeWrapper(SimpleTreeNode));
