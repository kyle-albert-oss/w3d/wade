//@ts-nocheck
import { createStyles, Theme } from "@material-ui/core";
import { CSSProperties } from "@material-ui/styles";

import { makeAppStyles } from "../../material";

const rowLandingBorderWidth = "2px";

export interface ITreeStylesProps {
	rowPadding?(theme: Theme): number | undefined;
}

export const useTreeStyles = makeAppStyles<Theme, ITreeStylesProps>((theme: Theme) => {
	const rowLandingPad: CSSProperties = {
		border: "none !important",
		boxShadow: "none !important",
		outline: "none !important",
		"& *": {
			opacity: "0 !important",
		},
		"&:before": {
			backgroundColor: theme.palette.secondary.main,
			border: `${rowLandingBorderWidth} dashed ${theme.palette.text.secondary}`,
			content: `""`,
			position: "absolute",
			top: 0,
			right: 0,
			bottom: 0,
			left: 0,
			zIndex: -1,
		},
	};
	const rowItemTemplate: CSSProperties = {
		display: "inline-block",
		verticalAlign: "middle",
	};
	const moveHandle = {
		...rowItemTemplate,
		background:
			'#d9d9d9 url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI0MiIgaGVpZ2h0PSI0MiI+PGcgc3Ryb2tlPSIjRkZGIiBzdHJva2Utd2lkdGg9IjIuOSIgPjxwYXRoIGQ9Ik0xNCAxNS43aDE0LjQiLz48cGF0aCBkPSJNMTQgMjEuNGgxNC40Ii8+PHBhdGggZD0iTTE0IDI3LjFoMTQuNCIvPjwvZz4KPC9zdmc+") no-repeat center',
		height: "100%",
		width: "44px",
		border: "solid #aaa 1px",
		boxShadow: "0 2px 2px -2px",
		cursor: "move",
		borderRadius: "1px",
		zIndex: 1,
	};

	return createStyles({
		appTreeNode: {
			display: "flex",
			flexWrap: "nowrap",
			height: "100%",
			position: "relative",
		},
		appLeafNodeBranch: {
			// "&:before": {
			// 	backgroundColor: theme.palette.divider,
			// 	content: `""`,
			// 	height: 1,
			// 	left: -18,
			// 	position: "absolute",
			// 	top: "50%",
			// 	width: 12,
			// },
		},
		expandToggleButton: {
			cursor: "pointer",
			padding: "0 !important",
			position: "absolute",
			top: "calc(50% - 12px)",
			transform: "translate3d(-50%, 0, 0)",
			zIndex: 2,
		},
		rowWrapper: (props) => {
			const rowPadding = `${props?.rowPadding(theme) ?? theme.spacing(1)}px`;
			return {
				boxSizing: "border-box",
				cursor: "move",
				height: "100%",
				padding: `${rowPadding} ${rowPadding} ${rowPadding} 0`,
				width: "100%",
			};
		},
		rowWrapperDragDisabled: {
			cursor: "default",
			"&:hover": {
				opacity: 1,
			},
		},
		row: {
			display: "flex",
			height: "100%",
			whiteSpace: "nowrap",
			width: "100%",
			"& > *": {
				boxSizing: "border-box",
			},
		},
		rowLandingPad,
		rowCancelPad: {
			...rowLandingPad,
			"&:before": {
				backgroundColor: theme.palette.error.main,
			},
		},
		rowContents: {
			...rowItemTemplate,
			position: "relative",
			height: "100%",
			width: "100%",
			padding: "0 6px 0 6px",
			borderRadius: "2px",
			flex: "1 0 auto",
			display: "flex",
			alignItems: "center",
			justifyContent: "space-between",
		},
		rowLabel: {
			...rowItemTemplate,
			flex: "1 0 auto",
		},
		rowToolbar: {
			flex: "0 1 auto",
			display: "flex",
		},
		moveHandle,
		loadingHandle: {
			...moveHandle,
			cursor: "default",
			background: "#d9d9d9",
		},
		lineChildren: {
			height: "100%",
			display: "inline-block",
			position: "absolute",
			"&:after": (props) => ({
				content: `""`,
				position: "absolute",
				backgroundColor: theme.palette.divider,
				width: "1px",
				left: "50%",
				bottom: 0,
				height: `${props?.rowPadding(theme) ?? theme.spacing(1)}px`,
			}),
		},
	});
});
