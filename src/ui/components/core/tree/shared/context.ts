import * as React from "react";

import { ISimpleTreeNode } from "../../../../../models/settings";

interface ITreeContext {
	dense?: boolean;
	onNodeClick?(node: ISimpleTreeNode): void;
	selectedNodes?: ISimpleTreeNode[];
}

export const TreeContext = React.createContext<ITreeContext>({});
