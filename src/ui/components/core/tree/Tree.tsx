import * as React from "react";
import { connect } from "react-redux";
import { DeepReadonly } from "ts-essentials";
import { createStyles, Theme } from "@material-ui/core";
import RSTTree, { PlaceholderRendererProps, ReactSortableTreeProps, ThemeProps } from "react-sortable-tree";

import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { AppRootState } from "../../../../store/redux/types/state";
import { ISimpleTreeNode } from "../../../../models/settings";
import { makeAppStyles } from "../material";

import { SimpleTreeNodeWrapper } from "./nodes";
import { TreeContext } from "./shared/context";

export interface ITreeProps extends ReactSortableTreeProps {
	dense?: boolean;
	onNodeClick?(node: ISimpleTreeNode): void;
	selectedNodes?: DeepReadonly<ISimpleTreeNode>[];
}

type MappedProps = DeepReadonly<{}>;

interface IProps extends ITreeProps, MappedProps, IActionProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<ITreeProps>): MappedProps => {
	return {};
};

const useTreeRootStyles = makeAppStyles<Theme, ThemeProps>((theme: Theme) =>
	createStyles({
		tree: ({ scaffoldBlockPxWidth }) => ({
			"& .rst__lineBlock": {
				"&::before": {
					backgroundColor: theme.palette.divider,
				},
				"&::after": {
					backgroundColor: theme.palette.divider,
				},
			},
			"& .rst__nodeContent": {
				width: `calc(100% - ${scaffoldBlockPxWidth}px)`,
			},
		}),
	})
);

const Placeholder: React.FC<PlaceholderRendererProps> = () => <></>;

const TreeFC: React.FC<IProps> = (props) => {
	const { dense, onNodeClick, selectedNodes, theme } = props;

	const treeTheme: ThemeProps = {
		nodeContentRenderer: SimpleTreeNodeWrapper,
		placeholderRenderer: Placeholder,
		rowHeight: dense ? 24 : 48,
		scaffoldBlockPxWidth: dense ? 24 : 36,
		...theme,
		style: {
			overflow: "auto",
			...theme?.style,
		},
	};

	const treeStyles = useTreeRootStyles(treeTheme);

	const treeCtx = React.useMemo(() => {
		return {
			dense,
			onNodeClick,
			selectedNodes,
		};
	}, [dense, onNodeClick, selectedNodes]);

	return (
		<div>
			<TreeContext.Provider value={treeCtx}>
				<RSTTree canDrag={false} className={treeStyles.tree} isVirtualized={false} theme={treeTheme} {...props} />
			</TreeContext.Provider>
		</div>
	);
};

export const Tree = connect(mapStateToProps, getDispatchActionMapper())(React.memo(TreeFC));
