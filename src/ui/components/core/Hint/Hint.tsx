import * as React from "react";
import { InfoOutlined } from "@material-ui/icons";

import { AppTooltip, IAppTooltipProps } from "../AppTooltip";

export interface IHintProps extends Omit<IAppTooltipProps, "children" | "title"> {
	children: NonNullable<React.ReactNode>;
}

const HintFC: React.FC<IHintProps> = (props) => {
	const { children, ...tooltipProps } = props;
	return (
		<AppTooltip title={children} {...tooltipProps}>
			<InfoOutlined className="app-display-block" />
		</AppTooltip>
	);
};

export const Hint = React.memo(HintFC);
