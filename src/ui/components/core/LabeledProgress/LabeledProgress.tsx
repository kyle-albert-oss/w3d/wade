import * as React from "react";
import { CircularProgress } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import LinearProgress from "@material-ui/core/LinearProgress";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import ErrorIcon from "@material-ui/icons/Error";
import WarningIcon from "@material-ui/icons/Warning";

import { withMaterial, MaterialProps } from "../material";

export interface ILabeledProgressProps {
	label: React.ReactElement;
	progress: "failure" | "indeterminate" | "success" | "warning" | number;
}

interface IProps extends ILabeledProgressProps, MaterialProps {}

const LabeledProgressFC: React.FC<IProps> = (props) => {
	const { classes, label, progress } = props;

	let indicatorNode: React.ReactNode;

	if (typeof progress === "number") {
		indicatorNode = <LinearProgress value={progress} variant="determinate" />;
	} else if (progress === "indeterminate") {
		indicatorNode = <CircularProgress disableShrink size={36} />;
	} else if (progress === "success") {
		indicatorNode = <CheckCircleIcon className={classes.successIcon} fontSize="large" />;
	} else if (progress === "failure") {
		indicatorNode = <ErrorIcon className={classes.errorIcon} fontSize="large" />;
	} else if (progress === "warning") {
		indicatorNode = <WarningIcon className={classes.warningIcon} fontSize="large" />;
	}

	return (
		<Grid container justify="space-between" alignItems="center">
			<Grid item>{label}</Grid>
			<Grid item style={{ height: 35 }}>
				{indicatorNode}
			</Grid>
		</Grid>
	);
};

export const LabeledProgress = withMaterial(React.memo(LabeledProgressFC));
