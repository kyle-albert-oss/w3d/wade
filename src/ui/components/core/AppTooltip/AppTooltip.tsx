import * as React from "react";
import { createStyles, Theme } from "@material-ui/core/styles";
import Tooltip, { TooltipProps } from "@material-ui/core/Tooltip";
import tsDefaults from "ts-defaults";
import clsx from "clsx";

import { makeAppStyles } from "../material";

const useTooltipStyles = makeAppStyles((theme: Theme) =>
	createStyles<string, IAppTooltipProps>({
		tooltip: (props) => ({
			position: "relative",
			padding: props.pad ? undefined : 0,
			fontSize: ".8rem",
			backgroundColor: "rgb(97,97,97)",
			boxShadow: theme.shadows[20],
			"& ul": {
				paddingInlineStart: "20px",
			},
		}),
	})
);

export interface IAppTooltipProps extends TooltipProps {
	disabled?: boolean;
	pad?: boolean;
	wrap?: keyof JSX.IntrinsicElements;
}

export const AppTooltipFC: React.FC<IAppTooltipProps> = (_props) => {
	const props = tsDefaults(_props, {
		arrow: true,
		pad: true,
	});
	const { children, disabled, pad, wrap: Wrap, ...rest } = props;
	const classes = useTooltipStyles(props);

	return (
		<Tooltip classes={classes} {...rest}>
			{Wrap ? <Wrap className={clsx({ "app-disabled-cursor": disabled })}>{children}</Wrap> : children}
		</Tooltip>
	);
};

export const AppTooltip = React.memo(AppTooltipFC);
