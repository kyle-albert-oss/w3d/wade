import * as React from "react";
import { Grid } from "@material-ui/core";
import tsDefaults from "ts-defaults";

export interface ICenteredProps extends React.PropsWithChildren<{}> {
	className?: string;
	height?: number | string;
	width?: number | string;
}

const CenteredFC: React.FC<ICenteredProps> = (_props) => {
	const props = tsDefaults(
		_props,
		{
			height: "100%",
			width: "100%",
		},
		["height", "width"]
	);
	const { children, className, height, width } = props;

	return (
		<Grid container alignItems="center" className={className} justify="center" style={{ height, width }}>
			<Grid item>{children}</Grid>
		</Grid>
	);
};
export const Centered = React.memo(CenteredFC);
