import * as React from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Slider from "@material-ui/core/Slider";
import Typography from "@material-ui/core/Typography";
import ZoomIn from "@material-ui/icons/ZoomIn";
import ZoomOut from "@material-ui/icons/ZoomOut";

import AppSliderTooltipBottom from "../AppSliderTooltip";
import { MaterialProps, withMaterial } from "../material";

interface IProps extends MaterialProps {
	max?: number;
	min?: number;
	step?: number;
	value?: number;
	onZoomChange?(value: number): void;
}

interface IState {
	value: number;
}

class ZoomerBase extends React.PureComponent<IProps, IState> {
	static defaultProps: Partial<IProps> = {
		max: 2,
		min: 0.05,
		step: 0.05,
		value: 1,
	};

	constructor(props: Readonly<IProps>) {
		super(props);
		this.state = {
			value: props.value!,
		};
	}

	onZoomInClick = () =>
		this.setState(
			(prevState) => ({ value: prevState.value + this.props.step! }),
			() => this.props.onZoomChange && this.props.onZoomChange(this.state.value)
		);
	onZoomOutClick = () =>
		this.setState(
			(prevState) => ({ value: prevState.value - this.props.step! }),
			() => this.props.onZoomChange && this.props.onZoomChange(this.state.value)
		);
	onZoomResetClick = () => this.setState({ value: 1 }, () => this.props.onZoomChange && this.props.onZoomChange(this.state.value));
	onZoomSliderChange = (event: React.ChangeEvent<{}>, value: number | number[]) =>
		this.setState({ value: value as number }, () => this.props.onZoomChange && this.props.onZoomChange(this.state.value));

	componentWillReceiveProps(nextProps: Readonly<IProps>, nextContext: any): void {
		if (nextProps.value !== this.props.value) {
			this.setState({ value: nextProps.value! });
		}
	}

	render() {
		const { classes, max, min, step } = this.props;
		const { value } = this.state;

		return (
			<div className={classes.sliderMapZoomContainer}>
				<Slider
					marks
					min={min}
					max={max}
					onChange={this.onZoomSliderChange}
					step={step}
					ValueLabelComponent={AppSliderTooltipBottom}
					value={value}
				/>
				<Grid container alignItems="center">
					<Grid item>
						<Button variant="outlined" onClick={this.onZoomResetClick}>
							100%
						</Button>
					</Grid>
					<Grid item xs>
						<Typography align="center">{Math.round(value * 100)}%</Typography>
					</Grid>
					<Grid item>
						<IconButton color="inherit" size="small" onClick={this.onZoomOutClick} disabled={value <= min!}>
							<ZoomOut />
						</IconButton>
					</Grid>
					<Grid item>
						<IconButton color="inherit" size="small" onClick={this.onZoomInClick} disabled={value >= max!}>
							<ZoomIn />
						</IconButton>
					</Grid>
				</Grid>
			</div>
		);
	}
}

export const Zoomer = withMaterial(ZoomerBase);
