import * as React from "react";
import { Box } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import { MenuItemProps } from "@material-ui/core/MenuItem";
import SvgIcon, { SvgIconProps } from "@material-ui/core/SvgIcon";
import tsDefaults from "ts-defaults";

import AppSliderTooltipBottom from "../AppSliderTooltip";
import { ZoomIcon } from "../icons/ZoomIcon";
import { MaterialProps, withMaterial } from "../material";
import { AppSlider } from "../AppSlider";

interface ISimpleZoomerProps {
	max?: number;
	min?: number;
	step?: number;
	value?: number;
	onZoomChange?(value: number): void;
}

interface IProps extends ISimpleZoomerProps, MaterialProps {}

const ZoomOptionsIcon = React.memo<SvgIconProps>((props) => (
	<SvgIcon {...props}>
		<path
			d="M15.5,14h-0.8l-0.3-0.3c1.2-1.4,1.8-3.3,1.5-5.3c-0.5-2.8-2.8-5-5.6-5.3c-4.2-0.5-7.8,3-7.3,7.3c0.3,2.8,2.6,5.1,5.3,5.6
				c2,0.3,3.9-0.3,5.3-1.5l0.3,0.3v0.8l5,5l1.5-1.5L15.5,14z M10.2,13.9c-3,0.5-5.6-2.1-5.2-5.2c0.3-1.9,1.8-3.4,3.7-3.7
				c3-0.5,5.6,2.1,5.2,5.2C13.7,12.1,12.1,13.7,10.2,13.9z M18.6,1.5l-3,3.9h5.9L18.6,1.5z"
		/>
	</SvgIcon>
));

const SimpleZoomerFC: React.FC<IProps> = (_props) => {
	const props = tsDefaults(
		_props,
		{
			max: 2,
			min: 0.05,
			step: 0.05,
			value: 1,
		},
		["max", "min", "step", "value"]
	);
	const { children: inputChildren, max, min, onZoomChange, step, value } = props;

	const onMenuCloseCb = React.useCallback(() => setMenuAnchorEl(undefined), []);
	const children = React.useMemo(() => {
		return React.Children.toArray((inputChildren || []) as Array<React.ReactElement<MenuItemProps>>)
			.filter((c) => c != null)
			.map((c) => {
				const menuItem = c as React.ReactElement<MenuItemProps>;
				const { props: itemProps } = menuItem;
				return React.cloneElement(menuItem!, {
					...itemProps,
					onClick: (e: React.MouseEvent<HTMLLIElement>) => {
						setMenuAnchorEl(undefined);
						if (itemProps.onClick) {
							itemProps.onClick(e);
						}
					},
				});
			});
	}, [inputChildren]);
	const [menuAnchorEl, setMenuAnchorEl] = React.useState<Element | undefined>(undefined);

	const onZoomOptionsClickCb = React.useCallback((e: React.MouseEvent) => setMenuAnchorEl(e.currentTarget), []);
	const onZoomSliderChangeCb = React.useCallback(
		(e: unknown, newValue: number | number[]) => {
			if (onZoomChange) {
				onZoomChange(newValue as number);
			}
		},
		[onZoomChange]
	);

	const hasMenuOptions = children && children.length > 0;

	return (
		<>
			<Grid container alignItems="center" spacing={2}>
				<Grid item>
					{hasMenuOptions ? (
						<IconButton color="inherit" size="small" onClick={hasMenuOptions ? onZoomOptionsClickCb : undefined}>
							<ZoomOptionsIcon />
						</IconButton>
					) : (
						<ZoomIcon />
					)}
				</Grid>
				<Grid item xs style={{ minWidth: "200px" }}>
					<Box pl={0.5} pr={2.25}>
						<AppSlider
							min={min}
							max={max}
							onChange={onZoomSliderChangeCb}
							step={step}
							ValueLabelComponent={AppSliderTooltipBottom}
							value={value}
						/>
					</Box>
				</Grid>
			</Grid>
			{hasMenuOptions && (
				<Menu
					anchorEl={menuAnchorEl}
					anchorOrigin={{
						horizontal: "center",
						vertical: "top",
					}}
					getContentAnchorEl={null}
					open={!!menuAnchorEl}
					onClose={onMenuCloseCb}
					transformOrigin={{
						horizontal: "center",
						vertical: "bottom",
					}}
				>
					{children}
				</Menu>
			)}
		</>
	);
};

export const SimpleZoomer = withMaterial(SimpleZoomerFC);
