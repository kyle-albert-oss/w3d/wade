import * as React from "react";
import { Box, BoxProps } from "@material-ui/core";

export interface ITabContentProps extends Omit<BoxProps, "display" | "role"> {
	selectedTab: number | null | undefined;
	tabIndex: number;
}

const TabContentFC: React.FC<ITabContentProps> = (props) => {
	const { children, selectedTab, tabIndex, ...boxProps } = props;
	if (selectedTab !== tabIndex) {
		return <></>;
	}
	return (
		<Box {...boxProps} role="tabpanel">
			{children}
		</Box>
	);
};

export const TabContent = React.memo(TabContentFC);
