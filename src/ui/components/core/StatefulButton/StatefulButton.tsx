import * as React from "react";
import { connect } from "react-redux";
import { ButtonProps } from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import clsx from "clsx";
import { DeepReadonly } from "ts-essentials";

import { MaybeConnectedProps } from "../../../../store/redux/actions";
import { uiStateSelector } from "../../../../store/redux/selectors";
import { AppRootState, IUiBaseState, UiAsyncState, UiAsyncStateType } from "../../../../store/redux/types/state";
import { KeysOfExactType } from "../../../../util/objects";
import { calcDisabledWithProps, FormDisableContext } from "../../context/form-disable";
import { AppButton } from "../AppButton";
import { MaterialProps, withMaterial } from "../material";

interface IStatefulButtonSharedProps extends Omit<ButtonProps, "classes"> {
	hideSpinner?: boolean;
}

export interface IStatefulButtonProps extends IStatefulButtonSharedProps {
	state?: UiAsyncState;
}

interface IProps extends IStatefulButtonProps, MaterialProps {}

const StatefulButtonFC: React.FC<IProps> = (props) => {
	const { actions, children, classes, dispatch, className, hideSpinner, state, theme, uiStateKey, ...rest } = props as MaybeConnectedProps<
		IProps,
		IConnectedStatefulButtonProps
	>;
	const isLoading = state === UiAsyncState.LOADING;
	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props) || isLoading;

	const buttonClassname = clsx(
		{
			[classes.buttonSuccess]: state === UiAsyncState.SUCCESS,
		},
		className
	);

	const button = (
		<AppButton {...rest} className={buttonClassname} disabled={disabled}>
			{children}
		</AppButton>
	);

	if (hideSpinner) {
		return button;
	}

	return (
		<div className="app-position-relative app-display-inline-block">
			{button}
			{isLoading && <CircularProgress className={classes.buttonProgress} size={24} thickness={6} />}
		</div>
	);
};

export const StatefulButton = withMaterial(React.memo(StatefulButtonFC));

// CONNECTED

export interface IConnectedStatefulButtonProps extends IStatefulButtonSharedProps {
	uiStateKey: KeysOfExactType<IUiBaseState, UiAsyncStateType>;
}

type MappedProps = DeepReadonly<{
	state: UiAsyncState | undefined;
}>;

export const ConnectedStatefulButton = connect(
	(state: AppRootState, ownProps: DeepReadonly<IConnectedStatefulButtonProps>): MappedProps => ({
		state: uiStateSelector(state)[ownProps.uiStateKey],
	})
)(StatefulButton);
