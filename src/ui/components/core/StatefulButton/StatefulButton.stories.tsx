import * as React from "react";
import { Grid } from "@material-ui/core";
import { boolean, select } from "@storybook/addon-knobs";

import { createStory } from "../../../../../stories/story-util";
import { UiAsyncState, UiAsyncStateType } from "../../../../store/redux/types/state";

import { StatefulButton } from "./StatefulButton";

const DefaultStatefulButton: React.FC = () => {
	const [asyncState, setAsyncState] = React.useState<UiAsyncStateType>();
	const onCreateClickCb = React.useCallback(() => {
		setAsyncState(UiAsyncState.LOADING);
		setTimeout(() => {
			setAsyncState(undefined);
		}, 2000);
	}, []);

	return (
		<Grid container spacing={2}>
			<Grid item>
				<StatefulButton hideSpinner state={asyncState} variant="outlined">
					Cancel
				</StatefulButton>
			</Grid>
			<Grid item>
				<StatefulButton color="primary" onClick={onCreateClickCb} state={asyncState} variant="contained">
					Create
				</StatefulButton>
			</Grid>
		</Grid>
	);
};

createStory("Core|StatefulButton", module, { style: { padding: 20 } })
	.add("Default", () => <DefaultStatefulButton />)
	.add("Knob Controlled", () => (
		<StatefulButton
			color="primary"
			hideSpinner={boolean("Hide Spinner", false)}
			// @ts-expect-error
			state={select("State", [undefined, UiAsyncState.LOADING, UiAsyncState.SUCCESS, UiAsyncState.ERROR], undefined) || undefined}
			variant="contained"
		>
			Create
		</StatefulButton>
	));
