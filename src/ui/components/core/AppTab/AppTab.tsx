import * as React from "react";
import Tab, { TabProps } from "@material-ui/core/Tab";

import { calcDisabledWithProps, FormDisableContext } from "../../context/form-disable";

export interface IAppTabProps extends TabProps {
	dense?: boolean;
}

export const AppTab: React.FC<IAppTabProps> = (props) => {
	const { dense, style, ...rest } = props;
	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);
	const styles = dense ? { minHeight: 0, ...style } : style;

	return <Tab {...rest} disabled={disabled} style={styles} />;
};
