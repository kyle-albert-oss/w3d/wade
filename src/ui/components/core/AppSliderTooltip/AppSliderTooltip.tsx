import * as React from "react";
import { ValueLabelProps } from "@material-ui/core/Slider";
import Tooltip from "@material-ui/core/Tooltip";
import PopperJs from "popper.js";

interface IAppSliderToolBottomProps extends ValueLabelProps {
	format?(value: number): string;
}

const AppSliderTooltipBottom: React.FC<IAppSliderToolBottomProps> = ({ children, ...props }) => {
	const { format, open, value } = props;
	const popperRef = React.useRef<PopperJs | null>(null);
	React.useEffect(() => {
		if (popperRef.current) {
			popperRef.current.update();
		}
	});

	return (
		<Tooltip
			enterTouchDelay={0}
			open={open}
			placement="bottom"
			PopperProps={{
				popperRef,
			}}
			title={format ? format(value) : `${Math.round(value * 100)}%`}
		>
			{children}
		</Tooltip>
	);
};

export default AppSliderTooltipBottom;
