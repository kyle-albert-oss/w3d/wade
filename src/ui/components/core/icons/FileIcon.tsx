import * as React from "react";
import { SvgIcon } from "@material-ui/core";

import { ReactComponent as FileIconSvg } from "./svg/file.svg";

export const FileIcon = () => <SvgIcon component={(svgProps) => <FileIconSvg {...svgProps} />} />;
