import * as React from "react";
import { SvgIcon } from "@material-ui/core";

import { ReactComponent as ZoomIconSvg } from "./svg/magnify.svg";

export const ZoomIcon = () => <SvgIcon component={(svgProps) => <ZoomIconSvg {...svgProps} />} />;
