import * as React from "react";
import { SvgIcon } from "@material-ui/core";

import { ReactComponent as WallIconSvg } from "./svg/wall.svg";

export const WallIcon = () => <SvgIcon component={(svgProps) => <WallIconSvg {...svgProps} />} />;
