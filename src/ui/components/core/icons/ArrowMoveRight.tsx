import * as React from "react";
import { SvgIcon } from "@material-ui/core";

import { ReactComponent as ArrowMoveRightSvg } from "./svg/arrow-expand-right.svg";

export const ArrowMoveRight = () => <SvgIcon component={(svgProps) => <ArrowMoveRightSvg {...svgProps} />} />;
