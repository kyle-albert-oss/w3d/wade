import * as React from "react";
import { SvgIcon } from "@material-ui/core";

import { ReactComponent as ArrowMoveLeftSvg } from "./svg/arrow-expand-left.svg";

export const ArrowMoveLeft = () => <SvgIcon component={(svgProps) => <ArrowMoveLeftSvg {...svgProps} />} />;
