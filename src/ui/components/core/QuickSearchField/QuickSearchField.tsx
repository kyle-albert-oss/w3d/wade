import * as React from "react";
import IconButton from "@material-ui/core/IconButton";
import { InputProps } from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import { TextFieldProps } from "@material-ui/core/TextField";
import ClearIcon from "@material-ui/icons/Clear";
import SearchIcon from "@material-ui/icons/Search";

import { AppTextField } from "../AppTextField";

export type IQuickSearchFieldProps = TextFieldProps & {
	onClearClick?(): void;
};

export const QuickSearchField: React.FC<IQuickSearchFieldProps> = (props) => {
	const { onClearClick, value, ...rest } = props;

	const inputProps: Partial<InputProps> = {
		startAdornment: (
			<InputAdornment position="start">
				<SearchIcon />
			</InputAdornment>
		),
	};
	if (onClearClick && value && value !== "") {
		inputProps.endAdornment = (
			<InputAdornment position="end">
				<IconButton edge="end" aria-label="clear" onClick={onClearClick}>
					<ClearIcon />
				</IconButton>
			</InputAdornment>
		);
	}
	Object.assign(inputProps, rest.inputProps || {});

	return <AppTextField fullWidth InputProps={inputProps} placeholder="Filter..." value={value} {...rest} />;
};
