import * as React from "react";
import { boolean, array, color } from "@storybook/addon-knobs";

import { createStory } from "../../../../../stories/story-util";

import { ColorPicker, ColorPickerField } from "./ColorPicker";

const StandardStory = () => {
	const colorKnob = color("Color", "#000000");
	const [clr, setColor] = React.useState(colorKnob);
	React.useEffect(() => {
		setColor(colorKnob);
	}, [colorKnob]);
	return (
		<ColorPicker
			color={clr}
			disableAlpha={!boolean("Enable Alpha", false)}
			onChange={(newColor) => setColor(newColor.hex)}
			presetColors={array("Preset Colors", ["#FF0000"])}
		/>
	);
};
const FieldStory = () => {
	const colorKnob = color("Color", "#000000");
	const [clr, setColor] = React.useState(colorKnob);
	React.useEffect(() => {
		setColor(colorKnob);
	}, [colorKnob]);
	return (
		<ColorPickerField
			color={clr}
			onChange={(newColor) => {
				setColor(newColor.hex.toUpperCase());
			}}
		/>
	);
};

createStory("Core|ColorPicker", module)
	.add("Standard", () => <StandardStory />)
	.add("As Field", () => <FieldStory />);
