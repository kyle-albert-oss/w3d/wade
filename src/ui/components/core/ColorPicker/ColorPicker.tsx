import chroma from "chroma-js";
import * as React from "react";
import { Color, ColorResult, CustomPicker, CustomPickerProps, InjectedColorProps } from "react-color";
import tsDefaults from "ts-defaults";
import { Box, Chip, Grid, InputAdornment, Popover, useTheme } from "@material-ui/core";
import { Alpha, Hue, Saturation } from "react-color/lib/components/common";
import { Clear, Opacity } from "@material-ui/icons";

import { AppIconButton } from "../AppIconButton";
import { AppTextField } from "../AppTextField";
import { toNumber } from "../../../../util/numbers";
import { useAppStyles, useChipStyles } from "../material";
import { AppChip } from "../AppChip";

export interface IColorPickerProps extends CustomPickerProps<HTMLElement> {
	disableAlpha?: boolean;
	onAfterPresetSelected?(): void;
	presetColors?: ReadonlyArray<string>;
}

const Pointer: React.FC = (props) => {
	const theme = useTheme();
	const barHeight = 32;
	const extra = theme.spacing(0.5);
	return (
		<div
			style={{
				backgroundColor: theme.palette.common.white,
				border: `1px solid ${theme.palette.divider}`,
				cursor: "ew-resize",
				height: barHeight + extra * 2,
				position: "relative",
				width: extra,
				top: -extra,
				bottom: -extra,
			}}
		/>
	);
};

interface IColorPresetsProps {
	colors: readonly (string | null)[];
	onClick?(color: string | null, index: number): void;
	onRightClick?(color: string | null, index: number): void;
}

const ColorPresetsFC: React.FC<IColorPresetsProps> = (props) => {
	const { colors, onClick, onRightClick } = props;
	const chipClasses = useChipStyles();
	const theme = useTheme();

	const onClickCb = (e: React.MouseEvent<HTMLElement>) => {
		const isRightClick = e.buttons === 2;
		if (isRightClick) {
			e.preventDefault();
		}
		const clickedColor = e.currentTarget.getAttribute("data-color") || null;
		const idx = toNumber(e.currentTarget.getAttribute("data-index"));
		if (isRightClick) {
			if (onRightClick) {
				onRightClick(clickedColor, idx);
			}
		} else if (onClick) {
			onClick(clickedColor, idx);
		}
	};

	return (
		<Grid container spacing={1}>
			{colors.map((c, i) => {
				const styles: React.CSSProperties = { width: 24, height: 24 };
				if (c) {
					styles.backgroundColor = c;
					styles.border = `2px solid ${chroma(c).brighten(0.125).hex()}`;
				} else {
					styles.border = `3px dashed ${theme.palette.divider}`;
				}

				return (
					<Grid key={`${c}-${i}`} item>
						<Chip
							className={chipClasses.square}
							onContextMenu={onRightClick ? onClickCb : undefined}
							onClick={onClickCb}
							style={styles}
							data-color={c}
							data-index={i}
						/>
					</Grid>
				);
			})}
		</Grid>
	);
};
export const ColorPresets = React.memo(ColorPresetsFC);

const ColorPickerFC: React.FC<IColorPickerProps & InjectedColorProps> = (_props) => {
	const props = tsDefaults(
		_props,
		{
			disableAlpha: true,
		},
		["disableAlpha"]
	);
	const { disableAlpha, onAfterPresetSelected, onChange, presetColors, ref, ...rest } = props;

	const chipClasses = useChipStyles();

	const onChangeCb = React.useCallback(
		(color: Color | ColorResult) => {
			if (onChange) {
				onChange(color);
			}
		},
		[onChange]
	);
	const setColorFromPresetCb = React.useCallback(
		(e: React.MouseEvent<HTMLElement>) => {
			onChangeCb(e.currentTarget.getAttribute("data-color")!);
			if (onAfterPresetSelected) {
				onAfterPresetSelected();
			}
		},
		[onAfterPresetSelected, onChangeCb]
	);

	return (
		<Grid container wrap="nowrap">
			<Grid container item direction="column" spacing={2}>
				<Grid item>
					<Box style={{ position: "relative", height: 100 }}>
						<Saturation {...rest} onChange={onChangeCb} />
					</Box>
				</Grid>
				<Grid container item>
					<Grid item xs>
						<Grid container direction="column">
							<Grid item>
								<Box style={{ position: "relative", height: 32 }}>
									<Hue {...rest} onChange={onChangeCb} pointer={Pointer} />
								</Box>
							</Grid>
							{!disableAlpha && (
								<Grid item>
									<Box style={{ position: "relative", height: 32 }}>
										<Alpha {...rest} onChange={onChangeCb} pointer={Pointer} />
									</Box>
								</Grid>
							)}
						</Grid>
					</Grid>
				</Grid>
				{presetColors && presetColors.length > 0 && (
					<Grid item>
						<Grid container spacing={1}>
							{presetColors.map((c, i) => (
								<Grid key={`${c}-${i}`} item>
									<Chip
										className={chipClasses.square}
										data-color={c}
										onClick={setColorFromPresetCb}
										style={{ backgroundColor: c, height: 24, width: 24 }}
									/>
								</Grid>
							))}
						</Grid>
					</Grid>
				)}
			</Grid>
		</Grid>
	);
};

export const ColorPicker = CustomPicker(React.memo(ColorPickerFC));

export interface IColorPickerFieldProps extends IColorPickerProps {
	className?: string;
	disabled?: boolean;
	label?: string;
	placeholder?: string;
	readOnly?: boolean;
	style?: React.CSSProperties;
}

export const ColorPickerFieldFC: React.FC<IColorPickerFieldProps & InjectedColorProps> = (props) => {
	const { color, className, disabled, hex, label, onChange, placeholder, readOnly, ref, style, ...rest } = props;

	const classes = useAppStyles();
	const theme = useTheme();
	const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);
	const [liveValue, setLiveValue] = React.useState(hex);

	React.useEffect(() => {
		const upperHex = hex?.toUpperCase() ?? "";
		setLiveValue(upperHex === "TRANSPARENT" ? "" : upperHex);
	}, [hex]);
	const onFieldChangeCb = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
		setLiveValue(e.currentTarget.value ?? "");
	}, []);
	const onChangeCb = React.useCallback(
		(newColor: Color | ColorResult) => {
			if (onChange) {
				onChange(newColor);
			}
		},
		[onChange]
	);
	const onColorPickClickCb = React.useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
		setAnchorEl(event.currentTarget);
	}, []);
	const onCloseCb = React.useCallback(() => {
		setAnchorEl(null);
	}, []);
	const updateValueCb = React.useCallback(() => {
		if (!liveValue) {
			onChange("transparent");
			return;
		}
		if (chroma.valid(liveValue)) {
			const newHex = chroma(liveValue).hex().toUpperCase();
			onChange(newHex);
			setLiveValue(newHex); // effect doesn't get called if the color string changes but is effectively the same color
		} else {
			const upperHex = hex?.toUpperCase() ?? "";
			setLiveValue(upperHex === "TRANSPARENT" ? "" : upperHex);
		}
	}, [hex, liveValue, onChange]);
	const onFocusCb = React.useCallback((e: React.FocusEvent<HTMLInputElement>) => {
		e.currentTarget.select();
	}, []);
	const onBlurCb = React.useCallback(
		(event: React.FocusEvent) => {
			updateValueCb();
		},
		[updateValueCb]
	);
	const onKeyPressCb = React.useCallback(
		(event: React.KeyboardEvent) => {
			if (event.key === "Enter") {
				updateValueCb();
			}
		},
		[updateValueCb]
	);
	const onClearClickCb = React.useCallback(() => {
		onChange("transparent");
	}, [onChange]);
	const open = !!anchorEl;

	return (
		<div className={className} style={{ position: "relative", ...style }}>
			<AppTextField
				disabled={disabled}
				InputProps={{
					endAdornment: (
						<InputAdornment position="end">
							<AppIconButton
								disabled={disabled || readOnly}
								onClick={onClearClickCb}
								style={{ padding: 2, visibility: liveValue ? "visible" : "hidden" }}
							>
								<Clear />
							</AppIconButton>
							<AppIconButton disabled={disabled || readOnly} onClick={onColorPickClickCb}>
								<Opacity />
							</AppIconButton>
						</InputAdornment>
					),
					startAdornment: (
						<InputAdornment position="start">
							<AppChip square style={{ backgroundColor: hex, border: `1px solid ${theme.palette.divider}`, height: 16, width: 16 }} />
						</InputAdornment>
					),
				}}
				label={label}
				onBlur={onBlurCb}
				onChange={onFieldChangeCb}
				onFocus={onFocusCb}
				onKeyPress={onKeyPressCb}
				placeholder={placeholder}
				readOnly={readOnly}
				value={liveValue ?? ""}
			/>
			<Popover
				anchorEl={anchorEl}
				anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
				open={open}
				onClose={onCloseCb}
				transformOrigin={{ horizontal: "right", vertical: "top" }}
			>
				<Box p={2} style={{ width: 200 }}>
					<ColorPickerFC onAfterPresetSelected={onCloseCb} onChange={onChangeCb} {...rest} />
				</Box>
			</Popover>
		</div>
	);
};

export const ColorPickerField = CustomPicker(React.memo(ColorPickerFieldFC));
