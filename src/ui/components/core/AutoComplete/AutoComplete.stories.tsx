import * as React from "react";
import { boolean, text, select } from "@storybook/addon-knobs";
import { Grid } from "@material-ui/core";

import { createStory } from "../../../../../stories/story-util";
import { AppTextField } from "../AppTextField";

import { AutoComplete, AutoCompleteNullableOptArray, AutoCompleteNullableOpt, StringAutoComplete } from "./AutoComplete";

const DefaultStory = () => {
	const [value, setValue] = React.useState<string | undefined>("Value 1");
	return (
		<AutoComplete
			isClearable={boolean("Clearable", true)}
			isDisabled={boolean("Disabled", false)}
			isMulti={boolean("Multi", false)}
			isSearchable={boolean("Searchable", true)}
			label={text("Label", "Items")}
			onChange={(newVal: AutoCompleteNullableOpt<string>) => setValue(newVal?.value)}
			options={[
				{ label: "Value 1 Label", value: "Value 1" },
				{ label: "Value 2 Label", value: "Value 2" },
				{ label: "Value 3 Label", value: "Value 3" },
				{ label: "Value 4 Label", value: "Value 4" },
			]}
			value={value}
		/>
	);
};

const TextFieldStory = () => {
	const size = select("Size", [undefined, "small", "medium"] as ("small" | "medium")[], undefined);
	const [value, setValue] = React.useState<string[] | undefined>(["Value 2", "Value 3"]);
	return (
		<Grid container spacing={1}>
			<Grid item xs={6}>
				<AppTextField fullWidth label="Text Field" size={size} />
			</Grid>
			<Grid item xs={6}>
				<AutoComplete
					fullWidth
					isClearable={boolean("Clearable", true)}
					isDisabled={boolean("Disabled", false)}
					isMulti
					isSearchable={boolean("Searchable", true)}
					label={text("Label", "Items")}
					onChange={(newVals: AutoCompleteNullableOptArray<string>) => setValue(newVals?.map((o) => o.value))}
					options={[
						{ label: "Value 1 Label", value: "Value 1" },
						{ label: "Value 2 Label", value: "Value 2" },
						{ label: "Value 3 Label", value: "Value 3" },
						{ label: "Value 4 Label", value: "Value 4" },
					]}
					TextFieldProps={{
						size,
					}}
					value={value}
				/>
			</Grid>
		</Grid>
	);
};

const StringStory = () => {
	const [value, setValue] = React.useState<readonly string[] | null | undefined>();
	return (
		<StringAutoComplete
			creatable
			isClearable={boolean("Clearable", true)}
			isDisabled={boolean("Disabled", false)}
			isMulti={boolean("Multi", true)}
			isOptionDisabled={(o) => o === "Value 2 Label"}
			isSearchable={boolean("Searchable", true)}
			label={text("Label", "Items")}
			onChange={(newVals: AutoCompleteNullableOptArray<string>) => {
				console.log(newVals);
				setValue(newVals?.map((o) => o.value));
			}}
			options={["Value 1 Label", "Value 2 Label", "Value 3 Label", "Value 4 Label"]}
			value={value}
		/>
	);
};

createStory("Core|AutoComplete", module, { style: { padding: 20 } })
	.add("Default", () => <DefaultStory />)
	.add("With TextField Comparison", () => <TextFieldStory />)
	.add("Strings only", () => <StringStory />);
