import * as React from "react";
import Select, { ActionMeta, ValueType } from "react-select";
import CreatableSelect from "react-select/creatable";
import { ValueContainerProps } from "react-select/src/components/containers";
import { ControlProps } from "react-select/src/components/Control";
import { IndicatorProps } from "react-select/src/components/indicators";
import { MenuProps, NoticeProps } from "react-select/src/components/Menu";
import { MultiValueProps } from "react-select/src/components/MultiValue";
import { OptionProps } from "react-select/src/components/Option";
import { PlaceholderProps } from "react-select/src/components/Placeholder";
import { SingleValueProps } from "react-select/src/components/SingleValue";
import { Props as SelectProps } from "react-select/src/Select";
import { StylesConfig } from "react-select/src/styles";
import MenuItem from "@material-ui/core/MenuItem";
import { createStyles, emphasize, Theme } from "@material-ui/core/styles";
import useTheme from "@material-ui/core/styles/useTheme";
import { BaseTextFieldProps } from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import CancelIcon from "@material-ui/icons/Cancel";
import ClearIcon from "@material-ui/icons/Clear";
import clsx from "clsx";
import { DeepReadonly, StrictOmit, Dictionary } from "ts-essentials";
import { EnumValues } from "enum-values";
import { ArrowDropDown } from "@material-ui/icons";
import tsDefaults from "ts-defaults";
import _ from "lodash";
import { useFormContext } from "react-hook-form";
import { GroupedOptionsType, GroupType, OptionsType } from "react-select/src/types";
import { Option as FilterOption } from "react-select/src/filters";

import { calcDisabled, FormDisableContext } from "../../context/form-disable";
import { AppIconButton } from "../AppIconButton";
import { AppPaper } from "../AppPaper";
import { AppTextField } from "../AppTextField";
import { makeAppStyles } from "../material";
import { AppChip } from "../AppChip";
import { arrayToRODictionary } from "../../../../util/objects";

export interface IOption<V = string> {
	isDisabled?: boolean;
	isNew?: boolean;
	label: string;
	value: V;
}

export type AutoCompleteOpt<T> = DeepReadonly<IOption<T>>;
export type AutoCompleteOptArray<T> = ReadonlyArray<AutoCompleteOpt<T>>;
export type AutoCompleteValueType<T = string> = ValueType<AutoCompleteOpt<T>>;
export type AutoCompleteNullableOptArray<T> = AutoCompleteOptArray<T> | null | undefined;
export type AutoCompleteNullableOpt<T> = AutoCompleteOpt<T> | null | undefined;
export type AutoCompleteFilterOption<T> = StrictOmit<FilterOption, "data"> & { data: AutoCompleteOpt<T> };
export type AutoCompleteGroups<T> = Array<GroupType<AutoCompleteOpt<T>> & { label?: string }>;
export type AutoCompleteOptionsProp<T> = AutoCompleteOptArray<T> | AutoCompleteGroups<T>;

const isSmall = (theme: Theme, props: IAutoCompleteProps<any>): boolean =>
	(props.TextFieldProps?.size ?? theme.props?.MuiTextField?.size) === "small";

const useAutocompleteStyles = makeAppStyles<Theme, IAutoCompleteProps<any>>((theme) =>
	createStyles({
		root: {
			flexGrow: 1,
			height: 250,
			minWidth: 290,
		},
		indicator: (props) => {
			const small = isSmall(theme, props);
			return {
				padding: small ? 6 : undefined,
				marginLeft: small ? 4 : undefined,
				marginRight: small ? 4 : undefined,
			};
		},
		input: (props) => ({
			display: "flex",
			padding: 0,
			height: "auto",
			minHeight: isSmall(theme, props) ? theme.spacing(2) : theme.spacing(5),
		}),
		valueContainer: (props) => {
			const small = isSmall(theme, props);
			return {
				display: "flex",
				flexWrap: "wrap",
				flex: 1,
				alignItems: "center",
				overflow: "hidden",
				position: "relative",
				marginLeft: theme.spacing(1.75),
				paddingTop: small ? 7 : 8,
				paddingBottom: small ? 6 : 8,
				minHeight: small ? theme.spacing(2) : theme.spacing(5),
			};
		},
		chip: (props) => ({
			margin: isSmall(theme, props) ? theme.spacing(0.125, 0.125) : theme.spacing(0.25, 0.25),
		}),
		chipSquare: {
			borderRadius: theme.spacing(0.5),
		},
		chipFocused: {
			backgroundColor: emphasize(theme.palette.type === "light" ? theme.palette.grey[300] : theme.palette.grey[700], 0.08),
		},
		noOptionsMessage: {
			padding: theme.spacing(1, 2),
		},
		singleValue: {
			fontSize: 16,
		},
		placeholder: {
			position: "absolute",
			left: 2,
			fontSize: 16,
		},
		paper: {
			position: "absolute",
			zIndex: 2,
			marginTop: theme.spacing(1),
			left: 0,
			right: 0,
			border: `1px solid ${theme.palette.divider}`,
			boxShadow: theme.shadows[20],
		},
		divider: {
			height: theme.spacing(2),
		},
	})
);

const NoOptionsMessage = <T extends object = IOption>(props: NoticeProps<T>) => {
	const { innerProps, children, selectProps } = props;
	return (
		<Typography color="textSecondary" className={selectProps.classes.noOptionsMessage} {...innerProps}>
			{children}
		</Typography>
	);
};

type InputComponentProps = Pick<BaseTextFieldProps, "inputRef"> & React.HTMLAttributes<HTMLDivElement>;

const inputComponent: React.FC<InputComponentProps> = ({ inputRef, ...props }) => <div ref={inputRef} {...props} />;

const Control = <T extends object = IOption>(props: ControlProps<T>) => {
	const {
		children,
		innerProps,
		innerRef,
		isDisabled,
		selectProps: { classes, TextFieldProps, label },
	} = props;

	const adaptedTextFieldProps = { ...TextFieldProps };
	if (label) {
		adaptedTextFieldProps.label = label;
	}

	return (
		<AppTextField
			disabled={isDisabled}
			fullWidth
			InputProps={{
				inputComponent,
				inputProps: {
					className: classes.input,
					ref: innerRef,
					children,
					...innerProps,
				},
			}}
			{...adaptedTextFieldProps}
		/>
	);
};

const ClearIndicator = <T extends object = IOption>(props: IndicatorProps<T>) => {
	const {
		children = <ClearIcon />,
		innerProps: { ref, ...restInnerProps },
		selectProps: { classes },
	} = props;
	return (
		<AppIconButton {...restInnerProps} className={classes.indicator} ref={ref}>
			{children}
		</AppIconButton>
	);
};

const DropdownIndicator = <T extends object = IOption>(props: IndicatorProps<T>) => {
	const {
		children = <ArrowDropDown />,
		innerProps: { ref, ...restInnerProps },
		selectProps: { classes },
	} = props;
	return (
		<AppIconButton {...restInnerProps} className={classes.indicator} ref={ref}>
			{children}
		</AppIconButton>
	);
};

const Option = <T extends object = IOption>(props: OptionProps<T>) => {
	const { children, data, innerProps, innerRef, isDisabled, isSelected, selectProps } = props;
	const { isNew } = data;
	const { entityLabel, isDisabled: selectIsDisabled } = selectProps;
	return (
		<MenuItem
			component="div"
			disabled={isDisabled || selectIsDisabled}
			ref={innerRef}
			selected={isSelected}
			style={{
				fontWeight: isSelected ? 500 : 400,
			}}
			{...innerProps}
		>
			{isNew ? (
				<strong>
					Create {entityLabel ? `new ${entityLabel} ` : null}"{children}"...
				</strong>
			) : (
				children
			)}
		</MenuItem>
	);
};

type MuiPlaceholderProps<T = IOption> = Omit<PlaceholderProps<T>, "innerProps"> & Partial<Pick<PlaceholderProps<T>, "innerProps">>;

const Placeholder = <T extends object = IOption>(props: MuiPlaceholderProps<T>) => {
	const { selectProps, innerProps = {}, children } = props;
	return (
		<Typography color="textSecondary" className={selectProps.classes.placeholder} {...innerProps}>
			{children}
		</Typography>
	);
};

const SingleValue = <T extends object = IOption>(props: SingleValueProps<T>) => (
	<Typography className={props.selectProps.classes.singleValue} {...props.innerProps}>
		{props.children}
	</Typography>
);

const ValueContainer = <T extends object = IOption>(props: ValueContainerProps<T>) => (
	<div className={props.selectProps.classes.valueContainer}>{props.children}</div>
);

const MultiValue = <T extends object = IOption>(props: MultiValueProps<T>) => (
	<AppChip
		{...props.innerProps}
		tabIndex={-1}
		label={props.children}
		className={clsx(props.selectProps.classes.chip, {
			[props.selectProps.classes.chipFocused]: props.isFocused,
		})}
		onDelete={props.removeProps.onClick}
		deleteIcon={<CancelIcon {...props.removeProps} />}
		size="small"
		square
	/>
);

const Menu = <T extends object = IOption>(props: MenuProps<T>) => (
	<AppPaper square className={props.selectProps.classes.paper} {...props.innerProps}>
		{props.children}
	</AppPaper>
);

export const MaterialAutoCompleteComponents: SelectProps<any>["components"] = {
	ClearIndicator,
	Control,
	DropdownIndicator,
	Menu,
	MultiValue,
	NoOptionsMessage,
	Option,
	Placeholder,
	SingleValue,
	ValueContainer,
} as const;

export const MACC = MaterialAutoCompleteComponents;

export interface IAutoCompleteProps<T = string> extends StrictOmit<SelectProps<IOption<T>>, "getOptionLabel" | "getOptionValue" | "value"> {
	creatable?: boolean;
	entityLabel?: string;
	entityPluralLabel?: string;
	getValueId?(value: T): string;
	getValueLabel?(value: T): string;
	fullWidth?: boolean;
	label?: string;
	required?: boolean;
	value?: DeepReadonly<T> | ReadonlyArray<DeepReadonly<T>> | null | undefined;
}

type OptionsProp<T> = GroupedOptionsType<T> | OptionsType<T>;
const isGrouped = <T extends object>(arg: OptionsProp<T>): arg is GroupedOptionsType<T> =>
	Array.isArray(arg) && arg[0] && "options" in arg[0];

type BaseOptionObject = {
	id?: string;
	label?: string;
	name?: string;
	value?: {
		name?: string;
		label?: string;
	};
};
export const AutoComplete = <T extends BaseOptionObject | string = string>(_props: IAutoCompleteProps<T>) => {
	const props = tsDefaults(_props, {
		createOptionPosition: "first",
		...(_props.creatable
			? {
					noOptionsMessage: () => {
						const singular = _props.entityLabel ?? "option";
						const plural = _props.entityPluralLabel ?? `${singular}s`;
						return `No ${plural}. Type to create a new ${singular}.`;
					},
			  }
			: undefined),
	});
	const {
		components: propComponents,
		creatable,
		fullWidth,
		getValueId,
		getValueLabel,
		isDisabled,
		onChange,
		options,
		required,
		value,
		...rest
	} = props;
	const opts = options as OptionsProp<AutoCompleteOpt<T>>;

	const valueLabelCb = React.useCallback(
		(val: T): string => {
			if (getValueLabel) {
				return getValueLabel(val);
			}
			if (val == null) {
				return "None";
			} else if (typeof val !== "object") {
				return `${val}`;
			}
			const objVal = val as BaseOptionObject;
			return `${objVal.label || objVal.name || objVal.value?.label || objVal.value?.name || "🐛"}`;
		},
		[getValueLabel]
	);
	const optionLabelCb = React.useCallback((option: AutoCompleteOpt<T>): string => option.label || valueLabelCb(option.value as T), [
		valueLabelCb,
	]);

	const valueIdCb = React.useCallback(
		(val: T): string => {
			if (getValueId) {
				return getValueId(val);
			}
			if (val == null) {
				return "none";
			} else if (typeof val !== "object") {
				return `${val}`;
			}
			const objVal = val as BaseOptionObject;
			return `${objVal.id || "🐛"}`;
		},
		[getValueId]
	);
	const optionValueCb = React.useCallback((option: AutoCompleteOpt<T>): string => valueIdCb(option.value as T), [valueIdCb]);

	const optsById = React.useMemo<Dictionary<AutoCompleteOpt<T>>>(() => {
		let allOpts: AutoCompleteNullableOptArray<T>;
		if (isGrouped(opts)) {
			allOpts = opts.flatMap((o) => o.options);
		} else {
			allOpts = opts;
		}
		return arrayToRODictionary(allOpts, (o) => optionValueCb(o));
	}, [opts, optionValueCb]);

	const realValue = React.useMemo<AutoCompleteValueType<T>>(() => {
		const toOpt = (val: T, valId: string): AutoCompleteOpt<T> | null =>
			optsById[valId] ?? (creatable ? { isNew: true, label: valueLabelCb(val), value: val } : null);
		if (value == null) {
			return null; // workaround bug with undefined: https://github.com/JedWatson/react-select/issues/3066
		} else if (Array.isArray(value)) {
			return (value as readonly T[]).map((v) => toOpt(v, valueIdCb(v))).filter((o): o is AutoCompleteOpt<T> => o != null);
		}
		return toOpt(value as T, valueIdCb(value as T));
	}, [creatable, optsById, value, valueIdCb, valueLabelCb]);

	const selectComponents: IAutoCompleteProps<T>["components"] = {
		...MaterialAutoCompleteComponents,
		...propComponents,
	};

	const formContext = useFormContext();
	const theme = useTheme();
	const classes = useAutocompleteStyles(props);
	const disabled = calcDisabled(React.useContext(FormDisableContext), isDisabled || props.disabled, props.id);
	const formRegister = formContext?.register;
	const formUnregister = formContext?.unregister;
	const formSetValue = formContext?.setValue;

	React.useEffect(() => {
		if (formRegister && props.name) {
			formRegister({ name: props.name, required: required ? { value: true, message: "This field is required." } : false });
		}
		return () => {
			if (formUnregister && props.name) {
				formUnregister(props.name);
			}
		};
	}, [props.name, required]);

	React.useEffect(() => {
		if (formSetValue && props.name) {
			let newValue;
			if (Array.isArray(realValue)) {
				newValue = realValue.map((o) => o.value);
			} else {
				newValue = realValue?.value;
			}
			formSetValue(props.name, newValue, {
				shouldDirty: true,
				shouldValidate: true,
			});
		}
	}, [props.name, rest.isMulti, realValue]);

	const onChangeCb = React.useCallback(
		(newValue: ValueType<DeepReadonly<IOption<T>>>, meta: ActionMeta<any>) => {
			if (!onChange) {
				return;
			} else if (value === undefined && newValue == null) {
				// workaround bug with undefined: https://github.com/JedWatson/react-select/issues/3066
				onChange(undefined, meta);
			} else {
				onChange(newValue, meta);
			}
		},
		[onChange, value]
	);

	const className = clsx({
		"app-width-full": fullWidth,
	});
	const indicatorColor = theme.palette.type === "light" ? theme.palette.grey[900] : theme.palette.grey[200];
	const hoverColor = emphasize(indicatorColor, 0.08);
	const selectStyles: StylesConfig = {
		clearIndicator: (base: React.CSSProperties, state: any) => ({
			...base,
			color: state.isFocused ? hoverColor : indicatorColor,
			cursor: "pointer",
			"&:hover": { color: hoverColor },
		}),
		dropdownIndicator: (base: React.CSSProperties, state: any) => ({
			...base,
			color: state.isFocused ? hoverColor : indicatorColor,
			cursor: "pointer",
			"&:hover": { color: hoverColor },
		}),
		indicatorSeparator: (base: React.CSSProperties) => ({ ...base, color: indicatorColor, opacity: 0.33 }),
		input: (base: React.CSSProperties) => ({
			...base,
			color: theme.palette.text.primary,
			"& input": {
				font: "inherit",
			},
		}),
		menuPortal: (base: React.CSSProperties) => ({ ...base, zIndex: 2 }),
	};

	const SelectComp = creatable ? CreatableSelect : Select;

	return (
		// @ts-expect-error
		<SelectComp
			classes={classes}
			className={className}
			components={selectComponents}
			isDisabled={disabled}
			getOptionLabel={optionLabelCb}
			getOptionValue={optionValueCb}
			onChange={onChangeCb}
			options={options}
			menuPosition="fixed"
			styles={selectStyles}
			value={realValue}
			{...rest}
		/>
	);
};

export type EnumNameValuePair<E> = { name: string; value: E };
export interface IEnumAutoCompleteProps<E> extends StrictOmit<IAutoCompleteProps<E>, "defaultValue" | "options" | "value"> {
	defaultValue?: OneOrMany<E>;
	filter?(e: E): boolean;
	isOptionDisabled?(e: E): boolean;
	labelProvider?(data: EnumNameValuePair<E>, disabled?: boolean): string;
	options: any; // enum class
	sort?(e1: E, e2: E): number;
	value?: OneOrMany<E>;
}

export interface IHOCEnumAutoCompleteProps<E> extends Omit<IEnumAutoCompleteProps<E>, "options"> {}

export const EnumAutoComplete = <E extends any>(_props: IEnumAutoCompleteProps<E>) => {
	const props = tsDefaults(_props, {
		labelProvider(data: EnumNameValuePair<E>) {
			const str = typeof data.value === "string" ? data.value : data.name;
			return _.upperFirst(_.camelCase(str));
		},
	});
	const { children, defaultValue, filter, isOptionDisabled, labelProvider, options, sort, value, ...rest } = props;
	const { isMulti } = rest;
	const realOptions = React.useMemo(() => {
		let pairs = EnumValues.getNamesAndValues<any>(options);
		if (filter) {
			pairs = pairs.filter((p) => filter(p.value));
		}
		if (sort) {
			pairs = pairs.sort((p1, p2) => sort(p1.value, p2.value));
		}
		return pairs.map((p) => {
			const isDisabled = isOptionDisabled?.(p.value) ?? false;
			return { isDisabled, label: labelProvider?.(p, isDisabled) ?? p.value, value: p.value };
		});
	}, [filter, isOptionDisabled, labelProvider, options, sort]);
	const defaultVal = React.useMemo(() => {
		if (Array.isArray(defaultValue)) {
			return realOptions.filter((o) => defaultValue.includes(o.value));
		} else if (defaultValue) {
			const opt = realOptions.find((o) => defaultValue === o.value);
			return opt ? (isMulti ? [opt] : opt) : null;
		}
		return null;
	}, [defaultValue, isMulti, realOptions]);

	return <AutoComplete {...rest} defaultValue={defaultVal} options={realOptions} value={value as DeepReadonly<E>} />;
};

export interface IStringAutoCompleteProps extends StrictOmit<IAutoCompleteProps<string>, "defaultValue" | "options" | "value"> {
	defaultValue?: string | readonly string[];
	isOptionDisabled?(opt: string): boolean;
	options?: string[];
	value?: string | readonly string[] | null | undefined;
}

export const StringAutoComplete: React.FC<IStringAutoCompleteProps> = (_props) => {
	const props = tsDefaults(_props, {
		getNewOptionData: (inputValue: string) => ({ label: inputValue, value: inputValue, isNew: true }),
	});
	const { defaultValue, isOptionDisabled, options, value, ...rest } = props;
	const realOptions = React.useMemo(() => {
		return (options ?? []).map((o) => {
			const isDisabled = isOptionDisabled?.(o) ?? false;
			return { isDisabled, label: o, value: o };
		});
	}, [isOptionDisabled, options]);
	const defaultVal = React.useMemo(() => {
		if (Array.isArray(defaultValue)) {
			return defaultValue?.map((dv) => ({ label: dv, value: dv })) ?? null;
		}
		return defaultValue ?? null;
	}, [defaultValue]);

	return <AutoComplete<string> {...rest} defaultValue={defaultVal} options={realOptions} value={value} />;
};
