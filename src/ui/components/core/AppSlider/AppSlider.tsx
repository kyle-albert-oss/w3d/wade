import * as React from "react";
import { Slider, SliderProps, createStyles, Theme } from "@material-ui/core";
import { StrictOmit } from "ts-essentials";

import { makeAppStyles } from "../material";

interface IAppSliderProps extends StrictOmit<SliderProps, "onChange"> {
	onChange?(event: React.ChangeEvent<{}>, value: number | number[]): void; // TODO remove when material ui fixes this https://github.com/mui-org/material-ui/issues/20191
	valueLabelPosition?: "top" | "bottom";
}

const useSliderStyles = makeAppStyles<Theme, IAppSliderProps>((theme: Theme) =>
	createStyles({
		mark: {
			height: 6,
			marginTop: -1,
			width: 1,
		},
		rail: {
			height: 4,
			borderRadius: 2,
		},
		root: {
			height: 4,
		},
		thumb: {
			marginTop: -4,
		},
		track: {
			height: 4,
			borderRadius: 2,
		},
	})
);

export const AppSlider = React.forwardRef<HTMLSpanElement, IAppSliderProps>((props, ref) => {
	const { valueLabelPosition, ...sliderProps } = props;
	const sliderClasses = useSliderStyles(props);
	const classes = React.useMemo<SliderProps["classes"]>(
		() => ({
			...(sliderClasses as SliderProps["classes"]),
			...sliderProps.classes,
		}),
		[sliderClasses, sliderProps.classes]
	);

	return <Slider {...sliderProps} classes={classes} ref={ref} />;
});
