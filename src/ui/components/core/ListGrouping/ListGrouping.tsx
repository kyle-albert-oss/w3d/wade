import * as React from "react";
import { ListSubheader } from "@material-ui/core";

import { MaterialProps, withMaterial } from "../material";

export interface IListGroupingProps {
	subheader?: React.ReactNode;
}

interface IProps extends IListGroupingProps, MaterialProps {}

const ListGroupingFC: React.FC<IProps> = (props) => {
	const { children, classes, subheader } = props;
	return (
		<li>
			<ul style={{ padding: 0, listStyleType: "none" }}>
				{subheader ? (
					<ListSubheader className={classes.listSubheader} disableGutters>
						{subheader}
					</ListSubheader>
				) : null}
				{children}
			</ul>
		</li>
	);
};

export const ListGrouping = withMaterial(React.memo(ListGroupingFC));
