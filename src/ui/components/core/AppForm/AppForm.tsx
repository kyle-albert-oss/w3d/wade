import * as React from "react";
import { FormProvider, useForm, UseFormOptions, SubmitHandler } from "react-hook-form";
import { useHistory } from "react-router";
import { FieldValues } from "@hookform/error-message/dist/types";

import { typedMemo } from "../../../../util/ui";

export interface IAppFormProps<T extends FieldValues> extends React.PropsWithChildren<{}> {
	formId: string;
	formOpts?: UseFormOptions<T>;
	navBlocking?: boolean;
	onSubmit: SubmitHandler<T>;
}

const AppFormFC = <T extends FieldValues>(props: IAppFormProps<T>) => {
	const { children, formId, formOpts, navBlocking, onSubmit } = props;

	const [navBlockUnregFn, setNavBlockUnregFn] = React.useState<() => any>(() => undefined);
	const formApi = useForm<T>(formOpts);
	const { formState } = formApi;
	const { isDirty, isSubmitting, isValid } = formState;
	const { block: blockNav } = useHistory();

	React.useEffect(() => {
		if (navBlocking && (!isValid || isDirty || isSubmitting)) {
			if (navBlockUnregFn) {
				navBlockUnregFn();
			}
			let msg: string;
			let unregBlockFn: Function | undefined;
			if (!isValid) {
				msg = "There are errors on this page. Fix and try again.";
				unregBlockFn = blockNav(() => {
					formApi.trigger();
					return msg;
				});
			} else if (isSubmitting) {
				msg = "Changes are being saved. Wait and try again.";
			} else {
				// dirty
				msg = "You have unsaved changes. Save them and then try again.";
			}
			if (!unregBlockFn) {
				unregBlockFn = blockNav(msg);
			}
			setNavBlockUnregFn(() => unregBlockFn);
			return () => {
				if (unregBlockFn) {
					unregBlockFn();
				}
			};
		} else if (navBlockUnregFn) {
			navBlockUnregFn();
			setNavBlockUnregFn(() => undefined);
		}
	}, [isDirty, isValid, isSubmitting, navBlocking]);

	return (
		<FormProvider {...formApi}>
			<form id={formId} onSubmit={formApi.handleSubmit(onSubmit)}>
				{children}
			</form>
		</FormProvider>
	);
};

export const AppForm = typedMemo(AppFormFC);
