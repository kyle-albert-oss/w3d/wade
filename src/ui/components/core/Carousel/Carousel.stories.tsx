import * as React from "react";
import { boolean } from "@storybook/addon-knobs";

import { createStory } from "../../../../../stories/story-util";

import { Carousel } from "./Carousel";

createStory("Core|Carousel", module, { style: { padding: 20 } }).add("Default", () => (
	<Carousel centerMode={boolean("Center Mode", false)}>
		<div style={{ width: 120, height: 200, backgroundColor: "red" }}>1</div>
		<div style={{ width: 150, height: 175, backgroundColor: "orange" }}>2</div>
		<div style={{ width: 100, height: 250, backgroundColor: "green" }}>3</div>
		<div style={{ width: 90, height: 200, backgroundColor: "blue" }}>4</div>
		<div style={{ width: 120, height: 120, backgroundColor: "purple" }}>5</div>
		<div style={{ width: 120, height: 120, backgroundColor: "pink" }}>6</div>
	</Carousel>
));
