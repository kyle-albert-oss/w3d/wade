import * as React from "react";
import SlickSlider, * as ReactSlick from "react-slick";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import clsx from "clsx";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import { DeepReadonly } from "ts-essentials";
import tsDefaults, { ObjectDefaults } from "ts-defaults";

import { makeAppStyles } from "../material";

export type CarouselProps = Omit<ReactSlick.Settings, "centerPadding" | "customPaging" | "ref"> &
	React.PropsWithChildren<{
		animate?: boolean;
		centerHighlightStyle?: React.CSSProperties;
		centerTransition?: number;
		nonCenterZoom?: boolean | number;
		selectedIndex?: number;
		trackMargin?: number;
	}>;

export interface ISlickInternalRef {
	getInternalRef(): SlickSlider | undefined;
}

const DefaultPrevArrow: React.FC<ReactSlick.CustomArrowProps> = (props) => (
	<div style={{ position: "absolute", top: "calc(50% - 15px)", left: 0 }}>
		<IconButton color="inherit" disabled={props.currentSlide === 0} onClick={props.onClick} size="small">
			<ChevronLeftIcon />
		</IconButton>
	</div>
);
const DefaultNextArrow: React.FC<ReactSlick.CustomArrowProps> = (props) => (
	<div style={{ position: "absolute", top: "calc(50% - 15px)", right: 0 }}>
		<IconButton
			color="inherit"
			disabled={props.currentSlide == null || props.slideCount === props.currentSlide + 1}
			onClick={props.onClick}
			size="small"
		>
			<ChevronRightIcon />
		</IconButton>
	</div>
);

interface IProps extends CarouselProps {
	forwardedRef: React.Ref<ISlickInternalRef>;
}

type DefaultProps = ObjectDefaults<IProps, undefined>;
export const defaultProps: DeepReadonly<DefaultProps> = {
	accessibility: true,
	animate: false,
	centerMode: true,
	centerTransition: 0.1,
	draggable: false,
	dots: false,
	focusOnSelect: true,
	infinite: false,
	initialSlide: 0,
	nextArrow: <DefaultNextArrow />,
	nonCenterZoom: false,
	prevArrow: <DefaultPrevArrow />,
	rows: 1,
	slidesToScroll: 1,
	speed: 100,
	swipeToSlide: true,
	trackMargin: 50,
	variableWidth: true,
	waitForAnimate: true,
};

const useStyles = makeAppStyles<string, IProps>({
	slickCenter: (p) => {
		const { centerHighlightStyle, centerMode } = p;
		if (!centerHighlightStyle || !centerMode) {
			return {};
		}
		return {
			"& .slick-slide.slick-center": centerHighlightStyle,
		};
	},
	slickMargin: (p) => {
		const { trackMargin: tm } = p;
		return tm ? { "& .slick-list": { margin: `0 ${tm}px` } } : {};
	},
	slickModifiers: {
		'& .slick-track [tabindex="-1"]': { outline: "none" },
		"& .slick-slide div": { verticalAlign: "bottom" },
	},
	slickTransform: (p) => {
		const { centerMode, nonCenterZoom: ncz } = p;
		if (!centerMode || !ncz) {
			return {};
		}
		return {
			"& .slick-slide:not(.slick-center)": {
				transform: `scale(${typeof ncz === "number" ? ncz : 0.9})`,
				transformOrigin: "center",
			},
		};
	},
	slickTransition: (p) => {
		const { animate: a, centerMode, centerTransition: ct } = p;
		if (!a || !centerMode || !ct) {
			return {};
		}
		return {
			"& .slick-slide": {
				transition: `transform ${ct}s ease`,
			},
		};
	},
});

const CarouselFC = React.memo((_props: IProps) => {
	const props = tsDefaults(_props, defaultProps as DefaultProps);

	const { animate, centerTransition, children, className, forwardedRef, nonCenterZoom, selectedIndex, trackMargin, ...rest } = props;

	const slickClasses = useStyles(props);
	const cls = clsx(
		slickClasses.slickCenter,
		slickClasses.slickModifiers,
		slickClasses.slickTransform,
		slickClasses.slickTransition,
		slickClasses.slickMargin,
		className
	);

	const [slickRef, setSlickRef] = React.useState<SlickSlider>();
	const slickRefCb = React.useCallback((internalRef: SlickSlider) => {
		setSlickRef(internalRef);
	}, []);
	React.useImperativeHandle(forwardedRef, () => ({
		getInternalRef: () => slickRef,
	}));

	const [lastIndex, setLastIndex] = React.useState(selectedIndex);
	React.useLayoutEffect(() => {
		if (slickRef == null || selectedIndex == null) {
			return;
		}
		const animateGoTo = animate && lastIndex && Math.abs(lastIndex - selectedIndex) <= 2;
		setLastIndex(selectedIndex);
		slickRef.slickGoTo(selectedIndex, !animateGoTo);
	}, [animate, lastIndex, selectedIndex, slickRef]);

	// children are mapped because slick applies styles to children automatically without merging
	return (
		<SlickSlider className={cls} {...rest} ref={slickRefCb}>
			{React.Children.map(children, (child, i) => (
				<div key={i}>{child}</div>
			))}
		</SlickSlider>
	);
});

export const Carousel = React.forwardRef<ISlickInternalRef, CarouselProps>((props, ref) => <CarouselFC forwardedRef={ref} {...props} />);
