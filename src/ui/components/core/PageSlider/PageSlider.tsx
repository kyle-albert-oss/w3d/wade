import * as React from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import InputLabel from "@material-ui/core/InputLabel";
import { ValueLabelProps } from "@material-ui/core/Slider";
import TextField, { TextFieldProps } from "@material-ui/core/TextField";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { DeepReadonly, PickProperties } from "ts-essentials";
import { Box } from "@material-ui/core";

import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { uiStateSelector } from "../../../../store/redux/selectors";
import { AppRootState, UiState } from "../../../../store/redux/types/state";
import { toNumber } from "../../../../util/numbers";
import AppSliderTooltipBottom from "../AppSliderTooltip";
import { MaterialProps, withMaterial } from "../material";
import { AppSlider } from "../AppSlider";

export interface IPageSliderProps {
	id?: TextFieldProps["id"];
	infinite?: boolean;
	itemLabel?(index: number): React.ReactNode;
	label?: React.ReactNode;
	maxIndex: number;
	minIndex?: number;
	onChange?(newIndex: number | undefined): void;
	selectedIndex?: number;
	variant?: "compact";
}

interface IProps extends IPageSliderProps, MaterialProps {}

interface IState {
	blank?: boolean;
	pendingIndex?: number;
}

class PageSliderBase extends React.PureComponent<IProps, IState> {
	static defaultProps: Partial<IProps> = {
		label: "Page",
		minIndex: 0,
		selectedIndex: 0,
	};

	constructor(props: Readonly<IProps>) {
		super(props);
		this.state = {};
	}

	renderSliderTooltip = (props: ValueLabelProps) => <AppSliderTooltipBottom format={this.formatSliderTooltipValue} {...props} />;

	formatSliderTooltipValue = (value: number) => {
		const itemLabel = this.props.itemLabel ? this.props.itemLabel(value) : undefined;
		return `${value}${itemLabel == null ? "" : ` - ${itemLabel}`}`;
	};

	onStepLeftClick = () => {
		const { infinite, onChange, maxIndex, minIndex, selectedIndex = 0 } = this.props;
		if (!onChange) {
			return;
		}
		const newIndex = Math.max(selectedIndex - 1, minIndex || 0);
		if (newIndex !== selectedIndex) {
			onChange(newIndex);
		} else if (infinite) {
			onChange(maxIndex);
		}
	};

	onStepRightClick = () => {
		const { infinite, onChange, maxIndex, minIndex, selectedIndex = 0 } = this.props;
		if (!onChange) {
			return;
		}
		const newIndex = Math.min(selectedIndex + 1, maxIndex);
		if (newIndex !== selectedIndex) {
			onChange(newIndex);
		} else if (infinite) {
			onChange(minIndex || 0);
		}
	};

	onSliderChange = (e: React.ChangeEvent<{}>, value: number | number[]) => this.setState({ pendingIndex: value as number });

	onSliderValueUpdate = (e: React.ChangeEvent<{}>, value: number | number[]) => {
		if (this.props.onChange) {
			this.props.onChange((value as number) || 0);
		}
		this.setState({ pendingIndex: undefined });
	};

	onTextFieldChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
		const { minIndex, maxIndex, onChange } = this.props;

		const newValue = toNumber(e.currentTarget.valueAsNumber, { min: minIndex, max: maxIndex, allowNaN: true });
		const isNil = isNaN(newValue) || newValue == null;

		this.setState({ blank: isNil });
		if (!isNil && onChange) {
			onChange(newValue);
		}
	};

	onTextFieldBlur = () => {
		if (this.state.blank) {
			this.setState({ blank: false }, this.forceUpdate);
		}
	};

	render() {
		const { classes, id, label, infinite, maxIndex, minIndex = 0, selectedIndex = 0, variant } = this.props;
		const { blank, pendingIndex } = this.state;
		let liveIndex: number | string | undefined = pendingIndex != null ? pendingIndex : selectedIndex;
		if (liveIndex == null) {
			liveIndex = "";
		}

		const controls = (
			<Grid container alignItems="center" justify="center" wrap="nowrap">
				<Grid item>
					<IconButton color="inherit" disabled={!infinite && selectedIndex === minIndex} onClick={this.onStepLeftClick} size="small">
						<ChevronLeftIcon />
					</IconButton>
				</Grid>
				<Grid item>
					<InputLabel htmlFor={id}>{label}</InputLabel>
				</Grid>
				<Grid item style={{ width: "72px" }}>
					<TextField
						id={id}
						inputProps={{
							className: classes.inputNoLabel,
							max: maxIndex,
							min: minIndex,
						}}
						margin="none"
						onBlur={this.onTextFieldBlur}
						onChange={this.onTextFieldChanged}
						type="number"
						value={blank ? "" : liveIndex}
						variant="filled"
					/>
				</Grid>
				<Grid item>
					<Box mr={1}>
						<IconButton color="inherit" size="small" onClick={this.onStepRightClick} disabled={!infinite && selectedIndex === maxIndex}>
							<ChevronRightIcon />
						</IconButton>
					</Box>
				</Grid>
			</Grid>
		);

		return (
			<Grid container alignItems="center" spacing={1}>
				<Grid item style={variant === "compact" ? { flexBasis: "100%" } : undefined}>
					{controls}
				</Grid>
				<Grid item xs>
					<Box ml={1} mr={2}>
						<AppSlider
							marks
							max={maxIndex}
							min={minIndex}
							onChange={this.onSliderChange}
							onChangeCommitted={this.onSliderValueUpdate}
							step={1}
							track={false}
							value={+liveIndex}
							ValueLabelComponent={this.renderSliderTooltip}
						/>
					</Box>
				</Grid>
			</Grid>
		);
	}
}

export const PageSlider = withMaterial(PageSliderBase);

type UiNumbers = PickProperties<UiState, number>;
type UiNumbersKey = Exclude<keyof UiNumbers, undefined>;
export interface IConnectedPageSliderProps extends Omit<IPageSliderProps, "maxIndex" | "minIndex" | "selectedIndex"> {
	maxIndex: UiNumbersKey | number | ((state: AppRootState) => number);
	minIndex?: UiNumbersKey;
	selectedIndex: UiNumbersKey;
}

export const ConnectedPageSlider = connect(
	(state: AppRootState, ownProps: DeepReadonly<IConnectedPageSliderProps>): IPageSliderProps => {
		const { minIndex, maxIndex, selectedIndex } = ownProps;
		const uiState = uiStateSelector(state);

		return {
			...ownProps,
			maxIndex: typeof maxIndex === "number" ? maxIndex : typeof maxIndex === "function" ? maxIndex(state) : uiState[maxIndex],
			minIndex: (minIndex ? uiState[minIndex] : undefined) || 0,
			selectedIndex: uiState[selectedIndex],
		};
	},
	getDispatchActionMapper(),
	(stateProps: IPageSliderProps, dispatchProps: IActionProps, ownProps: IConnectedPageSliderProps): IPageSliderProps & IActionProps => {
		return {
			...stateProps,
			...dispatchProps,
			onChange(newIndex: number | undefined): void {
				if (newIndex != null) {
					dispatchProps.actions.uiUpdate({ [ownProps.selectedIndex]: newIndex });
				}
				if (stateProps.onChange) {
					stateProps.onChange(newIndex);
				}
			},
		};
	}
)(PageSlider);
