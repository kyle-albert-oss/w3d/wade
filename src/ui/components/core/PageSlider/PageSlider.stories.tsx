import * as React from "react";
import { number, text } from "@storybook/addon-knobs";

import { createStory } from "../../../../../stories/story-util";

import { IPageSliderProps, PageSlider, ConnectedPageSlider } from "./PageSlider";

const DefaultPageSlider: React.FC<IPageSliderProps> = (props) => {
	const [idx, setIdx] = React.useState(0);
	const onChangeCb = React.useCallback((_idx: number) => {
		setIdx(_idx);
	}, []);
	return <PageSlider {...props} onChange={onChangeCb} selectedIndex={idx} />;
};

createStory("Core|PageSlider", module, { style: { padding: 20 } })
	.add("Default", () => <DefaultPageSlider label={text("Label", "Page")} maxIndex={number("Max", 20)} />)
	.add("Redux Connected", () => <ConnectedPageSlider infinite label="Page" maxIndex={() => 60} selectedIndex="selectedMapIndex" />);
