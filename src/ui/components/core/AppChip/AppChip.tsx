import * as React from "react";
import { Chip, ChipProps } from "@material-ui/core";
import clsx from "clsx";

import { useChipStyles } from "../material";

export interface IAppChipProps extends ChipProps {
	position?: "end";
	square?: boolean;
}

const AppChipFC: React.FC<IAppChipProps> = (props) => {
	const { className, position, square, ...chipProps } = props;
	const classes = useChipStyles();

	const cls = clsx(className, {
		[classes.rightSide]: position === "end",
		[classes.square]: square,
	});

	return <Chip {...chipProps} className={cls} />;
};

export const AppChip = React.memo(AppChipFC);
