import * as React from "react";
import ToggleButton, { ToggleButtonProps } from "@material-ui/lab/ToggleButton";
import clsx from "clsx";

import { calcDisabledWithProps, FormDisableContext } from "../../context/form-disable";
import { MaterialProps, withMaterial } from "../material";

import { ReactComponent as ArrowUpIcon } from "./arrow-up.svg";

import "./AppToggleButton.css";

interface IProps extends Omit<ToggleButtonProps, "classes">, MaterialProps {
	arrow?: "up";
	onArrowClick?(e: HTMLElement): void;
}

const AppToggleButtonFC: React.FC<IProps> = (props) => {
	const { arrow, children, classes, className, onArrowClick, theme, ...toggleButtonProps } = props;

	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);

	let arrowJSX: React.ReactNode;
	if (arrow === "up") {
		arrowJSX = <ArrowUpIcon className={clsx(classes.svgFill, "app-toggle-button-overlay", "app-toggle-button-arrow-up")} />;
	}

	const ref = React.useRef<any | null>(null);
	const arrowCb = React.useCallback(
		(e: React.MouseEvent<HTMLDivElement>) => {
			e.stopPropagation();
			if (disabled) {
				return;
			} else if (onArrowClick) {
				onArrowClick(ref.current!);
			}
		},
		[disabled, onArrowClick, ref]
	);

	return (
		<ToggleButton
			disableRipple
			className={clsx("app-toggle-button", { "with-arrow": arrow != null }, className)}
			{...toggleButtonProps}
			disabled={disabled}
			ref={ref}
		>
			<React.Fragment>
				{arrowJSX && (
					<div className={classes.toggleButtonArrowContainer} onClick={arrowCb}>
						{arrowJSX}
					</div>
				)}
				{children}
			</React.Fragment>
		</ToggleButton>
	);
};

export const AppToggleButton = withMaterial(React.memo(AppToggleButtonFC));
