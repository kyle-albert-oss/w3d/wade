import * as React from "react";
import { boolean } from "@storybook/addon-knobs";

import { createStory } from "../../../../../stories/story-util";

import FileSelect from "./FileSelect";

createStory("Core|FileSelect", module, { style: { padding: 20 } })
	.add("File", () => <FileSelect disabled={boolean("Disabled", false)} />)
	.add("Folder", () => <FileSelect disabled={boolean("Disabled", false)} folder />);
