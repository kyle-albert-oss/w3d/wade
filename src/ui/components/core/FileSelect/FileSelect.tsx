import * as React from "react";
import * as DZ from "react-dropzone";
import { createStyles, Theme } from "@material-ui/core";
import InputAdornment from "@material-ui/core/InputAdornment";
import useTheme from "@material-ui/core/styles/useTheme";
import ClearIcon from "@material-ui/icons/Clear";
import FolderIcon from "@material-ui/icons/Folder";
import clsx from "clsx";

import { ElectronHandle } from "../../../../util/env";
import { calcDisabledWithProps, FormDisableContext } from "../../context/form-disable";
import { AppIconButton } from "../AppIconButton";
import { AppTextField, AppTextFieldProps } from "../AppTextField";
import { FileIcon } from "../icons/FileIcon";
import { makeAppStyles } from "../material";

export interface IFileSelectHandle {
	clearFiles(): void;
}

interface IFileSelectSharedProps {
	className?: string;
	disabled?: boolean;
	hint?: React.ReactNode;
	label?: string;
	name?: string;
	onFileCleared?(): void;
	required?: boolean;
	textFieldProps?: AppTextFieldProps;
	value?: string;
}

export interface IFileSelectFolderProps extends IFileSelectSharedProps {
	folder: true;
	onFolderChanged?(folderPath: string | undefined): void;
}

export interface IFileSelectFileProps extends DZ.DropzoneOptions, IFileSelectSharedProps {
	acceptFile?(file: File): Promise<boolean> | boolean;
	folder?: undefined | false;
	onFileChanged?: DZ.DropzoneOptions["onDrop"]; // alias to match the folder option.
}

export type FileSelectProps = IFileSelectFileProps | IFileSelectFolderProps;

const useStyles = makeAppStyles((theme: Theme) =>
	createStyles({
		root: {
			flexGrow: 1,
		},
		input: {
			fontFamily: "monospace",
			textOverflow: "ellipsis",
		},
	})
);

type FileValue = File | string; // string = directory

const FileSelect = React.forwardRef<any, FileSelectProps>((props, ref) => {
	const { children, folder, hint, label, onFileCleared, required, value } = props;

	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);
	const acceptFile = folder ? undefined : (props as IFileSelectFileProps).acceptFile;
	const onDrop = folder ? undefined : (props as IFileSelectFileProps).onDrop ?? (props as IFileSelectFileProps).onFileChanged;
	const onFolderChanged = folder ? (props as IFileSelectFolderProps).onFolderChanged : undefined;

	let { textFieldProps } = props;
	if (!textFieldProps) {
		textFieldProps = {};
	}

	const theme = useTheme();
	const classes = useStyles(theme);

	const [files, setFiles] = React.useState<FileValue[]>(value ? [value] : []);

	const onDropHook = React.useCallback(
		(
			acceptedFiles: File[],
			rejectedFiles: DZ.FileRejection[],
			event: React.DragEvent<HTMLElement> | React.ChangeEvent<HTMLInputElement> | DragEvent | Event
		): void => {
			setFiles(acceptedFiles);
			if (onDrop) {
				onDrop(acceptedFiles, rejectedFiles, event);
			}
		},
		[onDrop, setFiles]
	);

	const getFilesFromEventHook = React.useCallback(
		async (event) => {
			const fileList: FileList = event.dataTransfer ? event.dataTransfer.files : event.target.files;
			if (!fileList) {
				return [];
			}
			const acceptedFiles: File[] = [];
			for (let x = 0; x < fileList.length; x++) {
				const file = fileList.item(x);
				if (file != null && (!acceptFile || (await acceptFile(file)))) {
					acceptedFiles.push(file);
				}
			}
			return acceptedFiles;
		},
		[acceptFile]
	);

	const { getRootProps, getInputProps, open } = DZ.useDropzone({
		...props,
		disabled,
		getFilesFromEvent: getFilesFromEventHook,
		// Disable click and keydown behavior
		noClick: true,
		noDrag: folder, // can't support this :(
		noKeyboard: true,
		onDrop: onDropHook,
	});

	const clearFiles = React.useCallback(() => {
		setFiles([]);
		if (onFileCleared) {
			onFileCleared();
		} else if (onFolderChanged) {
			onFolderChanged(undefined);
		}
	}, [setFiles, onFileCleared, onFolderChanged]);
	React.useImperativeHandle(
		ref,
		(): IFileSelectHandle => ({
			clearFiles,
		})
	);
	const onClearClick = React.useCallback(
		(e: React.MouseEvent<any>) => {
			e.stopPropagation();
			clearFiles();
		},
		[clearFiles]
	);

	const adaptedValue = React.useMemo(
		() => (files && files.length ? files.map((f) => (typeof f === "string" ? f : f.path || f.name)).join(", ") : ""),
		[files]
	);
	const rootClasses = clsx("dropzone", classes.root, props.className);
	const { InputProps, inputProps, ...restTextField } = textFieldProps;

	const onClick = React.useCallback(async () => {
		if (disabled) {
			return;
		}
		if (folder && ElectronHandle) {
			const result = await ElectronHandle.selectDirectory(); // using this directly as a click handler = 💣💥
			const paths = result.filePaths || [];
			let selectedFolder;
			if (paths.length > 0) {
				selectedFolder = paths[0];
				setFiles([selectedFolder]);
			}
			if (onFolderChanged) {
				onFolderChanged(selectedFolder);
			}
		} else {
			open();
		}
	}, [disabled, folder, onFolderChanged, open]);

	const rightButton = <AppIconButton disabled={disabled}>{folder ? <FolderIcon /> : <FileIcon />}</AppIconButton>;

	return (
		<div {...getRootProps({ className: rootClasses })}>
			<input {...getInputProps()} />
			<AppTextField
				disabled={disabled}
				fullWidth
				label={label ?? (folder ? "Folder" : "File")}
				{...restTextField}
				InputProps={{
					endAdornment: (
						<InputAdornment position="end">
							{files && files.length > 0 && (
								<AppIconButton disabled={disabled} onClick={onClearClick}>
									<ClearIcon />
								</AppIconButton>
							)}
							{rightButton}
							{hint}
						</InputAdornment>
					),
					...InputProps,
				}}
				inputProps={{
					...inputProps,
					className: classes.input,
				}}
				name={props.name}
				onClick={onClick}
				required={required}
				title={adaptedValue}
				value={adaptedValue}
			/>
			{children}
		</div>
	);
});

export default FileSelect;
