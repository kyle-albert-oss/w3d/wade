import * as React from "react";
import { Box, Container, Typography, Button } from "@material-ui/core";
import { FileCopy } from "@material-ui/icons";

import errorImage from "./error.png";

export interface IErrorPageProps {
	error?: Error | string;
}

interface IState {
	error?: Error | string;
	errorInfo?: React.ErrorInfo;
}

export class ErrorPage extends React.Component<IErrorPageProps, IState> {
	constructor(props: Readonly<IErrorPageProps>) {
		super(props);
		this.state = { error: props.error };
	}

	componentDidCatch(error: Error, errorInfo: React.ErrorInfo): void {
		this.setState({ error, errorInfo });
	}

	componentDidUpdate(prevProps: Readonly<IErrorPageProps>, prevState: Readonly<IState>, snapshot?: any): void {
		if (prevProps.error == null && this.props.error) {
			this.setState({ error: this.props.error });
		}
	}

	copyToClipboard = () => {
		const err = this.state.error;
		if (!err) {
			return;
		} else if (typeof err === "string") {
			navigator.clipboard.writeText(err);
		} else {
			const lines = [err.toString()];
			const stack = (err as any).mappedStack ?? err.stack;
			if (stack) {
				if (Array.isArray(stack)) {
					lines.push(...stack);
				} else if (typeof stack === "object") {
					lines.push(JSON.stringify(stack));
				} else {
					lines.push(stack);
				}
			}

			navigator.clipboard.writeText(lines.join("\n"));
		}
	};

	render() {
		if (!this.state.error) {
			return this.props.children;
		}

		const msg =
			typeof this.state.error === "string" ? (
				this.state.error
			) : (
				<React.Fragment>
					<div>{this.state.error.toString()}</div>
					<div>{(this.state.error as any).mappedStack ?? this.state.error.stack}</div>
				</React.Fragment>
			);

		return (
			<Box position="absolute" top={0} left={0} bottom={0} right={0} style={{ backgroundColor: "#801313" }}>
				<Container maxWidth="md">
					<Box display="flex" flexDirection="column" alignItems="center" mt={20}>
						<Box mb={4}>
							<img src={errorImage} />
						</Box>
						<Box mb={4}>
							<Typography variant="h3" align="center" style={{ fontFamily: "monospace" }}>
								An unexpected error has occurred.
							</Typography>
						</Box>
						<Box mb={4}>
							<Button onClick={this.copyToClipboard} startIcon={<FileCopy />}>
								Copy to Clipboard
							</Button>
						</Box>
						<Box width="100%">
							<Typography
								component="div"
								variant="body1"
								style={{ backgroundColor: "rgba(0,0,0,.2)", border: "1px solid #621313", fontFamily: "monospace", fontSize: 16 }}
							>
								<Box p={1}>{msg}</Box>
							</Typography>
						</Box>
					</Box>
				</Container>
			</Box>
		);
	}
}
