import * as React from "react";

import { createStory } from "../../../../../stories/story-util";

import { ErrorPage } from "./ErrorPage";

createStory("Core|ErrorPage", module).add("Default", () => <ErrorPage error="This is a test error message." />);
