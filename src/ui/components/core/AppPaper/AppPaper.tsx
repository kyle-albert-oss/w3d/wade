import * as React from "react";
import Paper, { PaperProps } from "@material-ui/core/Paper";
import clsx from "clsx";
import tsDefaults from "ts-defaults";
import { useTheme, createStyles, Theme, lighten } from "@material-ui/core";

import { makeAppStyles } from "../material";

export interface IAppPaperProps extends Omit<PaperProps, "ref"> {
	translucent?: boolean | "fake";
}

const usePaperStyles = makeAppStyles((theme: Theme) =>
	createStyles({
		fakeTranslucent: {
			backgroundColor: lighten(theme.palette.background.default, 0.02),
		},
		translucent: {
			backgroundColor: "rgba(255, 255, 255, 0.02) !important",
		},
	})
);

const AppPaperFC: React.FC<IAppPaperProps> = (_props) => {
	const props = tsDefaults(_props, {
		square: true,
	});
	const theme = useTheme();
	const classes = usePaperStyles();

	const { children, className, translucent, ...paperProps } = props;
	const paperClasses = clsx(className, {
		[classes.translucent]: translucent && translucent !== "fake",
		[classes.fakeTranslucent]: translucent === "fake",
	});
	return (
		<Paper {...paperProps} className={paperClasses}>
			{children}
		</Paper>
	);
};

export const AppPaper = React.memo(AppPaperFC);
