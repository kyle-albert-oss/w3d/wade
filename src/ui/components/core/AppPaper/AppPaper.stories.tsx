import * as React from "react";
import { Box } from "@material-ui/core";
import { boolean } from "@storybook/addon-knobs";

import { createStory } from "../../../../../stories/story-util";

import { AppPaper } from "./AppPaper";

createStory("Core|AppPaper", module, { style: { padding: 20 } }).add("Default", () => {
	const translucent = boolean("Translucent", false);
	return (
		<Box>
			<AppPaper translucent={translucent}>
				<Box p={2}>Paper Content {translucent ? "(Translucent)" : null}</Box>
				<Box p={2}>
					<AppPaper translucent>
						<Box p={2}>Inner Paper (Translucent)</Box>
					</AppPaper>
				</Box>
				<Box p={2}>
					<AppPaper>
						<Box p={2}>Inner Paper (No Translucency)</Box>
					</AppPaper>
				</Box>
			</AppPaper>
			<Box mt={1}>
				<AppPaper translucent="fake">
					<Box p={2}>Paper Content {translucent ? "(Fake Translucent)" : null}</Box>
				</AppPaper>
			</Box>
			<Box mt={1}>
				<AppPaper>
					<Box p={2}>Paper Content</Box>
				</AppPaper>
			</Box>
			<Box mt={1}>
				<AppPaper translucent={translucent} variant="outlined">
					<Box p={2}>Outlined {translucent ? "(Translucent)" : null}</Box>
				</AppPaper>
			</Box>
		</Box>
	);
});
