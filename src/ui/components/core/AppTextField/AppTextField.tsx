import * as React from "react";
import { createStyles, FormHelperText, TextField, Theme } from "@material-ui/core";
import { TextFieldProps } from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import { Save } from "@material-ui/icons";
import { useFormContext, Controller } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";

import { calcDisabledWithProps, FormDisableContext } from "../../context/form-disable";
import { AppIconButton } from "../AppIconButton";
import { makeAppStyles } from "../material";

type ValidationOptions = Parameters<typeof Controller>[0]["rules"];
export type AppTextFieldProps = TextFieldProps & {
	errorText?: React.ReactNode;
	maxLength?: number;
	minLength?: number;
	readOnly?: boolean;
	size?: "small" | "medium";
	validPattern?: RegExp;
	validate?(value: string): Promise<any>;
};

const useTextFieldStyles = makeAppStyles((theme: Theme) =>
	createStyles({
		root: {
			backgroundColor: "rgba(0,0,0,0.1)",
			paddingRight: 0, // adornment padding disabled
		},
	})
);

const AppTextFieldFC: React.FC<AppTextFieldProps> = (props) => {
	const {
		errorText,
		InputProps,
		inputProps,
		maxLength,
		minLength,
		onBlur,
		onChange,
		readOnly,
		validPattern,
		validate,
		value = "",
		...rest
	} = props;
	const { name } = props;

	const formContext = useFormContext();
	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);
	const classes = useTextFieldStyles();
	const formContextSetValue = formContext?.setValue;

	React.useEffect(() => {
		if (!formContextSetValue || !name) {
			return;
		}
		formContextSetValue(name, value);
	}, [name, value]);

	const internalInputProps = React.useMemo(
		(): TextFieldProps["InputProps"] => ({
			classes,
			...InputProps,
		}),
		[classes, InputProps]
	);

	const internalFormInputProps = React.useMemo(
		(): TextFieldProps["inputProps"] => ({
			...inputProps,
			...(readOnly ? { "aria-readonly": true, readOnly: true } : undefined),
		}),
		[inputProps, readOnly]
	);

	const rulesObj = React.useMemo(
		(): ValidationOptions => ({
			maxLength: maxLength != null ? { value: maxLength, message: `Must be no more than ${maxLength} characters.` } : undefined,
			minLength: minLength != null ? { value: minLength, message: `Must be at least ${minLength} characters.` } : undefined,
			pattern: validPattern,
			required: props.required === true ? { value: props.required, message: "This field is required." } : undefined,
			validate,
		}),
		[maxLength, minLength, props.required, validPattern, validate]
	);

	const hasError = errorText != null || !!(props.name && formContext?.errors[props.name] != null);

	const fieldJSX =
		formContext && props.name ? (
			<Controller
				control={formContext.control}
				defaultValue={props.defaultValue || ""}
				name={props.name}
				rules={rulesObj}
				render={(rhfProps) => (
					<TextField
						{...rest}
						{...rhfProps}
						disabled={disabled}
						error={hasError}
						InputProps={internalInputProps}
						inputProps={internalFormInputProps}
						onChange={(e) => {
							rhfProps.onChange(e.currentTarget.value);
							if (onChange) {
								onChange(e);
							}
						}}
					/>
				)}
			/>
		) : (
			<TextField
				error={hasError}
				{...rest}
				disabled={disabled}
				InputProps={internalInputProps}
				inputProps={internalFormInputProps}
				onBlur={onBlur}
				onChange={onChange}
				value={value}
			/>
		);

	const errorsJSX =
		formContext && props.name ? (
			<ErrorMessage
				errors={formContext.errors}
				name={props.name}
				render={({ message }) => <FormHelperText error>{message}</FormHelperText>}
			/>
		) : errorText ? (
			<FormHelperText error>{errorText}</FormHelperText>
		) : null;

	return (
		<>
			{fieldJSX}
			{errorsJSX}
		</>
	);
};

export const AppTextField = React.memo(AppTextFieldFC);

export type AppTextFieldWithSaveProps = AppTextFieldProps & {
	onSaveError(e: unknown, newValue: unknown, lastValue: unknown): void;
	onSave(value: unknown): Promise<void>;
};

export const AppTextFieldWithSave: React.FC<AppTextFieldWithSaveProps> = (props) => {
	const { onSaveError, onSave, value, ...rest } = props;
	const [liveValue, setLiveValue] = React.useState(value);
	const [saving, setSaving] = React.useState(false);
	React.useEffect(() => {
		setLiveValue(value);
		setSaving(false);
	}, [value]);

	const onChangeCb = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
		setLiveValue(e.currentTarget.value);
	}, []);

	const onSaveCb = React.useCallback(() => {
		setSaving(true);
		(async () => {
			try {
				await onSave(liveValue);
			} catch (e) {
				onSaveError(e, liveValue, value);
			} finally {
				setSaving(false);
			}
		})();
	}, [liveValue, onSaveError, onSave, value]);

	return (
		<AppTextField
			{...(rest as AppTextFieldProps)}
			InputProps={{
				...rest.InputProps,
				endAdornment: (
					<InputAdornment disableTypography position="end">
						<AppIconButton disabled={rest.disabled || rest.readOnly || saving || value === liveValue} onClick={onSaveCb}>
							<Save />
						</AppIconButton>
					</InputAdornment>
				),
			}}
			onChange={onChangeCb}
			value={liveValue}
		/>
	);
};
