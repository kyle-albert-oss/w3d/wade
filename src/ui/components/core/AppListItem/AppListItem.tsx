import * as React from "react";
import ListItem, { ListItemProps } from "@material-ui/core/ListItem";
import { useDrag, useDrop, DropTargetMonitor } from "react-dnd";
import { XYCoord } from "dnd-core";
import clsx from "clsx";
import { ListItemIcon } from "@material-ui/core";
import { DragIndicator } from "@material-ui/icons";

import { calcDisabledWithProps, FormDisableContext } from "../../context/form-disable";
import { IDragItem, useDragHandleStyles } from "../material";
import { typedForwardRef } from "../../../../util/ui";
import { DndProps } from "../../hooks";

export type ListItemLIProps = ListItemProps<"li", { button?: false }>;
export type ListItemDivProps = ListItemProps<"div", { button: true }>;
export type IAppListItemProps = ListItemLIProps | ListItemDivProps;

function AppListItemFC<R, T extends IAppListItemProps>(props: React.PropsWithChildren<T>, ref: React.Ref<R>): JSX.Element {
	const { children, innerRef, ...listItemProps } = props;

	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);

	// get around weird TS issue with the union I don't really want to dive into without ignoring
	if (listItemProps.button) {
		const castedRef = ref as React.Ref<HTMLDivElement>;
		return (
			<ListItem {...(listItemProps as ListItemDivProps)} disabled={disabled} ref={castedRef}>
				{children}
			</ListItem>
		);
	} else {
		const castedRef = ref as React.Ref<HTMLLIElement>;
		return (
			<ListItem {...(listItemProps as ListItemLIProps)} disabled={disabled} ref={castedRef}>
				{children}
			</ListItem>
		);
	}
}

export const AppListItem = typedForwardRef(AppListItemFC);

type DraggableListItemProps = IAppListItemProps &
	DndProps & {
		handleClassName?: string;
	};

export const DraggableListItem = React.memo<DraggableListItemProps>((props) => {
	const {
		children,
		dragDisabled,
		dragId,
		dragIndex,
		dragType = "default",
		handleClassName,
		onDndBegin,
		onDndEnd,
		onDndHover,
		...listItemProps
	} = props;

	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);
	const canDrag = !disabled && !dragDisabled;

	const cx = useDragHandleStyles();
	const previewRef = React.useRef<HTMLDivElement | HTMLLIElement | null>(null);
	const dragRef = React.useRef<HTMLElement | null>(null);

	const dragItem: IDragItem = { type: dragType, id: dragId, index: dragIndex };
	const [{ isDragging }, drag, preview] = useDrag({
		begin: (monitor) => onDndBegin?.(dragItem),
		canDrag,
		collect: (monitor) => ({
			isDragging: monitor.isDragging(),
		}),
		end: (dropResult, monitor) => {
			onDndEnd?.(monitor.getItem(), monitor.didDrop());
		},
		item: dragItem,
	});
	const [, drop] = useDrop({
		accept: canDrag ? dragType : "none",
		hover(item: IDragItem, monitor: DropTargetMonitor) {
			if (!previewRef.current) {
				return;
			}
			const itemIndex = item.index;
			const hoverIndex = dragIndex;

			// Don't replace items with themselves
			if (itemIndex === hoverIndex) {
				return;
			}

			// Determine rectangle on screen
			const hoverBoundingRect = previewRef.current.getBoundingClientRect();

			// Get vertical middle
			const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

			// Determine mouse position
			const clientOffset = monitor.getClientOffset();

			// Get pixels to the top
			const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top;

			// Only perform the move when the mouse has crossed half of the items height
			// When dragging downwards, only move when the cursor is below 50%
			// When dragging upwards, only move when the cursor is above 50%

			// Dragging downwards
			if (itemIndex < hoverIndex && hoverClientY < hoverMiddleY) {
				return;
			}

			// Dragging upwards
			if (itemIndex > hoverIndex && hoverClientY > hoverMiddleY) {
				return;
			}

			// Time to actually perform the action
			onDndHover(itemIndex, hoverIndex);

			// Note: we're mutating the monitor item here!
			// Generally it's better to avoid mutations,
			// but it's good here for the sake of performance
			// to avoid expensive index searches.
			item.index = hoverIndex;
		},
	});

	preview(drop(previewRef));
	drag(dragRef);

	return (
		<AppListItem {...listItemProps} className={clsx(listItemProps.className, { [cx.beingDragged]: isDragging })} ref={previewRef as any}>
			<ListItemIcon className={clsx(handleClassName, cx.areaListIcon, { [cx.area]: canDrag })} ref={dragRef}>
				{!dragDisabled ? <DragIndicator /> : <></>}
			</ListItemIcon>
			{children}
		</AppListItem>
	);
});
