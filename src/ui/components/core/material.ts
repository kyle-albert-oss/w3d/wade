import { createStyles, fade, Theme, WithStyles, withStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";

import * as Functions from "../../../util/functions";

export interface IMaterialThemeProps {
	theme: Theme;
}

export const styles = (theme: Theme) =>
	createStyles({
		root: {
			display: "flex",
			width: "100%",
			height: "100%",
		},
		autoHeight: {
			height: "auto !important",
		},
		buttonSuccess: {
			backgroundColor: theme.palette.success.main,
			"&:hover": {
				backgroundColor: theme.palette.success.dark,
			},
		},
		buttonProgress: {
			color: theme.palette.primary.main,
			position: "absolute",
			top: "50%",
			left: "50%",
			marginTop: -12,
			marginLeft: -12,
		},
		errorIcon: {
			color: theme.palette.error.main,
		},
		dialogTabs: {
			flexGrow: 1,
		},
		dialogTitleToolbar: {
			paddingRight: 0,
		},
		disabledCursor: {
			cursor: "not-allowed",
		},
		inputRoot: {
			color: "inherit",
		},
		inputInput: {
			padding: theme.spacing(1, 1, 1, 7),
			transition: theme.transitions.create("width"),
			width: "100%",
			[theme.breakpoints.up("md")]: {
				width: 200,
			},
		},
		inputNoLabel: {
			paddingBottom: 6,
			paddingTop: 6,
		},
		listSubheader: {
			backgroundColor: theme.palette.background.paper,
		},
		noPadding: {
			padding: 0,
		},
		paper: {
			padding: theme.spacing(1),
		},
		projectDialog: {
			minWidth: 600,
			height: 600,
		},
		search: {
			position: "relative",
			borderRadius: theme.shape.borderRadius,
			backgroundColor: fade(theme.palette.common.white, 0.15),
			"&:hover": {
				backgroundColor: fade(theme.palette.common.white, 0.25),
			},
			marginRight: theme.spacing(2),
			marginLeft: 0,
			width: "100%",
			[theme.breakpoints.up("sm")]: {
				marginLeft: 0,
				width: "auto",
			},
		},
		searchIcon: {
			width: theme.spacing(7),
			height: "100%",
			position: "absolute",
			pointerEvents: "none",
			display: "flex",
			alignItems: "center",
			justifyContent: "center",
		},
		spacingStackB1: {
			marginBottom: theme.spacing(1),
		},
		successIcon: {
			color: theme.palette.success.main,
		},
		svgFill: {
			fill: theme.palette.text.primary,
		},
		toggleButton32: {
			height: "32px",
			minWidth: "33px",
		},
		toggleButtonArrowContainer: {
			position: "absolute",
			right: 0,
			top: 0,
			bottom: 0,
			width: "12px",
			"border-left": `1px solid ${theme.palette.divider}`,
		},
		toolbar: {
			display: "flex",
			alignItems: "center",
			justifyContent: "flex-end",
			padding: "0 8px",
			...theme.mixins.toolbar,
		},
		toolbarDense: {
			display: "flex",
			alignItems: "center",
			justifyContent: "flex-end",
			padding: "0 8px",
			...theme.mixins.toolbar,
			minHeight: "48px !important",
		},
		sliderMapZoomContainer: {
			padding: theme.spacing(2),
			width: "300px",
		},
		warningIcon: {
			color: theme.palette.warning.main,
		},
	});

export type MaterialProps = WithStyles<ReturnType<typeof styles>, true>;
export const withMaterial = Functions.wrap(withStyles(styles, { withTheme: true }));

export const makeAppStyles: typeof makeStyles = (input: any, opts: any) => makeStyles(input, { index: 1000, ...opts });
export const useAppStyles = makeAppStyles(styles);
export const useChipStyles = makeAppStyles((theme: Theme) =>
	createStyles({
		square: {
			borderRadius: theme.spacing(0.5),
		},
		rightSide: {
			margin: theme.spacing(0, 0, 1, 1),
		},
	})
);
export const useGridStyles = makeAppStyles((theme: Theme) =>
	createStyles({
		fullHeight1: {
			height: `calc(100% + ${theme.spacing(1)}px)`,
		},
	})
);

export const useDragHandleStyles = makeAppStyles(() =>
	createStyles({
		area: {
			cursor: "move !important",
		},
		areaListIcon: {
			display: "inline-flex",
			alignItems: "center",
			justifyContent: "center",
			opacity: 0.5,
		},
		beingDragged: {
			opacity: 0.5,
		},
	})
);

export interface IDragItem {
	id: string | number;
	index: number;
	type: string;
}
