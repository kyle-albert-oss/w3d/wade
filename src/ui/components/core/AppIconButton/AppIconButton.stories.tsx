import * as React from "react";
import { Add, Delete, Settings } from "@material-ui/icons";
import { text, boolean, select } from "@storybook/addon-knobs";
import { MenuItem, ListItemIcon, ListItemText } from "@material-ui/core";

import { createStory } from "../../../../../stories/story-util";

import { AppIconButton, AppIconButtonWithConfirmation, AppIconButtonWithSubmit, AppMenuIconButton } from "./AppIconButton";

createStory("Core|AppIconButton", module)
	.add("Default", () => (
		<AppIconButton color={select("Color", ["default", "primary", "secondary"], "default")}>
			<Add />
		</AppIconButton>
	))
	.add("With Confirmation", () => (
		<AppIconButtonWithConfirmation
			color={select("Color", ["default", "primary", "secondary"], "default")}
			confirmation={text("Confirmation Text", "Are you sure???")}
		>
			<Delete />
		</AppIconButtonWithConfirmation>
	))
	.add("With Menu", () => (
		<AppMenuIconButton
			color={select("Color", ["default", "primary", "secondary"], "default")}
			disabled={boolean("Disabled", false)}
			icon={<Add />}
		>
			<MenuItem>
				<ListItemIcon>
					<Add />
				</ListItemIcon>
				<ListItemText primary="Add" />
			</MenuItem>
			<MenuItem>
				<ListItemIcon>
					<Delete />
				</ListItemIcon>
				<ListItemText primary="Delete" />
			</MenuItem>
		</AppMenuIconButton>
	))
	.add("With Submit Button", () => (
		<AppIconButtonWithSubmit
			color={select("Color", ["default", "primary", "secondary"], "default")}
			icon={<Settings />}
			onSubmit={async () =>
				new Promise((r, rj) => {
					setTimeout(() => {
						alert("Submitted");
						r();
					}, 2000);
				})
			}
		></AppIconButtonWithSubmit>
	));
