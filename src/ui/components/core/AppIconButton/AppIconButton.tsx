import * as React from "react";
import IconButton, { IconButtonProps } from "@material-ui/core/IconButton";
import { Popover, ButtonGroup, Button, Box, Popper, ClickAwayListener, MenuList } from "@material-ui/core";
import { Check, Close } from "@material-ui/icons";

import { calcDisabledWithProps, FormDisableContext } from "../../context/form-disable";
import { AppPaper } from "../AppPaper";
import { typedMemo } from "../../../../util/ui";

export interface IAppIconButtonProps extends IconButtonProps {}

export const AppIconButton = React.forwardRef<HTMLButtonElement, IAppIconButtonProps>((props, ref) => {
	const { children, ...iconButtonProps } = props;

	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);

	return (
		<IconButton {...iconButtonProps} disabled={disabled} ref={ref}>
			{children}
		</IconButton>
	);
});

export interface IAppIconButtonWithConfirmationProps extends IAppIconButtonProps {
	confirmation?: React.ReactNode;
	p?: number;
}

export const AppIconButtonWithConfirmation = React.forwardRef<HTMLButtonElement, IAppIconButtonWithConfirmationProps>((props, ref) => {
	const { children, confirmation = "Are you sure?", onClick, p, ...iconButtonProps } = props;

	const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

	const onButtonClickCb = React.useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
		setAnchorEl(event.currentTarget);
	}, []);
	const onCloseCb = React.useCallback(() => {
		setAnchorEl(null);
	}, []);
	const onConfirmClickCb = React.useCallback(
		(event: React.MouseEvent<HTMLButtonElement>) => {
			setAnchorEl(null);
			if (onClick) {
				onClick(event);
			}
		},
		[onClick]
	);

	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);
	const open = !!anchorEl;
	const confirmationPadding = p ?? (typeof confirmation === "string" ? 1 : 0);

	return (
		<div style={{ position: "relative" }}>
			<IconButton {...iconButtonProps} disabled={disabled} ref={ref} onClick={onButtonClickCb}>
				{children}
			</IconButton>
			<Popover
				anchorEl={anchorEl}
				anchorOrigin={{ horizontal: "center", vertical: "bottom" }}
				open={open}
				onClose={onCloseCb}
				transformOrigin={{ horizontal: "center", vertical: "top" }}
			>
				<Box p={confirmationPadding}>{confirmation}</Box>
				<div>
					<ButtonGroup className="app-width-full">
						<Button onClick={onCloseCb} variant="text" style={{ width: "50%" }}>
							<Close />
						</Button>
						<Button color="primary" onClick={onConfirmClickCb} style={{ width: "50%" }} variant="contained">
							<Check />
						</Button>
					</ButtonGroup>
				</div>
			</Popover>
		</div>
	);
});

export interface IAppIconButtonWithInputProps extends IAppIconButtonProps {
	icon: React.ReactNode;
	onSubmit(): Promise<void>;
}

export const AppIconButtonWithSubmit = React.forwardRef<HTMLButtonElement, IAppIconButtonWithInputProps>((props, ref) => {
	const { children, icon, onClick, onSubmit, ...iconButtonProps } = props;

	const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);
	const [submitting, setSubmitting] = React.useState(false);

	const onButtonClickCb = React.useCallback(
		(event: React.MouseEvent<HTMLButtonElement>) => {
			setAnchorEl(event.currentTarget);
			if (onClick) {
				onClick(event);
			}
		},
		[onClick]
	);
	const onCloseCb = React.useCallback(() => {
		setAnchorEl(null);
	}, []);
	const onSubmitClickCb = React.useCallback(
		(event: React.MouseEvent<HTMLButtonElement>) => {
			(async () => {
				setSubmitting(true);
				try {
					await onSubmit();
					setAnchorEl(null);
				} finally {
					setSubmitting(false);
				}
			})();
		},
		[onSubmit]
	);

	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);
	const open = !!anchorEl;

	return (
		<div style={{ position: "relative" }}>
			<IconButton {...iconButtonProps} disabled={disabled} ref={ref} onClick={onButtonClickCb}>
				{icon}
			</IconButton>
			<Popover
				anchorEl={anchorEl}
				anchorOrigin={{ horizontal: "center", vertical: "bottom" }}
				open={open}
				onClose={onCloseCb}
				transformOrigin={{ horizontal: "center", vertical: "top" }}
				TransitionProps={{ enter: false, exit: false }}
			>
				<Box p={1}>{children}</Box>
				<div>
					<ButtonGroup className="app-width-full">
						<Button disabled={submitting} onClick={onCloseCb} variant="text" style={{ width: "50% " }}>
							<Close />
						</Button>
						<Button disabled={submitting} color="primary" onClick={onSubmitClickCb} style={{ width: "50% " }} variant="contained">
							<Check />
						</Button>
					</ButtonGroup>
				</div>
			</Popover>
		</div>
	);
});

// POPOVER
export interface IAppPopoverIconButtonProps extends Omit<IAppIconButtonProps, "onClick"> {
	icon: NonNullable<React.ReactNode>;
}

const AppPopoverIconButtonFC: React.FC<IAppPopoverIconButtonProps> = (props) => {
	const { children, icon, id, ...buttonProps } = props;

	const [open, setOpen] = React.useState(false);
	const anchorRef = React.useRef<HTMLButtonElement>(null);
	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);

	const togglePopover = React.useCallback(() => {
		setOpen((prevOpen) => !prevOpen);
	}, []);

	const handleClose = React.useCallback((event: React.MouseEvent<Document, MouseEvent>) => {
		if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
			return;
		}
		setOpen(false);
	}, []);

	return (
		<Box>
			<IconButton {...buttonProps} disabled={disabled} ref={anchorRef} onClick={togglePopover}>
				{icon}
			</IconButton>
			<Popper anchorEl={anchorRef.current} open={open} placement="bottom-start" role={undefined} style={{ zIndex: 2 }}>
				<AppPaper>
					<ClickAwayListener onClickAway={handleClose}>{children}</ClickAwayListener>
				</AppPaper>
			</Popper>
		</Box>
	);
};

export const AppPopoverIconButton = typedMemo(AppPopoverIconButtonFC);

// MENU
export interface IAppMenuIconButtonProps extends Omit<IAppIconButtonProps, "children" | "onClick"> {
	children: React.ReactElement[];
	icon: NonNullable<React.ReactNode>;
}

const AppMenuIconButtonFC: React.FC<IAppMenuIconButtonProps> = (props) => {
	const { children, icon, id, ...buttonProps } = props;

	const [open, setOpen] = React.useState(false);
	const anchorRef = React.useRef<HTMLButtonElement>(null);
	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);

	const togglePopover = React.useCallback(() => {
		setOpen((prevOpen) => !prevOpen);
	}, []);

	const handleClose = React.useCallback((event: React.MouseEvent<Document, MouseEvent>) => {
		if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
			return;
		}
		setOpen(false);
	}, []);

	return (
		<Box>
			<IconButton {...buttonProps} disabled={disabled} ref={anchorRef} onClick={togglePopover}>
				{icon}
			</IconButton>
			<Popper anchorEl={anchorRef.current} open={open} placement="bottom-start" role={undefined} style={{ zIndex: 2 }} transition={false}>
				<AppPaper>
					<ClickAwayListener onClickAway={handleClose}>
						<MenuList>
							{React.Children.map(children, (c, i) =>
								React.cloneElement(c, {
									key: i,
									disabled,
									onClick: (...args: Parameters<React.MouseEventHandler>) => {
										setOpen(false);
										if (c.props.onClick) {
											c.props.onClick(...args);
										}
									},
								})
							)}
						</MenuList>
					</ClickAwayListener>
				</AppPaper>
			</Popper>
		</Box>
	);
};

export const AppMenuIconButton = typedMemo(AppMenuIconButtonFC);
