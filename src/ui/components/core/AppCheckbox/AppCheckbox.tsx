import * as React from "react";
import { Checkbox, CheckboxProps, FormControlLabel, FormControlLabelProps } from "@material-ui/core";

import { calcDisabledWithProps, FormDisableContext } from "../../context/form-disable";

export interface IAppCheckboxProps extends CheckboxProps {
	label: React.ReactNode;
	labelProps?: Omit<FormControlLabelProps, "label">;
}

const AppCheckboxFC: React.FC<IAppCheckboxProps> = (props) => {
	const { label, labelProps, ...checkboxProps } = props;

	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);

	return (
		<FormControlLabel control={<Checkbox {...checkboxProps} disabled={disabled} />} disabled={disabled} label={label} {...labelProps} />
	);
};

export const AppCheckbox = React.memo(AppCheckboxFC);
