import * as React from "react";
import { connect } from "react-redux";
import { ToggleButton } from "@material-ui/lab";
import { ToggleButtonProps } from "@material-ui/lab/ToggleButton";
import { DeepReadonly } from "ts-essentials";

import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { uiStateSelector } from "../../../../store/redux/selectors";
import { AppRootState, UiToggles } from "../../../../store/redux/types/state";
import { calcDisabledWithProps, FormDisableContext } from "../../context/form-disable";

export interface IConnectionButtonProps extends Omit<ToggleButtonProps, "selected"> {
	uiToggleKey: keyof UiToggles;
}

type MappedProps = DeepReadonly<{
	selected: boolean;
}>;

interface IProps extends IConnectionButtonProps, MappedProps, IActionProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IConnectionButtonProps>): MappedProps => {
	return {
		selected: uiStateSelector(state)[ownProps.uiToggleKey] || false,
	};
};

const SettingToggleButtonFC: React.FC<IProps> = (props: IProps) => {
	const { actions, children, onClick, selected, uiToggleKey, ...buttonProps } = props;

	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);
	const onUiToggleClickCb = React.useCallback(
		(e: React.MouseEvent<HTMLButtonElement>) => {
			actions.uiToggle(uiToggleKey);
			if (onClick) {
				onClick(e);
			}
		},
		[actions, onClick, uiToggleKey]
	);

	return (
		<ToggleButton {...buttonProps} disabled={disabled} onClick={onUiToggleClickCb} selected={selected}>
			{children || "Toggle"}
		</ToggleButton>
	);
};

export const SettingToggleButton = connect(mapStateToProps, getDispatchActionMapper())(React.memo(SettingToggleButtonFC));
