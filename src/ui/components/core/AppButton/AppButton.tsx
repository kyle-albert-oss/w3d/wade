import * as React from "react";
import { Button, ButtonGroup, Popper, ClickAwayListener, MenuList, MenuItem, MenuItemProps, Box } from "@material-ui/core";
import { ButtonProps } from "@material-ui/core/Button";
import { ArrowDropDown } from "@material-ui/icons";
import { DeepReadonly } from "ts-essentials";
import tsDefaults from "ts-defaults";
import clsx from "clsx";

import { calcDisabledWithProps, FormDisableContext } from "../../context/form-disable";
import { AppPaper } from "../AppPaper";
import { typedMemo } from "../../../../util/ui";

export interface IAppButtonProps extends ButtonProps {
	innerRef?: React.Ref<HTMLButtonElement>;
}

const AppButtonFC: React.FC<IAppButtonProps> = (props) => {
	const { children, innerRef, ...buttonProps } = props;

	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);

	return (
		<Button {...buttonProps} disabled={disabled} ref={innerRef}>
			{children}
		</Button>
	);
};

export const AppButton = React.memo(AppButtonFC);

//
// SPLIT BUTTON
//
export interface IAppSplitButtonOption<E = never> extends MenuItemProps {
	contents: React.ReactNode;
	extra?: E;
}

export interface IAppSplitButtonProps<E = never> extends Omit<IAppButtonProps, "children" | "onClick"> {
	buttonOptionIndex?: number;
	onClick?(option: DeepReadonly<IAppSplitButtonOption<E>>, index: number, event: React.MouseEvent): void;
	options: IAppSplitButtonOption<E>[];
}

function AppSplitButtonFC<E = never>(_props: IAppSplitButtonProps<E>) {
	const props = tsDefaults(
		_props,
		{
			buttonOptionIndex: 0,
			color: "primary",
			variant: "contained",
		},
		["buttonOptionIndex"]
	);
	const { buttonOptionIndex, color, fullWidth, id, onClick, options, variant } = props;

	const [open, setOpen] = React.useState(false);
	const anchorRef = React.useRef<HTMLDivElement>(null);
	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);

	const handleItemClick = (event: React.MouseEvent, index: number) => {
		setOpen(false);
		if (onClick) {
			onClick(options[index] as DeepReadonly<IAppSplitButtonOption<E>>, index, event);
		}
	};

	const handleToggle = () => {
		setOpen((prevOpen) => !prevOpen);
	};

	const handleClose = (event: React.MouseEvent<Document, MouseEvent>) => {
		if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
			return;
		}

		setOpen(false);
	};

	return (
		<Box className={clsx({ "app-width-full": fullWidth })}>
			<ButtonGroup
				aria-label="split button"
				className={clsx({ "app-width-full": fullWidth })}
				color={color}
				disabled={disabled}
				ref={anchorRef}
				variant={variant}
			>
				<Button className="app-grow" onClick={(e) => handleItemClick(e, buttonOptionIndex)}>
					{options[buttonOptionIndex].contents}
				</Button>
				<Button
					color={color}
					size="small"
					aria-controls={open ? id : undefined}
					aria-expanded={open ? "true" : undefined}
					aria-haspopup="menu"
					onClick={handleToggle}
				>
					<ArrowDropDown />
				</Button>
			</ButtonGroup>
			<Popper open={open} anchorEl={anchorRef.current} placement="bottom-end" role={undefined} style={{ zIndex: 2 }} disablePortal>
				<AppPaper>
					<ClickAwayListener onClickAway={handleClose}>
						<MenuList id={id}>
							{options
								.map(({ contents, ...menuItemProps }, index) =>
									index !== buttonOptionIndex ? (
										// @ts-expect-error
										<MenuItem
											key={index}
											{...(menuItemProps as MenuItemProps)}
											{...(disabled ? { disabled: true } : undefined)}
											onClick={(e) => handleItemClick(e, index)}
										>
											{contents}
										</MenuItem>
									) : null
								)
								.filter((o) => o != null)}
						</MenuList>
					</ClickAwayListener>
				</AppPaper>
			</Popper>
		</Box>
	);
}

export const AppSplitButton = typedMemo(AppSplitButtonFC);

// MENU BUTTON
export interface IAppMenuButtonOption<E = never> extends MenuItemProps {
	contents: React.ReactNode;
	extra?: E;
}

export interface IAppMenuButtonProps<E = never> extends Omit<IAppButtonProps, "onClick"> {
	onClick?(option: DeepReadonly<IAppMenuButtonOption<E>>, index: number, event: React.MouseEvent): void;
	options: IAppMenuButtonOption<E>[];
}

function AppMenuButtonFC<E = never>(_props: IAppMenuButtonProps<E>) {
	const props = tsDefaults(_props, {
		color: "primary",
		endIcon: <ArrowDropDown />,
		variant: "contained",
	});
	const { children, fullWidth, id, onClick, options, ...buttonProps } = props;

	const [open, setOpen] = React.useState(false);
	const anchorRef = React.useRef<HTMLButtonElement>(null);
	const disabled = calcDisabledWithProps(React.useContext(FormDisableContext), props);

	const handleItemClick = (event: React.MouseEvent, index: number) => {
		setOpen(false);
		if (onClick) {
			onClick(options[index] as DeepReadonly<IAppMenuButtonOption<E>>, index, event);
		}
	};

	const handleToggle = () => {
		setOpen((prevOpen) => !prevOpen);
	};

	const handleClose = (event: React.MouseEvent<Document, MouseEvent>) => {
		if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
			return;
		}

		setOpen(false);
	};

	return (
		<Box className={clsx({ "app-width-full": fullWidth })}>
			<AppButton {...buttonProps} fullWidth={fullWidth} innerRef={anchorRef} onClick={handleToggle}>
				{children}
			</AppButton>
			<Popper anchorEl={anchorRef.current} disablePortal open={open} placement="bottom-start" role={undefined} style={{ zIndex: 2 }}>
				<AppPaper>
					<ClickAwayListener onClickAway={handleClose}>
						<MenuList id={id}>
							{options
								.map(({ contents, ...menuItemProps }, index) => (
									// @ts-expect-error
									<MenuItem
										key={index}
										{...(menuItemProps as MenuItemProps)}
										{...(disabled ? { disabled: true } : undefined)}
										onClick={(e) => handleItemClick(e, index)}
									>
										{contents}
									</MenuItem>
								))
								.filter((o) => o != null)}
						</MenuList>
					</ClickAwayListener>
				</AppPaper>
			</Popper>
		</Box>
	);
}

export const AppMenuButton = typedMemo(AppMenuButtonFC);
