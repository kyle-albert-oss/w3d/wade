import * as React from "react";
import { Add, Delete } from "@material-ui/icons";
import { ListItemIcon, ListItemText } from "@material-ui/core";
import { select } from "@storybook/addon-knobs";

import { createStory } from "../../../../../stories/story-util";

import { AppButton, AppMenuButton, AppSplitButton } from "./AppButton";

createStory("Core|AppButton", module)
	.add("Default", () => <AppButton>Test Button</AppButton>)
	.add("Menu Button", () => (
		<AppMenuButton
			color={select("Color", ["default", "primary", "secondary"], "primary")}
			options={[
				{
					contents: (
						<>
							<ListItemIcon>
								<Add />
							</ListItemIcon>
							<ListItemText primary={"Add"} />
						</>
					),
				},
			]}
			variant={select("Variant", ["contained", "outlined", "text"], "contained")}
		>
			<Add />
		</AppMenuButton>
	))
	.add("Split Button", () => (
		<AppSplitButton
			color={select("Color", ["default", "primary", "secondary"], "primary")}
			options={[
				{
					contents: <Add />,
				},
				{
					contents: (
						<>
							<ListItemIcon>
								<Delete />
							</ListItemIcon>
							<ListItemText primary="Delete" />
						</>
					),
				},
			]}
			variant={select("Variant", ["contained", "outlined", "text"], "contained")}
		></AppSplitButton>
	));
