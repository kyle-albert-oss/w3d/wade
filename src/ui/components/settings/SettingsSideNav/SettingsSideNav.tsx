import * as React from "react";
import { Typography, Grid, Divider } from "@material-ui/core";

import { ProjectSettingsList } from "../ProjectSettingsList";
import { GlobalSettingsList } from "../GlobalSettingsList";

const SettingsSideNavFC: React.FC = () => {
	return (
		<Grid container className="app-height-full" direction="column">
			<Grid item style={{ flexBasis: "50%" }}>
				<Grid container className="app-height-full" direction="column" spacing={1}>
					<Grid item>
						<Typography variant="subtitle1">Project Settings</Typography>
					</Grid>
					<Grid item>
						<Divider />
					</Grid>
					<Grid item xs>
						<ProjectSettingsList />
					</Grid>
				</Grid>
			</Grid>
			<Grid item style={{ flexBasis: "50%" }}>
				<Grid container className="app-height-full" direction="column" spacing={1}>
					<Grid item>
						<Typography variant="subtitle1">Global Settings</Typography>
					</Grid>
					<Grid item>
						<Divider />
					</Grid>
					<Grid item xs>
						<GlobalSettingsList />
					</Grid>
				</Grid>
			</Grid>
		</Grid>
	);
};

export const SettingsSideNav = React.memo(SettingsSideNavFC);
