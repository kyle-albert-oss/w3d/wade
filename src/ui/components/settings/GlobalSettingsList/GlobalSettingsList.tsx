import * as React from "react";
import { Tabs, useTheme } from "@material-ui/core";
import { useLocation } from "react-router";

import { AppTab } from "../../core/AppTab";
import { useActions } from "../../../../store/redux/actions";

const GLOBAL_SETTINGS_BASE_PATH = "/settings/global";

const TABS = [
	{
		label: "Color Presets",
		subRoute: "color-presets",
	},
	{
		label: "Theme",
		subRoute: "theme",
	},
];

const GlobalSettingsListFC: React.FC = () => {
	const actions = useActions();
	const theme = useTheme();
	const { pathname: path } = useLocation();

	const [selectedTab, setSelectedTab] = React.useState<number>(0);
	React.useEffect(() => {
		const tabIndex = TABS.findIndex((t) => path.startsWith(`${GLOBAL_SETTINGS_BASE_PATH}/${t.subRoute}`));
		setSelectedTab(tabIndex !== -1 ? tabIndex + 1 : 0);
	}, [path]);
	const tabsUI = React.useMemo(
		() =>
			TABS.map((t) => (
				<AppTab key={t.subRoute} dense label={t.label} onClick={() => actions.updateRoute(`${GLOBAL_SETTINGS_BASE_PATH}/${t.subRoute}`)} />
			)),
		[actions]
	);

	return (
		<Tabs orientation="vertical" style={{ marginLeft: -theme.spacing(1), marginRight: -theme.spacing(1) }} value={selectedTab}>
			<AppTab className="app-hide" label="Default" />
			{tabsUI}
		</Tabs>
	);
};

export const GlobalSettingsList = React.memo(GlobalSettingsListFC);
