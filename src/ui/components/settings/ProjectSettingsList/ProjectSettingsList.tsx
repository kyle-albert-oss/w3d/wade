import * as React from "react";
import { Tabs, useTheme } from "@material-ui/core";
import { useLocation } from "react-router";

import { AppTab } from "../../core/AppTab";
import { useActions } from "../../../../store/redux/actions";

export interface IProjectSettingsListProps {}
const PROJECT_SETTINGS_BASE_PATH = "/settings/project";

const TABS = [
	{
		label: "General",
		subRoute: "general",
	},
	{
		label: "Maps",
		subRoute: "maps",
	},
	{
		label: "Map Definition",
		subRoute: "map-def",
	},
	{
		label: "Palette",
		subRoute: "palette",
	},
];

const ProjectSettingsListFC: React.FC<IProjectSettingsListProps> = (props) => {
	const actions = useActions();
	const theme = useTheme();
	const { pathname: path } = useLocation();

	const [selectedTab, setSelectedTab] = React.useState<number>(0);
	React.useEffect(() => {
		const tabIndex = TABS.findIndex((t) => path.startsWith(`${PROJECT_SETTINGS_BASE_PATH}/${t.subRoute}`));
		setSelectedTab(tabIndex !== -1 ? tabIndex + 1 : 0);
	}, [path]);
	const tabsUI = React.useMemo(
		() =>
			TABS.map((t) => (
				<AppTab key={t.subRoute} dense label={t.label} onClick={() => actions.updateRoute(`${PROJECT_SETTINGS_BASE_PATH}/${t.subRoute}`)} />
			)),
		[actions]
	);

	return (
		<Tabs orientation="vertical" style={{ marginLeft: -theme.spacing(1), marginRight: -theme.spacing(1) }} value={selectedTab}>
			<AppTab className="app-hide" label="Default" />
			{tabsUI}
		</Tabs>
	);
};

export const ProjectSettingsList = React.memo(ProjectSettingsListFC);
