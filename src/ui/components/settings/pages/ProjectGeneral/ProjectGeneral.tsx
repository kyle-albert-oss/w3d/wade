import * as React from "react";
import { useSelector } from "react-redux";
import { Box } from "@material-ui/core";

import { AppForm } from "../../../core/AppForm";
import { activeProjectSelector } from "../../../../../store/redux/selectors";
import { ProjectForm } from "../../../project/ProjectForm";
import { SettingsPanel } from "../../SettingsPanel";
import { GameProject } from "../../../../../models/project";
import { useActions } from "../../../../../store/redux/actions";
import { UseFormObjOnUpdate } from "../../../hooks";

export interface IProjectGeneralProps {}

const ProjectGeneralFC: React.FC<IProjectGeneralProps> = (props) => {
	const activeProject = useSelector(activeProjectSelector);
	if (!activeProject) {
		throw new Error("No active project.");
	}

	const { updateActiveProject } = useActions();
	const onUpdateCb = React.useCallback<UseFormObjOnUpdate<GameProject>>(
		(update) => {
			updateActiveProject({ project: update });
		},
		[updateActiveProject]
	);

	return (
		<SettingsPanel header="General">
			<AppForm formId="edit-project" navBlocking onSubmit={(data) => console.log(data)}>
				<Box width={600}>
					<ProjectForm initialProject={activeProject} onUpdate={onUpdateCb} />
				</Box>
			</AppForm>
		</SettingsPanel>
	);
};

export const ProjectGeneral = React.memo(ProjectGeneralFC);
