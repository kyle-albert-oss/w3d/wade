import * as React from "react";
import { useSelector } from "react-redux";
import { DeepReadonly } from "ts-essentials";
import { ValueType } from "react-select";
import { Typography, Grid, InputAdornment, Box } from "@material-ui/core";

import { useActions } from "../../../../../store/redux/actions";
import { activeMapDefPlanesSelector } from "../../../../../store/redux/selectors";
import { MapPlaneDefinition, PlaneType } from "../../../../../models/map";
import { IOption, EnumAutoComplete } from "../../../core/AutoComplete";
import { AppTextField } from "../../../core/AppTextField";
import { Hint } from "../../../core/Hint";
import { AppCheckbox } from "../../../core/AppCheckbox";

export interface IMapDefPlaneFormProps {
	plane: number;
}

export const MapDefPlaneForm = React.memo<IMapDefPlaneFormProps>((props) => {
	const { plane: planeIdx } = props;

	const actions = useActions();
	const planes = useSelector(activeMapDefPlanesSelector);
	const plane = planes?.[planeIdx] ?? ({} as DeepReadonly<MapPlaneDefinition>);
	const { defaultToHighLowByteMode = false, index, name, type, zIndex = "" } = plane ?? {};

	const onNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		actions.updateActiveMapDefPlaneMeta({ name: e.currentTarget.value }, { index: planeIdx });
	};
	const onNameBlur = () => {
		if (name == null || name.trim() === "") {
			actions.updateActiveMapDefPlaneMeta({ name: `Plane ${planeIdx + 1}` }, { index: planeIdx });
		}
	};
	const onTypeChange = (val: ValueType<IOption<PlaneType>>) => {
		const { value: newType } = val as IOption<PlaneType>;
		actions.updateActiveMapDefPlaneMeta({ type: newType }, { index: planeIdx });
	};
	const onDefaultToHighLowByteModeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		actions.updateActiveMapDefPlaneMeta({ defaultToHighLowByteMode: e.currentTarget.checked }, { index: planeIdx });
	};
	const onZIndexChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const newZIndex = e.currentTarget.value === "" ? undefined : e.currentTarget.valueAsNumber;
		actions.updateActiveMapDefPlaneMeta({ zIndex: newZIndex }, { index: planeIdx });
	};

	if (index == null) {
		return <Typography>Plane {planeIdx} not found.</Typography>;
	}

	return (
		<Grid container direction="column" spacing={2}>
			<Grid item>
				<AppTextField fullWidth label="Name" onBlur={onNameBlur} onChange={onNameChange} value={name} data-id="name" />
			</Grid>
			<Grid item>
				<EnumAutoComplete isClearable={false} label="Type" onChange={onTypeChange} options={PlaneType} value={type} />
			</Grid>
			<Grid item>
				<AppTextField
					fullWidth
					InputProps={{
						endAdornment: (
							<InputAdornment position="end">
								<Box width={24} mr={1}>
									<Hint>
										The order the plane is drawn in editor relative to other planes. Planes without a value will be rendered before ones
										with a value.
										<br />
										If two planes do not have a z index value, the one with the greater index will be drawn over the other.
									</Hint>
								</Box>
							</InputAdornment>
						),
					}}
					inputProps={{ min: 0 }}
					label="Z Index"
					onChange={onZIndexChange}
					type="number"
					value={zIndex}
					data-id="zindex"
				/>
			</Grid>
			<Grid item>
				<AppCheckbox onChange={onDefaultToHighLowByteModeChange} label={"Default to High/Low Byte Mode"} value={defaultToHighLowByteMode} />
			</Grid>
		</Grid>
	);
});
