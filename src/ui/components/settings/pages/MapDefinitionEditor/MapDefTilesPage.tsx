import * as React from "react";
import { useSelector } from "react-redux";
import { DeepReadonly } from "ts-essentials";
import _ from "lodash";
import { Add, FileCopy, Delete } from "@material-ui/icons";
import { Box, Typography, Grid } from "@material-ui/core";

import { MapTileData, TileType } from "../../../../../models/map";
import { IAppSplitButtonOption, AppSplitButton } from "../../../core/AppButton";
import {
	selectedEditTileSelector,
	selectedPlaneNumSelector,
	activeMapDefMaxTileCodeForSelectedPlaneSelector,
	activeMapDefTileFnSelector,
} from "../../../../../store/redux/selectors";
import { useActions } from "../../../../../store/redux/actions";
import { AppIconButton, AppIconButtonWithConfirmation } from "../../../core/AppIconButton";
import { SettingsPanel } from "../../SettingsPanel";
import { MapDefPanel } from "../../../mapping/MapDefPanel";

import { MapDefinitionTileEditor } from "./MapDefinitionTileEditor";

type AddPrefabData = { prefab: Partial<MapTileData> };
type AddOpts = IAppSplitButtonOption<AddPrefabData>[];

export const MapDefTilesPage = React.memo(() => {
	const actions = useActions();
	const selectedEditTile = useSelector(selectedEditTileSelector);
	const selectedPlane = useSelector(selectedPlaneNumSelector);
	const maxTileCodeForPlane = useSelector(activeMapDefMaxTileCodeForSelectedPlaneSelector);
	const tileFn = useSelector(activeMapDefTileFnSelector);
	const selectedTilePlane = selectedEditTile?.plane;
	const selectedTileCode = selectedEditTile?.tileCode;
	const tile = tileFn(selectedTilePlane, selectedTileCode);

	const onOptClick = React.useCallback(
		(opt: DeepReadonly<IAppSplitButtonOption<AddPrefabData>>) => {
			const data = _.cloneDeep(opt?.extra?.prefab) as MapTileData;
			actions.createActiveMapDefTile({ ...data, code: maxTileCodeForPlane + 1, label: data.label ?? "New Tile", plane: selectedPlane });
		},
		[actions, maxTileCodeForPlane, selectedPlane]
	);

	const addOpts = React.useMemo<AddOpts>(() => {
		const opts: AddOpts = [
			{
				contents: (
					<>
						<Add />
						&nbsp;Add
					</>
				),
				extra: { prefab: {} },
			},
		];

		if (selectedPlane === 0) {
			opts.push(
				{
					contents: "Add Wall",
					extra: { prefab: { label: "New Wall", types: { [TileType.SOLID]: 1, [TileType.WALL]: 1 } } },
				},
				{
					contents: "Add Door",
					extra: { prefab: { label: "New Door", types: { [TileType.DOOR]: 1, [TileType.SOLID]: 1 } } },
				},
				{
					contents: "Add Floor Code",
					extra: { prefab: { label: "New Floor Code", types: { [TileType.FLOOR_CODE]: 1 } } },
				}
			);
		} else if (selectedPlane === 1) {
			opts.push(
				{
					contents: "Add Static Item",
					extra: { prefab: { label: "New Item", types: { [TileType.STATIC]: 1 } } },
				},
				{
					contents: "Add Static Solid Item",
					extra: { prefab: { label: "New Item", types: { [TileType.SOLID]: 1, [TileType.STATIC]: 1 } } },
				},
				{
					contents: "Add Bonus Item",
					extra: { prefab: { label: "New Item", types: { [TileType.BONUS]: 1, [TileType.STATIC]: 1 } } },
				},
				{
					contents: "Add Enemy",
					extra: { prefab: { difficulty: 1, label: "New Enemy", types: { [TileType.ACTOR]: 1 } } },
				},
				{
					contents: "Add Boss",
					extra: { prefab: { difficulty: 1, label: "New Boss", types: { [TileType.BOSS]: 1, [TileType.ACTOR]: 1 } } },
				}
			);
		}
		return opts;
	}, [selectedPlane]);

	const onTileCopyClick = React.useCallback(() => {
		(async () => {
			const currentTile = tile! as MapTileData;
			const newCode = maxTileCodeForPlane + 1;
			await actions.createActiveMapDefTile({ ...currentTile, code: newCode });
			actions.uiUpdate({ selectedEditTile: { plane: currentTile.plane, tileCode: newCode } });
		})();
	}, [actions, maxTileCodeForPlane, tile]);

	const onTileDeleteClick = React.useCallback(() => {
		actions.deleteActiveMapDefTile([{ plane: selectedTilePlane!, tileCode: selectedTileCode! }]);
	}, [actions, selectedTilePlane, selectedTileCode]);

	const tileEditorHeader: React.ReactNode = (
		<Box display="flex" justifyContent="space-between">
			<Typography variant="subtitle1">Tile {selectedTileCode}</Typography>
			{tile && selectedTileCode !== 0 && (
				<Box display="flex">
					<AppIconButton onClick={onTileCopyClick} size="small">
						<FileCopy />
					</AppIconButton>
					<AppIconButtonWithConfirmation confirmation="Are you sure you want to delete this tile?" onClick={onTileDeleteClick} size="small">
						<Delete />
					</AppIconButtonWithConfirmation>
				</Box>
			)}
		</Box>
	);

	return (
		<Grid container className="app-height-full" wrap="nowrap">
			<Grid item className="app-height-full" style={{ width: 300 }}>
				<SettingsPanel header="Tiles" variant="grid-items">
					<Grid item className="app-grow">
						<MapDefPanel />
					</Grid>
					<Grid item>
						<AppSplitButton<AddPrefabData> color="default" fullWidth onClick={onOptClick} options={addOpts} variant="text" />
					</Grid>
				</SettingsPanel>
			</Grid>
			<Grid item className="app-height-full" xs>
				<Box className="app-height-full" pl={1}>
					<SettingsPanel header={tileEditorHeader}>
						<MapDefinitionTileEditor plane={selectedTilePlane} tileCode={selectedTileCode} />
					</SettingsPanel>
				</Box>
			</Grid>
		</Grid>
	);
});
