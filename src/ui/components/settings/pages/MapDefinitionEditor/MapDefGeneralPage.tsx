import * as React from "react";
import { useSelector } from "react-redux";
import { Grid, Box, Typography, List, ListItemText } from "@material-ui/core";
import { Delete, Add } from "@material-ui/icons";

import { useActions } from "../../../../../store/redux/actions";
import { activeMapDefSelector, selectedPlaneNumSelector } from "../../../../../store/redux/selectors";
import { WadeMapDefinition } from "../../../../../models/map-definition";
import { toNumber } from "../../../../../util/numbers";
import { MapPlaneDefinition, PlaneType } from "../../../../../models/map";
import { useTransactionalDragHandlers } from "../../../hooks";
import { AppTextField } from "../../../core/AppTextField";
import { AppPaper } from "../../../core/AppPaper";
import { DraggableListItem } from "../../../core/AppListItem";
import { AppIconButtonWithConfirmation } from "../../../core/AppIconButton";
import { disableFormsIf } from "../../../context/form-disable";
import { AppButton } from "../../../core/AppButton";

import { MapDefPlaneForm } from "./MapDefPlaneForm";

const planeExceptedFormIds = new Set<string>(["name", "zindex"]);

export const MapDefGeneralPage = React.memo(() => {
	const actions = useActions();
	const editMapDef = useSelector(activeMapDefSelector);
	const selectedPlaneIndex = useSelector(selectedPlaneNumSelector);
	const { name, planes } = editMapDef ?? ({} as WadeMapDefinition);

	const itemsRef = React.useRef<React.RefObject<HTMLDivElement>[]>([]);
	const planeKeys = planes ? Object.keys(planes) : [];
	React.useEffect(() => {
		if (planeKeys.length <= itemsRef.current.length) {
			itemsRef.current = itemsRef.current.slice(0, planeKeys.length);
		} else {
			const deficit = planeKeys.length - itemsRef.current.length;
			const newRefs: React.RefObject<HTMLDivElement>[] = [];
			for (let i = 0; i < deficit; i++) {
				newRefs.push(React.createRef());
			}
			itemsRef.current = [...itemsRef.current, ...newRefs];
		}
	}, [planeKeys.length]);

	const onNameChangeCb = (e: React.ChangeEvent<HTMLInputElement>) => {
		actions.updateActiveMapDef({ name: e.currentTarget.value });
	};
	const onAddPlaneClick = () => {
		const nextPlaneIndex = Math.max(...Object.keys(planes).map((k) => toNumber(k))) + 1;
		const newPlane: MapPlaneDefinition = {
			index: nextPlaneIndex,
			name: `Plane ${nextPlaneIndex}`,
			tiles: {},
			tilesOrdering: [],
			type: PlaneType.DEFAULT,
		};
		actions.createActiveMapDefPlane(newPlane);
	};
	const dragHandlers = useTransactionalDragHandlers((dragIndex, hoverIndex) => {
		actions.updateActiveMapDefPlaneMeta({}, { index: dragIndex, moveToIndex: hoverIndex });
	});

	if (!editMapDef) {
		return <></>;
	}

	return (
		<Grid container direction="column" spacing={2}>
			<Grid item>
				<AppTextField label="Name" onChange={onNameChangeCb} value={name} />
			</Grid>
			<Grid item>
				<AppPaper translucent>
					<Box p={2}>
						<Box pb={1}>
							<Typography variant="subtitle2">Planes</Typography>
						</Box>
						<Grid container spacing={2} wrap="nowrap">
							<Grid item>
								<List dense style={{ width: 300 }}>
									{planeKeys
										.map((k) => toNumber(k))
										.sort((i1, i2) => i1 - i2)
										.map((planeIdx, idx, planeIdxs) => {
											const plane = planes[planeIdx];
											const readOnly = planeIdx === 0 || planeIdx === 1;
											const deleteDisabled = planeIdx !== planeIdxs.length - 1 || readOnly;
											return (
												<DraggableListItem
													key={plane.name}
													button
													dragDisabled={readOnly}
													dragId={plane.name}
													dragIndex={idx}
													onClick={() => actions.uiUpdate({ selectedPlaneNum: planeIdx })}
													{...dragHandlers}
													selected={selectedPlaneIndex === planeIdx}
												>
													<ListItemText primary={plane.name} primaryTypographyProps={{ display: "block", noWrap: true }} />
													<div>
														<AppIconButtonWithConfirmation
															confirmation={
																<Typography display="inline" variant="body2">
																	Deleting this plane is not reversible and will delete all of its tiles.
																	<br />
																	Are you sure you want to delete this plane?
																</Typography>
															}
															disabled={deleteDisabled}
															onClick={() => actions.deleteActiveMapDefPlane(planeIdx)}
															p={1}
														>
															<Delete />
														</AppIconButtonWithConfirmation>
													</div>
												</DraggableListItem>
											);
										})}
								</List>
							</Grid>
							<Grid item>
								{disableFormsIf(
									{ disabled: selectedPlaneIndex <= 1, exceptedElementIds: planeExceptedFormIds },
									<MapDefPlaneForm plane={selectedPlaneIndex} />
								)}
							</Grid>
						</Grid>
						<Box p={1}>
							<AppButton onClick={onAddPlaneClick} startIcon={<Add />}>
								Add
							</AppButton>
						</Box>
					</Box>
				</AppPaper>
			</Grid>
		</Grid>
	);
});
