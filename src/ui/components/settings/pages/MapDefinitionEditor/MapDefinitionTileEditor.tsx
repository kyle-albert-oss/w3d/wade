import * as PIXI from "pixi.js";
import * as React from "react";
import { useMemo } from "react";
import { Box, Card, Divider, FormHelperText, Grid, ListItemIcon, ListItemText, Typography } from "@material-ui/core";
import { useSelector, useStore } from "react-redux";
import { DeepReadonly, SafeDictionary } from "ts-essentials";
import _ from "lodash";
import InputAdornment from "@material-ui/core/InputAdornment";
import { Save } from "@material-ui/icons";
import { ToggleButton, ToggleButtonGroup } from "@material-ui/lab";
import { ColorResult } from "react-color";
import { Container, Graphics, Stage } from "@inlet/react-pixi";
import chroma from "chroma-js";
import tsDefaults from "ts-defaults";
import CardContent from "@material-ui/core/CardContent";
import { ValueType } from "react-select";

import { AppTextField } from "../../../core/AppTextField";
import { GameSprite } from "../../../graphics/GameSprite";
import {
	activeMapDefAllTagsForSelectedPlaneSelector,
	activeMapDefAllTileCodesForSelectedPlaneSelector,
	activeMapDefDifficultiesSelector,
	activeMapDefTileFnSelector,
	colorPresetsSelector,
	graphicChunksSelector,
	spriteStartIndexSelector,
} from "../../../../../store/redux/selectors";
import { GraphicMethod, MapTileData, TileType } from "../../../../../models/map";
import { useActions } from "../../../../../store/redux/actions";
import { AutoComplete, EnumAutoComplete, IOption, StringAutoComplete } from "../../../core/AutoComplete";
import { AppIconButton } from "../../../core/AppIconButton";
import { ColorPickerField, ColorPresets } from "../../../core/ColorPicker";
import { PageSlider } from "../../../core/PageSlider";
import FileSelect from "../../../core/FileSelect";
import { readFileAsDataUrl } from "../../../../../util/files";
import { Centered } from "../../../core/Centered";
import { Arrays } from "../../../../../util/collections";
import { Hint } from "../../../core/Hint";
import { AppMenuButton, IAppMenuButtonOption } from "../../../core/AppButton";
import { Strings, compareIgnoreCase } from "../../../../../util/strings";
import { MapDefinitionDifficulty } from "../../../../../models/map-definition";
import { undefIfBlank } from "../../../../../util/objects";
import { DEFAULT_MAP_TEXTURE_OPTS } from "../../../../../models/pixi";

type ColorData = NonNullable<MapTileData["gridImage"]>;

export interface IInteractivePixelGridProps {
	color?: string | null;
	colorData: ColorData | undefined;
	forceCanvas?: boolean;
	onChange?(colorData: (string | null)[]): void;
	onColorSiphon?(color: string | null, which: "primary" | "secondary"): void;
	pixelsPerTile?: number;
	secondaryColor?: string | null;
	size: number;
}

export const InteractivePixelGrid: React.FC<IInteractivePixelGridProps> = (_props) => {
	const props = tsDefaults(
		_props,
		{
			pixelsPerTile: 16,
		},
		["pixelsPerTile"]
	);
	const { color = null, colorData, forceCanvas, onChange, onColorSiphon, pixelsPerTile, secondaryColor, size } = props;
	const total = size * size;
	const pixels = size * pixelsPerTile;
	const [painting, setPainting] = React.useState(false);
	const [rightClick, setRightClick] = React.useState(false);
	const [lastIdx, setLastIdx] = React.useState<number | undefined>();

	const onMouseDownCb = React.useCallback((e: PIXI.InteractionEvent) => {
		setPainting(true);
		if (e.type === "rightdown") {
			setRightClick(true);
		}
	}, []);
	const onClickCb = React.useCallback(
		(e: PIXI.InteractionEvent) => {
			if (!onChange || (e.type === "mousemove" && !painting)) {
				return;
			}
			const { x, y } = e.data.global;
			const idx = Math.floor(x / pixelsPerTile) + size * Math.floor(y / pixelsPerTile);
			if (idx === lastIdx && painting) {
				return;
			}

			let newColorData: IInteractivePixelGridProps["colorData"];
			if (!colorData) {
				newColorData = Array(total).fill(null);
			} else if (colorData.length < total) {
				newColorData = [...colorData];
				for (let i = 0; i < total - colorData.length; i++) {
					newColorData.push(null);
				}
			} else {
				newColorData = [...colorData];
			}
			const isRightClick = e.type === "rightclick" || rightClick;
			const hasAltKey = e.data.originalEvent.altKey;
			const hasShiftKey = e.data.originalEvent.shiftKey;
			const hasCtrlKey = e.data.originalEvent.ctrlKey;

			let newColor = isRightClick ? secondaryColor ?? null : color;
			if (newColor != null && (newColor === "transparent" || chroma(newColor).num() === 0)) {
				// treat black as null, add transparent check because color picker lib doesn't support null value
				newColor = null;
			}

			if (hasAltKey && onColorSiphon) {
				onColorSiphon(newColorData[idx], isRightClick ? "secondary" : "primary");
				return;
			} else if (hasShiftKey) {
				newColorData = Arrays.replaceFill2D(newColorData, idx, size, newColor);
			} else if (hasCtrlKey) {
				newColorData = Arrays.replace(newColorData, newColorData[idx], newColor);
			} else {
				newColorData.splice(idx, 1, newColor);
			}

			setLastIdx(idx);
			onChange(newColorData);
		},
		[color, colorData, lastIdx, onChange, onColorSiphon, painting, pixelsPerTile, rightClick, secondaryColor, size, total]
	);
	const onMouseUpCb = React.useCallback((e: PIXI.InteractionEvent) => {
		setPainting(false);
		setRightClick(false);
	}, []);

	const drawGraphics = React.useCallback(
		(g: PIXI.Graphics) => {
			g.clear();
			for (let i = 0; i < total; i++) {
				const clr = colorData?.[i] ?? "black";
				const x = (i % size) * pixelsPerTile;
				const y = Math.floor(i / size) * pixelsPerTile;
				g.beginFill(chroma(clr).num());
				g.drawRect(x, y, pixels, pixels);
				g.endFill();
			}
		},
		[colorData, pixels, pixelsPerTile, size, total]
	);

	const onContextMenuCb = React.useCallback((e: React.MouseEvent<HTMLCanvasElement>) => e.preventDefault(), []);

	return (
		<Stage height={pixels} width={pixels} onContextMenu={onContextMenuCb} options={{ forceCanvas }}>
			<Container
				interactive
				click={onClickCb}
				mousedown={onMouseDownCb}
				mousemove={onClickCb}
				mouseup={onMouseUpCb}
				rightclick={onClickCb}
				rightdown={onMouseDownCb}
				rightup={onMouseUpCb}
				mouseout={onMouseUpCb}
			>
				<Graphics draw={drawGraphics} />
			</Container>
		</Stage>
	);
};

export interface IPixelGridProps {
	colorData: (string | null)[] | undefined;
	forceCanvas?: boolean;
	pixelsPerTile?: number;
	size: number;
}

export const PixelGrid: React.FC<IPixelGridProps> = (_props) => {
	const props = tsDefaults(
		_props,
		{
			forceCanvas: true,
			pixelsPerTile: 16,
		},
		["pixelsPerTile"]
	);
	const { colorData, forceCanvas, pixelsPerTile, size } = props;
	const total = size * size;
	const pixels = size * pixelsPerTile;

	const drawGraphics = React.useCallback(
		(g: PIXI.Graphics) => {
			g.clear();
			for (let i = 0; i < total; i++) {
				const clr = colorData?.[i] ?? "black";
				const x = (i % size) * pixelsPerTile;
				const y = Math.floor(i / size) * pixelsPerTile;
				g.beginFill(chroma(clr).num());
				g.drawRect(x, y, pixels, pixels);
				g.endFill();
			}
		},
		[colorData, pixels, pixelsPerTile, size, total]
	);

	return (
		<Stage height={pixels} width={pixels} options={{ forceCanvas }}>
			<Container>
				<Graphics draw={drawGraphics} />
			</Container>
		</Stage>
	);
};

export interface IMapDefinitionTileEditorProps {
	plane?: number;
	tileCode?: number;
}
type DifficultyVal = MapDefinitionDifficulty & { index: number };

const MapDefinitionTileEditorFC: React.FC<IMapDefinitionTileEditorProps> = (props) => {
	const { plane, tileCode } = props;
	const actions = useActions();
	const store = useStore();
	const tileFn = useSelector(activeMapDefTileFnSelector);
	const colorPresets = useSelector(colorPresetsSelector);
	const tile = React.useMemo<DeepReadonly<MapTileData> | undefined>(() => tileFn(plane, tileCode), [plane, tileCode, tileFn]);
	const numGraphicChunks = useSelector(graphicChunksSelector)?.length ?? 0;
	const planeTags = useSelector(activeMapDefAllTagsForSelectedPlaneSelector);
	const difficulties = useSelector(activeMapDefDifficultiesSelector);
	const spriteStartIndex = useSelector(spriteStartIndexSelector);
	const [liveTileCode, setLiveTileCode] = React.useState(tileCode);
	const [gridImageColor, setGridImageColor] = React.useState<string | null>(null);
	const [gridImageSecondaryColor, setGridImageSecondaryColor] = React.useState<string | null>(null);
	const difficultyOptions = React.useMemo<IOption<DifficultyVal>[]>(() => {
		return difficulties.map((d, i) => ({ label: `${i} - ${d.name}`, value: { ...d, index: i } }));
	}, [difficulties]);
	const size = 7;

	const [form, setForm] = React.useState<MapTileData | undefined>();
	// reset when the tile changes, only works if we immediately commit changes to the redux store
	React.useEffect(() => {
		setLiveTileCode(tile?.code);
		setForm(tile != null ? (_.cloneDeep(tile) as MapTileData) : undefined);
	}, [tile]);

	const onLabelChangeCb = React.useCallback(
		(e: React.ChangeEvent<HTMLInputElement>) => {
			actions.updateActiveMapDefTile({ label: e.currentTarget.value ?? "" }, { code: tileCode!, plane: plane! });
		},
		[actions, plane, tileCode]
	);
	const onLiveTileCodeChangeCb = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
		setLiveTileCode(e.currentTarget.valueAsNumber);
	}, []);
	const onLiveTileCodeSubmitCb = React.useCallback(() => {
		const existingTileCodes = activeMapDefAllTileCodesForSelectedPlaneSelector(store.getState());
		if (
			plane == null ||
			liveTileCode == null ||
			liveTileCode <= 0 ||
			(liveTileCode !== tileCode && existingTileCodes?.has?.(liveTileCode))
		) {
			setLiveTileCode(tileCode);
			return;
		}
		actions.updateActiveMapDefTile({ code: liveTileCode }, { code: tileCode!, plane: plane! });
		actions.uiUpdate({ selectedEditTile: { plane, tileCode: liveTileCode } });
	}, [store, liveTileCode, tileCode, actions, plane]);
	const onGraphicMethodChangeCb = React.useCallback(
		(event: React.MouseEvent<HTMLElement>, newMethod: GraphicMethod) => {
			actions.updateActiveMapDefTile({ graphicMethod: newMethod }, { code: tileCode!, plane: plane! });
		},
		[actions, plane, tileCode]
	);
	const onImageGridChangeCb = React.useCallback(
		(colorData: (string | null)[]) => {
			actions.updateActiveMapDefTile({ gridImage: colorData }, { code: tileCode!, plane: plane! });
		},
		[actions, plane, tileCode]
	);
	const onGridImageColorChangeCb = React.useCallback((color: ColorResult | string | null) => {
		setGridImageColor(typeof color === "string" ? color : color?.hex ?? null);
	}, []);
	const onGridImageSecondaryColorChangeCb = React.useCallback((color: ColorResult | string | null) => {
		setGridImageSecondaryColor(typeof color === "string" ? color : color?.hex ?? null);
	}, []);
	const onImageGridSiphonCb = React.useCallback((color: string | null, which: "primary" | "secondary") => {
		if (which === "secondary") {
			setGridImageSecondaryColor(color);
		} else {
			setGridImageColor(color);
		}
	}, []);
	const onGraphicIndexChangeCb = React.useCallback(
		(newGraphicIndex: number) => {
			actions.updateActiveMapDefTile({ graphicIndex: newGraphicIndex }, { code: tileCode!, plane: plane! });
		},
		[actions, plane, tileCode]
	);
	const acceptFileCb = React.useCallback((f: File) => f.name.toLowerCase().endsWith(".png"), []);
	const onDropCb = React.useCallback(
		(acceptedFiles: File[]) => {
			(async () => {
				const dataUrl = await readFileAsDataUrl(acceptedFiles[0]);
				PIXI.Texture.from(dataUrl, DEFAULT_MAP_TEXTURE_OPTS);
				actions.updateActiveMapDefTile({ customImage: dataUrl }, { code: tileCode!, plane: plane! });
			})();
		},
		[actions, plane, tileCode]
	);
	const onFileClearedCb = React.useCallback(() => {
		actions.updateActiveMapDefTile({ customImage: undefined }, { code: tileCode!, plane: plane! });
	}, [actions, plane, tileCode]);

	const gridPresets = React.useMemo<IAppMenuButtonOption<ColorData>[]>(() => {
		const atmp = chroma((gridImageColor === "transparent" ? undefined : gridImageColor) || "black");
		const a = atmp.num() === 0 ? null : atmp.hex();
		const btmp = chroma((gridImageSecondaryColor === "transparent" ? undefined : gridImageSecondaryColor) || "black");
		const b = btmp.num() === 0 ? null : btmp.hex();

		const len = size * size;
		const newArray = () => Array(len).fill(null);
		const newSolidArray = () => Array(len).fill(a);
		const newCheckerboardArray = (altColor: string | null) => {
			const arr = newSolidArray();
			for (let row = 0; row < size; row++) {
				let d = row % 2 !== 0;
				for (let col = 0; col < size; col += 2) {
					const i = row * size + col;
					if (d) {
						arr[i] = altColor;
						if (i + 1 < len) {
							arr[i + 1] = altColor;
						}
					}
					d = !d;
				}
			}
			return arr;
		};
		const addDot = (arr: ColorData, dotColor: string | null) => {
			const dotSize = Math.floor(size / 2);
			for (let dotX = 0; dotX < dotSize; dotX++) {
				for (let dotY = 0; dotY < dotSize; dotY++) {
					const realDotX = dotSize + dotX - 1;
					const realDotY = (dotSize + dotY - 1) * size;
					const idx = realDotX + realDotY;
					arr[idx] = dotColor;
				}
			}
			return arr;
		};

		const newPresets: IAppMenuButtonOption<ColorData>[] = [];
		let colorData: ColorData;

		// solid
		colorData = newArray().fill(gridImageColor);
		newPresets.push({
			contents: (
				<>
					<ListItemIcon>
						<PixelGrid colorData={colorData} pixelsPerTile={3} size={size} />
					</ListItemIcon>
					<ListItemText primary="Solid" />
				</>
			),
			extra: colorData,
		});

		// solid w/ dot
		colorData = newSolidArray();
		colorData = addDot(colorData, b);
		newPresets.push({
			contents: (
				<>
					<ListItemIcon>
						<PixelGrid colorData={colorData} pixelsPerTile={3} size={size} />
					</ListItemIcon>
					<ListItemText primary="Solid w/ Dot" />
				</>
			),
			extra: colorData,
		});

		// checker
		colorData = newCheckerboardArray(b);
		newPresets.push({
			contents: (
				<>
					<ListItemIcon>
						<PixelGrid colorData={colorData} pixelsPerTile={3} size={size} />
					</ListItemIcon>
					<ListItemText primary="Checkerboard" />
				</>
			),
			extra: colorData,
		});

		// checker w/ dot
		colorData = newCheckerboardArray(null);
		colorData = addDot(colorData, b);
		newPresets.push({
			contents: (
				<>
					<ListItemIcon>
						<PixelGrid colorData={colorData} pixelsPerTile={3} size={size} />
					</ListItemIcon>
					<ListItemText primary="Checkerboard w/ Dot" />
				</>
			),
			extra: colorData,
		});

		return newPresets;
	}, [gridImageColor, gridImageSecondaryColor]);

	const onClickGridPresetCb = React.useCallback(
		(option: DeepReadonly<IAppMenuButtonOption<ColorData>>) => {
			actions.updateActiveMapDefTile({ gridImage: [...(option.extra ?? [])] }, { code: tileCode!, plane: plane! });
		},
		[actions, plane, tileCode]
	);
	const onTagsChange = React.useCallback(
		(val: ValueType<IOption>) => {
			let newTags = val as IOption[] | undefined;
			if (newTags == null || newTags.length === 0) {
				newTags = undefined;
			}
			actions.updateActiveMapDefTile(
				{ tags: newTags?.map((opt) => opt.value).sort(Strings.compareIgnoreCase) },
				{ code: tileCode!, plane: plane! }
			);
		},
		[actions, plane, tileCode]
	);
	const onTypesChange = React.useCallback(
		(val: ValueType<IOption<TileType>>) => {
			const newTypes = undefIfBlank(val as IOption<TileType>[] | undefined);
			actions.updateActiveMapDefTile(
				{
					types: newTypes
						?.map((t) => t.value)
						.sort(Strings.compareIgnoreCase)
						.reduce((acc, t) => {
							acc[t] = 1;
							return acc;
						}, {} as SafeDictionary<1, TileType>),
				},
				{ code: tileCode!, plane: plane! }
			);
		},
		[actions, plane, tileCode]
	);
	const onDifficultyChange = React.useCallback(
		(val: ValueType<IOption<DifficultyVal>>) => {
			actions.updateActiveMapDefTile(
				{ difficulty: (val as IOption<DifficultyVal> | undefined)?.value.index },
				{ code: tileCode!, plane: plane! }
			);
		},
		[actions, plane, tileCode]
	);

	const typesArray = useMemo(() => Object.keys(form?.types ?? {}).sort(compareIgnoreCase), [form?.types]);

	if (!form) {
		return <Typography>Select a tile</Typography>;
	}
	const { code, customImage, difficulty, gridImage, graphicIndex, graphicMethod, label, tags } = form;
	const difficultyVal = difficulty != null ? difficultyOptions?.[difficulty] : undefined;
	let offsetGraphicIndex;
	let maxGraphicIndex = numGraphicChunks - 1;
	if (graphicIndex != null) {
		offsetGraphicIndex = plane === 1 ? graphicIndex + (spriteStartIndex || 0) : graphicIndex;
		maxGraphicIndex -= spriteStartIndex || 0;
	}

	return (
		<Grid container direction="column" spacing={2} wrap="nowrap">
			<Grid item xs>
				<Grid container direction="column" spacing={2}>
					<Grid item>
						<AppTextField
							className="app-text-right"
							fullWidth
							InputProps={{
								endAdornment: code !== 0 && (
									<InputAdornment position="end">
										<AppIconButton onClick={onLiveTileCodeSubmitCb}>
											<Save />
										</AppIconButton>
									</InputAdornment>
								),
							}}
							inputProps={{
								min: 1,
							}}
							label="Tile Code"
							onChange={onLiveTileCodeChangeCb}
							readOnly={code === 0}
							type="number"
							value={liveTileCode}
						/>
					</Grid>
					<Grid item>
						<AppTextField fullWidth label="Label" value={label} onChange={onLabelChangeCb} />
					</Grid>
					<Grid item>
						<EnumAutoComplete
							className="app-width-full"
							isMulti
							label="Types"
							onChange={onTypesChange}
							options={TileType}
							value={typesArray}
						/>
					</Grid>
					<Grid item>
						<StringAutoComplete creatable entityLabel="tag" isMulti label="Tags" onChange={onTagsChange} options={planeTags} value={tags} />
					</Grid>
					<Grid item>
						<AutoComplete
							entityLabel="difficulty"
							entityPluralLabel="difficulties"
							isClearable
							isSearchable
							label="Difficulty"
							onChange={onDifficultyChange}
							options={difficultyOptions}
							value={difficultyVal}
						/>
					</Grid>
				</Grid>
			</Grid>
			{code !== 0 && (
				<>
					<Grid item>
						<Box>
							<Divider />
						</Box>
					</Grid>
					<Grid item>
						<Grid container item alignItems="center" spacing={2}>
							<Grid item>
								<Typography variant="subtitle2">Image Preference</Typography>
							</Grid>
							<Grid item>
								<ToggleButtonGroup exclusive onChange={onGraphicMethodChangeCb} size="small" value={graphicMethod}>
									<ToggleButton value={GraphicMethod.GRID}>Grid</ToggleButton>
									<ToggleButton value={GraphicMethod.VSWAP}>Vswap</ToggleButton>
									<ToggleButton value={GraphicMethod.CUSTOM_IMAGE}>Custom</ToggleButton>
								</ToggleButtonGroup>
							</Grid>
						</Grid>
					</Grid>
					<Grid item>
						<Grid container spacing={2} wrap="nowrap">
							<Grid item>
								<Grid container direction="column" spacing={2}>
									<Grid item>
										<Grid container alignItems="center" spacing={1} wrap="nowrap">
											<Grid item>
												<Typography variant="subtitle2">Grid Image</Typography>
											</Grid>
											<Grid item>
												<Hint>
													<Typography variant="body2">
														This is what will be drawn if the tile image preference is "Grid" or the map editor is set to use grid images by
														default.
														<br />
														Use left/right click/drag to paint colors.
														<br />
													</Typography>
													<ul>
														<li>
															<Typography display="inline" variant="button">
																Shift&nbsp;+&nbsp;Click:
															</Typography>{" "}
															<Typography display="inline" variant="body2">
																Fill with selected color
															</Typography>
														</li>
														<li>
															<Typography display="inline" variant="button">
																Ctrl&nbsp;+&nbsp;Click:
															</Typography>{" "}
															<Typography display="inline" variant="body2">
																Replace with selected color
															</Typography>
														</li>
														<li>
															<Typography display="inline" variant="button">
																Alt&nbsp;+&nbsp;Click:
															</Typography>{" "}
															<Typography display="inline" variant="body2">
																Siphon color
															</Typography>
														</li>
													</ul>
												</Hint>
											</Grid>
										</Grid>
									</Grid>
									<Grid item>
										<Grid container spacing={2} wrap="nowrap">
											<Grid item>
												<InteractivePixelGrid
													color={gridImageColor}
													colorData={gridImage}
													onChange={onImageGridChangeCb}
													onColorSiphon={onImageGridSiphonCb}
													pixelsPerTile={18}
													secondaryColor={gridImageSecondaryColor}
													size={size}
												/>
											</Grid>
											<Grid item>
												<Box style={{ width: 128 }}>
													<ColorPresets
														colors={colorPresets}
														onClick={onGridImageColorChangeCb}
														onRightClick={onGridImageSecondaryColorChangeCb}
													/>
												</Box>
											</Grid>
										</Grid>
									</Grid>
									<Grid item>
										<ColorPickerField
											color={gridImageColor ?? ""}
											label="Primary"
											onChange={onGridImageColorChangeCb}
											style={{ width: 264 }}
										/>
									</Grid>
									<Grid item>
										<ColorPickerField
											color={gridImageSecondaryColor ?? ""}
											label="Secondary"
											onChange={onGridImageSecondaryColorChangeCb}
											style={{ width: 264 }}
										/>
									</Grid>
									<Grid item>
										<AppMenuButton color="default" onClick={onClickGridPresetCb} options={gridPresets} variant="outlined">
											Presets
										</AppMenuButton>
									</Grid>
								</Grid>
							</Grid>
							{graphicMethod != null && graphicMethod !== GraphicMethod.GRID && (
								<>
									<Grid item>
										<Box className="app-height-full">
											<Divider orientation="vertical" />
										</Box>
									</Grid>
									<Grid item>
										<Grid container direction="column" spacing={2}>
											<Grid item>
												<Typography variant="subtitle2">Image</Typography>
											</Grid>
											<Grid item>
												<Card style={{ height: 376 }}>
													{graphicMethod === GraphicMethod.VSWAP ? (
														<GameSprite graphicIndex={offsetGraphicIndex} width={256} height={256} />
													) : customImage ? (
														<img src={customImage} style={{ height: 256, imageRendering: "pixelated", width: 256 }} />
													) : (
														<Centered height={256} width={256}>
															<Typography>No image uploaded</Typography>
														</Centered>
													)}
													<CardContent>
														{graphicMethod === GraphicMethod.CUSTOM_IMAGE ? (
															<>
																<Box style={{ maxWidth: 256 - 32 }}>
																	<FileSelect
																		acceptFile={acceptFileCb}
																		onDrop={onDropCb}
																		onFileCleared={onFileClearedCb}
																		textFieldProps={{ placeholder: "Select/drop image..." }}
																	/>
																</Box>
																<FormHelperText>
																	Only PNG format is supported.
																	<br />
																	Width and height should match.
																</FormHelperText>
															</>
														) : (
															<PageSlider
																maxIndex={maxGraphicIndex}
																onChange={onGraphicIndexChangeCb}
																selectedIndex={graphicIndex}
																variant="compact"
															/>
														)}
													</CardContent>
												</Card>
											</Grid>
										</Grid>
									</Grid>
								</>
							)}
						</Grid>
					</Grid>
				</>
			)}
		</Grid>
	);
};

export const MapDefinitionTileEditor = React.memo(MapDefinitionTileEditorFC);
