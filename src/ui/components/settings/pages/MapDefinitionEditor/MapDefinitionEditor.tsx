import * as React from "react";
import { createStyles, Grid, Tabs, Theme, useTheme, Divider, Box } from "@material-ui/core";
import clsx from "clsx";

import { FormMode, FormModeContext } from "../../../context/form-mode";
import { TabContent } from "../../../core/TabContent";
import { AppTab } from "../../../core/AppTab";
import { makeAppStyles, useGridStyles } from "../../../core/material";
import { SettingsPanel } from "../../SettingsPanel";

import { MapDefDifficultiesPage } from "./MapDefDifficultiesPage";
import { MapDefTilesPage } from "./MapDefTilesPage";
import { MapDefGeneralPage } from "./MapDefGeneralPage";
import { MapDefImportExportPage } from "./MapDefImportExportPage";

export interface IMapDefinitionEditorProps {
	parentScrolled?: boolean; // hack to conditionally show shadow
}

const useMapDefEditorStyles = makeAppStyles((theme: Theme) =>
	createStyles({
		mapDefPanelContainer: {
			left: -theme.spacing(0.5),
			position: "sticky",
			zIndex: 2,
		},
		mapDefPanelShadow: {
			"&:after": {
				content: `""`,
				position: "absolute",
				right: theme.spacing(1.5),
				top: theme.spacing(1),
				bottom: theme.spacing(1),
				width: 1,
				boxShadow: `${theme.spacing(1)}px 0 5px 4px rgba(0, 0, 0, 0.5)`,
				zIndex: -1,
			},
		},
	})
);

const MapDefinitionEditorFC: React.FC<IMapDefinitionEditorProps> = (props) => {
	const { parentScrolled } = props;
	const theme = useTheme();
	const [selectedTab, setSelectedTab] = React.useState(0);
	const gridClasses = useGridStyles();
	const classes = useMapDefEditorStyles(props);

	return (
		<FormModeContext.Provider value={{ formMode: FormMode.EDIT }}>
			<Grid container className={gridClasses.fullHeight1} spacing={1} wrap="nowrap" style={{ position: "relative" }}>
				<Grid item className={classes.mapDefPanelContainer} style={{ flexShrink: 0, width: 180 }}>
					<SettingsPanel
						header="Map Definition"
						overflowScroll={false}
						paperProps={{ className: clsx({ [classes.mapDefPanelShadow]: parentScrolled }), translucent: "fake" }}
					>
						<Tabs
							orientation="vertical"
							onChange={(e, i) => setSelectedTab(i)}
							style={{ marginLeft: -theme.spacing(1), marginRight: -theme.spacing(1) }}
							value={selectedTab}
						>
							<AppTab dense label="Import" />
							<Box mt={1} mb={1}>
								<Divider variant="middle" />
							</Box>
							<AppTab dense label="General" />
							<AppTab dense label="Difficulties" />
							<AppTab dense label="Tiles" />
							<AppTab dense label="Utilities" />
						</Tabs>
					</SettingsPanel>
				</Grid>
				<Grid item className="app-grow">
					<TabContent className="app-height-full" selectedTab={selectedTab} tabIndex={0}>
						<SettingsPanel header="Import">
							<MapDefImportExportPage />
						</SettingsPanel>
					</TabContent>
					<TabContent className="app-height-full" selectedTab={selectedTab} tabIndex={2}>
						<SettingsPanel header="General">
							<MapDefGeneralPage />
						</SettingsPanel>
					</TabContent>
					<TabContent className="app-height-full" selectedTab={selectedTab} tabIndex={3}>
						<SettingsPanel header="Difficulties">
							<MapDefDifficultiesPage />
						</SettingsPanel>
					</TabContent>
					<TabContent className="app-height-full" selectedTab={selectedTab} tabIndex={4}>
						<MapDefTilesPage />
					</TabContent>
					<TabContent selectedTab={selectedTab} tabIndex={5}>
						TBD
					</TabContent>
				</Grid>
			</Grid>
		</FormModeContext.Provider>
	);
};

export const MapDefinitionEditor = React.memo(MapDefinitionEditorFC);
