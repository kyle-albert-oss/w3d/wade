import * as React from "react";
import { useSelector } from "react-redux";
import { Grid, Box, Typography } from "@material-ui/core";
import { Delete, Add } from "@material-ui/icons";

import { useActions } from "../../../../../store/redux/actions";
import { activeMapDefDifficultiesSelector } from "../../../../../store/redux/selectors";
import { AppPaper } from "../../../core/AppPaper";
import { AppTextField } from "../../../core/AppTextField";
import { AppIconButtonWithConfirmation } from "../../../core/AppIconButton";
import { AppButton } from "../../../core/AppButton";

export const MapDefDifficultiesPage = React.memo(() => {
	const actions = useActions();
	const difficulties = useSelector(activeMapDefDifficultiesSelector) ?? [];

	return (
		<Grid container direction="column" spacing={2}>
			{difficulties.map((d, i, all) => (
				<Grid item key={i}>
					<AppPaper key={i}>
						<Box p={1}>
							<Grid container alignItems="center" spacing={1}>
								<Grid item>
									<Box width={36}>
										<Typography align="center" display="block" variant="overline" style={{ fontSize: "1rem" }}>
											{i + 1}
										</Typography>
									</Box>
								</Grid>
								<Grid item className="app-grow">
									<Box pl={1.5}>
										<AppTextField
											fullWidth
											label="Name"
											onChange={({ currentTarget: { value } }) => actions.updateActiveMapDefDifficulty({ name: value ?? "" }, { index: i })}
											value={d.name ?? ""}
										/>
									</Box>
								</Grid>
								<Grid item>
									<AppIconButtonWithConfirmation
										confirmation={
											<Typography display="block" style={{ maxWidth: 200 }}>
												Deleting this difficulty will clear the difficulty of any tiles with it. Are you sure?
											</Typography>
										}
										disabled={i !== all.length - 1}
										onClick={() => actions.deleteActiveMapDefDifficulty(i)}
										p={1}
									>
										<Delete />
									</AppIconButtonWithConfirmation>
								</Grid>
							</Grid>
						</Box>
					</AppPaper>
				</Grid>
			))}
			<Grid item>
				<AppButton onClick={() => actions.createActiveMapDefDifficulty({ name: "New Difficulty" })} startIcon={<Add />}>
					Add
				</AppButton>
			</Grid>
		</Grid>
	);
});
