import * as path from "path";

import * as React from "react";
import { FormHelperText, Box, Typography, ListItemText, Divider } from "@material-ui/core";
import { DeepReadonly } from "ts-essentials";
import { useSelector } from "react-redux";

import FileSelect from "../../../core/FileSelect";
import { IAppMenuButtonOption, AppMenuButton, AppButton } from "../../../core/AppButton";
import { AutoCompleteOpt } from "../../../core/AutoComplete";
import { useActions } from "../../../../../store/redux/actions";
import { ColorPresets } from "../../../core/ColorPicker";
import { DEFAULT_COLOR_PRESETS, MAP_EDIT_COLOR_PRESETS } from "../../../../../models/wade";
import { activeMapDefSelector } from "../../../../../store/redux/selectors";
import { arrayToRODictionary } from "../../../../../util/objects";
import { MapDefAutoComplete } from "../../../mapping/MapDefAutoComplete";
import { IWadeMapDefRef } from "../../../../../models/map-definition";

export interface IMapDefImportExportPageProps {}

interface IGridPalette {
	label: string;
	colors: readonly string[];
}

const MapDefImportExportPageFC: React.FC<IMapDefImportExportPageProps> = (props) => {
	const activeMapDef = useSelector(activeMapDefSelector);
	const { importWDCMapDefFile } = useActions();
	const [importFile, setImportFile] = React.useState<File>();
	const [importError, setImportError] = React.useState<string>();

	const importFileExt = path.extname(importFile?.name ?? "").toLowerCase() || undefined;
	const onFileChanged = React.useCallback((files: File[]) => {
		setImportFile(files[0]);
	}, []);
	const onFileCleared = React.useCallback(() => setImportFile(undefined), []);

	const presetPalettes = React.useMemo<IAppMenuButtonOption<IGridPalette>[]>(
		() => [
			{
				contents: <ListItemText primary={"WADE (Default)"} secondary={<ColorPresets colors={DEFAULT_COLOR_PRESETS} />} />,
				extra: {
					label: "WADE (Default)",
					colors: DEFAULT_COLOR_PRESETS,
				},
			},
			{
				contents: (
					<ListItemText
						primary={"MapEdit"}
						secondary={<ColorPresets colors={MAP_EDIT_COLOR_PRESETS} />}
						secondaryTypographyProps={{ display: "block" }}
					/>
				),
				extra: {
					label: "MapEdit",
					colors: MAP_EDIT_COLOR_PRESETS,
				},
			},
		],
		[]
	);
	const [selectedGridPalette, setSelectedGridPalette] = React.useState<IGridPalette>(presetPalettes[0].extra!);
	const onPaletteClickCb = React.useCallback((palette: DeepReadonly<IAppMenuButtonOption<IGridPalette>>) => {
		setSelectedGridPalette(palette.extra!);
	}, []);

	const onImportClickCb = React.useCallback(() => {
		if (importFile && importFileExt === ".wmc") {
			(async () => {
				const result = await importWDCMapDefFile(
					{
						filePath: importFile.path,
						convertOpts: {
							id: activeMapDef?.id, // should not be null
							name: activeMapDef?.name,
							extensions: activeMapDef?.extensions,
							colorConversion: arrayToRODictionary(selectedGridPalette.colors, (c, i) => i),
						},
					},
					{
						returnError: true,
						setActive: true, // this will update the existing map def in settings panel and trigger unsaved changes for it.
					}
				);
				if ("error" in result) {
					setImportError("File could not be imported. Check the console for details.");
				}
			})();
		}
	}, [activeMapDef, importFile, importFileExt, selectedGridPalette, importWDCMapDefFile]);

	const [selectedMapDef, setSelectedMapDef] = React.useState<DeepReadonly<IWadeMapDefRef>>();
	const onSelectedMapDefChangeCb = React.useCallback((md: AutoCompleteOpt<IWadeMapDefRef>) => {
		setSelectedMapDef(md?.value);
	}, []);

	return (
		<Box>
			<Typography variant="body2">
				Importing a map definition will overwrite the current project's map definition and cannot be undone after saving.
			</Typography>
			<Box mt={2}>
				<Typography>Import from File</Typography>
				<Box mt={2}>
					<FileSelect accept={".wmc"} onFileChanged={onFileChanged} onFileCleared={onFileCleared} />
					{importError && <FormHelperText error>{importError}</FormHelperText>}
					<FormHelperText component="div">
						Browse or drag'n'drop a map definition file to import.
						<br />
						<br />
						Supported Formats:
						<ul>
							<li>
								WDC:&nbsp;<strong>*.wmc</strong>
							</li>
						</ul>
					</FormHelperText>
				</Box>
				{importFileExt === ".wmc" && (
					<Box mt={1}>
						<AppMenuButton color="default" onClick={onPaletteClickCb} options={presetPalettes} variant="outlined">
							Grid Palette: {selectedGridPalette.label}
						</AppMenuButton>
					</Box>
				)}
				<Box mt={1}>
					<AppButton disabled={importFile == null} onClick={onImportClickCb} variant="contained">
						Import
					</AppButton>
				</Box>
			</Box>
			<Box mt={2} mb={2}>
				<Divider />
			</Box>
			<Typography>Copy from Project or Template</Typography>
			<Box mt={2}>
				<MapDefAutoComplete fullWidth grouped label="Map Definition" onChange={onSelectedMapDefChangeCb} value={selectedMapDef} />
			</Box>
			<Box mt={1}>
				<AppButton disabled={selectedMapDef == null} onClick={() => alert("TODO")} variant="contained">
					Copy
				</AppButton>
			</Box>
		</Box>
	);
};

export const MapDefImportExportPage = React.memo(MapDefImportExportPageFC);
