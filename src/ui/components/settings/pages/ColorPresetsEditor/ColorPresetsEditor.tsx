import * as React from "react";
import { useSelector } from "react-redux";
import { Grid, Box, FormHelperText, ListItemText } from "@material-ui/core";
import { ColorResult } from "react-color";
import { DeepReadonly } from "ts-essentials";

import { ColorPresets, ColorPickerField } from "../../../core/ColorPicker";
import { colorPresetsSelector } from "../../../../../store/redux/selectors";
import { useActions } from "../../../../../store/redux/actions";
import { MAX_COLOR_PRESETS, DEFAULT_COLOR_PRESETS, MAP_EDIT_COLOR_PRESETS } from "../../../../../models/wade";
import { AppMenuButton, IAppMenuButtonOption } from "../../../core/AppButton";
import { SettingsPanel } from "../../SettingsPanel";

export interface IColorPresetsEditorProps {}

const ColorPresetsEditorFC: React.FC<IColorPresetsEditorProps> = (props) => {
	const actions = useActions();
	const colorPresets = useSelector(colorPresetsSelector);
	const [color, setColor] = React.useState<string>("transparent");
	const [colors, setColors] = React.useState<(string | null)[]>([]);
	React.useEffect(() => {
		const newColors: (string | null)[] = [...colorPresets];
		if (newColors.length < MAX_COLOR_PRESETS) {
			for (let i = newColors.length; i < MAX_COLOR_PRESETS; i++) {
				newColors.push(null);
			}
		}
		setColors(newColors);
	}, [colorPresets]);

	const onColorChangeCb = (newColor: ColorResult) => {
		setColor(newColor.hex);
	};
	const onPresetClickCb = (presetColor: string | null, index: number) => {
		const tmpColors = [...colors!];
		const newColor = color && color !== "transparent" ? color : null;
		tmpColors.splice(index, 1, newColor);
		const newColors = tmpColors.filter((nc) => nc != null) as string[];
		actions.uiUpdate({ colorPresets: newColors });
	};
	const presetPalettes = React.useMemo<IAppMenuButtonOption<string[]>[]>(() => {
		const opts = [
			{
				contents: <ListItemText primary={"WADE"} secondary={<ColorPresets colors={DEFAULT_COLOR_PRESETS} />} />,
				extra: DEFAULT_COLOR_PRESETS,
			},
			{
				contents: (
					<ListItemText
						primary={"MapEdit"}
						secondary={<ColorPresets colors={MAP_EDIT_COLOR_PRESETS} />}
						secondaryTypographyProps={{ display: "block" }}
					/>
				),
				extra: MAP_EDIT_COLOR_PRESETS,
			},
		];
		return opts;
	}, []);
	const onPaletteClickCb = React.useCallback(
		(palette: DeepReadonly<IAppMenuButtonOption<string[]>>) => {
			actions.uiUpdate({ colorPresets: [...palette.extra!] });
		},
		[actions]
	);

	return (
		<SettingsPanel header="Color Presets">
			<Grid container direction="column" spacing={3}>
				<Grid item>
					<ColorPickerField color={color} onChange={onColorChangeCb} />
					<FormHelperText>
						Select a color and then click a box to clear/set a preset color.
						<br />
						Changing a preset has no effect other than providing a quick set of colors for color fields.
					</FormHelperText>
				</Grid>
				<Grid item>
					<Box style={{ width: 32 * 4 }}>
						<ColorPresets colors={colors} onClick={onPresetClickCb} />
					</Box>
				</Grid>
				<Grid item>
					<AppMenuButton color="default" onClick={onPaletteClickCb} options={presetPalettes} variant="outlined">
						Palettes
					</AppMenuButton>
				</Grid>
			</Grid>
		</SettingsPanel>
	);
};

export const ColorPresetsEditor = React.memo(ColorPresetsEditorFC);
