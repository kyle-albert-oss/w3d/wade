import * as React from "react";
import { Box, BoxProps, Divider, Grid, Typography } from "@material-ui/core";
import clsx from "clsx";
import tsDefaults from "ts-defaults";

import { AppPaper, IAppPaperProps } from "../../core/AppPaper";

export interface ISettingsPanelProps {
	boxProps?: BoxProps;
	header: React.ReactNode;
	overflowScroll?: boolean;
	paperProps?: IAppPaperProps;
	variant?: "grid-items";
}

const SettingsPanelFC: React.FC<React.PropsWithChildren<ISettingsPanelProps>> = (_props) => {
	const props = tsDefaults(_props, {
		overflowScroll: true,
	});
	const { children, header, overflowScroll, paperProps, variant } = props;

	let content: React.ReactNode;
	if (variant === "grid-items") {
		content = children;
	} else {
		const { boxProps } = props;
		const childItemClasses = clsx("app-grow", {
			"app-overflow-x-hidden": overflowScroll,
			"app-overflow-y-auto": overflowScroll,
		});
		content = (
			<Grid item className={childItemClasses} {...(overflowScroll ? { style: { paddingRight: 12 } } : undefined)}>
				<Box className="app-height-full" pt={1} {...boxProps}>
					{children}
				</Box>
			</Grid>
		);
	}

	return (
		<AppPaper translucent {...paperProps} className={clsx("app-height-full", paperProps?.className)}>
			<Box className="app-height-full" p={2}>
				<Grid container className="app-height-full" direction="column" spacing={1} wrap="nowrap">
					<Grid item>{typeof header === "string" ? <Typography variant="subtitle1">{header}</Typography> : header}</Grid>
					<Grid item>
						<Divider />
					</Grid>
					{content}
				</Grid>
			</Box>
		</AppPaper>
	);
};

export const SettingsPanel = React.memo(SettingsPanelFC);
