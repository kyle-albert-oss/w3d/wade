import * as React from "react";
import { useDispatch } from "react-redux";
import { button } from "@storybook/addon-knobs";

import { devSetFormsDisabled } from "../../../../store/redux/actions/dev";
import { updateProjectCompileStatus, compileActiveProjectAsync } from "../../../../store/redux/actions/project";
import { AppButton } from "../../core/AppButton/AppButton";
import { createStory } from "../../../../../stories/story-util";

import { ProjectCompileStatusDialog } from "./ProjectCompileStatusDialog";

let disabled = false;

const DefaultStory = () => {
	const dispatch = useDispatch();

	button("Toggle Disable", () => {
		disabled = !disabled;
		dispatch(devSetFormsDisabled(disabled));
	});

	button("Simulate maps end", () => {
		dispatch(updateProjectCompileStatus({ maps: true }));
	});

	button("Simulate compile complete", () => {
		dispatch(compileActiveProjectAsync.success(undefined));
	});

	return (
		<>
			<AppButton
				onClick={() => {
					dispatch(compileActiveProjectAsync.request(true));
					dispatch(updateProjectCompileStatus({ maps: false }));
				}}
			>
				Simulate Compile Start
			</AppButton>
			<ProjectCompileStatusDialog />
		</>
	);
};

createStory("Project|ProjectCompileStatusDialog", module, { style: { padding: 20 } }).add("Default", () => <DefaultStory />);
