import * as React from "react";
import { connect } from "react-redux";
import AppBar from "@material-ui/core/AppBar/AppBar";
import Box from "@material-ui/core/Box";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/core/SvgIcon/SvgIcon";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import FolderIcon from "@material-ui/icons/Folder";
import PlayIcon from "@material-ui/icons/PlayCircleFilled";
import { DeepReadonly } from "ts-essentials";

import { ICompileProjectResult } from "../../../../models/project";
import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { activeProjectCompileStatusSelector, activeProjectSelector, uiStateSelector } from "../../../../store/redux/selectors";
import { AppRootState } from "../../../../store/redux/types/state";
import { ElectronHandle } from "../../../../util/env";
import { showFile } from "../../../../util/os";
import { FormDisableContext } from "../../context/form-disable";
import { AppButton } from "../../core/AppButton";
import { AppIconButton } from "../../core/AppIconButton";
import { LabeledProgress } from "../../core/LabeledProgress";
import { ConnectedStatefulButton } from "../../core/StatefulButton";
import { withMaterial, MaterialProps } from "../../core/material";

export interface IProjectCompileStatusDialogProps {}

type MappedProps = DeepReadonly<{
	compileResult: ICompileProjectResult | undefined;
	open: boolean;
	projectDir: string | undefined;
	projectName: string | undefined;
	projectPlayArgs: string[] | undefined;
	projectPlayCmd: string | undefined;
}>;

interface IProps extends IProjectCompileStatusDialogProps, MappedProps, IActionProps, MaterialProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IProjectCompileStatusDialogProps>): MappedProps => {
	const project = activeProjectSelector(state);
	return {
		compileResult: activeProjectCompileStatusSelector(state),
		open: uiStateSelector(state).isProjectCompileStatusDialogOpen,
		projectDir: project?.gameResourceDir ?? project?.pk3FilePath,
		projectName: project?.name,
		projectPlayArgs: project?.playArgs,
		projectPlayCmd: project?.playCmd,
	};
};

const ProjectCompileStatusDialogFC: React.FC<IProps> = (props) => {
	const { actions, classes, compileResult, open, projectDir, projectName, projectPlayCmd, projectPlayArgs } = props;

	const disabled = React.useContext(FormDisableContext).disabled || compileResult == null || !compileResult.complete;
	const onCloseCb = React.useCallback(() => actions.uiToggle({ isProjectCompileStatusDialogOpen: false }), [actions]);
	const onOpenProjectDirCb = React.useCallback(() => {
		if (projectDir) {
			showFile(projectDir);
		}
	}, [projectDir]);
	const onPlayProjectCb = React.useCallback(() => {
		if (projectPlayCmd && ElectronHandle) {
			ElectronHandle.execFile(projectPlayCmd, projectPlayArgs);
		}
	}, [projectPlayCmd, projectPlayArgs]);

	return (
		<Dialog
			aria-labelledby="project-cs-dialog-title"
			classes={{
				paper: classes.projectDialog,
			}}
			onClose={onCloseCb}
			open={open}
			scroll="paper"
		>
			<DialogTitle id="project-cs-dialog-title" disableTypography className={classes.noPadding}>
				<AppBar position="static">
					<Toolbar className={classes.dialogTitleToolbar} variant="dense">
						<Typography variant="h6">Compile Project {projectName}</Typography>
						<AppIconButton aria-label="close" disabled={disabled} onClick={onCloseCb}>
							<CloseIcon />
						</AppIconButton>
					</Toolbar>
				</AppBar>
			</DialogTitle>
			<DialogContent dividers>
				<Box component="form" pt={2}>
					{compileResult && "maps" in compileResult && (
						<LabeledProgress label={<Typography>Maps</Typography>} progress={compileResult.maps ? "success" : "indeterminate"} />
					)}
					{compileResult && "pk3" in compileResult && (
						<LabeledProgress label={<Typography>PK3</Typography>} progress={compileResult.maps ? "success" : "indeterminate"} />
					)}
				</Box>
			</DialogContent>
			<DialogActions>
				{projectDir && (
					<AppButton color="secondary" onClick={onOpenProjectDirCb} startIcon={<FolderIcon />} variant="outlined">
						Open
					</AppButton>
				)}
				{projectPlayCmd && (
					<ConnectedStatefulButton
						color="primary"
						hideSpinner
						onClick={onPlayProjectCb}
						startIcon={<PlayIcon />}
						uiStateKey="projectCompileDialogStatus"
						variant="outlined"
					>
						Play
					</ConnectedStatefulButton>
				)}
				<ConnectedStatefulButton
					color="primary"
					hideSpinner
					onClick={onCloseCb}
					uiStateKey="projectCompileDialogStatus"
					variant="contained"
				>
					Close
				</ConnectedStatefulButton>
			</DialogActions>
		</Dialog>
	);
};

export const ProjectCompileStatusDialog = connect(
	mapStateToProps,
	getDispatchActionMapper()
)(withMaterial(React.memo(ProjectCompileStatusDialogFC)));
