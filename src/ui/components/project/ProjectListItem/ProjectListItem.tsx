import * as React from "react";
import { connect } from "react-redux";
import { ListItemSecondaryAction } from "@material-ui/core";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Tooltip from "@material-ui/core/Tooltip";
import DeleteIcon from "@material-ui/icons/Delete";
import FolderIcon from "@material-ui/icons/Folder";
import { DeepReadonly } from "ts-essentials";

import { GameResourceId, GameResourceRef } from "../../../../models/resources";
import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { activeProjectIdSelector, availableProjectRefsSelector, selectedProjectIdSelector } from "../../../../store/redux/selectors";
import { AppRootState } from "../../../../store/redux/types/state";
import { AppIconButton } from "../../core/AppIconButton";
import { AppListItem } from "../../core/AppListItem";

export interface IMapDefListItemProps {
	deletable?: boolean;
	projectId: GameResourceRef["id"];
}

type MappedProps = DeepReadonly<{
	activeProjectId?: GameResourceId;
	project: GameResourceRef;
	selectedProjectId?: GameResourceId;
}>;

export interface IProps extends IMapDefListItemProps, MappedProps, IActionProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IMapDefListItemProps>): MappedProps => {
	const projects = availableProjectRefsSelector(state);
	return {
		activeProjectId: activeProjectIdSelector(state),
		project: projects[ownProps.projectId],
		selectedProjectId: selectedProjectIdSelector(state),
	};
};

const ProjectListItemFC: React.FC<IProps> = (props) => {
	const { actions, activeProjectId, deletable, project, selectedProjectId } = props;
	const { id: projectId } = project;

	const onListItemClickCb = React.useCallback(() => actions.uiUpdate({ selectedProjectId: projectId }), [actions, projectId]);
	const isSelected = React.useMemo(() => selectedProjectId === projectId, [selectedProjectId, projectId]);
	const onDeleteClickCb = React.useCallback(() => actions.deleteProjects([projectId]), [actions, projectId]);

	return (
		<AppListItem button dense onClick={onListItemClickCb} onContextMenu={onListItemClickCb} selected={isSelected}>
			<ListItemIcon>
				<FolderIcon />
			</ListItemIcon>
			<ListItemText disableTypography primary={project.name} />
			{deletable && activeProjectId !== project.id && (
				<ListItemSecondaryAction>
					<Tooltip title="Delete project">
						<AppIconButton onClick={onDeleteClickCb} size="small" aria-label="Delete project">
							<DeleteIcon />
						</AppIconButton>
					</Tooltip>
				</ListItemSecondaryAction>
			)}
		</AppListItem>
	);
};

export const ProjectListItem = connect(mapStateToProps, getDispatchActionMapper())(React.memo(ProjectListItemFC));
