import * as React from "react";
import { useSelector } from "react-redux";
import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import Tabs from "@material-ui/core/Tabs";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import { useMount } from "react-use";
import { DeepReadonly } from "ts-essentials";

import { GameProject, NewGameProject } from "../../../../models/project";
import { useActions } from "../../../../store/redux/actions";
import {
	activeProjectSelector,
	availableProjectRefsArraySelector,
	recentProjectsSelector,
	selectedProjectSelector,
} from "../../../../store/redux/selectors";
import { SharedRootState } from "../../../../store/redux/types/state";
import { isStorybookEnv } from "../../../../util/env";
import * as Strings from "../../../../util/strings";
import { AppButton } from "../../core/AppButton";
import { AppIconButton } from "../../core/AppIconButton";
import { AppTab } from "../../core/AppTab";
import { ListGrouping } from "../../core/ListGrouping";
import { makeAppStyles } from "../../core/material";
import { QuickSearchField } from "../../core/QuickSearchField";
import { ConnectedStatefulButton } from "../../core/StatefulButton";
import { ProjectListItem } from "../ProjectListItem";
import { ProjectForm } from "../ProjectForm";
import { AppForm } from "../../core/AppForm";

export interface IProjectDialogProps {
	onClose?(): void;
}

const useProjectDialogStyles = makeAppStyles({
	dialogPaper: {
		minHeight: 600,
		width: 600,
	},
	dialogTabs: {
		flexGrow: 1,
	},
	dialogTitleToolbar: {
		paddingRight: 0,
	},
	noPadding: {
		padding: 0,
	},
});

const isProjectDialogOpenSelector = (state: SharedRootState) => state.ui.isProjectDialogOpen;

const ProjectDialogFC: React.FC<IProjectDialogProps> = (props) => {
	const { onClose } = props;
	const activeProject = useSelector(activeProjectSelector);
	const availableProjects = useSelector(availableProjectRefsArraySelector);
	const isProjectDialogOpen = useSelector(isProjectDialogOpenSelector);
	const recentProjects = useSelector(recentProjectsSelector);
	const selectedProject = useSelector(selectedProjectSelector);

	const [projectSearchText, setProjectSearchText] = React.useState("");
	const [selectedTab, setSelectedTab] = React.useState(1);
	const [newProject, setNewProject] = React.useState<DeepReadonly<GameProject>>();
	const { createProject, setActiveProject, uiToggle } = useActions();
	const classes = useProjectDialogStyles();

	useMount(() => {
		setSelectedTab(availableProjects?.length > 0 ? 0 : 1);
	});

	const tabChangeCb = React.useCallback((e: React.ChangeEvent<{}>, newTabIndex: number) => {
		setSelectedTab(newTabIndex);
	}, []);

	const closeCb = React.useCallback(() => {
		if (onClose) {
			onClose();
		} else {
			uiToggle("isProjectDialogOpen");
		}
	}, [onClose, uiToggle]);

	const openProjectCb = React.useCallback(() => {
		if (!selectedProject) {
			return;
		}
		setActiveProject(selectedProject.id);
	}, [selectedProject, setActiveProject]);
	const createProjectCb = React.useCallback(
		(p: NewGameProject) => {
			createProject(p);
		},
		[createProject]
	);

	const projectSearchChangeCb = React.useCallback(
		(e: React.ChangeEvent<HTMLInputElement>) => setProjectSearchText(e.currentTarget.value || ""),
		[]
	);
	const cleanProjectSearchCb = React.useCallback(() => setProjectSearchText(""), []);

	if (!isProjectDialogOpen) {
		return <></>;
	}

	const closable = activeProject != null || isStorybookEnv();

	let content: React.ReactNode;
	let formId: string | undefined;
	if (selectedTab === 0) {
		let recents = recentProjects;
		let allProjects = availableProjects;

		if (projectSearchText != null && projectSearchText !== "") {
			recents = recents.filter((p) => Strings.containsIgnoreCase(p.name, projectSearchText));
			allProjects = allProjects.filter((p) => Strings.containsIgnoreCase(p.name, projectSearchText));
		}

		content = (
			<div>
				<QuickSearchField onChange={projectSearchChangeCb} onClearClick={cleanProjectSearchCb} value={projectSearchText} />
				<Divider />
				<List dense subheader={<li />}>
					{recents && recents.length > 0 && (
						<ListGrouping subheader="Recent">
							{recents.map((p) => (
								<ProjectListItem key={p.id} projectId={p.id} />
							))}
						</ListGrouping>
					)}
					{allProjects && allProjects.length > 0 && (
						<ListGrouping subheader="All">
							{allProjects.map((p) => (
								<ProjectListItem key={p.id} deletable projectId={p.id} />
							))}
						</ListGrouping>
					)}
				</List>
			</div>
		);
	} else if (selectedTab === 1) {
		formId = "create-project";
		content = (
			<AppForm<NewGameProject> formId={formId} onSubmit={createProjectCb}>
				<ProjectForm initialProject={newProject} onUpdate={(update, data) => setNewProject(data)} />
			</AppForm>
		);
	}

	return (
		<Dialog
			aria-labelledby="project-dialog-title"
			classes={{
				paper: classes.dialogPaper,
			}}
			onClose={closeCb}
			open={isProjectDialogOpen}
			scroll="paper"
		>
			<DialogTitle id="project-dialog-title" disableTypography className={classes.noPadding}>
				<AppBar position="static">
					<Toolbar className={classes.dialogTitleToolbar} variant="dense">
						<Typography variant="h6">Projects</Typography>
						<Box className={classes.dialogTabs} pl={3}>
							<Tabs onChange={tabChangeCb} value={selectedTab} variant="fullWidth">
								<AppTab disabled={availableProjects.length === 0} label="Open" />
								<AppTab label="Create" />
							</Tabs>
						</Box>
						<AppIconButton aria-label="close" disabled={!closable} onClick={closeCb}>
							<CloseIcon />
						</AppIconButton>
					</Toolbar>
				</AppBar>
			</DialogTitle>
			<DialogContent dividers>
				<Box pt={2}>{content}</Box>
			</DialogContent>
			<DialogActions>
				<AppButton disabled={!closable} onClick={closeCb} variant="outlined">
					Cancel
				</AppButton>
				{selectedTab === 0 ? (
					<ConnectedStatefulButton
						color="primary"
						disabled={selectedProject == null}
						onClick={openProjectCb}
						uiStateKey="projectDialogPrimaryButtonStatus"
						variant="contained"
					>
						Open
					</ConnectedStatefulButton>
				) : (
					<ConnectedStatefulButton
						color="primary"
						form={formId}
						uiStateKey="projectDialogPrimaryButtonStatus"
						variant="contained"
						type="submit"
					>
						Create
					</ConnectedStatefulButton>
				)}
			</DialogActions>
		</Dialog>
	);
};

export const ProjectDialog = React.memo(ProjectDialogFC);
