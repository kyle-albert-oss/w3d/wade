import * as React from "react";
import { useDispatch } from "react-redux";
import { button } from "@storybook/addon-knobs";

import { generateUUID } from "../../../../util/strings";
import { createProjectAsync } from "../../../../store/redux/actions/project";
import { devSetFormsDisabled } from "../../../../store/redux/actions/dev";
import { SettingToggleButton } from "../../core/SettingToggleButton";
import { createStory } from "../../../../../stories/story-util";

import { ProjectDialog } from "./ProjectDialog";

let disabled = false;

const DefaultProjectDialog = () => {
	const dispatch = useDispatch();

	button("Add Project", () => {
		const id = generateUUID();
		dispatch(
			createProjectAsync.success({
				id,
				name: id,
			})
		);
	});

	button("Toggle Disable", () => {
		disabled = !disabled;
		dispatch(devSetFormsDisabled(disabled));
	});

	return (
		<>
			<SettingToggleButton uiToggleKey="isProjectDialogOpen">Open</SettingToggleButton>
			<ProjectDialog />
		</>
	);
};
createStory("Project|ProjectDialog", module, { router: true, style: { padding: 20 } }).add("Default", () => <DefaultProjectDialog />);
