import * as React from "react";
import { useSelector } from "react-redux";
import { ActionMeta, GroupType } from "react-select/src/types";

import { GameResourceRef } from "../../../../models/resources";
import { useActions } from "../../../../store/redux/actions";
import {
	availableProjectRefsArraySelector,
	recentProjectsSelector,
	selectedProjectSelector,
	activeProjectSelector,
} from "../../../../store/redux/selectors";
import * as Strings from "../../../../util/strings";
import { AutoComplete, IAutoCompleteProps, AutoCompleteOpt } from "../../core/AutoComplete";

export interface IProjectAutoCompleteProps extends IAutoCompleteProps<GameResourceRef> {
	excludeActiveProject?: boolean;
	filter?(project: GameResourceRef): boolean;
}

export const ProjectAutoComplete: React.FC<IProjectAutoCompleteProps> = (props) => {
	const { excludeActiveProject, filter, onChange, ...rest } = props;

	const { uiUpdate } = useActions();
	const activeProject = useSelector(activeProjectSelector);
	const projects = useSelector(availableProjectRefsArraySelector);
	const recentProjects = useSelector(recentProjectsSelector);
	const storeSelectedProject = useSelector(selectedProjectSelector);
	const selectedProject = "value" in props ? props.value : storeSelectedProject;

	const options = React.useMemo(() => {
		const opts: Array<GroupType<AutoCompleteOpt<GameResourceRef>>> = [];
		const filterFn = (p: GameResourceRef): boolean => {
			if (excludeActiveProject && activeProject?.id === p.id) {
				return false;
			} else if (filter) {
				return filter(p);
			}
			return true;
		};
		const recentsFiltered = recentProjects.filter(filterFn).map((p) => ({ label: p.name, value: p }));
		const projectsFiltered = projects.filter(filterFn).map((p) => ({ label: p.name, value: p }));
		if (recentsFiltered?.length) {
			opts.push({
				label: "Recent",
				options: recentsFiltered,
			});
		}
		if (projectsFiltered?.length) {
			opts.push({
				label: "All",
				options: projectsFiltered,
			});
		}
		return opts;
	}, [activeProject, excludeActiveProject, filter, projects, recentProjects]);

	const filterOptionCb = React.useCallback(
		(option: AutoCompleteOpt<GameResourceRef>, rawInput: string) => Strings.containsIgnoreCase(option.label, rawInput),
		[]
	);
	const onChangeCb = React.useCallback(
		(p: AutoCompleteOpt<GameResourceRef>, action: ActionMeta<any>) => {
			if (onChange) {
				onChange(p, action);
			} else {
				uiUpdate({ selectedProjectId: p?.value.id });
			}
		},
		[onChange, uiUpdate]
	);

	return (
		<AutoComplete<GameResourceRef>
			filterOption={filterOptionCb}
			isClearable
			isSearchable
			label="Project"
			onChange={onChangeCb}
			options={options}
			value={selectedProject}
			{...rest}
			isMulti={false}
		/>
	);
};
