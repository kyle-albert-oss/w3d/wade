import * as React from "react";

import { GameProjectType } from "../../../../models/project";
import { EnumAutoComplete, IHOCEnumAutoCompleteProps, EnumNameValuePair } from "../../core/AutoComplete";

const ProjectTypeAutoCompleteFC: React.FC<IHOCEnumAutoCompleteProps<GameProjectType>> = (props) => {
	const { children, ...rest } = props;

	const labelProvider = React.useCallback(
		(e: EnumNameValuePair<GameProjectType>, disabled: boolean) => (disabled ? `${e.value} (not yet supported)` : e.value),
		[]
	);

	return <EnumAutoComplete label="Type" labelProvider={labelProvider} {...rest} options={GameProjectType} />;
};

export const ProjectTypeAutoComplete = React.memo(ProjectTypeAutoCompleteFC);
