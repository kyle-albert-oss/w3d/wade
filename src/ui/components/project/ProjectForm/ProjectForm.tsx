import * as React from "react";
import Grid from "@material-ui/core/Grid";
import { DeepReadonly } from "ts-essentials";

import { AppTextField } from "../../core/AppTextField";
import { ProjectTypeAutoComplete } from "../ProjectTypeAutoComplete";
import FileSelect from "../../core/FileSelect";
import { GameProjectType, GameProject } from "../../../../models/project";
import { AutoCompleteOpt } from "../../core/AutoComplete";
import { withResourceWorker } from "../../../../workers";
import { useFormObj, UseFormObjOnUpdate } from "../../hooks";

export interface IProjectFormProps {
	initialProject?: DeepReadonly<GameProject>;
	onUpdate?: UseFormObjOnUpdate<GameProject>;
}

const DEFAULT_PROJECT: DeepReadonly<GameProject> = {
	id: "",
	name: "",
	type: GameProjectType.ORIGINAL,
	wadeVersion: "",
};

const ProjectFormFC: React.FC<IProjectFormProps> = (props) => {
	const { initialProject, onUpdate } = props;
	const [formObj, updateFormObj] = useFormObj(initialProject ?? DEFAULT_PROJECT, onUpdate);
	const [detectedExtensionLoading, setDetectedExtensionLoading] = React.useState(false);

	const onProjectDirChange = React.useCallback(
		(folderPath: string | undefined) => {
			(async () => {
				updateFormObj({ gameResourceDir: folderPath });
				if (folderPath) {
					setDetectedExtensionLoading(true);
					const exts = await withResourceWorker((w) => w.detectGameResourceExt({ dir: folderPath! }));
					const ext = (exts?.[0] ?? "").toLocaleUpperCase().replace(".", "") || undefined;
					updateFormObj({ gameResourceExt: ext });
				}
				setDetectedExtensionLoading(false);
			})();
		},
		[updateFormObj]
	);

	return (
		<Grid container direction="column" spacing={2}>
			<Grid item className="app-width-full">
				<AppTextField
					fullWidth
					label="Name"
					name="name"
					onChange={(e) => updateFormObj({ name: e.currentTarget.value })}
					required
					value={formObj.name}
				/>
			</Grid>
			<Grid item className="app-width-full">
				<ProjectTypeAutoComplete
					name="type"
					onChange={(value: AutoCompleteOpt<GameProjectType>) => updateFormObj({ type: value.value })}
					value={formObj.type}
				/>
			</Grid>
			<Grid item className="app-width-full">
				<FileSelect
					disabled={detectedExtensionLoading}
					folder
					label="Game Folder"
					name="gameResourceDir"
					onFolderChanged={onProjectDirChange}
					required
					value={formObj.gameResourceDir}
				/>
			</Grid>
			<Grid item className="app-width-full">
				<AppTextField
					disabled={detectedExtensionLoading}
					fullWidth
					label="Game File Extension"
					name="gameResourceExt"
					onChange={(e) => updateFormObj({ gameResourceExt: e.currentTarget.value })}
					required={formObj.type === GameProjectType.ORIGINAL}
					value={formObj.gameResourceExt}
				/>
			</Grid>
			{formObj.type === GameProjectType.ECWOLF && (
				<Grid item className="app-width-full">
					<FileSelect
						name="pk3FilePath"
						label="PK3 File"
						onDrop={(f) => updateFormObj({ pk3FilePath: f?.[0]?.path })}
						value={formObj.pk3FilePath}
					/>
				</Grid>
			)}
		</Grid>
	);
};

export const ProjectForm = React.memo(ProjectFormFC);
