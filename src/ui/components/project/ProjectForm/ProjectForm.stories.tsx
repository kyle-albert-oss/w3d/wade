import * as React from "react";
import { FormProvider, useForm } from "react-hook-form";

import { createStory } from "../../../../../stories/story-util";
import { AppButton } from "../../core/AppButton/AppButton";

import { ProjectForm } from "./ProjectForm";

const ProjectFormStory: React.FC = () => {
	const form = useForm();
	const onSubmit = (data: any) => console.log(data);
	return (
		<FormProvider {...form}>
			<form onSubmit={form.handleSubmit(onSubmit)}>
				<ProjectForm />
				<hr />
				<AppButton type="submit">Submit</AppButton>
			</form>
		</FormProvider>
	);
};

createStory("Project|ProjectForm", module, { style: { padding: 20 } }).add("Default", () => <ProjectFormStory />);
