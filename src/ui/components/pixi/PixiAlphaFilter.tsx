import * as PIXI from "pixi.js";
import * as React from "react";
import { useMemo } from "react";
import { Container } from "@inlet/react-pixi";

export interface IPixiAlphaFilterProps {
	alpha: number;
	children?: React.ReactNode;
}

export const PixiAlphaFilter = React.memo<IPixiAlphaFilterProps>(({ alpha, children }) => {
	const filters = useMemo(() => [new PIXI.filters.AlphaFilter(alpha)], [alpha]);
	return <Container filters={filters}>{children}</Container>;
});
