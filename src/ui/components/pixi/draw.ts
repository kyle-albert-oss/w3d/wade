export const drawDashedLine = (
	g: PIXI.Graphics,
	x1: number,
	y1: number,
	x2: number,
	y2: number,
	dash: number = 8,
	gap: number = 4,
	offset: number = dash / 2
) => {
	const dx = x2 - x1;
	const dy = y2 - y1;
	const slope = dy / dx;
	const length = Math.sqrt(dx * dx + dy * dy);
	const slopeAngleRads = Math.atan(slope);
	const cos = Math.cos(slopeAngleRads);
	const sin = Math.sin(slopeAngleRads);
	const xcomp = dx >= 0 ? Math.min : Math.max;
	const ycomp = dy >= 0 ? Math.min : Math.max;
	let x = x1;
	let y = y1;
	let lineSegment = true;
	let traveled = 0;

	while (traveled < length) {
		let segmentLength = lineSegment ? dash : gap;
		if (traveled === 0) {
			segmentLength -= offset;
		}
		const sdx = cos * segmentLength;
		const sdy = sin * segmentLength;
		g.moveTo(x, y);
		x = xcomp(x + sdx, x2);
		y = ycomp(y + sdy, y2);
		if (lineSegment) {
			g.lineTo(x, y);
		}
		traveled += segmentLength;
		lineSegment = !lineSegment;
	}
};
