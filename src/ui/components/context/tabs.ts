import * as React from "react";

export interface ITabsContext {
	selectedTabIndex: number;
}

export const TabsContext = React.createContext<ITabsContext>({ selectedTabIndex: 0 });
