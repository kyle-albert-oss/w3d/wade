import * as React from "react";

export interface IFormDisableContext {
	disabled: boolean;
	exceptedElementIds?: Set<string>;
}

export const FormDisableContext = React.createContext<IFormDisableContext>({
	disabled: false,
});

export const calcDisabledWithProps = (ctx?: IFormDisableContext, props?: { disabled?: boolean; id?: string }): boolean | undefined => {
	if (!props) {
		return ctx ? ctx.disabled : false;
	}
	const { disabled, id } = props;
	const dataId = (props as any)["data-id"];
	return calcDisabled(ctx, disabled, id ?? dataId);
};

export const calcDisabled = (ctx?: IFormDisableContext, other?: boolean, id?: string): boolean | undefined => {
	if (other) {
		return true;
	} else if (id && ctx && ctx.disabled && ctx.exceptedElementIds && ctx.exceptedElementIds.size > 0) {
		return !ctx.exceptedElementIds.has(id);
	} else if (ctx) {
		return ctx.disabled;
	}
	return false;
};

export const disableFormsIf = (ctx: IFormDisableContext, content: React.ReactNode): React.ReactNode =>
	!ctx.disabled ? content : <FormDisableContext.Provider value={ctx}>{content}</FormDisableContext.Provider>;
