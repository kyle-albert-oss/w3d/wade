import * as React from "react";

export enum FormMode {
	DEFAULT,
	EDIT,
}

export interface IFormModeContext {
	formMode: FormMode;
}

export const FormModeContext = React.createContext<IFormModeContext>({ formMode: FormMode.DEFAULT });
