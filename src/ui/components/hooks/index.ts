import * as React from "react";
import { Dispatch } from "react";
import { DeepReadonly } from "ts-essentials";
import _ from "lodash";

import { IDragItem } from "../core/material";

export type DndMoveFn = (fromIndex: number, toIndex: number) => void;
export type DndProps = {
	dragDisabled?: boolean;
	dragId: string | number;
	dragIndex: number;
	dragType?: string;
	onDndBegin?(item: IDragItem): void;
	onDndEnd?(dragItem: IDragItem, dropped: boolean): void;
	onDndHover(itemIndex: number, hoverIndex: number): void;
};
export const useTransactionalDragHandlers = (fn: DndMoveFn): Pick<DndProps, "onDndBegin" | "onDndEnd" | "onDndHover"> => {
	const [startIndex, setStartIndex] = React.useState<number>();
	const onDndBegin = React.useCallback((item: IDragItem) => {
		setStartIndex(item.index);
	}, []);
	const onDndHover = React.useCallback(
		(itemIndex: number, hoverIndex: number) => {
			fn(itemIndex, hoverIndex);
		},
		[fn]
	);
	return React.useMemo(
		() => ({
			onDndBegin,
			onDndEnd: (item: IDragItem, dropped: boolean) => {
				if (!dropped && startIndex != null) {
					fn(item.index, startIndex);
				}
				setStartIndex(undefined);
			},
			onDndHover,
		}),
		[fn, onDndBegin, onDndHover, startIndex]
	);
};

export type DndPreparedProps = Omit<DndProps, "onDndBegin" | "onDndEnd" | "onDndHover"> & { move: DndMoveFn };
export const useDragProps = (props: DndPreparedProps): DndProps => {
	const { move, ...dndProps } = props;
	const handlers = useTransactionalDragHandlers(move);
	return {
		...dndProps,
		...handlers,
	};
};

const formObjReducer = <T extends object, A extends Partial<DeepReadonly<T>>>(obj: DeepReadonly<T>, update: A): DeepReadonly<T> => ({
	...obj,
	...update,
});
export type UseFormObjOnUpdate<T> = (update: Partial<DeepReadonly<T>>, data: DeepReadonly<T>) => void;

export const useFormObj = <T extends object>(
	initializerArg: DeepReadonly<T>,
	onUpdate?: UseFormObjOnUpdate<T>
): [DeepReadonly<T>, Dispatch<Partial<DeepReadonly<T>>>] => {
	const [obj, dispatch] = React.useReducer<React.Reducer<DeepReadonly<T>, Partial<DeepReadonly<T>>>, DeepReadonly<T>>(
		formObjReducer,
		initializerArg,
		(arg) => _.cloneDeep(arg)
	);
	const [lastUpdate, setLastUpdate] = React.useState<Partial<DeepReadonly<T>>>();

	const wrappedDispatch = React.useCallback((update: Partial<DeepReadonly<T>>): void => {
		dispatch(update);
		setLastUpdate(update);
	}, []);

	React.useEffect(() => {
		if (onUpdate && lastUpdate && obj) {
			onUpdate(lastUpdate, obj);
		}
	}, [lastUpdate]);

	return React.useMemo(() => [obj, wrappedDispatch], [obj, wrappedDispatch]);
};
