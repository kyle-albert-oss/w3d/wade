import * as React from "react";
import { connect } from "react-redux";
import { DeepReadonly } from "ts-essentials";

import { IActionProps, getDispatchActionMapper } from "../../../../store/redux/actions";
import { AppRootState } from "../../../../store/redux/types/state";
import { graphicChunksSelector } from "../../../../store/redux/selectors";

export interface IGameSpriteProps {
	graphicIndex?: number;
	height?: number | string;
	width?: number | string;
}

type MappedProps = DeepReadonly<{
	imgHeight: number | string | undefined;
	imgSrc: string | undefined;
	imgWidth: number | string | undefined;
}>;

interface IProps extends IGameSpriteProps, MappedProps, IActionProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IGameSpriteProps>): MappedProps => {
	const { graphicIndex } = ownProps;
	if (graphicIndex == null) {
		return { imgHeight: undefined, imgSrc: undefined, imgWidth: undefined };
	}
	const graphicChunks = graphicChunksSelector(state);
	const graphicChunk = graphicChunks?.[graphicIndex];
	return {
		imgHeight: ownProps.height ?? graphicChunk?.meta?.height,
		imgSrc: graphicChunk?.url,
		imgWidth: ownProps.width ?? graphicChunk?.meta?.width,
	};
};

const GameSpriteFC: React.FC<IProps> = (props) => {
	const { imgHeight: height, imgSrc, imgWidth: width } = props;
	if (imgSrc == null || height == null || width == null) {
		return <></>;
	}
	return <img src={imgSrc} style={{ height, imageRendering: "pixelated", width }} />;
};

export const GameSprite = connect(mapStateToProps, getDispatchActionMapper())(React.memo(GameSpriteFC));
