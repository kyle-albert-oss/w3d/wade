import * as React from "react";
import { connect } from "react-redux";
import { Card } from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { DeepReadonly } from "ts-essentials";

import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { graphicChunksSelector, vswapEditorZoomSelector } from "../../../../store/redux/selectors";
import { AppRootState } from "../../../../store/redux/types/state";
import { GameSprite } from "../GameSprite";

export interface IGameGraphicCardProps {
	graphicIndex: number;
}

type MappedProps = DeepReadonly<{
	width: number | undefined;
	zoomFactor: number;
}>;

interface IProps extends IGameGraphicCardProps, MappedProps, IActionProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IGameGraphicCardProps>): MappedProps => {
	const { graphicIndex } = ownProps;
	const graphicChunks = graphicChunksSelector(state);
	const graphicChunk = graphicChunks?.[graphicIndex];
	const { value: zoomFactor } = vswapEditorZoomSelector(state)();
	return {
		width: graphicChunk?.meta?.width,
		zoomFactor,
	};
};

const GameGraphicCardFC: React.FC<IProps> = (props) => {
	let { graphicIndex, width, zoomFactor } = props;

	width = React.useMemo(() => (width != null ? width * zoomFactor : undefined), [width, zoomFactor]);
	if (width == null) {
		return <></>;
	}

	return (
		<Card style={{ maxWidth: width }}>
			<GameSprite graphicIndex={graphicIndex} />
			<CardContent>
				<Typography variant="body2" color="textSecondary" component="p">
					Content
				</Typography>
			</CardContent>
		</Card>
	);
};

export const GameGraphicCard = connect(mapStateToProps, getDispatchActionMapper())(React.memo(GameGraphicCardFC));
