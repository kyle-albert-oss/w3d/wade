import * as React from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router";
import { DeepReadonly } from "ts-essentials";

import { IGamePalette } from "../../../models/palette";
import { getDispatchActionMapper, IActionProps } from "../../../store/redux/actions";
import { activePaletteSelector } from "../../../store/redux/selectors";
import { AppRootState } from "../../../store/redux/types/state";
import { AppPaper } from "../../components/core/AppPaper";
import { PaletteGrid } from "../../components/palette/PaletteGrid";

export interface IPaletteEditorProps {}

type MappedProps = DeepReadonly<{
	palettes: IGamePalette[];
}>;

interface IProps extends IPaletteEditorProps, MappedProps, IActionProps, RouteComponentProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IPaletteEditorProps>): MappedProps => {
	const activePalette = activePaletteSelector(state);
	return {
		palettes: activePalette ? [activePalette] : [],
	};
};

const PaletteEditorFC: React.FC<IProps> = (props) => {
	const { palettes } = props;

	const colors = palettes?.[0]?.data?.colors ?? [];
	if (!colors.length) {
		return null;
	}

	return (
		<AppPaper>
			<PaletteGrid colors={colors} maxColumns={Math.ceil(Math.sqrt(colors.length))} />
		</AppPaper>
	);
};

export const PaletteEditor = withRouter(connect(mapStateToProps, getDispatchActionMapper())(React.memo(PaletteEditorFC)));
