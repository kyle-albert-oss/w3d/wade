import * as React from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import MapIcon from "@material-ui/icons/Map";
import PaletteIcon from "@material-ui/icons/Palette";
import clsx from "clsx";
import { DeepReadonly } from "ts-essentials";
import { Settings } from "@material-ui/icons";
import { createStyles, Theme, Box } from "@material-ui/core";

import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { uiStateSelector } from "../../../../store/redux/selectors";
import { AppRootState } from "../../../../store/redux/types/state";
import { WallIcon } from "../../../components/core/icons/WallIcon";
import { makeAppStyles } from "../../../components/core/material";
import { LEFT_NAV_WIDTH_PX } from "../../../../models/wade";

type MappedProps = DeepReadonly<{
	isNavOpen?: boolean;
}>;

interface IProps extends MappedProps, IActionProps, RouteComponentProps {}

const mapStateToProps = (state: AppRootState): MappedProps => {
	return {
		isNavOpen: uiStateSelector(state).isNavOpen,
	};
};

interface INavItemProps {
	icon: React.ElementType;
	iconProps?: any;
	selected?: boolean;
	text: string;
	onClick(): void;
}

const useNavItemStyles = makeAppStyles((theme: Theme) =>
	createStyles({
		navIcon: {
			paddingLeft: theme.spacing(1),
		},
	})
);

const NavItem: React.FC<INavItemProps> = React.memo((props: INavItemProps) => {
	const { onClick, icon: Icon, iconProps, selected, text } = props;
	const classes = useNavItemStyles();
	return (
		<ListItem button onClick={onClick} selected={selected}>
			<ListItemIcon classes={{ root: classes.navIcon }}>
				<Icon {...iconProps} />
			</ListItemIcon>
			<ListItemText primary={text} />
		</ListItem>
	);
});

const useNavStyles = makeAppStyles((theme: Theme) =>
	createStyles({
		drawer: {
			width: LEFT_NAV_WIDTH_PX,
			flexShrink: 0,
			whiteSpace: "nowrap",
		},
		drawerPaper: {
			display: "flex",
			flexDirection: "column",
			flexWrap: "nowrap",
		},
		drawerOpen: {
			width: LEFT_NAV_WIDTH_PX,
			transition: theme.transitions.create("width", {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.enteringScreen,
			}),
		},
		drawerClose: {
			transition: theme.transitions.create("width", {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.leavingScreen,
			}),
			overflowX: "hidden",
			width: theme.spacing(7) + 1,
			[theme.breakpoints.up("sm")]: {
				width: theme.spacing(9) + 1,
			},
		},
		navList: {
			display: "flex",
			flexDirection: "column",
			flexGrow: 1,
			flexWrap: "nowrap",
		},
		toolbarDense: {
			display: "flex",
			alignItems: "center",
			justifyContent: "flex-end",
			padding: "0 8px",
			...theme.mixins.toolbar,
			minHeight: "48px !important",
		},
	})
);

const LeftNavDrawerFC: React.FC<IProps> = (props: IProps) => {
	const { actions, location, isNavOpen } = props;
	const { pathname } = location;
	const classes = useNavStyles();

	const onNavOpenToggleClickCb = React.useCallback(() => actions.uiToggle("isNavOpen"), [actions]);
	const onMapsRouteClickCb = React.useCallback(() => actions.updateRoute("/maps"), [actions]);
	const onPalettesRouteClickCb = React.useCallback(() => actions.updateRoute("/palettes"), [actions]);
	const onSettingsRouteClickCb = React.useCallback(() => actions.updateRoute("/settings"), [actions]);
	const onVswapRouteClickCb = React.useCallback(() => actions.updateRoute("/vswap"), [actions]);

	return (
		<Drawer
			variant="permanent"
			className={clsx(classes.drawer, {
				[classes.drawerOpen]: isNavOpen,
				[classes.drawerClose]: !isNavOpen,
			})}
			classes={{
				paper: clsx(classes.drawerPaper, {
					[classes.drawerOpen]: isNavOpen,
					[classes.drawerClose]: !isNavOpen,
				}),
			}}
			open={isNavOpen}
		>
			<div className={classes.toolbarDense}>
				<IconButton onClick={onNavOpenToggleClickCb}>
					<ChevronLeftIcon />
				</IconButton>
			</div>
			<List className={classes.navList}>
				<NavItem icon={MapIcon} onClick={onMapsRouteClickCb} selected={pathname === "/maps"} text="Maps" />
				<Divider />
				<NavItem icon={PaletteIcon} onClick={onPalettesRouteClickCb} selected={pathname === "/palettes"} text="Palettes" />
				<Divider />
				<NavItem icon={WallIcon} selected={pathname === "/vswap"} onClick={onVswapRouteClickCb} text="Vswap" />
				<Divider />
				<Box flexGrow={1} />
				<Divider />
				<NavItem icon={Settings} selected={pathname.includes("/settings")} onClick={onSettingsRouteClickCb} text="Settings" />
			</List>
		</Drawer>
	);
};

export const LeftNavDrawer = withRouter(connect(mapStateToProps, getDispatchActionMapper())(React.memo(LeftNavDrawerFC)));
