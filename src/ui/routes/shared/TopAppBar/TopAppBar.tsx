import * as React from "react";
import { connect } from "react-redux";
import { Typography, createStyles, Theme } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import BuildIcon from "@material-ui/icons/Build";
import MenuIcon from "@material-ui/icons/Menu";
import SaveIcon from "@material-ui/icons/Save";
import clsx from "clsx";
import { DeepReadonly } from "ts-essentials";

import { GameProjectType } from "../../../../models/project";
import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { activeProjectSelector, projectHasUnsavedChangesSelector, uiStateSelector } from "../../../../store/redux/selectors";
import { AppRootState } from "../../../../store/redux/types/state";
import { AppIconButton } from "../../../components/core/AppIconButton";
import { AppTooltip } from "../../../components/core/AppTooltip";
import { makeAppStyles } from "../../../components/core/material";
import { LEFT_NAV_WIDTH_PX } from "../../../../models/wade";

type MappedProps = DeepReadonly<{
	isDevConsoleOpen: boolean;
	isNavOpen: boolean;
	projectDir: string | undefined;
	projectHasUnsavedChanges: boolean | undefined;
	projectName: string | undefined;
	projectType: GameProjectType | undefined;
}>;

interface IProps extends MappedProps, IActionProps {}

const mapStateToProps = (state: AppRootState): MappedProps => {
	const uiState = uiStateSelector(state);
	const activeProject = activeProjectSelector(state);

	return {
		isDevConsoleOpen: uiState.isDevConsoleOpen,
		isNavOpen: uiState.isNavOpen,
		projectDir: activeProject ? activeProject.gameResourceDir || activeProject.pk3FilePath : undefined,
		projectHasUnsavedChanges: projectHasUnsavedChangesSelector(state),
		projectName: activeProject?.name,
		projectType: activeProject?.type,
	};
};

const useTopAppBarStyles = makeAppStyles((theme: Theme) =>
	createStyles({
		appBar: {
			zIndex: theme.zIndex.drawer + 1,
			transition: theme.transitions.create(["width", "margin"], {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.leavingScreen,
			}),
		},
		appBarShift: {
			marginLeft: LEFT_NAV_WIDTH_PX,
			width: `calc(100% - ${LEFT_NAV_WIDTH_PX}px)`,
			transition: theme.transitions.create(["width", "margin"], {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.enteringScreen,
			}),
		},
		menuButton: {
			marginRight: 36,
		},
	})
);

const TopAppBarFC: React.FC<IProps> = (props: IProps) => {
	const { actions, isNavOpen, projectHasUnsavedChanges, projectName } = props;

	const classes = useTopAppBarStyles();

	const onNavOpenToggleClickCb = React.useCallback(() => actions.uiToggle("isNavOpen"), [actions]);
	const isDevConsoleOpenCb = React.useCallback(() => actions.uiToggle("isDevConsoleOpen"), [actions]);
	const onCompileProjectClickCb = React.useCallback(() => actions.compileActiveProject(true), [actions]);

	return (
		<AppBar
			position="fixed"
			className={clsx(classes.appBar, {
				[classes.appBarShift]: isNavOpen,
			})}
		>
			<Toolbar variant="dense">
				<AppIconButton
					color="inherit"
					aria-label="Open drawer"
					onClick={onNavOpenToggleClickCb}
					edge="start"
					className={clsx(classes.menuButton, {
						"app-hide": isNavOpen,
					})}
				>
					<MenuIcon />
				</AppIconButton>
				<Typography noWrap variant="h6">
					{projectName}
				</Typography>
				<div className="app-grow" />
				<AppTooltip
					className="MuiIconButton-edgeEnd"
					title={projectHasUnsavedChanges ? "Save project changes" : "There are no unsaved project changes"}
					wrap="div"
				>
					<AppIconButton color="inherit" disabled={!projectHasUnsavedChanges} onClick={onCompileProjectClickCb}>
						<SaveIcon />
					</AppIconButton>
				</AppTooltip>
				{process.env.NODE_ENV !== "production" && (
					<AppIconButton edge="end" color="inherit" onClick={isDevConsoleOpenCb}>
						<BuildIcon />
					</AppIconButton>
				)}
			</Toolbar>
		</AppBar>
	);
};

export const TopAppBar = connect(mapStateToProps, getDispatchActionMapper())(React.memo(TopAppBarFC));
