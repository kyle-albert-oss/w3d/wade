import * as React from "react";
import { useSelector } from "react-redux";
import createMuiTheme, { ThemeOptions } from "@material-ui/core/styles/createMuiTheme";
import ThemeProvider from "@material-ui/styles/ThemeProvider";

import { useActions } from "../../../../store/redux/actions";
import { themeStateSelector, updateThemeSelector } from "../../../../store/redux/selectors";

export const MaterialThemeProvider: React.FC<React.PropsWithChildren<{}>> = (props) => {
	const { children } = props;
	const { uiToggle } = useActions();
	const themeFromStore = useSelector(themeStateSelector);
	const updateTheme = useSelector(updateThemeSelector);
	const [theme, setTheme] = React.useState<ThemeOptions>();

	React.useEffect(() => {
		setTheme(createMuiTheme(themeFromStore as ThemeOptions));
	}, [themeFromStore]);
	React.useEffect(() => {
		if (!updateTheme) {
			return;
		}
		uiToggle({ updateTheme: false });
		setTheme(createMuiTheme(themeFromStore as ThemeOptions));
	}, [themeFromStore, updateTheme, uiToggle]);

	return !theme ? null : <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export const ConnectedMaterialThemeProvider = React.memo(MaterialThemeProvider);
