import * as React from "react";
import { connect } from "react-redux";
import { Redirect, Route, Switch, withRouter } from "react-router";
import CircularProgress from "@material-ui/core/CircularProgress";
import CssBaseline from "@material-ui/core/CssBaseline";
import { StylesProvider, CSSProperties } from "@material-ui/styles";
import { DeepReadonly } from "ts-essentials";
import { Theme, createStyles } from "@material-ui/core";
import { useMount } from "react-use";

import { getDispatchActionMapper, IActionProps } from "../../store/redux/actions";
import { activeProjectSelector, appStateSelector, formsDisabledSelector, uiStateSelector } from "../../store/redux/selectors";
import { AppRootState } from "../../store/redux/types/state";
import { FormDisableContext, IFormDisableContext } from "../components/context/form-disable";
import { MaterialProps, makeAppStyles } from "../components/core/material";
import DevDrawer from "../components/dev/DevDrawer";
import { ProjectCompileStatusDialog } from "../components/project/ProjectCompileStatusDialog";
import { ProjectDialog } from "../components/project/ProjectDialog";
import "./App.scss";
import { AddRemove, TRACKED_KEYS } from "../../models/wade";

import { MapEditor } from "./MapEditor";
import { PaletteEditor } from "./PaletteEditor";
import { LeftNavDrawer } from "./shared/LeftNavDrawer";
import { TopAppBar } from "./shared/TopAppBar";
import { VswapEditor } from "./Vswap";
import { SettingsEditor } from "./SettingsEditor";

type MappedProps = DeepReadonly<{
	displayBackdrop: boolean;
	formsDisabled: boolean;
	isInitialLoadComplete: boolean;
	isNavOpen: boolean;
	isProjectCompileStatusDialogOpen: boolean;
	isProjectDialogOpen: boolean;
}>;

interface IProps extends MappedProps, IActionProps, MaterialProps {}

const mapStateToProps = (state: AppRootState): MappedProps => {
	const uiState = uiStateSelector(state);
	const { isNavOpen, isProjectDialogOpen, isProjectCompileStatusDialogOpen } = uiState;
	return {
		displayBackdrop: activeProjectSelector(state) == null,
		formsDisabled: formsDisabledSelector(state),
		isInitialLoadComplete: appStateSelector(state).isInitialLoadComplete,
		isNavOpen,
		isProjectCompileStatusDialogOpen,
		isProjectDialogOpen,
	};
};

const useAppStyles = makeAppStyles((theme: Theme) => {
	const fullscreenContainerBase: CSSProperties = {
		position: "fixed",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		zIndex: Math.max(...Object.values(theme.zIndex)) + 1,
	};
	return createStyles({
		content: {
			flexGrow: 1,
			padding: theme.spacing(7, 1, 1, 1),
			overflow: "hidden",
		},
		fullScreenOpaqueContainer: {
			...fullscreenContainerBase,
			backgroundColor: theme.palette.primary.main,
		},
		fullScreenTransparentContainer: {
			...fullscreenContainerBase,
		},
		root: {
			display: "flex",
			height: "100%",
			width: "100%",
		},
	});
});

const App: React.FC<IProps> = (props) => {
	const { actions, displayBackdrop, formsDisabled, isInitialLoadComplete, isProjectDialogOpen, isProjectCompileStatusDialogOpen } = props;

	// entry point logic
	useMount(() => {
		actions.getBootstrapData();
		document.addEventListener("keydown", (e) => {
			const key = e.key;
			if (!TRACKED_KEYS.has(key)) {
				return;
			}
			actions.updateKeysDown(key, AddRemove.ADD);
		});
		document.addEventListener("keyup", (e) => {
			const key = e.key;
			if (!TRACKED_KEYS.has(key)) {
				return;
			}
			actions.updateKeysDown(key, AddRemove.REMOVE);
		});
	});

	const classes = useAppStyles();

	const disableCtx = React.useMemo<IFormDisableContext>(
		() => ({
			disabled: formsDisabled,
		}),
		[formsDisabled]
	);

	return (
		<div className={classes.root}>
			<StylesProvider injectFirst>
				<CssBaseline />
			</StylesProvider>
			{!isInitialLoadComplete && (
				<div className={classes.fullScreenOpaqueContainer}>
					<CircularProgress color="secondary" />
				</div>
			)}
			<FormDisableContext.Provider value={disableCtx}>
				{!displayBackdrop && (
					<>
						<TopAppBar />
						<LeftNavDrawer />
						<main className={classes.content}>
							<Switch>
								<Route path="/maps" component={MapEditor} />
								<Route path="/palettes" component={PaletteEditor} />
								<Route path="/settings" component={SettingsEditor} />
								<Route path="/vswap" component={VswapEditor} />
								<Redirect exact path="/" to="/maps" />
							</Switch>
						</main>
					</>
				)}
				<DevDrawer />
				{isProjectDialogOpen && <ProjectDialog />}
				{isProjectCompileStatusDialogOpen && <ProjectCompileStatusDialog />}
			</FormDisableContext.Provider>
		</div>
	);
};

export default withRouter(connect(mapStateToProps, getDispatchActionMapper())(React.memo(App)));
