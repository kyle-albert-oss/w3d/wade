import * as React from "react";
import { connect } from "react-redux";
import { Box, MenuItem } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import { DeepReadonly } from "ts-essentials";

import { IWadeVswapGraphicChunk } from "../../../models/vswap";
import { getDispatchActionMapper, IActionProps } from "../../../store/redux/actions";
import {
	graphicChunksSelector,
	selectedGraphicChunkIndexSelector,
	spriteStartIndexSelector,
	vswapEditorZoomSelector,
} from "../../../store/redux/selectors";
import { AppRootState } from "../../../store/redux/types/state";
import { AppPaper } from "../../components/core/AppPaper";
import { Carousel } from "../../components/core/Carousel";
import { MaterialProps, withMaterial, useGridStyles } from "../../components/core/material";
import { ConnectedPageSlider } from "../../components/core/PageSlider";
import { SimpleZoomer } from "../../components/core/Zoomer";
import { GameGraphicCard } from "../../components/graphics/GameGraphicCard";

export interface IVswapEditorProps {}

type MappedProps = DeepReadonly<{
	graphicChunks: IWadeVswapGraphicChunk[];
	selectedGraphicChunkIndex: number;
	spritePageOffset: number | undefined;
	vswapEditorZoom: number;
	vswapEditorZoomDefault: number;
	vswapEditorZoomMax: number;
	vswapEditorZoomMin: number;
}>;

interface IProps extends IVswapEditorProps, MappedProps, IActionProps, MaterialProps {}

const mapStateToProps = (state: AppRootState, ownProps: DeepReadonly<IVswapEditorProps>): MappedProps => {
	const {
		defaultValue: vswapEditorZoomDefault,
		max: vswapEditorZoomMax,
		min: vswapEditorZoomMin,
		value: vswapEditorZoom,
	} = vswapEditorZoomSelector(state)();

	return {
		graphicChunks: graphicChunksSelector(state),
		selectedGraphicChunkIndex: selectedGraphicChunkIndexSelector(state) || 0,
		spritePageOffset: spriteStartIndexSelector(state),
		vswapEditorZoom,
		vswapEditorZoomDefault,
		vswapEditorZoomMax,
		vswapEditorZoomMin,
	};
};

const VswapEditorFC: React.FC<IProps> = (props) => {
	const {
		actions,
		classes,
		graphicChunks,
		selectedGraphicChunkIndex,
		theme,
		vswapEditorZoom,
		vswapEditorZoomDefault,
		vswapEditorZoomMax,
		vswapEditorZoomMin,
	} = props;

	const gridClasses = useGridStyles();
	const onBeforeSliderChangeCb = React.useCallback(
		(current: number, next: number) => {
			if (current !== next) {
				actions.uiUpdate({ selectedGraphicChunkIndex: next });
			}
		},
		[actions]
	);

	const onZoomChangeCb = React.useCallback(
		(value: number) => {
			actions.uiUpdate({ vswapEditorZoom: value });
		},
		[actions]
	);
	const onZoomDefaultCb = React.useCallback(() => {
		actions.uiUpdate({ vswapEditorZoom: vswapEditorZoomDefault });
	}, [actions, vswapEditorZoomDefault]);

	return (
		<Grid container direction="column" className="app-full-wh-content" spacing={1} wrap="nowrap">
			<Grid item>
				<AppPaper translucent>
					<Box p={1}>
						<ConnectedPageSlider maxIndex={graphicChunks.length - 1} selectedIndex="selectedGraphicChunkIndex" />
						<Box mt={2}>
							<Carousel
								animate
								beforeChange={onBeforeSliderChangeCb}
								centerMode
								centerHighlightStyle={{ backgroundColor: theme.palette.background.paper }}
								initialSlide={0}
								selectedIndex={selectedGraphicChunkIndex}
							>
								{graphicChunks.map((gc) => (
									<img
										key={gc.url}
										src={gc.url}
										style={{ display: "block", height: 64, imageRendering: "pixelated", width: 64, margin: theme.spacing(1) }}
									/>
								))}
							</Carousel>
						</Box>
					</Box>
				</AppPaper>
			</Grid>
			<Grid item xs className={gridClasses.fullHeight1}>
				<AppPaper className="app-height-full" translucent>
					<Box p={1}>
						<Grid container direction="column" className="app-height-full">
							<Grid item xs>
								<Grid container alignItems="center" className="app-height-full" justify="center">
									<Grid item>
										<GameGraphicCard graphicIndex={selectedGraphicChunkIndex} />
									</Grid>
								</Grid>
							</Grid>
							<Grid item className="app-width-full">
								<Box mb={1}>
									<Divider />
								</Box>
								<SimpleZoomer max={vswapEditorZoomMax} min={vswapEditorZoomMin} onZoomChange={onZoomChangeCb} value={vswapEditorZoom}>
									<MenuItem onClick={onZoomDefaultCb}>Reset ({Math.floor(vswapEditorZoomDefault * 100)}%)</MenuItem>
								</SimpleZoomer>
							</Grid>
						</Grid>
					</Box>
				</AppPaper>
			</Grid>
		</Grid>
	);
};

export const VswapEditor = connect(mapStateToProps, getDispatchActionMapper())(withMaterial(React.memo(VswapEditorFC)));
