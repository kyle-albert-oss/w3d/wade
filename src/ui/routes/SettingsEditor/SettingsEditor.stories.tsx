import * as React from "react";
import { Route } from "react-router";

import { createStory } from "../../../../stories/story-util";

import { SettingsEditor } from "./SettingsEditor";

createStory("Routes|SettingsEditor", module, {
	resizable: { width: 1170 },
	router: { initialEntries: ["/settings"] },
	style: { height: "100%", padding: 20 },
}).add("Default", () => <Route path="/settings" component={SettingsEditor} />);
