import * as React from "react";
import { Route, Switch, useRouteMatch } from "react-router";
import { Box, Grid, Toolbar, Typography } from "@material-ui/core";
import { Save } from "@material-ui/icons";
import { Provider, useSelector, useStore } from "react-redux";
import { Store } from "redux";
import _ from "lodash";
import { DeepWritable } from "ts-essentials";
import { useMount } from "react-use";

import { useGridStyles } from "../../components/core/material";
import { SettingsSideNav } from "../../components/settings/SettingsSideNav";
import { MapDefinitionEditor } from "../../components/settings/pages/MapDefinitionEditor";
import { AppPaper } from "../../components/core/AppPaper";
import { Centered } from "../../components/core/Centered";
import { IStatefulButtonProps, StatefulButton } from "../../components/core/StatefulButton";
import { useActions } from "../../../store/redux/actions";
import { ColorPresetsEditor } from "../../components/settings/pages/ColorPresetsEditor";
import { AppRootState, SettingsRootState } from "../../../store/redux/types/state";
import { configureStore } from "../../../store/redux";
import { hasUnsavedSettingsSelector, uiStateSelector } from "../../../store/redux/selectors";
import { attachSharedStoreListeners } from "../../../store/redux/listeners";
import { ProjectGeneral } from "../../components/settings/pages/ProjectGeneral";

const NoRoute: React.FC = React.memo(() => (
	<AppPaper className="app-full-wh-content" translucent>
		<Centered>
			<Typography>Select a settings page on the left.</Typography>
		</Centered>
	</AppPaper>
));

export const createSettingsStore = (store: Store<AppRootState>) => {
	const currentState = store.getState();
	const tempState = {
		...currentState,
		settingsEditor: {},
	};
	delete tempState.app;
	delete tempState.map;
	delete tempState.router;
	const currentStateCopy = _.cloneDeep(tempState as SettingsRootState) as DeepWritable<SettingsRootState>;

	const newSettingsStore: Store<SettingsRootState> = attachSharedStoreListeners(
		configureStore({ initialState: currentStateCopy as SettingsRootState })
	);
	return newSettingsStore;
};

// this needs to be its own component because it will be provided the settings store but need props passed in to hook it to the main store.
const SettingsEditorSaveButton = React.memo<IStatefulButtonProps>((props) => {
	const hasUnsavedChanges = useSelector(hasUnsavedSettingsSelector);
	return (
		<StatefulButton color="primary" disabled={!hasUnsavedChanges} startIcon={<Save />} variant="contained" {...props}>
			Save
		</StatefulButton>
	);
});

const SettingsEditorFC: React.FC = () => {
	const [settingsStore, setSettingsStore] = React.useState<Store<SettingsRootState, any>>();
	const appStore = useStore<AppRootState>();
	const uiState = useSelector(uiStateSelector);
	const [paneScrolled, setPaneScrolled] = React.useState(false);
	useMount(() => {
		// create a copy of the app store to use as our working copy of the store for changing settings
		// upon save, we can selectively merge the copy into the real store and update local config stores.
		const newSettingsStore = createSettingsStore(appStore);
		setSettingsStore(newSettingsStore);
	});
	const gridClasses = useGridStyles();
	const actions = useActions();
	const match = useRouteMatch();
	const saveAllSettings = () => {
		if (!settingsStore) {
			throw new Error("No settings store.");
		}
		actions.saveAllSettings({ state: settingsStore.getState() });
	};
	const onScroll = (e: React.UIEvent) => {
		setPaneScrolled(e.currentTarget.scrollLeft > 8);
	};

	return (
		<Grid container className="app-height-full" direction="column" spacing={1} wrap="nowrap">
			<Grid item className="app-grow">
				<Grid container className={gridClasses.fullHeight1} spacing={1} wrap="nowrap">
					<Grid item style={{ flexShrink: 0, width: 180 }}>
						<AppPaper className="app-height-full" translucent>
							<Box p={2} className="app-height-full">
								<SettingsSideNav />
							</Box>
						</AppPaper>
					</Grid>
					<Grid item className="app-overflow-x-auto app-grow" onScroll={onScroll}>
						{settingsStore && (
							<Provider store={settingsStore}>
								<Switch>
									<Route path={`${match.path}/project/general`} component={ProjectGeneral} />
									<Route path={`${match.path}/project/map-def`} render={(p) => <MapDefinitionEditor parentScrolled={paneScrolled} />} />
									<Route path={`${match.path}/global/color-presets`} component={ColorPresetsEditor} />
									<Route path={match.path} component={NoRoute} />
								</Switch>
							</Provider>
						)}
					</Grid>
				</Grid>
			</Grid>
			<Grid item>
				<AppPaper translucent>
					<Box p={1}>
						<Toolbar disableGutters variant="dense">
							<Box flexGrow={1} />
							{settingsStore && (
								<Provider store={settingsStore}>
									<SettingsEditorSaveButton onClick={saveAllSettings} state={uiState.settingsSaveStatus} />
								</Provider>
							)}
						</Toolbar>
					</Box>
				</AppPaper>
			</Grid>
		</Grid>
	);
};

export const SettingsEditor = React.memo(SettingsEditorFC);
