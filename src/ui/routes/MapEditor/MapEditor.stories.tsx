import * as React from "react";

import { createStory } from "../../../../stories/story-util";

import { MapEditor } from "./MapEditor";

createStory("Routes|MapEditor", module, { style: { height: "100%", padding: 20 } }).add("Default", () => <MapEditor />);
