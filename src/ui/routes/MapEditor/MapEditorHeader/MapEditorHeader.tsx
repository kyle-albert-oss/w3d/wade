import * as React from "react";
import { useSelector } from "react-redux";
import { Grid, ListItemIcon, ListItemText } from "@material-ui/core";
import { Delete, FileCopy, SystemUpdateAlt } from "@material-ui/icons";
import { DeepReadonly } from "ts-essentials";

import { useActions } from "../../../../store/redux/actions";
import { mapNamesSelector, mapsSelector, selectedMapIndexSelector } from "../../../../store/redux/selectors";
import { ConnectedPageSlider } from "../../../components/core/PageSlider";
import { AppRootState } from "../../../../store/redux/types/state";
import { AppIconButtonWithConfirmation, AppIconButton } from "../../../components/core/AppIconButton";
import { AppSplitButton, IAppSplitButtonOption } from "../../../components/core/AppButton";
import { ArrowMoveLeft } from "../../../components/core/icons/ArrowMoveLeft";
import { ArrowMoveRight } from "../../../components/core/icons/ArrowMoveRight";

type AddAction = "add" | "copy" | "import";
const ADD_OPTIONS: IAppSplitButtonOption<AddAction>[] = [
	{ contents: "New", extra: "add" },
	{
		contents: (
			<>
				<ListItemIcon>
					<FileCopy />
				</ListItemIcon>
				<ListItemText primary="Copy" />
			</>
		),
		extra: "copy",
	},
	{
		contents: (
			<>
				<ListItemIcon>
					<SystemUpdateAlt />
				</ListItemIcon>
				<ListItemText primary="Import" />
			</>
		),
		extra: "import",
	},
];

const MapEditorHeaderFC: React.FC = () => {
	const actions = useActions();
	const mapNames = useSelector(mapNamesSelector);
	const selectedMapIndex = useSelector(selectedMapIndexSelector);
	const numMaps = useSelector<AppRootState, number>((state) => mapsSelector(state).length);
	const maxIndex = React.useMemo(() => numMaps - 1, [numMaps]);
	const getMapLabelCb = React.useCallback((idx: number) => mapNames?.[idx], [mapNames]);

	const onDeleteCurrentMapClick = React.useCallback(() => {
		actions.deleteMap(undefined, { index: selectedMapIndex });
	}, [actions, selectedMapIndex]);
	const onAddOptionClick = React.useCallback(
		(option: DeepReadonly<IAppSplitButtonOption<AddAction>>) => {
			if (option.extra === "add") {
				actions.createNewMap({ insertionIndex: selectedMapIndex + 1 });
			} else if (option.extra === "copy") {
				actions.createNewMap({ insertionIndex: selectedMapIndex + 1, copyFromIndex: selectedMapIndex });
			} else if (option.extra === "import") {
				// TODO
			}
		},
		[actions, selectedMapIndex]
	);
	const onMoveLeftClick = React.useCallback(() => {
		actions.moveMap({ fromIndex: selectedMapIndex, toIndex: selectedMapIndex - 1 });
		actions.uiUpdate({ selectedMapIndex: selectedMapIndex - 1 });
	}, [actions, selectedMapIndex]);
	const onMoveRightClick = React.useCallback(() => {
		actions.moveMap({ fromIndex: selectedMapIndex, toIndex: selectedMapIndex + 1 });
		actions.uiUpdate({ selectedMapIndex: selectedMapIndex + 1 });
	}, [actions, selectedMapIndex]);

	return (
		<Grid container alignItems="center">
			<Grid item xs>
				<ConnectedPageSlider infinite itemLabel={getMapLabelCb} label="Map" maxIndex={maxIndex} selectedIndex="selectedMapIndex" />
			</Grid>
			<Grid item>
				<AppSplitButton color="default" onClick={onAddOptionClick} options={ADD_OPTIONS} variant="outlined" />
			</Grid>
			<Grid item>
				<AppIconButton disabled={selectedMapIndex === 0} onClick={onMoveLeftClick}>
					<ArrowMoveLeft />
				</AppIconButton>
			</Grid>
			<Grid item>
				<AppIconButton disabled={selectedMapIndex === numMaps - 1} onClick={onMoveRightClick}>
					<ArrowMoveRight />
				</AppIconButton>
			</Grid>
			<Grid item>
				<AppIconButtonWithConfirmation confirmation={"Are you sure you want to delete this map?"} onClick={onDeleteCurrentMapClick}>
					<Delete />
				</AppIconButtonWithConfirmation>
			</Grid>
		</Grid>
	);
};

export const MapEditorHeader = React.memo(MapEditorHeaderFC);
