import * as React from "react";
import { connect } from "react-redux";
import { Box, Switch, Typography, Divider, FormLabel } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { Visibility } from "@material-ui/icons";
import { DeepReadonly } from "ts-essentials";

import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { mapTileZoomSelector, uiStateSelector, activeMapDefPlaneIndexNameSelector } from "../../../../store/redux/selectors";
import { AppRootState, UiState } from "../../../../store/redux/types/state";
import { SimpleZoomer } from "../../../components/core/Zoomer";
import { AppPopoverIconButton } from "../../../components/core/AppIconButton";
import { MapPlaneDefinition } from "../../../../models/map";
import { AddRemove } from "../../../../models/wade";
import { AppSlider } from "../../../components/core/AppSlider";

type MappedProps = DeepReadonly<
	Pick<
		UiState,
		| "isMouseOverMapCanvas"
		| "mapCanvasFloorCodeOpacity"
		| "mapCanvasHiddenPlanes"
		| "mapPointerXCoord"
		| "mapPointerYCoord"
		| "mapTileZoom"
		| "mapTileZoomMin"
		| "mapTileZoomMax"
	> & {
		planes: MapPlaneDefinition[];
	}
>;

interface IProps extends MappedProps, IActionProps {}

const mapStateToProps = (state: AppRootState): MappedProps => {
	const uiState = uiStateSelector(state);
	const planes = activeMapDefPlaneIndexNameSelector(state);
	return {
		isMouseOverMapCanvas: uiState.isMouseOverMapCanvas,
		mapCanvasFloorCodeOpacity: Math.floor(uiState.mapCanvasFloorCodeOpacity * 100),
		mapCanvasHiddenPlanes: uiState.mapCanvasHiddenPlanes,
		mapPointerXCoord: uiState.mapPointerXCoord,
		mapPointerYCoord: uiState.mapPointerYCoord,
		mapTileZoom: mapTileZoomSelector(state),
		mapTileZoomMax: uiState.mapTileZoomMax,
		mapTileZoomMin: uiState.mapTileZoomMin,
		planes,
	};
};

const MapCanvasFooter: React.FC<IProps> = (props) => {
	const {
		actions,
		isMouseOverMapCanvas,
		mapCanvasFloorCodeOpacity,
		mapCanvasHiddenPlanes,
		mapPointerXCoord,
		mapPointerYCoord,
		mapTileZoom,
		mapTileZoomMin,
		mapTileZoomMax,
		planes,
	} = props;

	const xLabel = isMouseOverMapCanvas && mapPointerXCoord != null ? mapPointerXCoord : "-";
	const yLabel = isMouseOverMapCanvas && mapPointerYCoord != null ? mapPointerYCoord : "-";

	const onZoomChangeCb = React.useCallback(
		(value: number) => {
			actions.uiUpdate({ mapTileZoom: value || 1 });
		},
		[actions]
	);
	const onFloorCodeOpacityChange = React.useCallback(
		(e: React.ChangeEvent<{}>, value: number | number[]) => {
			actions.uiUpdate({ mapCanvasFloorCodeOpacity: Math.floor(value as number) / 100 });
		},
		[actions]
	);

	return (
		<Grid container alignItems="center" spacing={2}>
			<Grid item>
				<Box fontFamily="monospace" fontSize={16}>
					({xLabel}, {yLabel})
				</Box>
			</Grid>
			<Grid item xs />
			<Grid item>
				<AppPopoverIconButton icon={<Visibility />}>
					<Box p={2} style={{ minWidth: 300 }}>
						<Grid container direction="column" spacing={1}>
							<Grid item>
								<Typography variant="subtitle1">Visibility</Typography>
							</Grid>
							<Grid item>
								<Divider />
							</Grid>
							<Grid item>
								<Typography variant="subtitle2">Planes</Typography>
							</Grid>
							{planes.map((p) => {
								return (
									<Grid key={p.index} item>
										<Box display="flex" alignItems="center" justifyContent="space-between">
											<FormLabel>{p.name}</FormLabel>
											<Switch
												checked={!mapCanvasHiddenPlanes.includes(p.index)}
												color="primary"
												onChange={(e, checked) =>
													actions.uiUpdate({ mapCanvasHiddenPlanes: [p.index] }, { op: checked ? AddRemove.REMOVE : AddRemove.ADD })
												}
												size="small"
											/>
										</Box>
									</Grid>
								);
							})}
							<Grid item>
								<Divider />
							</Grid>
							<Grid item>
								<Box display="flex" alignItems="center">
									<FormLabel>Floor Code Opacity</FormLabel>
									<AppSlider
										max={100}
										min={10}
										onChangeCommitted={onFloorCodeOpacityChange}
										value={mapCanvasFloorCodeOpacity}
										valueLabelDisplay="auto"
									/>
								</Box>
							</Grid>
						</Grid>
					</Box>
				</AppPopoverIconButton>
			</Grid>
			<Grid item>
				<SimpleZoomer min={mapTileZoomMin} max={mapTileZoomMax} onZoomChange={onZoomChangeCb} value={mapTileZoom} />
			</Grid>
		</Grid>
	);
};

export default connect(mapStateToProps, getDispatchActionMapper())(React.memo(MapCanvasFooter));
