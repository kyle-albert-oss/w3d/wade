import * as React from "react";
import { connect } from "react-redux";
import { ListItemIcon } from "@material-ui/core";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Typography from "@material-ui/core/Typography";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import { DeepReadonly } from "ts-essentials";

import MapCanvasTool from "../../../../models/map-canvas-tool";
import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import {
	selectedBrushMapToolSelector,
	selectedMapToolsArraySelector,
	selectedMapToolsSetSelector,
	selectedSelectionMapToolSelector,
} from "../../../../store/redux/selectors";
import { AppRootState } from "../../../../store/redux/types/state";
import { AppToggleButton } from "../../../components/core/AppToggleButton";
import { MaterialProps, withMaterial } from "../../../components/core/material";

import { ReactComponent as DropperIcon } from "./icons/dropper.svg";
import { ReactComponent as EraserIcon } from "./icons/eraser.svg";
import { ReactComponent as FillIcon } from "./icons/fill.svg";
import { ReactComponent as PencilIcon } from "./icons/pencil.svg";
import { ReactComponent as RectangleOutlineIcon } from "./icons/rectangle-outline.svg";
import { ReactComponent as RectangleIcon } from "./icons/rectangle.svg";
import { ReactComponent as SelectionIcon } from "./icons/selection.svg";

type MappedProps = DeepReadonly<{
	selectedBrushMapTool?: MapCanvasTool;
	selectedMapTools: MapCanvasTool[];
	selectedMapToolsSet: Set<MapCanvasTool>;
	selectedSelectionMapTool?: MapCanvasTool;
}>;

interface IProps extends MappedProps, IActionProps, MaterialProps {}

interface IState {
	menuAnchorEl?: Element;
	menuType?: "brush" | "selection";
}

const mapStateToProps = (state: AppRootState): MappedProps => {
	return {
		selectedBrushMapTool: selectedBrushMapToolSelector(state),
		selectedMapTools: selectedMapToolsArraySelector(state),
		selectedMapToolsSet: selectedMapToolsSetSelector(state),
		selectedSelectionMapTool: selectedSelectionMapToolSelector(state),
	};
};

class MapToolPalette extends React.PureComponent<IProps, IState> {
	state: IState = {};

	onMenuClose = () => this.setState({ menuAnchorEl: undefined, menuType: undefined });

	trySelectToolFromEl = (el: HTMLElement) => {
		const tool = el.getAttribute("data-tool") as MapCanvasTool;
		if (!tool) {
			return false;
		}

		const { selectedMapToolsSet, actions } = this.props;
		let select = true;
		if (el.hasAttribute("data-toggle")) {
			select = !selectedMapToolsSet.has(tool);
		}
		actions.uiUpdate({ selectedMapCanvasTools: { [tool]: select } });

		return true;
	};

	onArrowClick = (buttonEl: HTMLElement) => {
		const index = parseInt(buttonEl.getAttribute("data-index") || "", 10);
		let menuType: IState["menuType"];
		if (index === 0) {
			menuType = "brush";
		} else if (index === 3) {
			menuType = "selection";
		}
		if (menuType == null) {
			return;
		}
		this.setState({ menuAnchorEl: buttonEl, menuType });
	};

	onToolClick = (e: React.MouseEvent<any>) => {
		if (this.trySelectToolFromEl(e.currentTarget)) {
			this.onMenuClose();
		}
	};

	render() {
		const { classes, selectedBrushMapTool, selectedMapTools, selectedMapToolsSet, selectedSelectionMapTool } = this.props;
		const { menuAnchorEl, menuType } = this.state;

		let menuContents: React.ReactNode | undefined;
		if (menuAnchorEl) {
			if (menuType === "brush") {
				menuContents = [
					<MenuItem
						button
						data-tool={MapCanvasTool.FILL}
						key={MapCanvasTool.FILL}
						onClick={this.onToolClick}
						selected={selectedMapToolsSet.has(MapCanvasTool.FILL)}
					>
						<ListItemIcon>
							<FillIcon className={classes.svgFill} />
						</ListItemIcon>
						<Typography>Fill</Typography>
					</MenuItem>,
					<MenuItem
						button
						data-tool={MapCanvasTool.RECTANGLE_OUTLINE}
						key={MapCanvasTool.RECTANGLE_OUTLINE}
						onClick={this.onToolClick}
						selected={selectedMapToolsSet.has(MapCanvasTool.RECTANGLE_OUTLINE)}
					>
						<ListItemIcon>
							<RectangleOutlineIcon className={classes.svgFill} />
						</ListItemIcon>
						<Typography>Rectangle Outline</Typography>
					</MenuItem>,
					<MenuItem
						button
						data-tool={MapCanvasTool.RECTANGLE_FILL}
						key={MapCanvasTool.RECTANGLE_FILL}
						onClick={this.onToolClick}
						selected={selectedMapToolsSet.has(MapCanvasTool.RECTANGLE_FILL)}
					>
						<ListItemIcon>
							<RectangleIcon className={classes.svgFill} />
						</ListItemIcon>
						<Typography>Rectangle Fill</Typography>
					</MenuItem>,
					<MenuItem
						button
						data-tool={MapCanvasTool.PENCIL}
						key={MapCanvasTool.PENCIL}
						onClick={this.onToolClick}
						selected={selectedMapToolsSet.has(MapCanvasTool.PENCIL)}
					>
						<ListItemIcon>
							<PencilIcon className={classes.svgFill} />
						</ListItemIcon>
						<Typography>Pencil</Typography>
					</MenuItem>,
				];
			} else if (menuType === "selection") {
				menuContents = [
					<MenuItem
						button
						data-tool={MapCanvasTool.SELECTION_RECTANGLE}
						key={MapCanvasTool.SELECTION_RECTANGLE}
						onClick={this.onToolClick}
						selected={selectedMapToolsSet.has(MapCanvasTool.SELECTION_RECTANGLE)}
					>
						<ListItemIcon>
							<SelectionIcon className={classes.svgFill} />
						</ListItemIcon>
						<Typography>Rectangular</Typography>
					</MenuItem>,
				];
			}
		}

		let brushToolIcon: React.ReactNode;
		switch (selectedBrushMapTool) {
			case MapCanvasTool.FILL:
				brushToolIcon = <FillIcon className={classes.svgFill} />;
				break;
			case MapCanvasTool.RECTANGLE_FILL:
				brushToolIcon = <RectangleIcon className={classes.svgFill} />;
				break;
			case MapCanvasTool.RECTANGLE_OUTLINE:
				brushToolIcon = <RectangleOutlineIcon className={classes.svgFill} />;
				break;
			case MapCanvasTool.PENCIL:
			default:
				brushToolIcon = <PencilIcon className={classes.svgFill} />;
				break;
		}

		let selectionToolIcon: React.ReactNode;
		switch (selectedSelectionMapTool) {
			case MapCanvasTool.SELECTION_RECTANGLE:
			default:
				selectionToolIcon = <SelectionIcon className={classes.svgFill} />;
				break;
		}

		return (
			<>
				<ToggleButtonGroup size="small" value={selectedMapTools}>
					<AppToggleButton
						arrow="up"
						className={classes.toggleButton32}
						data-index={0}
						data-tool={selectedBrushMapTool || MapCanvasTool.PENCIL}
						onArrowClick={this.onArrowClick}
						onClick={this.onToolClick}
						value={selectedBrushMapTool || MapCanvasTool.PENCIL}
					>
						{brushToolIcon}
					</AppToggleButton>
					<AppToggleButton
						className={classes.toggleButton32}
						data-index={1}
						data-toggle
						data-tool={MapCanvasTool.ERASER}
						onClick={this.onToolClick}
						value={MapCanvasTool.ERASER}
					>
						<EraserIcon className={classes.svgFill} />
					</AppToggleButton>
					<AppToggleButton
						className={classes.toggleButton32}
						data-index={2}
						data-toggle
						data-tool={MapCanvasTool.DROPPER}
						onClick={this.onToolClick}
						value={MapCanvasTool.DROPPER}
					>
						<DropperIcon className={classes.svgFill} />
					</AppToggleButton>
					<AppToggleButton
						arrow="up"
						className={classes.toggleButton32}
						data-index={3}
						data-tool={selectedSelectionMapTool || MapCanvasTool.SELECTION_RECTANGLE}
						onArrowClick={this.onArrowClick}
						onClick={this.onToolClick}
						value={selectedSelectionMapTool || MapCanvasTool.SELECTION_RECTANGLE}
					>
						{selectionToolIcon}
					</AppToggleButton>
				</ToggleButtonGroup>
				<Menu
					anchorEl={menuAnchorEl}
					anchorOrigin={{
						horizontal: "center",
						vertical: "top",
					}}
					getContentAnchorEl={null}
					onClose={this.onMenuClose}
					open={!!menuAnchorEl}
					transformOrigin={{
						horizontal: "center",
						vertical: "bottom",
					}}
				>
					{menuContents}
				</Menu>
			</>
		);
	}
}

export default connect(mapStateToProps, getDispatchActionMapper())(withMaterial(MapToolPalette));
