import * as React from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { DeepReadonly } from "ts-essentials";

import { getDispatchActionMapper, IActionProps } from "../../../../store/redux/actions";
import { selectedMapNameSelector } from "../../../../store/redux/selectors";
import { AppRootState } from "../../../../store/redux/types/state";

type MappedProps = DeepReadonly<{
	selectedMapName: string;
}>;

interface IProps extends MappedProps, IActionProps {}

const mapStateToProps = (state: AppRootState): MappedProps => {
	return {
		selectedMapName: selectedMapNameSelector(state) || "",
	};
};

const MapCanvasHeader: React.FC<IProps> = (props) => {
	const { selectedMapName } = props;

	return (
		<Grid container alignItems="center">
			<Grid item style={{ width: 204 }}>
				<Typography>{selectedMapName}</Typography>
			</Grid>
			<Grid item xs />
		</Grid>
	);
};

export default connect(mapStateToProps, getDispatchActionMapper())(React.memo(MapCanvasHeader));
