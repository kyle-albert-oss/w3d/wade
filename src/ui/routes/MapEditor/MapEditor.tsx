import * as React from "react";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import { Box, useTheme } from "@material-ui/core";

import { AppPaper } from "../../components/core/AppPaper";
import { useGridStyles } from "../../components/core/material";
import { MapCanvas } from "../../components/mapping/MapCanvas";
import { MapDefPanel } from "../../components/mapping/MapDefPanel";

import MapCanvasFooter from "./MapCanvasFooter";
import MapCanvasHeader from "./MapCanvasHeader";
import { MapEditorHeader } from "./MapEditorHeader";
import { MapTileSelections } from "./MapTileSelections";
import MapToolPalette from "./MapToolPalette";

const MapEditorFC: React.FC = () => {
	const theme = useTheme();
	const gridClasses = useGridStyles();
	const canvas = <MapCanvas renderMode="webgl" />;

	return (
		<Grid container direction="column" className="app-full-wh-content" spacing={1} wrap="nowrap">
			<Grid item>
				<AppPaper translucent>
					<Box p={1}>
						<MapEditorHeader />
					</Box>
				</AppPaper>
			</Grid>
			<Grid item xs className={gridClasses.fullHeight1}>
				<Grid container direction="row" spacing={1} alignContent="stretch" className={gridClasses.fullHeight1}>
					<Grid item xs className={gridClasses.fullHeight1}>
						<AppPaper className="app-height-full" translucent>
							<Box className="app-height-full" p={1}>
								<Grid container direction="column" className="app-height-full">
									<Grid item className="app-width-full">
										<MapCanvasHeader />
										<Box mt={1}>
											<Divider />
										</Box>
									</Grid>
									<Grid item xs>
										{canvas}
									</Grid>
									<Grid item className="app-width-full">
										<Box mb={1}>
											<Divider />
										</Box>
										<MapCanvasFooter />
									</Grid>
								</Grid>
							</Box>
						</AppPaper>
					</Grid>
					<Grid item xs={2} className={gridClasses.fullHeight1}>
						<AppPaper className="app-height-full app-overflow-hidden" translucent>
							<Box className="app-height-full" p={1}>
								<Grid container className="app-height-full app-overflow-hidden" direction="column">
									<Grid item xs>
										<MapDefPanel />
									</Grid>
									<Grid item className="app-width-full">
										<MapToolPalette />
									</Grid>
									<Grid item>
										<Box mt={1} mb={1}>
											<Divider />
										</Box>
									</Grid>
									<Grid item className="app-width-full" style={{ height: 64 - theme.spacing(2) }}>
										<MapTileSelections />
									</Grid>
								</Grid>
							</Box>
						</AppPaper>
					</Grid>
				</Grid>
			</Grid>
		</Grid>
	);
};

export const MapEditor = React.memo(MapEditorFC);
