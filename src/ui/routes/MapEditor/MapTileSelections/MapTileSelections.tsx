import * as React from "react";
import { useSelector } from "react-redux";
import { Box, Typography, useTheme } from "@material-ui/core";

import {
	selectedTileMouse1Selector,
	selectedTileMouse2Selector,
	mapDefTileSizeSelector,
	activeMapDefTileFnSelector,
} from "../../../../store/redux/selectors";
import { MapTile } from "../../../components/mapping/MapTile";
import MapTileTooltip from "../../../components/mapping/MapTileTooltip";

const MapTileSelectionsFC: React.FC = () => {
	const theme = useTheme();
	const selectedTileM1 = useSelector(selectedTileMouse1Selector);
	const selectedTileM2 = useSelector(selectedTileMouse2Selector);
	const mapTileFn = useSelector(activeMapDefTileFnSelector);
	const selectedTileM1Data = React.useMemo(
		() => (!selectedTileM1 || !mapTileFn ? undefined : mapTileFn(selectedTileM1.plane, selectedTileM1.tileCode)),
		[selectedTileM1, mapTileFn]
	);
	const mapTileSize = Math.min((64 - theme.spacing(2)) / 1.5, useSelector(mapDefTileSizeSelector));
	const halfSize = mapTileSize / 2;

	return (
		<Box className="app-width-full" display="flex" alignItems="center">
			<Box position="relative" flexShrink={0} height={mapTileSize + halfSize} width={mapTileSize + halfSize}>
				<Box position="absolute" left={0} top={0} zIndex={3}>
					<MapTileTooltip plane={selectedTileM1?.plane} tileCode={selectedTileM1?.tileCode} wrap="div">
						<MapTile
							isolated
							plane={selectedTileM1?.plane}
							renderMode="basic"
							renderWhenBlank
							size={mapTileSize}
							tileCode={selectedTileM1?.tileCode}
						/>
					</MapTileTooltip>
				</Box>
				<Box position="absolute" left={halfSize} top={halfSize} zIndex={2}>
					<MapTileTooltip plane={selectedTileM2?.plane} tileCode={selectedTileM2?.tileCode} wrap="div">
						<MapTile
							isolated
							plane={selectedTileM2?.plane}
							renderMode="basic"
							renderWhenBlank
							size={mapTileSize}
							tileCode={selectedTileM2?.tileCode}
						/>
					</MapTileTooltip>
				</Box>
			</Box>
			{selectedTileM1Data && (
				<Box ml={1} minWidth={0}>
					<Typography noWrap>
						{selectedTileM1Data.code}: {selectedTileM1Data.label}
					</Typography>
				</Box>
			)}
		</Box>
	);
};

export const MapTileSelections = React.memo(MapTileSelectionsFC);
