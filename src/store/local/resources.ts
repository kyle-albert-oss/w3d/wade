import * as path from "path";

import * as fs from "fs-extra";

import { WadeResourceType } from "../../models/resources";
import { maybeInElectronEnv } from "../../util/env";

const wadeResourceDirFor = (resourceType: WadeResourceType, basePath?: string): string => {
	const dirPath = path.resolve(basePath || getRootWadeResourceDir(), `${resourceType}s`);
	if (!fs.existsSync(dirPath)) {
		fs.mkdirpSync(dirPath);
	}
	return dirPath;
};

export const getRootWadeResourceDir = () =>
	maybeInElectronEnv(
		(el) => process.env.RESOURCE_DIR || path.resolve((el.app || el.remote.app).getPath("userData"), "app-resources"),
		() => process.env.RESOURCE_DIR || path.resolve(process.cwd(), "tmp")
	);
export const getWadeMapDefPath = (basePath?: string, relativePath?: string): string =>
	path.resolve(wadeResourceDirFor("map-def", basePath), relativePath || "");
export const getWadePalettePath = (basePath?: string, relativePath?: string): string =>
	path.resolve(wadeResourceDirFor("palette", basePath), relativePath || "");
export const getWadeProjectPath = (basePath?: string, relativePath?: string): string =>
	path.resolve(wadeResourceDirFor("project", basePath), relativePath || "");
export const getWadeZipStagingPath = (basePath?: string, relativePath?: string): string =>
	path.resolve(basePath || getRootWadeResourceDir(), "staging", relativePath || "");
