import Conf from "conf";

import { MAX_RECENT_PROJECTS } from "../../models/wade";

import { AppConfigKeys, IAppConfig } from "./config";

export const promoteRecentProject = (projectId: string, store: Conf<IAppConfig> = window.appConfig) => {
	const recentProjectIds = [...(store.get(AppConfigKeys.Projects.Recent) || [])];
	const newRecentProjectIds = [
		projectId,
		...recentProjectIds.filter((pId) => pId !== projectId).slice(0, Math.max(0, MAX_RECENT_PROJECTS - 1)),
	];
	store.set({
		[AppConfigKeys.Projects.Recent]: newRecentProjectIds,
	});
};

export const onProjectsDeleted = (projectIds: readonly string[], store: Conf<IAppConfig> = window.appConfig) => {
	const update: Partial<IAppConfig> = {};

	const recentProjectIds = [...(store.get(AppConfigKeys.Projects.Recent) || [])];
	const newRecentProjectIds = recentProjectIds.filter((pId) => !projectIds.includes(pId)).slice(0, Math.max(0, MAX_RECENT_PROJECTS - 1));

	if (recentProjectIds.length !== newRecentProjectIds.length) {
		update[AppConfigKeys.Projects.Recent] = newRecentProjectIds;
	}

	if (Object.keys(update).length > 0) {
		store.set(update);
	}
};
