import Conf, { Options as ConfOptions } from "conf";

import { DEFAULT_COLOR_PRESETS } from "../../models/wade";

export const AppConfigKeys = {
	ColorPresets: "color-presets",
	Projects: {
		Recent: "projects.recent",
	} as const,
} as const;

export interface IAppConfig {
	[AppConfigKeys.ColorPresets]: string[];
	[AppConfigKeys.Projects.Recent]: string[];
}

export const defaultStoreValues: Readonly<IAppConfig> = {
	[AppConfigKeys.ColorPresets]: DEFAULT_COLOR_PRESETS,
	[AppConfigKeys.Projects.Recent]: [],
};

export const getLocalConfigOptions = (cwd: string): ConfOptions<any> => ({
	accessPropertiesByDotNotation: false,
	configName: "wade-config",
	cwd,
	defaults: defaultStoreValues,
});

export const fetchAppConfigKeys = <K extends keyof IAppConfig>(
	keys: ReadonlyArray<K>,
	store: Conf<IAppConfig>
): Partial<Pick<IAppConfig, K>> =>
	keys.reduce<Partial<Pick<IAppConfig, K>>>((acc, key) => {
		acc[key] = store.get(key);
		return acc;
	}, {});
