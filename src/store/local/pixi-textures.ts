import * as PIXI from "pixi.js";

export const clearGlobalTexture = (key: string): void => {
	const texture = getGlobalTexture(key);
	if (texture) {
		PIXI.Texture.removeFromCache(texture);
	}
};
export const getGlobalTexture = (key: string): PIXI.Texture | undefined => PIXI.utils.TextureCache[key];
