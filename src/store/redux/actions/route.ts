import { push, replace } from "connected-react-router";

import { AppThunkAction } from "../types/action";

export interface IUpdateRouteOpts {
	type?: "push" | "replace";
}

export const updateRoute = (path: string, options: IUpdateRouteOpts = {}): AppThunkAction => {
	return async (dispatch, getState) => {
		const { type = "push" } = options;
		if (type === "push") {
			return dispatch(push(path));
		} else {
			return dispatch(replace(path));
		}
	};
};
