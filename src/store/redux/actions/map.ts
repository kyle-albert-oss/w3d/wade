import _ from "lodash";
import { createAction, createAsyncAction } from "typesafe-actions";
import { IGameMap, IGameMapPlane } from "wolf3d-data";
import { DeepReadonly } from "ts-essentials";

import { IEditableMap } from "../../../models/map";
import {
	selectedTileMouse1Selector,
	selectedTileMouse2Selector,
	uiStateSelector,
	mapsSelector,
	activeProjectSelector,
	activeMapDefSelector,
} from "../selectors";
import { AppThunkAction, IActionDefaultError } from "../types/action";
import { IMapOpExtra, IMapOpOptions, MapOperation } from "../types/map-ops";
import { AppRootState } from "../types/state";
import { ResourceSrc } from "../../../models/project";

import { uiUpdate } from "./ui";

export const clearPendingMapOperations = createAction("CLEAR_PENDING_MAP_OPERATIONS")<undefined>();
export const commitPendingMapOperations = createAction("COMMIT_PENDING_MAP_OPERATIONS")<undefined>();
export const setActiveMaps = createAction("SET_ACTIVE_MAPS")<{ maps: DeepReadonly<IEditableMap[]>; src: ResourceSrc }>();
export const doPendingMapOperationAsync = createAsyncAction(
	"PENDING_MAP_OPERATION",
	"PENDING_MAP_OPERATION_SUCCESS",
	"PENDING_MAP_OPERATION_ERROR"
)<{ ops: OneOrMany<MapOperation>; supplement: IMapOpExtra }, {}, IActionDefaultError>();
export const deleteMap = createAction("DELETE_MAP")<void, { index: number }>();
interface ICreateMapOpts {
	copyFromIndex?: number;
	insertionIndex: number;
	map?: IGameMap;
}
export const createMapAsync = createAsyncAction("CREATE_MAP", "CREATE_MAP_SUCCESS", "CREATE_MAP_ERROR")<
	ICreateMapOpts,
	IGameMap,
	IActionDefaultError
>();
export const moveMap = createAction("MOVE_MAP")<{ fromIndex: number; toIndex: number }>();
export const exportMapAsync = createAsyncAction("EXPORT_MAP", "EXPORT_MAP_SUCCESS", "EXPORT_MAP_ERROR")<
	undefined,
	undefined,
	IActionDefaultError
>();

export const doPendingMapOperation = (input: OneOrMany<MapOperation>, options: IMapOpOptions = {}): AppThunkAction => async (
	dispatch,
	getState
) => {
	const state: AppRootState = getState();
	const selectedTools = uiStateSelector(state).selectedMapCanvasTools;
	if (!selectedTools || Object.keys(selectedTools).length === 0) {
		return;
	}

	const { clearPending, commit } = options;
	const ops = _.castArray(input);
	const m1Selection = selectedTileMouse1Selector(state);
	const m2Selection = selectedTileMouse2Selector(state);

	const supplement: IMapOpExtra = {
		clearPending,
		commit,
		m1Selection,
		m2Selection,
		selectedTools,
	};

	dispatch(doPendingMapOperationAsync.request({ ops, supplement }));
};

export const createNewMap = (opts: ICreateMapOpts): AppThunkAction => async (dispatch, getState) => {
	dispatch(createMapAsync.request(opts));

	try {
		const { insertionIndex, copyFromIndex, map } = opts;

		let resultMap: IGameMap;
		if (map) {
			resultMap = {
				...map,
				index: insertionIndex,
			};
		} else if (copyFromIndex != null) {
			const curState = getState();
			const maps = mapsSelector(curState);
			if (!maps?.[copyFromIndex]) {
				throw new Error(`No map found at index ${copyFromIndex}.`);
			}
			const srcMap = maps[copyFromIndex].data as IGameMap;
			resultMap = {
				...srcMap,
				name: `${srcMap.name} (c)`.slice(0, 16),
				index: insertionIndex,
			};
		} else {
			const curState = getState();
			const project = activeProjectSelector(curState);
			const mapDef = activeMapDefSelector(curState);
			if (!project) {
				throw new Error(`Cannot add map without an active project.`);
			}
			if (!mapDef) {
				throw new Error(`Cannot add map without an active map definition.`);
			}
			const { mapHeight: height = 64, mapWidth: width = 64 } = project;
			const { planes } = mapDef;
			const numPlanes = Object.keys(planes).length;

			resultMap = {
				height,
				index: insertionIndex,
				name: `Map ${insertionIndex + 1}`,
				planes: (() => {
					const result: IGameMapPlane[] = [];
					for (let plane = 0; plane < numPlanes; plane++) {
						const planeData: number[][] = [];
						for (let y = 0; y < height; y++) {
							planeData.push([]);
							for (let x = 0; x < width; x++) {
								planeData[y].push(0);
							}
						}
						result.push({
							height,
							index: plane,
							data: planeData,
							width,
						});
					}
					return result;
				})(),
				width,
			};
		}

		await dispatch(createMapAsync.success(resultMap));
		dispatch(uiUpdate({ selectedMapIndex: insertionIndex }));
	} catch (e) {
		dispatch(createMapAsync.failure(e));
		throw e;
	}
};

export const clearMapSelection = createAction("CLEAR_MAP_SELECTION")<undefined>();
