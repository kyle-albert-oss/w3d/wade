import * as PIXI from "pixi.js";
import { createAction, createAsyncAction } from "typesafe-actions";

import { IGamePalette } from "../../../models/palette";
import { IWadeVswapGraphicChunk } from "../../../models/vswap";
import { convertBitmapToObjUrl } from "../../../util/canvases";
import { AppThunkAction, IActionDefaultError } from "../types/action";
import { GraphicState } from "../types/state";
import { DEFAULT_MAP_TEXTURE_OPTS } from "../../../models/pixi";

export const setActivePalette = createAction("SET_ACTIVE_PALETTE")<IGamePalette>();
export const setActiveVswapAsync = createAsyncAction("SET_ACTIVE_VSWAP", "SET_ACTIVE_VSWAP_SUCCESS", "SET_ACTIVE_VSWAP_ERROR")<
	undefined,
	Partial<GraphicState>,
	IActionDefaultError
>();

export const setActiveVswap = (vswapData: Partial<GraphicState>): AppThunkAction => async (dispatch, getState) => {
	dispatch(setActiveVswapAsync.request());
	try {
		// TODO: refactor as store listener

		// unload previous resources
		const currentGraphicChunks = getState().graphic.graphicChunks ?? ([] as IWadeVswapGraphicChunk[]);
		currentGraphicChunks
			.filter((gc) => gc.url != null)
			.map((gc) => gc.url!)
			.forEach((url) => {
				try {
					URL.revokeObjectURL(url);
				} catch (e) {
					console.warn(`Failed to revoke object url ${url}:`, e);
				}
			});

		const graphicChunks = vswapData?.graphicChunks as IWadeVswapGraphicChunk[];

		if (graphicChunks) {
			for (const gc of graphicChunks) {
				if (!gc.rawData) {
					continue;
				}
				gc.url = await convertBitmapToObjUrl(gc.rawData);
				PIXI.Texture.from(gc.url, DEFAULT_MAP_TEXTURE_OPTS);
				delete gc.rawData;
			}
		}

		dispatch(setActiveVswapAsync.success(vswapData));
	} catch (e) {
		dispatch(setActiveVswapAsync.failure(e));
		throw e;
	}
	return true;
};
