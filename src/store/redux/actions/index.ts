import { MapDispatchToPropsFunction, useDispatch } from "react-redux";
import { createSelector } from "reselect";
import { OmitProperties } from "ts-essentials";
import { ActionType, AsyncActionCreatorBuilder, TypeConstant } from "typesafe-actions";

import * as Objects from "../../../util/objects";
import { AppDispatch, AppThunkAction } from "../types/action";

import * as BootstrapActions from "./app";
import * as DevActions from "./dev";
import * as GraphicActions from "./graphic";
import * as MapActions from "./map";
import * as MapDefActions from "./map-def";
import * as ProjectActions from "./project";
import * as RouteActions from "./route";
import * as UiActions from "./ui";

type AsyncActionType = AsyncActionCreatorBuilder<[TypeConstant, any], [TypeConstant, any], [TypeConstant, any]>;

// This includes the async action creators to properly type the reducers. The thunk actions should be used, not those.
const AllActions = {
	...BootstrapActions,
	...DevActions,
	...GraphicActions,
	...MapActions,
	...MapDefActions,
	...ProjectActions,
	...RouteActions,
	...UiActions,
} as const;

type AllAppActionsType = typeof AllActions;

declare module "typesafe-actions" {
	// eslint-disable-next-line @typescript-eslint/interface-name-prefix
	interface Types {
		RootAction: ActionType<typeof AllActions>;
	}
}

type ThunkActionMorph<T> = T extends (...args: infer P) => infer R ? (...args: P) => R extends AppThunkAction<infer U> ? U : R : T;
type ThunkMorphed<T> = {
	[K in keyof T]: ThunkActionMorph<T[K]>;
};
export type AppActionsType<QueryParams = any> = ThunkMorphed<OmitProperties<AllAppActionsType, AsyncActionType>>;
const mappedActions = Object.entries(AllActions)
	.filter(([k]) => !k.startsWith("_")) // ⚠️ HACK: all AsyncActionTypes start with underscore.
	.reduce((acc, [k, v]) => {
		acc[k] = v;
		return acc;
	}, {} as any) as AppActionsType;
const mappedActionKeys = Objects.typedKeysOf(mappedActions);

export type MaybeConnectedProps<P, CP> = P & { actions?: AppActionsType; dispatch?: AppDispatch } & Omit<CP, keyof P>;
export interface IActionProps<QueryParams = any> {
	actions: AppActionsType<QueryParams>;
}
export const mapActions = (dispatch: AppDispatch): AppActionsType =>
	mappedActionKeys.reduce((acc, key) => {
		const actionFn = mappedActions[key] as (...args: any[]) => any;
		acc[key] = (...args: any[]) => dispatch(actionFn(...args));
		return acc;
	}, {} as any);

// memomize the result so referential equality is maintained for actions
const actionsSelector = createSelector(
	(dispatch: AppDispatch) => dispatch,
	(dispatch) => ({ ...mapActions(dispatch) })
);

export const getDispatchActionMapper = <OWNPROPS = any>(): MapDispatchToPropsFunction<IActionProps, OWNPROPS> => (
	dispatch: AppDispatch,
	ownProps: OWNPROPS
): IActionProps => ({
	actions: actionsSelector(dispatch),
});

export const useActions = (): AppActionsType => {
	const dispatch = useDispatch();
	return actionsSelector(dispatch);
};

export { BootstrapActions, GraphicActions, MapActions, MapDefActions, ProjectActions, RouteActions, UiActions };

export default AllActions;
