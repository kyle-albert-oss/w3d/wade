import { DeepReadonly } from "ts-essentials";
import { createAction, createAsyncAction } from "typesafe-actions";
import { IGameMap } from "wolf3d-data";

import { GameProject, GameProjectType, ICompileProjectResult, NewGameProject, ResourceSrc } from "../../../models/project";
import { GameResourceFilename, GameResourceId, GameResourceRef } from "../../../models/resources";
import { Arrays } from "../../../util/collections";
import { isElectronEnv, isStorybookEnv } from "../../../util/env";
import { throwIfNil } from "../../../util/objects";
import { generateUUID, anyContainsIgnoreCase } from "../../../util/strings";
import { withResourceWorker } from "../../../workers";
import { onProjectsDeleted, promoteRecentProject } from "../../local/project";
import {
	activeProjectSelector,
	mapsSelector,
	mapSaveTargetSelector,
	activePk3StagingDirSelector,
	availableMapDefRefsArraySelector,
	availablePaletteRefsArraySelector,
} from "../selectors";
import { AppThunkAction, IActionDefaultError } from "../types/action";
import { getRootWadeResourceDir } from "../../local/resources";
import { IEditableMap } from "../../../models/map";
import { WadeMapDefinition, WL6_MAP_DEF_ID } from "../../../models/map-definition";
import { IGamePalette, WL6_PAL_ID } from "../../../models/palette";
import { GraphicState } from "../types/state";
import { allKeyed } from "../../../util/async";
import { ReadResourceReturnType } from "../../../workers/shared";

import { uiToggle } from "./ui";
import { setActiveMapDef } from "./map-def";
import { setActiveMaps } from "./map";
import { setActivePalette, setActiveVswap } from "./graphic";

export const compileActiveProjectAsync = createAsyncAction(
	"COMPILE_ACTIVE_PROJECT",
	"COMPILE_ACTIVE_PROJECT_SUCCESS",
	"COMPILE_ACTIVE_PROJECT_ERROR"
)<boolean, DeepReadonly<Partial<ICompileProjectResult>> | undefined, IActionDefaultError>();
export const updateProjectCompileStatus = createAction("UPDATE_PROJECT_COMPILE_STATUS")<
	DeepReadonly<Partial<ICompileProjectResult>> | undefined
>();
export const createProjectAsync = createAsyncAction("CREATE_PROJECT", "CREATE_PROJECT_SUCCESS", "CREATE_PROJECT_ERROR")<
	NewGameProject,
	GameResourceRef,
	IActionDefaultError
>();
export const deleteProjectsAsync = createAsyncAction("DELETE_PROJECTS", "DELETE_PROJECTS_SUCCESS", "DELETE_PROJECTS_ERROR")<
	readonly GameResourceId[],
	readonly GameResourceId[],
	IActionDefaultError
>();
export const getProjectAsync = createAsyncAction("GET_PROJECT", "GET_PROJECT_SUCCESS", "GET_PROJECT_ERROR")<
	GameResourceId,
	GameProject,
	IActionDefaultError
>();
export const setActiveProjectAsync = createAsyncAction("OPEN_PROJECT", "OPEN_PROJECT_SUCCESS", "OPEN_PROJECT_ERROR")<
	GameResourceId,
	{ project: DeepReadonly<GameProject>; pk3StagingDir?: string },
	IActionDefaultError
>();
export const updateActiveProjectAsync = createAsyncAction(
	"UPDATE_ACTIVE_PROJECT",
	"UPDATE_ACTIVE_PROJECT_SUCCESS",
	"UPDATE_ACTIVE_PROJECT_ERROR"
)<DeepReadonly<Partial<GameProject>>, boolean | undefined, IActionDefaultError>();

export const compileActiveProject = (openDialog: boolean): AppThunkAction => async (dispatch, getState) => {
	try {
		dispatch(compileActiveProjectAsync.request(openDialog));

		const state = getState();
		const project = throwIfNil(activeProjectSelector(state), () => new Error(`Cannot compile active project because there is none.`));

		let compilePk3 = false;
		const editableMaps = mapsSelector(state);
		const areMapsUnsaved = Arrays.anyMatch(editableMaps, (em) => em.hasUnsavedChanges);
		const pk3StagingDir = activePk3StagingDirSelector(state);

		if (areMapsUnsaved) {
			dispatch(updateProjectCompileStatus({ maps: false }));
			const mapSaveTarget = mapSaveTargetSelector(state);
			if (mapSaveTarget === "pk3") {
				if (!pk3StagingDir) {
					throw new Error(`New PK3 not supported yet.`);
				}
				compilePk3 = true;
				dispatch(updateProjectCompileStatus({ pk3: false }));
				await withResourceWorker((w) =>
					w.writePk3StagingMaps({
						maps: editableMaps,
						pk3StagingDir,
					})
				);
			} else if (mapSaveTarget === GameResourceFilename.GAMEMAPS) {
				const gameResourceDir = throwIfNil(
					project.gameResourceDir,
					() => new Error(`Cannot save GAMEMAPS because there's no project game resource directory.`)
				);
				const maps = editableMaps.map((em) => em.data as IGameMap);
				await withResourceWorker((w) =>
					w.writeGameMaps({
						gameResourceDir,
						gameResourceExt: project.gameResourceExt ?? "WL6",
						maps,
					})
				);
			} else {
				throw new Error(`Unsupported map target ${mapSaveTarget}.`);
			}
			dispatch(updateProjectCompileStatus({ maps: true }));
		}

		if (compilePk3) {
			await withResourceWorker((w) => w.saveStagingPk3({ pk3StagingDir: pk3StagingDir!, pk3FilePath: project.pk3FilePath! }));
			dispatch(updateProjectCompileStatus({ pk3: true }));
		}

		dispatch(compileActiveProjectAsync.success(undefined));
	} catch (e) {
		dispatch(compileActiveProjectAsync.failure({ error: e }));
		throw e;
	}
};

export const createProject = (project: NewGameProject): AppThunkAction => async (dispatch, getState) => {
	try {
		const p = project as GameProject;
		p.id = generateUUID();

		dispatch(createProjectAsync.request(p));

		if (isElectronEnv()) {
			// determine what map def, palette is best to assign

			if (p.gameResourceExt) {
				const ext = p.gameResourceExt;
				const state = getState();

				if (!p.mapDefinitionId) {
					const availableMapDefs = availableMapDefRefsArraySelector(state);
					p.mapDefinitionId = availableMapDefs.find((md) => anyContainsIgnoreCase(md.extensions, ext))?.id;
				}

				if (!p.paletteId) {
					const availablePalettes = availablePaletteRefsArraySelector(state);
					p.paletteId = availablePalettes.find((pal) => anyContainsIgnoreCase(pal.extensions, ext))?.id;
				}
			}

			p.mapDefinitionId = p.mapDefinitionId || WL6_MAP_DEF_ID;
			p.paletteId = p.paletteId || WL6_PAL_ID;

			const resourceRef = await withResourceWorker((w) => w.writeProjectFile(getRootWadeResourceDir(), p));
			if (resourceRef == null) {
				// noinspection ExceptionCaughtLocallyJS
				throw new Error(`createProject: no response from worker.`);
			}

			dispatch(createProjectAsync.success(resourceRef));
			dispatch(setActiveProject(resourceRef.id));
		} else if (isStorybookEnv()) {
			dispatch(
				createProjectAsync.success({
					id: p.id,
					name: p.name,
				})
			);
		}
	} catch (e) {
		dispatch(createProjectAsync.failure({ error: e }));
		throw e;
	}
};

export const updateActiveProject = (opts: {
	project?: DeepReadonly<Partial<GameProject>>;
	persist?: boolean | DeepReadonly<GameProject>;
}): AppThunkAction<Promise<void>> => async (dispatch, getState) => {
	let activeProject: DeepReadonly<GameProject> | undefined;
	try {
		const { project, persist } = opts;

		if (!project && !persist) {
			return;
		}
		if (project) {
			await dispatch(updateActiveProjectAsync.request(project));
		}
		if (persist) {
			activeProject = typeof persist === "object" ? persist : activeProjectSelector(getState());
			if (!activeProject) {
				throw new Error("No currently active project.");
			}
			await withResourceWorker((w) => w.writeProjectFile(getRootWadeResourceDir(), activeProject!));
		}

		dispatch(updateActiveProjectAsync.success(!!persist));
	} catch (e) {
		dispatch(updateActiveProjectAsync.failure({ error: e }));
		throw e;
	}
};

export const deleteProjects = (projectIds: readonly GameResourceId[]): AppThunkAction => async (dispatch, getState) => {
	try {
		dispatch(deleteProjectsAsync.request(projectIds));
		if (isElectronEnv()) {
			const response = await withResourceWorker((w) =>
				w.deleteResource({ baseResourcePath: getRootWadeResourceDir(), request: { ids: projectIds, type: "project" } })
			);
			if (response == null) {
				// noinspection ExceptionCaughtLocallyJS
				throw new Error(`deleteProjects: no response from worker.`);
			}
			const { ids } = response;

			onProjectsDeleted(ids); // update local store
			dispatch(deleteProjectsAsync.success(ids));
		} else if (isStorybookEnv()) {
			dispatch(deleteProjectsAsync.success(projectIds));
		}
	} catch (e) {
		dispatch(deleteProjectsAsync.failure({ error: e }));
		throw e;
	}
};

type WorkerResourcePromise<T> = Promise<ReadResourceReturnType<T>>;
type ProjectResourceLoads = {
	gameMaps?: Promise<IEditableMap[]>;
	mapDef?: WorkerResourcePromise<WadeMapDefinition>;
	palette?: WorkerResourcePromise<IGamePalette>;
	pk3Maps?: Promise<IEditableMap[]>;
	vswap?: WorkerResourcePromise<Partial<GraphicState>>;
};

export const setActiveProject = (projectId: string): AppThunkAction => async (dispatch, getState) => {
	try {
		dispatch(setActiveProjectAsync.request(projectId));

		const state = getState();

		const oldPk3StagingDir = activePk3StagingDirSelector(state);
		if (oldPk3StagingDir) {
			await withResourceWorker((w) => w.fsDelete(oldPk3StagingDir));
		}
		const baseResourcePath = getRootWadeResourceDir();
		const response = await withResourceWorker((w) =>
			w.readResource({ baseResourcePath, extra: { projectAction: "open" }, request: { id: projectId, type: "project" } })
		);
		const { resourceData, extra } = response;
		const project = resourceData as GameProject;

		const { mapDefinitionId, paletteId, type } = project;
		const { gamemapsFilePath, mapheadFilePath, pk3StagingDir, vswapFilePath } = extra || {};

		const resourceLoads: ProjectResourceLoads = {};
		if (paletteId && vswapFilePath) {
			resourceLoads.palette = withResourceWorker((w) => w.readResource({ baseResourcePath, request: { id: paletteId, type: "palette" } }));
			resourceLoads.vswap = withResourceWorker((w) =>
				w.readResource({ baseResourcePath, request: { path: [vswapFilePath, paletteId], type: GameResourceFilename.VSWAP } })
			);
		}
		if (mapDefinitionId) {
			resourceLoads.mapDef = withResourceWorker((w) =>
				w.readResource({ baseResourcePath, request: { id: mapDefinitionId, type: "map-def" } })
			);
		}

		let mapSrc: ResourceSrc;
		if (type === GameProjectType.ECWOLF) {
			mapSrc = "pk3";
			if (pk3StagingDir) {
				resourceLoads.pk3Maps = withResourceWorker((w) => w.readPk3StagingMaps({ pk3StagingDir })).then((maps) =>
					maps.map((m, i): IEditableMap => ({ data: m.gameMap, fileName: m.fileName, index: i }))
				);
			} else {
				resourceLoads.pk3Maps = Promise.resolve([] as IEditableMap[]);
			}
		} else {
			mapSrc = GameResourceFilename.GAMEMAPS;
			if (gamemapsFilePath && mapheadFilePath) {
				resourceLoads.gameMaps = withResourceWorker((w) =>
					w.readResource({ baseResourcePath, request: { path: [gamemapsFilePath, mapheadFilePath], type: GameResourceFilename.GAMEMAPS } })
				).then((r) => r.resourceData.map((gm: IGameMap, i: number) => ({ index: i, data: gm })));
			} else {
				resourceLoads.gameMaps = Promise.resolve([] as IEditableMap[]);
			}
		}

		const results = await allKeyed(resourceLoads);

		results.palette && dispatch(setActivePalette(results.palette.resourceData));
		results.mapDef && dispatch(setActiveMapDef(results.mapDef.resourceData));
		const maps = results.pk3Maps ?? results.gameMaps ?? [];
		dispatch(setActiveMaps({ maps, src: mapSrc }));
		results.vswap && (await dispatch(setActiveVswap(results.vswap.resourceData))); // this one is asynchronous due to blob conversion done on render thread
		dispatch(uiToggle({ isProjectDialogOpen: false }));
		dispatch(setActiveProjectAsync.success({ project, pk3StagingDir })); // this will render the main app components

		promoteRecentProject(project.id);
	} catch (e) {
		dispatch(setActiveProjectAsync.failure({ error: e }));
		throw e;
	}
};
