import { createAction, createAsyncAction } from "typesafe-actions";
import { DeepReadonly } from "ts-essentials";

import { MapDefinitionDifficulty, WadeMapDefinition, IWadeMapDefRef } from "../../../models/map-definition";
import { MapPlaneDefinition, MapTileData } from "../../../models/map";
import { AppThunkAction, IActionDefaultError, ICopyWadeResourceOpts } from "../types/action";
import { withResourceWorker } from "../../../workers";
import { WorkerSafe } from "../../../workers/shared";
import { IWDCToWadeConversionOpts } from "../../../game/data/maps/MapDefinitionFiles";
import { getRootWadeResourceDir } from "../../local/resources";

export const setActiveMapDef = createAction("SET_ACTIVE_MAP_DEF")<WadeMapDefinition>();
export const deleteActiveMapDefTile = createAction("DELETE_ACTIVE_MAP_DEF_TILES")<
	DeepReadonly<Array<{ plane: number; tileCode: number }>>
>();
export const createActiveMapDefPlane = createAction("CREATE_ACTIVE_MAP_DEF_PLANE")<MapPlaneDefinition>();
export const deleteActiveMapDefPlane = createAction("DELETE_ACTIVE_MAP_DEF_PLANE")<number>();
export const updateActiveMapDef = createAction("UPDATE_ACTIVE_MAP_DEF")<Partial<Omit<WadeMapDefinition, "planes">>>();
export const updateActiveMapDefPlaneMeta = createAction("UPDATE_ACTIVE_MAP_DEF_PLANE_META")<
	Partial<Omit<MapPlaneDefinition, "index" | "tiles">>,
	{ index: number; moveToIndex?: number }
>();
export const updateActiveMapDefTile = createAction("UPDATE_ACTIVE_MAP_DEF_TILE")<
	Partial<Omit<MapTileData, "plane">>,
	{ code: number; plane: number }
>();
export const moveActiveMapDefTile = createAction("MOVE_ACTIVE_MAP_DEF_TILE")<{ fromIndex: number; toIndex: number }, { plane: number }>();
export const createActiveMapDefTile = createAction("CREATE_ACTIVE_MAP_DEF_TILE")<MapTileData>();
export const createActiveMapDefDifficulty = createAction("CREATE_ACTIVE_MAP_DEF_DIFFICULTY")<MapDefinitionDifficulty>();
export const updateActiveMapDefDifficulty = createAction("UPDATE_ACTIVE_MAP_DEF_DIFFICULTY")<
	Partial<MapDefinitionDifficulty>,
	{ index: number }
>();
export const deleteActiveMapDefDifficulty = createAction("DELETE_ACTIVE_MAP_DEF_DIFFICULTY")<number>();

type ImportWDCMapDefFileRequest = {
	filePath: string;
	convertOpts?: WorkerSafe<IWDCToWadeConversionOpts>;
};
export const importWDCMapDefFileAsync = createAsyncAction(
	"IMPORT_WDC_MAP_DEF_FILE",
	"IMPORT_WDC_MAP_DEF_FILE_SUCCESS",
	"IMPORT_WDC_MAP_DEF_FILE_ERROR"
)<ImportWDCMapDefFileRequest, WadeMapDefinition, IActionDefaultError>();
export const importWDCMapDefFile = (
	importRequest: ImportWDCMapDefFileRequest,
	opts: { setActive?: boolean; returnError?: boolean } = {}
): AppThunkAction<Promise<WadeMapDefinition | { error: any }>> => async (dispatch, getState) => {
	const { setActive, returnError } = opts;
	try {
		dispatch(importWDCMapDefFileAsync.request(importRequest));
		const newMapDef = await withResourceWorker((w) => w.convertWdcMapDefToWade(importRequest));
		dispatch(importWDCMapDefFileAsync.success(newMapDef));
		if (setActive) {
			dispatch(setActiveMapDef(newMapDef));
		}
		return newMapDef;
	} catch (e) {
		dispatch(importWDCMapDefFileAsync.failure({ error: e }));
		if (returnError) {
			return { error: e };
		} else {
			throw e;
		}
	}
};

export const copyMapDefAsync = createAsyncAction("COPY_MAP_DEF", "COPY_MAP_DEF_SUCCESS", "COPY_MAP_DEF_ERROR")<
	ICopyWadeResourceOpts,
	IWadeMapDefRef,
	IActionDefaultError
>();
export const copyMapDef = (
	copyRequest: ICopyWadeResourceOpts,
	opts: { setActive?: boolean; returnError?: boolean } = {}
): AppThunkAction => async (dispatch, getState) => {
	const { setActive, returnError } = opts;
	try {
		dispatch(copyMapDefAsync.request(copyRequest));
		const { destId, srcId } = copyRequest;

		const { ref, resource: newMapDef } = await withResourceWorker((w) =>
			w.copyMapDef({ baseResourcePath: getRootWadeResourceDir(), destId, srcId })
		);

		dispatch(copyMapDefAsync.success(ref));
		if (setActive) {
			dispatch(setActiveMapDef(newMapDef));
		}
		return ref;
	} catch (e) {
		dispatch(copyMapDefAsync.failure({ error: e }));
		if (returnError) {
			return { error: e };
		} else {
			throw e;
		}
	}
};
