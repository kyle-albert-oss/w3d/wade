import { createAction } from "typesafe-actions";

export const devSetFormsDisabled = createAction("DEV_SET_FORMS_DISABLED")<boolean>();
