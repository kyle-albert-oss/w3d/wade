import { createAsyncAction } from "typesafe-actions";

import { withResourceWorker } from "../../../workers";
import { AppThunkAction, IActionDefaultError } from "../types/action";
import { PartialAppRootState, SettingsRootState } from "../types/state";
import { onlyInElectronEnv, RemoteElectronBuilderFactory } from "../../../util/env";
import { AddRemove } from "../../../models/wade";
import MapCanvasTool from "../../../models/map-canvas-tool";
import { getRootWadeResourceDir } from "../../local/resources";

import { uiUpdate } from "./ui";
import { updateActiveProject } from "./project";

/**
 * This file can import from other action files due to the app menu logic being here. Other action files should not import from here.
 */

export const updateAppSystemMenuAsync = createAsyncAction(
	"UPDATE_APP_SYSTEM_MENU",
	"UPDATE_APP_SYSTEM_MENU_SUCCESS",
	"UPDATE_APP_SYSTEM_MENU_ERROR"
)<undefined, undefined, IActionDefaultError>();
export const getBootstrapDataAsync = createAsyncAction("GET_BOOTSTRAP_DATA", "GET_BOOTSTRAP_DATA_SUCCESS", "GET_BOOTSTRAP_DATA_ERROR")<
	undefined,
	PartialAppRootState,
	IActionDefaultError
>();
export const updateKeysDownAsync = createAsyncAction("UPDATE_KEYS_DOWN", "UPDATE_KEYS_DOWN_SUCCESS", "UPDATE_KEYS_DOWN_ERROR")<
	{ key: string; op: AddRemove },
	undefined,
	IActionDefaultError
>();
interface ISaveAllSettingsInput {
	state: SettingsRootState;
}
export const saveAllSettingsAsync = createAsyncAction("SAVE_ALL_SETTINGS", "SAVE_ALL_SETTINGS_SUCCESS", "SAVE_ALL_SETTINGS_ERROR")<
	ISaveAllSettingsInput,
	undefined,
	IActionDefaultError
>();

export const getBootstrapData = (): AppThunkAction => async (dispatch, getState) => {
	dispatch(getBootstrapDataAsync.request());
	try {
		const partialState = await withResourceWorker((w) => w.getBootstrapData({ resourceBasePath: getRootWadeResourceDir() }));
		await dispatch(updateAppSystemMenu());
		dispatch(getBootstrapDataAsync.success(partialState));
	} catch (e) {
		dispatch(getBootstrapDataAsync.failure(e));
		throw e;
	}
};

export const saveAllSettings = (data: ISaveAllSettingsInput): AppThunkAction => async (dispatch, getState) => {
	// first, save file system dependencies
	// second, save to app store
	// that way if file system save fails for some reason we lessen the chance for the app store to be out of sync.
	try {
		const promises: Array<Promise<any>> = [];
		const { hasUnsavedActiveMapDef, hasUnsavedActiveProject } = data.state.settingsEditor;
		if (hasUnsavedActiveMapDef && data.state.mapDef.activeMapDef) {
			const activeMapDef = data.state.mapDef.activeMapDef;
			promises.push(
				withResourceWorker((w) =>
					w.writeWadeResource({ baseResourcePath: getRootWadeResourceDir(), request: { resourceData: activeMapDef, type: "map-def" } })
				)
			);
		}
		if (hasUnsavedActiveProject && data.state.project.activeProject) {
			promises.push(dispatch(updateActiveProject({ persist: data.state.project.activeProject })));
		}

		if (promises.length) {
			await Promise.all(promises);
		}

		dispatch(saveAllSettingsAsync.request(data));
		dispatch(saveAllSettingsAsync.success());
	} catch (e) {
		dispatch(saveAllSettingsAsync.failure(e));
		throw e;
	}
};

// TODO: may not need this
export const updateKeysDown = (key: string, op: AddRemove): AppThunkAction => async (dispatch, getState) => {
	dispatch(updateKeysDownAsync.request({ key, op }));

	let data;
	try {
		dispatch(updateKeysDownAsync.success());
	} catch (e) {
		dispatch(updateKeysDownAsync.failure(e));
		throw e;
	}
	return data;
};

const KeyboardShortcuts = {
	alt: (key: string) => `Alt+${key}`,
	cmdOrCtrl: (key: string) => `CommandOrControl+${key}`,
	ctrl: (key: string) => `Control+${key}`,
} as const;

type ElectronMenuClickHandler = NonNullable<Electron.MenuItemConstructorOptions["click"]>;
export const updateAppSystemMenu = (): AppThunkAction => (dispatch, getState) =>
	onlyInElectronEnv((el) => {
		try {
			dispatch(updateAppSystemMenuAsync.request());

			const { app, Menu } = el.remote;
			const isMac = process.platform === "darwin";
			const isDev = process.env.NODE_ENV === "development";
			const ch = (handler: ElectronMenuClickHandler): ElectronMenuClickHandler => {
				return (...args) => {
					// TODO: clear keys down

					handler(...args);
				};
			};

			const bf = new RemoteElectronBuilderFactory(el);
			const b = bf.newMenuBuilder();

			if (isMac) {
				b.submenu(app.getName(), (sb) =>
					sb
						.item({ role: "about" })
						.separator()
						.item({ role: "services" })
						.separator()
						.item({ role: "hide" })
						.item({ role: "unhide" })
						.separator()
						.item({ role: "quit" })
				);
			}

			b.submenu("Edit", (sb) =>
				sb
					.item({ role: "undo" })
					.item({ role: "redo" })
					.separator()
					.item({ role: "cut" })
					.item({ role: "copy" })
					.item({ role: "paste" })
					.item({ role: "delete" })
					.item({ role: "selectAll" })
			);

			b.submenu("Map", (sb) =>
				sb.item({
					label: "Pencil",
					accelerator: KeyboardShortcuts.cmdOrCtrl("P"),
					click: ch(() => dispatch(uiUpdate({ selectedMapCanvasTools: { [MapCanvasTool.PENCIL]: true } }))),
				})
			);

			b.submenu("View", (sb) =>
				sb.itemIf(isDev, { role: "reload" }).itemIf(isDev, { role: "forceReload" }).itemIf(isDev, { role: "toggleDevTools" })
			);

			Menu.setApplicationMenu(b.build());

			dispatch(updateAppSystemMenuAsync.success());
		} catch (e) {
			dispatch(updateAppSystemMenuAsync.failure(e));
			throw e;
		}
	});
