import { ThemeOptions } from "@material-ui/core/styles/createMuiTheme";
import { DeepReadonly } from "ts-essentials";
import { createAction } from "typesafe-actions";

import MapCanvasTool, { ISelectedMapCanvasTools, selectionTools } from "../../../models/map-canvas-tool";
import { selectedMapPlanesSelector, uiStateSelector } from "../selectors";
import { AppThunkAction } from "../types/action";
import { IUiBaseState, UiToggleKey, UiToggles } from "../types/state";
import { IMapTileSelectionFrom, MapTileSelectionChange } from "../types/ui";
import { AddRemove } from "../../../models/wade";

export type UiToggleActionParams = Partial<UiToggles> | UiToggleKey;
export const uiUpdate = createAction("UI_UPDATE")<Partial<IUiBaseState>, void | { op: AddRemove }>();
export const uiToggle = createAction("UI_TOGGLE")<UiToggleActionParams>();

export const updateMapTileSelection = createAction("UI_UPDATE_MAP_TILE_SELECTION")<MapTileSelectionChange>();

export const doDropperSiphon = (data: IMapTileSelectionFrom): AppThunkAction => async (dispatch, getState) => {
	const state = getState();
	const selectedMapPlanes = selectedMapPlanesSelector(state);
	if (!selectedMapPlanes) {
		return;
	}

	const { plane, button, y, x } = data;
	const tileCode = selectedMapPlanes?.[plane]?.data?.[y]?.[x];
	if (tileCode == null) {
		return;
	}

	await dispatch(updateMapTileSelection({ button, operation: "set", plane, tileCode }));
	const lastSelectedMapCanvasTools = uiStateSelector(state).lastSelectedMapCanvasTools;
	const nextSelectedMapCanvasTools: ISelectedMapCanvasTools = {
		...lastSelectedMapCanvasTools,
		[MapCanvasTool.ERASER]: false,
	};
	selectionTools.forEach((t) => (nextSelectedMapCanvasTools[t] = false));

	return dispatch(
		uiUpdate({
			lastSelectedMapCanvasTools: undefined,
			selectedMapCanvasTools: nextSelectedMapCanvasTools,
		})
	);
};

export const updateTheme = createAction("UPDATE_THEME")<DeepReadonly<ThemeOptions>>();
