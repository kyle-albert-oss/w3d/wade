import { Dictionary } from "ts-essentials";
import { IGameMapPlane } from "wolf3d-data";

import { IEditableMap } from "../../../models/map";
import { ISelectedMapCanvasTools } from "../../../models/map-canvas-tool";

export interface ITileSelection {
	plane: number;
	tileCode: number;
}

export interface IXYCoord {
	x: number;
	y: number;
}

export type PlaneXYCoord = IXYCoord & {
	plane?: number;
};

export type MapOpCode = number;
export type MapOpCodeOrEnum = MapOpCode | "MOUSE1" | "MOUSE2";

export type PlaneXYCoordWithCode = PlaneXYCoord & {
	code?: MapOpCodeOrEnum;
};

export type ItemSelector<T> = OneOrMany<T> | "all";

export interface IMapOperationBase<T extends string> {
	type: T;
	maps: ItemSelector<number>;
}

export interface IPointBasedMapOperation<T extends string, P = IXYCoord> extends IMapOperationBase<T> {
	points: P;
}

export interface IPlaneBasedMapOperation<T extends string, P = PlaneXYCoord | IXYCoord> extends IPointBasedMapOperation<T, P> {
	planes: ItemSelector<number>;
}

export interface IMapSetOperation extends IPlaneBasedMapOperation<"set", ItemSelector<PlaneXYCoordWithCode>> {
	code?: MapOpCodeOrEnum;
}

export interface IMapFillOperation extends IPlaneBasedMapOperation<"fill", IXYCoord> {
	code?: MapOpCodeOrEnum;
}

export interface IMapReplaceOperation extends IPlaneBasedMapOperation<"replace", ItemSelector<PlaneXYCoord | IXYCoord>> {
	find: OneOrMany<number>;
	replace: number;
}

export enum MapTileSelectState {
	ADD = "ADD",
	SELECTED = "SELECTED",
	SET = "SET",
	SUBTRACT = "SUBTRACT",
}

export interface IMapSelectOperation extends IPointBasedMapOperation<"select", IXYCoord[]> {
	subOperation: MapTileSelectState;
}

export type MapOperation = IMapSetOperation | IMapFillOperation | IMapReplaceOperation | IMapSelectOperation;

export interface IMapMutationData {
	maps: Dictionary<{
		// key = map index
		planes: Dictionary<{
			// key = plane index
			rows: Set<number>;
		}>;
	}>;
	pendingOps: boolean;
}

export interface IMapPlaneReference {
	map: IEditableMap;
	plane: IGameMapPlane;
}

export interface IMapOpExtra {
	clearPending?: boolean; // clearPending pending
	commit?: boolean;
	m1Selection?: ITileSelection;
	m2Selection?: ITileSelection;
	selectedTools?: ISelectedMapCanvasTools;
}

export interface IMapOpOptions {
	clearPending?: boolean;
	commit?: boolean;
}
