import { ThemeOptions } from "@material-ui/core/styles/createMuiTheme";
import { RouterState } from "connected-react-router";
import { DeepPartial, DeepReadonly, Dictionary } from "ts-essentials";
import { IGameMapPlane } from "wolf3d-data";

import { IEditableMap } from "../../../models/map";
import MapCanvasTool, { ISelectedMapCanvasTools } from "../../../models/map-canvas-tool";
import { IWadeMapDefRef, WadeMapDefinition } from "../../../models/map-definition";
import { defaultTheme } from "../../../models/material";
import { IGamePalette, IGamePaletteRef } from "../../../models/palette";
import { GameProject, ICompileProjectResult, ResourceSrc } from "../../../models/project";
import { GameResourceRef } from "../../../models/resources";
import { IWadeVswapGraphicChunk } from "../../../models/vswap";
import { DEFAULT_COLOR_PRESETS } from "../../../models/wade";

import { IMapOpExtra, ITileSelection, MapOperation, MapTileSelectState } from "./map-ops";

export type SharedRootState = DeepReadonly<{
	graphic: GraphicState;
	mapDef: MapDefState;
	project: ProjectState;
	theme: ThemeState;
	ui: UiState;
}>;

export type AppRootState = SharedRootState &
	Readonly<{
		app: AppState;
		map: MapState;
		router: RouterState;
	}>;

export type SettingsRootState = SharedRootState &
	Readonly<{
		settingsEditor: SettingsEditorState;
	}>;

// App
export type AppState = DeepReadonly<{
	isInitialLoadComplete: boolean;
	keysDown: Dictionary<boolean>;
}>;

export const appInitialState: AppState = {
	isInitialLoadComplete: false,
	keysDown: {},
};

// Graphics
export type GraphicState = DeepReadonly<{
	activePalette?: IGamePalette;
	availablePaletteRefs: Dictionary<IGamePaletteRef>;
	graphicChunks: IWadeVswapGraphicChunk[];
	spriteStartIndex?: number;
	soundStartIndex?: number;
}>;

export const graphicInitialState: GraphicState = {
	availablePaletteRefs: {},
	graphicChunks: [],
};

// Map
export interface IPendingMapData {
	code?: number;
}

export type MapState = DeepReadonly<{
	clipboard: IGameMapPlane[];
	mapSrc?: ResourceSrc;
	maps: IEditableMap[];
	mapSelectionData: Dictionary<MapTileSelectState>;
	pendingMapDataToRender: Dictionary<IPendingMapData>;
	pendingMapOperations: MapOperation[];
	pendingMapOperationSupplement?: IMapOpExtra;
	pendingMapSelectionToRender: Dictionary<MapTileSelectState>;
}>;

export const mapInitialState: MapState = {
	clipboard: [],
	maps: [],
	mapSelectionData: {},
	pendingMapDataToRender: {},
	pendingMapOperations: [],
	pendingMapSelectionToRender: {},
};

// Map Def
export type MapDefState = DeepReadonly<{
	availableMapDefsRefs: Dictionary<IWadeMapDefRef>;
	activeMapDef?: WadeMapDefinition;
}>;

export const mapDefInitialState: MapDefState = {
	availableMapDefsRefs: {},
};

// Project
export type ProjectState = DeepReadonly<{
	activePk3StagingDir?: string;
	activeProject?: GameProject;
	activeProjectCompileResult?: ICompileProjectResult;
	activeProjectReset?: GameProject;
	availableProjectRefs: Dictionary<GameResourceRef>;
	recentProjectIds?: Array<GameProject["id"]>;
}>;

export const projectInitialState: ProjectState = {
	availableProjectRefs: {},
};

// Settings Editor
export type SettingsEditorState = DeepReadonly<{
	hasUnsavedActiveMapDef?: boolean;
	hasUnsavedActiveProject?: boolean;
}>;

// Theme
export type ThemeState = DeepReadonly<ThemeOptions>;
export const initialThemeState: ThemeState = defaultTheme;

// UI
export type TileSelectionDictionary = Dictionary<ITileSelection> & {
	MOUSE1?: ITileSelection;
	MOUSE2?: ITileSelection;
};

export type UiState = DeepReadonly<UiToggles & IUiBaseState & IUiDerivedState>;

export type UiToggleKey =
	| "isDevConsoleOpen"
	| "isMouseOverMapCanvas"
	| "isNavOpen"
	| "isProjectCompileStatusDialogOpen"
	| "isProjectDialogOpen"
	| "updateTheme"
	| "useLegacyTileGraphics";
export type UiToggles = Record<UiToggleKey, boolean>;

export enum UiAsyncState {
	ERROR = "ERROR",
	LOADING = "LOADING",
	SUCCESS = "SUCCESS",
}

export type UiAsyncStateType = UiAsyncState | undefined;
export enum WorkflowStep {
	MAP_PASTE,
}

export interface IUiBaseState {
	colorPresets: string[];
	lastSelectedMapCanvasTools: ISelectedMapCanvasTools; // used to switch back to from dropper
	mapCanvasHiddenPlanes: number[];
	mapCanvasFloorCodeOpacity: number;
	mapCanvasTileSize: number;
	mapDefTileSize: number;
	mapPointerXCoord?: number;
	mapPointerYCoord?: number;
	mapTileZoom: number;
	mapTileZoomMin: number;
	mapTileZoomMax: number;
	projectCompileDialogStatus?: UiAsyncStateType;
	projectDialogPrimaryButtonStatus?: UiAsyncStateType;
	selectedGraphicChunkIndex: number;
	selectedMapIndex: number;
	selectedMapCanvasTools: ISelectedMapCanvasTools;
	selectedPlaneNum: number;
	selectedProjectId?: GameProject["id"];
	selectedEditTile?: ITileSelection;
	selectedTiles: TileSelectionDictionary;
	settingsSaveStatus?: UiAsyncStateType;
	vswapEditorZoom: number;
	workflowStep?: WorkflowStep;
}

export const uiInitialState: UiState = {
	colorPresets: DEFAULT_COLOR_PRESETS,
	formsDisabled: [],
	mapCanvasHiddenPlanes: [],
	mapCanvasFloorCodeOpacity: 0.6,
	isDevConsoleOpen: false,
	isMouseOverMapCanvas: false,
	isNavOpen: false,
	isProjectCompileStatusDialogOpen: false,
	isProjectDialogOpen: false,
	lastSelectedMapCanvasTools: {},
	mapCanvasTileSize: 32,
	mapDefTileSize: 36,
	mapTileZoom: 1,
	mapTileZoomMax: 2,
	mapTileZoomMin: 0.05,
	selectedGraphicChunkIndex: 0,
	selectedMapCanvasTools: {
		[MapCanvasTool.PENCIL]: true,
	},
	selectedMapIndex: 0,
	selectedPlaneNum: 0,
	selectedTiles: {},
	updateTheme: true,
	useLegacyTileGraphics: false,
	vswapEditorZoom: 1,
};

export interface IUiDerivedState {
	formsDisabled: string[];
}

export type PartialAppRootState = DeepPartial<AppRootState>;
