import { AnyAction, Store } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { DeepWritable } from "ts-essentials";
import { Action } from "typesafe-actions";

import { AppRootState } from "./state";
import { GameResourceRef, GameResource } from "../../../models/resources";

export type AppDispatch = ThunkDispatch<AppRootState, any, AnyAction>;
export type AppThunkAction<R = unknown> = ThunkAction<R, AppRootState, any, AnyAction>;
export type AppStoreType = Store<AppRootState>;

export interface IActionDefaultError {
	error: Error | string;
}

export type ActionHandler<S, A = Action> = (state: S, action: A) => S;
export type ActionHandlerModifier<S, A = Action> = ActionHandler<DeepWritable<S>, A>;

// cannot use these in actual action creators because of the way the typing system works.
export const ACTION_TYPE_SUCCESS_SUFFIX = "_SUCCESS";
export const ACTION_TYPE_ERROR_SUFFIX = "_ERROR";

export interface ICopyWadeResourceOpts {
	baseResourcePath: string;
	destId?: string;
	destName?: string;
	srcId: string;
}

export interface ICopyWadeResourceResult<R extends GameResource, REF = GameResourceRef> {
	ref: REF;
	resource: R;
}
