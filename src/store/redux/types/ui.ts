import { TileSelectionDictionary } from "./state";

export interface IMapTileSelectionBase<O extends string> {
	operation: O;
}

export interface IMapTileSelectionClear extends IMapTileSelectionBase<"clear"> {}

export interface IMapTileSelectionSet extends IMapTileSelectionBase<"set"> {
	plane: number;
	tileCode: number;
}

export interface IMapTileSelectionFrom {
	button: keyof TileSelectionDictionary;
	plane: number;
	x: number;
	y: number;
}

export type MapTileSelectionChange = (IMapTileSelectionClear | IMapTileSelectionSet) & {
	button: keyof TileSelectionDictionary;
};
