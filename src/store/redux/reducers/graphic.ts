import _ from "lodash";
import { DeepPartial, DeepWritable } from "ts-essentials";
import { ActionType, createReducer } from "typesafe-actions";

import * as MoreObjects from "../../../util/objects";
import { getBootstrapDataAsync } from "../actions/app";
import { setActivePalette, setActiveVswapAsync } from "../actions/graphic";
import { GraphicState, graphicInitialState } from "../types/state";

export const graphicReducer = createReducer<GraphicState>(graphicInitialState)
	.handleAction(
		getBootstrapDataAsync.success,
		(state: GraphicState, action: ActionType<typeof getBootstrapDataAsync.success>): GraphicState => {
			const bootstrapGraphicState =
				(action.payload ? action.payload.graphic : undefined) || ({} as DeepWritable<DeepPartial<GraphicState>>);

			let newState = _.cloneDeep(state) as DeepWritable<GraphicState>;
			newState.availablePaletteRefs = {};
			newState = _.merge(newState, bootstrapGraphicState);

			return newState;
		}
	)
	.handleAction(
		setActiveVswapAsync.success,
		(state: GraphicState, action: ActionType<typeof setActiveVswapAsync.success>): GraphicState => {
			const newState = { ...state } as DeepWritable<GraphicState>;
			newState.graphicChunks = [];
			delete newState.spriteStartIndex;
			delete newState.soundStartIndex;

			Object.assign(newState, action.payload || {});

			return newState;
		}
	)
	.handleAction(
		setActivePalette,
		(state: GraphicState, action: ActionType<typeof setActivePalette>): GraphicState => {
			let newState = { ...state } as DeepWritable<GraphicState>;
			newState = MoreObjects.safeAssign(newState, { activePalette: action.payload });
			return newState;
		}
	);

export default graphicReducer;
