import { connectRouter } from "connected-react-router";
import { History } from "history";
import { combineReducers, Reducer } from "redux";

import { AppRootState, SettingsRootState } from "../types/state";

import appReducer from "./app";
import graphicReducer from "./graphic";
import mapReducer from "./map";
import mapDefReducer from "./map-def";
import projectReducer from "./project";
import themeReducer from "./theme";
import uiReducer from "./ui";
import settingsEditorReducer from "./settings-editor";

export const createRootReducer = (history: History): Reducer<AppRootState> => {
	return combineReducers({
		app: appReducer,
		graphic: graphicReducer,
		map: mapReducer,
		mapDef: mapDefReducer,
		project: projectReducer,
		router: connectRouter(history),
		theme: themeReducer,
		ui: uiReducer,
	}) as Reducer<AppRootState>;
};

// For working copy of store for settings page that requires pressing save.
export const createSettingsRootReducer = (): Reducer<SettingsRootState> => {
	return combineReducers({
		graphic: graphicReducer,
		mapDef: mapDefReducer,
		project: projectReducer,
		settingsEditor: settingsEditorReducer,
		theme: themeReducer,
		ui: uiReducer,
	}) as Reducer<SettingsRootState>;
};
