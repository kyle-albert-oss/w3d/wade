import { EnumValues } from "enum-values";
import _ from "lodash";
import { DeepWritable } from "ts-essentials";
import { Action, ActionType, createReducer } from "typesafe-actions";

import MapCanvasTool, { brushTools, selectionTools } from "../../../models/map-canvas-tool";
import { Sets } from "../../../util/collections";
import { KeysOfExactType } from "../../../util/objects";
import { getBootstrapDataAsync, saveAllSettingsAsync } from "../actions/app";
import { devSetFormsDisabled } from "../actions/dev";
import { compileActiveProjectAsync, setActiveProjectAsync } from "../actions/project";
import { uiToggle, uiUpdate, updateMapTileSelection, updateTheme } from "../actions/ui";
import { ACTION_TYPE_ERROR_SUFFIX, ACTION_TYPE_SUCCESS_SUFFIX, ActionHandler, ActionHandlerModifier } from "../types/action";
import { IUiBaseState, UiAsyncState, UiAsyncStateType, UiState, uiInitialState } from "../types/state";
import { IMapTileSelectionSet } from "../types/ui";
import { updateActiveMapDefPlaneMeta } from "../actions/map-def";
import { AddRemove } from "../../../models/wade";

/**
 * For redux state, we only keep tools w/ true. False = delete from state.
 * @param payload
 * @param prevState
 */
const constrainMapTools = (payload: Partial<IUiBaseState> /*🤧*/, prevState: UiState) => {
	if (!("selectedMapCanvasTools" in payload) && !("lastSelectedMapCanvasTools" in payload)) {
		return;
	}
	let clear = false;
	let { selectedMapCanvasTools } = payload;
	if (selectedMapCanvasTools == null) {
		selectedMapCanvasTools = {};
		clear = true;
	}

	if (selectedMapCanvasTools[MapCanvasTool.DROPPER]) {
		if (!(MapCanvasTool.DROPPER in prevState.selectedMapCanvasTools) && !Object.keys(prevState.lastSelectedMapCanvasTools || {}).length) {
			payload.lastSelectedMapCanvasTools = { ...prevState.selectedMapCanvasTools };
		}
		payload.selectedMapCanvasTools = { [MapCanvasTool.DROPPER]: true };
		return;
	}

	const newSelectedTools = new Set(clear ? [] : Object.keys(prevState.selectedMapCanvasTools)) as Set<MapCanvasTool>;
	newSelectedTools.delete(MapCanvasTool.DROPPER); // delete it if it exists, we handled above
	const addedTools = new Set<MapCanvasTool>();

	for (const t of EnumValues.getValues<MapCanvasTool>(MapCanvasTool)) {
		if (selectedMapCanvasTools[t] && !(t in prevState.selectedMapCanvasTools)) {
			addedTools.add(t);
		} else {
			// noinspection PointlessBooleanExpressionJS this is used specially for toggling
			if (selectedMapCanvasTools[t] === false && t in prevState.selectedMapCanvasTools) {
				// absence of value means no change, so check if equal to bool false
				newSelectedTools.delete(t);
			}
		}
	}

	const { added: brushAdded, selected: brushSelected } = constrainToolGroup(brushTools, addedTools, newSelectedTools);
	const { added: selectionAdded, selected: selectionSelected } = constrainToolGroup(selectionTools, addedTools, newSelectedTools);
	let selectBrushIfNeeded = false;
	if (selectionAdded) {
		newSelectedTools.clear();
		if (selectionSelected) {
			// should always be true, but TS is not smart enough for this case
			newSelectedTools.add(selectionSelected);
		}
	} else {
		if (addedTools.has(MapCanvasTool.ERASER)) {
			// eraser is a toggle
			newSelectedTools.add(MapCanvasTool.ERASER);
			selectBrushIfNeeded = true;
		}
		if (brushAdded) {
			if (brushSelected) {
				// should always be true, but TS is not smart enough for this case
				newSelectedTools.add(brushSelected);
			}
			brushTools.filter((bt) => bt !== brushSelected).forEach((bt) => newSelectedTools.delete(bt));
		}

		// if there's any selection tools previously selected and other tools are selected now, delete selection tools
		if (Sets.differenceWithIterable(newSelectedTools, selectionTools).size > 0) {
			Sets.deleteAll(newSelectedTools, selectionTools);
		}
	}

	if (newSelectedTools.size === 0 || (selectBrushIfNeeded && !Sets.hasAny(newSelectedTools, brushTools))) {
		newSelectedTools.add(MapCanvasTool.PENCIL);
	}

	payload.selectedMapCanvasTools = {};
	for (const t of newSelectedTools) {
		payload.selectedMapCanvasTools[t] = true;
	}
};

interface IConstrainResult {
	added?: boolean;
	selected?: MapCanvasTool;
}

const constrainToolGroup = (
	group: readonly MapCanvasTool[],
	addedTools: ReadonlySet<MapCanvasTool>,
	newSelectedTools: ReadonlySet<MapCanvasTool>
): IConstrainResult => {
	let selected: MapCanvasTool | undefined;
	let added = false;
	for (const t of group) {
		if (addedTools.has(t)) {
			added = true;
			selected = t; // added has precedence over existing
			break;
		}
		if (selected == null && newSelectedTools.has(t)) {
			selected = t;
		}
	}
	return { added, selected };
};

const withModifiers = <S, A = Action>(
	handler: ActionHandler<S, A>,
	...modifiers: Array<ActionHandlerModifier<S, A>>
): ActionHandler<S, A> => {
	return (inputState, action) => {
		const state = handler.call(null, inputState, action);
		let writableState = state as DeepWritable<S>;
		for (const modifier of modifiers) {
			writableState = modifier.call(null, writableState, action);
		}
		return writableState as S;
	};
};

// ⚠️ side effect: mutates passed ui state to allow for easy inline and prevent duplicating massive objects with spreads.
const formDisableModifier: ActionHandlerModifier<UiState> = (uiState, action) => {
	const isSuccess = action.type.endsWith(ACTION_TYPE_SUCCESS_SUFFIX);
	const isError = action.type.endsWith(ACTION_TYPE_ERROR_SUFFIX);
	if (isSuccess && uiState.formsDisabled) {
		const suffixLength = ACTION_TYPE_SUCCESS_SUFFIX.length;
		const baseType = action.type.substr(0, action.type.length - suffixLength);
		const idx = uiState.formsDisabled.indexOf(baseType);
		if (idx !== -1) {
			uiState.formsDisabled = [...uiState.formsDisabled];
			uiState.formsDisabled.splice(idx, 1);
		}
	} else if (!isError) {
		uiState.formsDisabled = [...(uiState.formsDisabled || []), action.type];
	}
	return uiState;
};

const uiAsyncStateModifier = (
	uiStateKey: KeysOfExactType<IUiBaseState, UiAsyncStateType>,
	onlyLoad?: boolean
): ActionHandlerModifier<UiState> => {
	return (uiState, action) => {
		let newValue: UiAsyncStateType | undefined;
		if (action.type.endsWith(ACTION_TYPE_SUCCESS_SUFFIX)) {
			newValue = onlyLoad ? undefined : UiAsyncState.SUCCESS;
		} else if (action.type.endsWith(ACTION_TYPE_ERROR_SUFFIX)) {
			newValue = onlyLoad ? undefined : UiAsyncState.ERROR;
		} else {
			newValue = UiAsyncState.LOADING;
		}

		if (newValue) {
			uiState[uiStateKey] = newValue;
		} else {
			delete uiState[uiStateKey];
		}

		return uiState;
	};
};

export const uiReducer = createReducer<UiState>(uiInitialState)
	.handleAction(
		getBootstrapDataAsync.success,
		(state: UiState, action: ActionType<typeof getBootstrapDataAsync.success>): UiState => {
			const bootstrapUiState = (action.payload ? action.payload.ui : undefined) || {};
			const newState = _.merge(_.cloneDeep(state), bootstrapUiState) as DeepWritable<UiState>;
			return newState;
		}
	)
	.handleAction(
		uiUpdate,
		(state: UiState, action: ActionType<typeof uiUpdate>): UiState => {
			const { payload, meta } = action;
			if (meta) {
				const { op } = meta;
				const newState = { ...state } as DeepWritable<UiState>;
				for (const [key, value] of Object.entries(payload)) {
					if (Array.isArray(value)) {
						switch (op) {
							case AddRemove.ADD:
								//@ts-expect-error
								newState[key] = [...(newState[key] ?? []), ...value];
								break;
							case AddRemove.REMOVE:
								//@ts-expect-error
								newState[key] = [...(newState[key] ?? [])].filter((v) => !value.includes(v));
								break;
							default:
								throw new Error("Unsupported");
						}
					}
				}
				return newState;
			} else {
				const payloadCopy: Partial<IUiBaseState> = { ...action.payload };
				constrainMapTools(payloadCopy, state);
				return { ...state, ...payloadCopy };
			}
		}
	)
	.handleAction(
		uiToggle,
		(state: UiState, action: ActionType<typeof uiToggle>): UiState => {
			const newState = { ...state } as DeepWritable<UiState>;
			if (typeof action.payload === "object") {
				Object.assign(newState, action.payload);
			} else {
				newState[action.payload] = !state[action.payload];
			}
			return newState;
		}
	)
	.handleAction(
		updateMapTileSelection,
		(state: UiState, action: ActionType<typeof updateMapTileSelection>): UiState => {
			const newState = { ...state, selectedTiles: { ...state.selectedTiles } } as DeepWritable<UiState>;
			const { button, operation } = action.payload;
			if (operation === "clear") {
				delete newState.selectedTiles[button];
			} else {
				const { plane, tileCode } = action.payload as IMapTileSelectionSet;
				newState.selectedTiles[button] = { plane, tileCode };
			}
			return newState;
		}
	)
	.handleAction(updateTheme, (state: UiState): UiState => ({ ...state, updateTheme: true }))
	.handleAction(
		setActiveProjectAsync.request,
		withModifiers<UiState>((state) => ({ ...state }), formDisableModifier, uiAsyncStateModifier("projectDialogPrimaryButtonStatus"))
	)
	.handleAction(
		setActiveProjectAsync.success,
		withModifiers<UiState>(
			(state) => ({
				...state,
				selectedGraphicChunkIndex: 0,
				selectedMapIndex: 0,
			}),
			formDisableModifier,
			uiAsyncStateModifier("projectDialogPrimaryButtonStatus")
		)
	)
	.handleAction(
		setActiveProjectAsync.failure,
		(state: UiState, action: Action): UiState => ({ ...state, projectDialogPrimaryButtonStatus: UiAsyncState.ERROR })
	)
	.handleAction(
		compileActiveProjectAsync.request,
		withModifiers<UiState, ActionType<typeof compileActiveProjectAsync.request>>(
			(state, action) => ({ ...state, isProjectCompileStatusDialogOpen: action.payload ? true : state.isProjectCompileStatusDialogOpen }),
			formDisableModifier,
			uiAsyncStateModifier("projectCompileDialogStatus")
		)
	)
	.handleAction(
		compileActiveProjectAsync.success,
		withModifiers<UiState>((state) => ({ ...state }), formDisableModifier, uiAsyncStateModifier("projectCompileDialogStatus"))
	)
	.handleAction(
		compileActiveProjectAsync.failure,
		withModifiers<UiState>((state) => ({ ...state }), formDisableModifier, uiAsyncStateModifier("projectCompileDialogStatus"))
	)
	.handleAction(
		devSetFormsDisabled,
		(state: UiState, action: ActionType<typeof devSetFormsDisabled>): UiState => {
			const { payload: disabled, type } = action;
			const newState = { ...state } as DeepWritable<UiState>;
			if (disabled) {
				newState.formsDisabled = [...(newState.formsDisabled || []), type];
			} else if (newState.formsDisabled) {
				const idx = newState.formsDisabled.indexOf(type);
				if (idx !== -1) {
					newState.formsDisabled = [...newState.formsDisabled];
					newState.formsDisabled.splice(idx, 1);
				}
			}
			return newState;
		}
	)
	.handleAction(
		saveAllSettingsAsync.request,
		withModifiers<UiState, ActionType<typeof saveAllSettingsAsync.request>>(
			(state, action) => {
				const {
					state: { ui },
				} = action.payload;
				return {
					...state,
					colorPresets: ui.colorPresets,
				};
			},
			formDisableModifier,
			uiAsyncStateModifier("settingsSaveStatus", true)
		)
	)
	.handleAction(
		saveAllSettingsAsync.success,
		withModifiers<UiState>((state) => ({ ...state }), formDisableModifier, uiAsyncStateModifier("settingsSaveStatus", true))
	)
	.handleAction(
		saveAllSettingsAsync.failure,
		withModifiers<UiState>((state) => ({ ...state }), formDisableModifier, uiAsyncStateModifier("settingsSaveStatus", true))
	)
	.handleAction(
		updateActiveMapDefPlaneMeta,
		(state: UiState, action: ActionType<typeof updateActiveMapDefPlaneMeta>): UiState => {
			const {
				meta: { moveToIndex },
			} = action;
			if (moveToIndex == null) {
				return state;
			}
			return {
				...state,
				selectedPlaneNum: moveToIndex,
			};
		}
	);

export default uiReducer;
