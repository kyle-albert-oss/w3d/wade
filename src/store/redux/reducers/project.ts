import _ from "lodash";
import { DeepWritable } from "ts-essentials";
import { ActionType, createReducer } from "typesafe-actions";

import { ICompileProjectResult } from "../../../models/project";
import { getBootstrapDataAsync, saveAllSettingsAsync } from "../actions/app";
import {
	compileActiveProjectAsync,
	createProjectAsync,
	deleteProjectsAsync,
	setActiveProjectAsync,
	updateProjectCompileStatus,
	updateActiveProjectAsync,
} from "../actions/project";
import { ProjectState, projectInitialState } from "../types/state";

export const projectReducer = createReducer<ProjectState>(projectInitialState)
	.handleAction(
		getBootstrapDataAsync.success,
		(state: ProjectState, action: ActionType<typeof getBootstrapDataAsync.success>): ProjectState => {
			const bootstrapProjectState = (action.payload ? action.payload.project : undefined) || {};

			let newState = _.cloneDeep(state) as DeepWritable<ProjectState>;
			newState.availableProjectRefs = {};
			newState = _.merge(newState, bootstrapProjectState) as DeepWritable<ProjectState>;

			return newState;
		}
	)
	.handleAction(
		setActiveProjectAsync.success,
		(state: ProjectState, action: ActionType<typeof setActiveProjectAsync.success>): ProjectState => {
			const { project, pk3StagingDir } = action.payload;
			const newState = {
				...state,
				activePk3StagingDir: pk3StagingDir,
				activeProject: _.cloneDeep(project),
			} as DeepWritable<ProjectState>;

			delete newState.activeProjectCompileResult;

			return newState;
		}
	)
	.handleAction(
		createProjectAsync.success,
		(state: ProjectState, action: ActionType<typeof createProjectAsync.success>): ProjectState => {
			const newState = {
				...state,
				availableProjectRefs: { ...state.availableProjectRefs },
			} as DeepWritable<ProjectState>;

			const { id, name } = action.payload;
			newState.availableProjectRefs[id] = { id, name };

			return newState;
		}
	)
	.handleAction(
		updateActiveProjectAsync.request,
		(state: ProjectState, action: ActionType<typeof updateActiveProjectAsync.request>): ProjectState => {
			const { payload: project } = action;
			if (!state.activeProject) {
				throw new Error(`No currently active project.`);
			}
			if (project.id && project.id !== state.activeProject.id) {
				throw new Error(`Cannot update active project: id cannot be changed.`);
			}

			return {
				...state,
				activeProject: {
					...state.activeProject,
					...project,
				},
				...(!state.activeProjectReset ? { activeProjectReset: state.activeProject } : undefined),
			};
		}
	)
	.handleAction(
		updateActiveProjectAsync.success,
		(state: ProjectState, action: ActionType<typeof updateActiveProjectAsync.success>): ProjectState => {
			const { payload: persisted } = action;
			if (!persisted) {
				return state;
			}
			const newState = { ...state } as DeepWritable<ProjectState>;
			delete newState.activeProjectReset;
			return newState;
		}
	)
	.handleAction(
		deleteProjectsAsync.success,
		(state: ProjectState, action: ActionType<typeof deleteProjectsAsync.success>): ProjectState => {
			const { payload: deletedProjectIds } = action;

			const newState = {
				...state,
				availableProjectRefs: { ...state.availableProjectRefs },
				recentProjectIds:
					state.recentProjectIds != null ? [...state.recentProjectIds.filter((id) => !deletedProjectIds.includes(id))] : undefined,
			} as DeepWritable<ProjectState>;

			for (const deletedProjectId of deletedProjectIds) {
				delete newState.availableProjectRefs[deletedProjectId];
			}
			if (newState.activeProject && deletedProjectIds.includes(newState.activeProject.id)) {
				delete newState.activeProject;
			}

			return newState;
		}
	)
	.handleAction(
		compileActiveProjectAsync.request,
		(state: ProjectState, action: ActionType<typeof compileActiveProjectAsync.request>): ProjectState => {
			return {
				...state,
				activeProjectCompileResult: {
					complete: false,
				},
			};
		}
	)
	.handleAction(
		compileActiveProjectAsync.success,
		(state: ProjectState, action: ActionType<typeof compileActiveProjectAsync.success>): ProjectState => {
			return {
				...state,
				activeProjectCompileResult: {
					...state.activeProjectCompileResult,
					...action.payload,
					complete: true,
				} as ICompileProjectResult,
			};
		}
	)
	.handleAction(
		updateProjectCompileStatus,
		(state: ProjectState, action: ActionType<typeof updateProjectCompileStatus>): ProjectState => {
			return {
				...state,
				activeProjectCompileResult: {
					...state.activeProjectCompileResult,
					...action.payload,
				} as ICompileProjectResult,
			};
		}
	)
	.handleAction(
		saveAllSettingsAsync.request,
		(state: ProjectState, action: ActionType<typeof saveAllSettingsAsync.request>): ProjectState => {
			const {
				state: { project },
			} = action.payload;

			return {
				...state,
				activeProject: project.activeProject,
			};
		}
	);

export default projectReducer;
