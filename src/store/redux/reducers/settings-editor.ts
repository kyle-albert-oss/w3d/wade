import { createReducer } from "typesafe-actions";

import { SettingsEditorState } from "../types/state";
import * as mapDefActions from "../actions/map-def";
import * as projectActions from "../actions/project";

const UNSAVED_MAP_DEF_ACTIONS = [
	mapDefActions.setActiveMapDef,
	mapDefActions.deleteActiveMapDefTile,
	mapDefActions.createActiveMapDefPlane,
	mapDefActions.deleteActiveMapDefPlane,
	mapDefActions.updateActiveMapDef,
	mapDefActions.updateActiveMapDefPlaneMeta,
	mapDefActions.updateActiveMapDefTile,
	mapDefActions.moveActiveMapDefTile,
	mapDefActions.createActiveMapDefTile,
	mapDefActions.createActiveMapDefDifficulty,
	mapDefActions.updateActiveMapDefDifficulty,
	mapDefActions.deleteActiveMapDefDifficulty,
];

const UNSAVED_PROJECT_ACTIONS = [projectActions.updateActiveProjectAsync.request];

export const settingsEditorReducer = createReducer<SettingsEditorState>({})
	.handleAction(UNSAVED_MAP_DEF_ACTIONS, (state: SettingsEditorState): SettingsEditorState => ({ ...state, hasUnsavedActiveMapDef: true }))
	.handleAction(UNSAVED_PROJECT_ACTIONS, (state: SettingsEditorState) => ({ ...state, hasUnsavedActiveProject: true }));

export default settingsEditorReducer;
