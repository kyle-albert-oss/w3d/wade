import _ from "lodash";
import { DeepReadonly, DeepWritable } from "ts-essentials";
import { ActionType, createReducer } from "typesafe-actions";
import { IGameMap, IGameMapPlane } from "wolf3d-data";

import { IEditableMap, IMapPlaneXYData } from "../../../models/map";
import { getBootstrapDataAsync } from "../actions/app";
import {
	clearPendingMapOperations,
	commitPendingMapOperations,
	doPendingMapOperationAsync,
	setActiveMaps,
	createMapAsync,
	deleteMap,
	moveMap,
	clearMapSelection,
} from "../actions/map";
import { updateProjectCompileStatus } from "../actions/project";
import {
	IMapFillOperation,
	IMapMutationData,
	IMapOpExtra,
	IMapPlaneReference,
	IMapSelectOperation,
	IMapSetOperation,
	MapOpCode,
	MapOpCodeOrEnum,
	MapOperation,
	MapTileSelectState,
} from "../types/map-ops";
import { MapState, mapInitialState } from "../types/state";
import { Arrays } from "../../../util/collections";

export const getMapPlaneXYKey = (map: number, plane: number, x: number, y: number): string => `${map}_${plane}_${x}_${y}`;
export const getMapPlaneXYData = (key: string, data?: { [key: string]: { code?: number } }): IMapPlaneXYData => {
	const tokens = key.split("_");
	const result: IMapPlaneXYData = {
		key,
		map: parseInt(tokens[0], 10),
		plane: parseInt(tokens[1], 10),
		x: parseInt(tokens[2], 10),
		y: parseInt(tokens[3], 10),
	};
	const code = data?.[key]?.code;
	if (code != null) {
		result.code = code;
	}
	return result;
};

export const mapReducer = createReducer<MapState>(mapInitialState)
	.handleAction(
		getBootstrapDataAsync.success,
		(state: MapState, action: ActionType<typeof getBootstrapDataAsync.success>): MapState => {
			const newMaps = ((action.payload && action.payload.map ? action.payload.map.maps : undefined) || []) as IEditableMap[];

			const newState = _.cloneDeep(state) as DeepWritable<MapState>;
			// TODO: clear out other properties?
			newState.maps = [...newMaps];

			return newState;
		}
	)
	.handleAction(
		setActiveMaps,
		(state: MapState, action: ActionType<typeof setActiveMaps>): MapState => {
			const { maps, src } = action.payload;
			return {
				...state,
				maps,
				mapSrc: src,
			};
		}
	)
	.handleAction(
		clearPendingMapOperations,
		(state: MapState, action: ActionType<typeof clearPendingMapOperations>): MapState => {
			return { ...state, pendingMapDataToRender: {}, pendingMapOperations: [], pendingMapOperationSupplement: undefined };
		}
	)
	.handleAction(
		doPendingMapOperationAsync.request,
		(state: MapState, action: ActionType<typeof doPendingMapOperationAsync.request>): MapState => {
			const { ops, supplement } = action.payload;
			const newState = { ...state } as DeepWritable<MapState>;
			return performOperations(newState, ops, supplement);
		}
	)
	.handleAction(
		commitPendingMapOperations,
		(state: MapState, action: ActionType<typeof commitPendingMapOperations>): MapState => {
			const newState = { ...state } as DeepWritable<MapState>;
			return performOperations(newState, state.pendingMapOperations, {
				...state.pendingMapOperationSupplement,
				clearPending: true,
				commit: true,
			});
		}
	)
	.handleAction(
		updateProjectCompileStatus,
		(state: MapState, action: ActionType<typeof updateProjectCompileStatus>): MapState => {
			const { payload: compileUpdate } = action;
			if (compileUpdate?.maps !== true) {
				return state;
			}

			return {
				...state,
				maps: state.maps.map((em) => ({
					...em,
					hasUnsavedChanges: false,
				})),
			};
		}
	)
	.handleAction(
		createMapAsync.success,
		(state: MapState, action: ActionType<typeof createMapAsync.success>): MapState => {
			const { payload: gameMapData } = action;

			const newState = { ...state } as DeepWritable<MapState>;
			const insertionIndex = gameMapData.index;
			newState.maps = [...newState.maps].map((m, i) =>
				i >= insertionIndex
					? {
							...m,
							index: i + 1,
							data: {
								...m.data,
								index: i + 1,
							},
					  }
					: m
			);
			newState.maps.splice(gameMapData.index, 0, {
				data: _.cloneDeep(gameMapData),
				hasUnsavedChanges: true,
				index: gameMapData.index,
			});

			return newState;
		}
	)
	.handleAction(
		deleteMap,
		(state: MapState, action: ActionType<typeof deleteMap>): MapState => {
			const newState = { ...state } as DeepWritable<MapState>;
			newState.maps = newState.maps.map((m, i) =>
				i > action.meta.index
					? {
							...m,
							index: i - 1,
							data: {
								...m.data,
								index: i - 1,
							},
					  }
					: m
			);
			newState.maps.splice(action.meta.index, 1);
			return newState;
		}
	)
	.handleAction(
		moveMap,
		(state: MapState, action: ActionType<typeof moveMap>): MapState => {
			const {
				payload: { fromIndex, toIndex },
			} = action;

			const newState = { ...state } as DeepWritable<MapState>;
			newState.maps = Arrays.moveToIndex([...newState.maps], fromIndex, toIndex, (m, newIndex) => ({
				...m,
				index: newIndex,
				data: {
					...m.data,
					index: newIndex,
				},
			}));

			return newState;
		}
	)
	.handleAction(
		clearMapSelection,
		(state: MapState): MapState => ({
			...state,
			mapSelectionData: {},
		})
	);

export default mapReducer;

type PendingSelectionData = MapState["pendingMapSelectionToRender"];
type WritablePendingSelectionData = DeepWritable<PendingSelectionData>;
const performSelectOp = (
	state: DeepWritable<MapState>,
	op: DeepReadonly<IMapSelectOperation>,
	opSupplement: DeepReadonly<IMapOpExtra>,
	mutationMeta: IMapMutationData,
	isForPendingRender?: boolean
): DeepWritable<MapState> => {
	const { maps: opMaps, points, subOperation } = op;

	const maps = opMaps === "all" ? state.maps : _.castArray(opMaps).map((mapIndex) => state.maps[mapIndex]);
	const newSelectionData = isForPendingRender
		? state.mapSelectionData
		: subOperation === MapTileSelectState.SET
		? {}
		: { ...state.mapSelectionData };
	const newPendingSelectionData = isForPendingRender
		? ({ ...state.pendingMapSelectionToRender } as WritablePendingSelectionData)
		: state.pendingMapSelectionToRender;

	for (const map of maps) {
		const { index: mapIndex } = map;
		for (const point of points) {
			const { x, y } = point;
			const selectionKey = getMapPlaneXYKey(mapIndex, -1, x, y);
			if (subOperation === MapTileSelectState.SUBTRACT) {
				if (isForPendingRender) {
					newPendingSelectionData[selectionKey] = MapTileSelectState.SUBTRACT;
				} else {
					delete newSelectionData[selectionKey];
				}
			} else {
				if (isForPendingRender) {
					if (subOperation === MapTileSelectState.SET) {
						newPendingSelectionData[selectionKey] = MapTileSelectState.SET;
					} else {
						newPendingSelectionData[selectionKey] = newSelectionData[selectionKey] ? MapTileSelectState.SELECTED : MapTileSelectState.ADD;
					}
				} else {
					newSelectionData[selectionKey] = MapTileSelectState.SELECTED;
				}
			}
		}
	}

	state.mapSelectionData = newSelectionData;
	state.pendingMapSelectionToRender = newPendingSelectionData;
	if (isForPendingRender) {
		if (!mutationMeta.pendingOps) {
			state.pendingMapOperations = [...state.pendingMapOperations];
			mutationMeta.pendingOps = true;
		}
		state.pendingMapOperations.push(op as MapOperation);
	}

	return state;
};

/* ️⚠️ mutation of state needs to be done properly otherwise weird things can happen */
const performSetOp = (
	state: DeepWritable<MapState>,
	op: DeepReadonly<IMapSetOperation>,
	mapOpSupplement: DeepReadonly<IMapOpExtra>,
	mutationMeta: IMapMutationData,
	forPendingRender?: boolean
): DeepWritable<MapState> => {
	const { code: opCode, maps: opMaps, planes: opPlanes, points: opPoints } = op;
	const code = transformCode(opCode, mapOpSupplement);
	const maps = opMaps === "all" ? state.maps : _.castArray(opMaps).map((mapIndex) => state.maps[mapIndex]);

	if (forPendingRender) {
		state.pendingMapDataToRender = { ...state.pendingMapDataToRender };
		if (!mutationMeta.pendingOps) {
			state.pendingMapOperations = [...state.pendingMapOperations];
			mutationMeta.pendingOps = true;
		}
	}

	const mutatedMaps: IEditableMap[] = [];
	for (let map of maps) {
		const mapAlreadyMutated = map.index in mutationMeta.maps;

		const data: IGameMap = map.data;
		const planes = opPlanes === "all" ? data.planes : _.castArray(opPlanes).map((planeIndex) => data.planes[planeIndex]);

		for (let plane of planes) {
			let mapAndPlane = { map, plane };
			if (opPoints === "all") {
				for (let y = 0; y < plane.height; y++) {
					for (let x = 0; x < plane.width; x++) {
						if (forPendingRender) {
							state.pendingMapDataToRender[getMapPlaneXYKey(map.index, plane.index, x, y)] = {
								code,
							};
						} else {
							mapAndPlane = setPlaneCode(mapAndPlane, x, y, code, mutationMeta);
							map = mapAndPlane.map;
							plane = mapAndPlane.plane;
						}
					}
				}
			} else {
				const points = _.castArray(opPoints).filter((p) => p.plane == null || p.plane === plane.index);
				for (const point of points) {
					const { x, y } = point;
					const pointCodeOrCode = transformCode(point?.code ?? code, mapOpSupplement);
					if (forPendingRender) {
						state.pendingMapDataToRender[getMapPlaneXYKey(map.index, plane.index, x, y)] = {
							code: pointCodeOrCode,
						};
					} else {
						mapAndPlane = setPlaneCode(mapAndPlane, x, y, pointCodeOrCode, mutationMeta);
						map = mapAndPlane.map;
						plane = mapAndPlane.plane;
					}
				}
			}
		}

		if (!mapAlreadyMutated && mutationMeta.maps[map.index]) {
			mutatedMaps.push(map);
		}
	}

	if (forPendingRender) {
		state.pendingMapOperations.push(op as MapOperation);
	}

	if (mutatedMaps.length > 0) {
		state.maps = [...state.maps];
		mutatedMaps.forEach((map) => (state.maps[map.index] = map));
	}

	return state;
};

/* ️⚠️ mutation of state needs to be done properly otherwise weird things can happen */
const performFillOp = (
	state: DeepWritable<MapState>,
	op: DeepReadonly<IMapFillOperation>,
	mapOpSupplement: DeepReadonly<IMapOpExtra>,
	mutationMeta: IMapMutationData
): DeepWritable<MapState> => {
	const { code: opCode, maps: opMaps, planes: opPlanes, points: opPoint } = op;
	const code = transformCode(opCode, mapOpSupplement);
	const maps = opMaps === "all" ? state.maps : _.castArray(opMaps).map((mapIndex) => state.maps[mapIndex]);

	const mutatedMaps: IEditableMap[] = [];
	for (let map of maps) {
		const mapAlreadyMutated = map.index in mutationMeta.maps;

		const data: IGameMap = map.data;
		const planes = opPlanes === "all" ? data.planes : _.castArray(opPlanes).map((planeIndex) => data.planes[planeIndex]);

		for (let plane of planes) {
			const { x, y } = opPoint;
			const searchCode = getPlaneCode(plane, x, y);
			if (searchCode == null) {
				continue;
			}

			let mapAndPlane = { map, plane };
			mapAndPlane = doFill(mapAndPlane, x, y, searchCode, code, mutationMeta);
			map = mapAndPlane.map;
			plane = mapAndPlane.plane;
		}

		if (!mapAlreadyMutated && mutationMeta.maps[map.index]) {
			mutatedMaps.push(map);
		}
	}

	if (mutatedMaps.length > 0) {
		state.maps = [...state.maps];
		mutatedMaps.forEach((map) => (state.maps[map.index] = map));
	}

	return state;
};

const doFill = (
	mapAndPlane: IMapPlaneReference,
	x: number,
	y: number,
	searchCode: number,
	fillCode: number,
	mutationMeta: IMapMutationData /*🤧*/
): IMapPlaneReference => {
	if (searchCode === fillCode) {
		return mapAndPlane;
	}

	const isObjectFill = mapAndPlane.plane.index === 1;
	let wallPlane: IGameMapPlane;
	let wallPlaneCodeBoundary: number | undefined;
	if (isObjectFill) {
		wallPlane = mapAndPlane.map.data.planes[0];
		wallPlaneCodeBoundary = getPlaneCode(wallPlane, x, y);
	}
	mapAndPlane = setPlaneCode(mapAndPlane, x, y, fillCode, mutationMeta);

	const north = getPlaneCode(mapAndPlane.plane, x, y - 1);
	if (north === searchCode && (!isObjectFill || getPlaneCode(wallPlane!, x, y - 1) === wallPlaneCodeBoundary)) {
		mapAndPlane = doFill(mapAndPlane, x, y - 1, searchCode, fillCode, mutationMeta);
	}

	const east = getPlaneCode(mapAndPlane.plane, x + 1, y);
	if (east === searchCode && (!isObjectFill || getPlaneCode(wallPlane!, x + 1, y) === wallPlaneCodeBoundary)) {
		mapAndPlane = doFill(mapAndPlane, x + 1, y, searchCode, fillCode, mutationMeta);
	}

	const south = getPlaneCode(mapAndPlane.plane, x, y + 1);
	if (south === searchCode && (!isObjectFill || getPlaneCode(wallPlane!, x, y + 1) === wallPlaneCodeBoundary)) {
		mapAndPlane = doFill(mapAndPlane, x, y + 1, searchCode, fillCode, mutationMeta);
	}

	const west = getPlaneCode(mapAndPlane.plane, x - 1, y);
	if (west === searchCode && (!isObjectFill || getPlaneCode(wallPlane!, x - 1, y) === wallPlaneCodeBoundary)) {
		mapAndPlane = doFill(mapAndPlane, x - 1, y, searchCode, fillCode, mutationMeta);
	}

	return mapAndPlane;
};

const transformCode = (code: MapOpCodeOrEnum | undefined, mapOpSupplement?: IMapOpExtra): MapOpCode => {
	if (typeof code === "number") {
		return code;
	} else if (code === "MOUSE1") {
		return mapOpSupplement?.m1Selection?.tileCode ?? 0;
	} else if (code === "MOUSE2") {
		return mapOpSupplement?.m2Selection?.tileCode ?? 0;
	}
	return 0;
};

const getPlaneCode = (plane: IGameMapPlane, x: number, y: number): number | undefined => plane?.data?.[y]?.[x];

const setPlaneCode = (
	mapAndPlane: IMapPlaneReference /*🤧*/,
	x: number,
	y: number,
	code: number,
	mutationMeta: IMapMutationData /*🤧*/
): IMapPlaneReference => {
	let { map, plane } = mapAndPlane;
	const existingCode = getPlaneCode(plane, x, y);

	if (existingCode === code) {
		return mapAndPlane;
	}

	const { index: mapIndex } = map;
	const { index: planeIndex } = plane;
	if (!mutationMeta.maps[mapIndex]) {
		mutationMeta.maps[mapIndex] = { planes: {} };
		map = {
			...map,
			data: {
				...map.data,
				planes: [...map.data.planes],
			},
			hasUnsavedChanges: true,
		};
		mapAndPlane.map = map;
	}
	if (!mutationMeta.maps[mapIndex].planes[planeIndex]) {
		mutationMeta.maps[mapIndex].planes[planeIndex] = { rows: new Set() };
		plane = { ...plane, data: [...plane.data] };
		mapAndPlane.plane = plane;
		map.data.planes[planeIndex] = plane;
	}

	const planeRows = mutationMeta.maps[mapIndex].planes[planeIndex].rows;
	if (!planeRows.has(y)) {
		plane.data[y] = [...plane.data[y]];
		planeRows.add(y);
	}

	plane.data[y][x] = code;

	return mapAndPlane;
};

// Entry Point
export const performOperations = (
	state: DeepWritable<MapState>,
	ops: DeepReadonly<OneOrMany<MapOperation>>,
	opSupplement: DeepReadonly<IMapOpExtra>
): MapState => {
	ops = _.castArray(ops);

	if (opSupplement.clearPending) {
		state.pendingMapDataToRender = {};
		state.pendingMapOperations = [];
		state.pendingMapOperationSupplement = undefined;
		state.pendingMapSelectionToRender = {};
	}
	const isForPendingRender = opSupplement.commit === false;
	if (isForPendingRender) {
		state.pendingMapOperationSupplement = { ...opSupplement };
	}

	const mutationMeta: IMapMutationData = { maps: {}, pendingOps: !!opSupplement.clearPending };

	for (const op of ops) {
		if (op.type === "set") {
			state = performSetOp(state, op, opSupplement, mutationMeta, isForPendingRender);
		} else if (op.type === "fill") {
			state = performFillOp(state, op, opSupplement, mutationMeta);
		} else if (op.type === "select") {
			state = performSelectOp(state, op, opSupplement, mutationMeta, isForPendingRender);
		}
	}

	return state;
};
