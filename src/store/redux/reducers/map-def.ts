import _ from "lodash";
import { DeepReadonly, DeepWritable, Dictionary } from "ts-essentials";
import { ActionType, createReducer } from "typesafe-actions";

import { getBootstrapDataAsync, saveAllSettingsAsync } from "../actions/app";
import {
	createActiveMapDefDifficulty,
	createActiveMapDefPlane,
	createActiveMapDefTile,
	deleteActiveMapDefDifficulty,
	deleteActiveMapDefPlane,
	deleteActiveMapDefTile,
	setActiveMapDef,
	updateActiveMapDefDifficulty,
	updateActiveMapDefPlaneMeta,
	updateActiveMapDefTile,
	moveActiveMapDefTile,
} from "../actions/map-def";
import { mapDefInitialState, MapDefState } from "../types/state";
import { Dictionaries, Arrays } from "../../../util/collections";
import { GraphicMethod, MapPlaneDefinition, MapTileData } from "../../../models/map";
import { clearGlobalTexture } from "../../local/pixi-textures";
import { MapDefinitionDifficulty, WadeMapDefinition } from "../../../models/map-definition";
import { numberKeysOf, safeAssign } from "../../../util/objects";

const doTileOperationForAllPlanes = (
	inputPlanes: DeepReadonly<WadeMapDefinition["planes"]>,
	tileConditionFn: Fn<DeepReadonly<MapTileData>, boolean>,
	tileModFn: Fn<MapTileData, MapTileData>
): DeepReadonly<WadeMapDefinition["planes"]> => {
	let planes = inputPlanes as DeepWritable<WadeMapDefinition["planes"]>;
	let planesNewRef;
	for (const planeIndex of Object.keys(planes)) {
		const [tiles, changed] = doTileOperation(inputPlanes[planeIndex].tiles, tileConditionFn, tileModFn);
		if (!changed) {
			continue;
		}
		if (!planesNewRef) {
			planes = { ...planes };
			planesNewRef = true;
		}
		planes[planeIndex] = { ...planes[planeIndex], tiles: tiles as DeepWritable<Dictionary<MapTileData>> };
	}
	return planes;
};

const doTileOperation = (
	inputTiles: DeepReadonly<Dictionary<MapTileData>>,
	tileConditionFn: Fn<DeepReadonly<MapTileData>, boolean>,
	tileModFn: Fn<MapTileData, MapTileData>
): [DeepReadonly<Dictionary<MapTileData>>, boolean] => {
	let tiles = inputTiles as Dictionary<MapTileData>;
	let tilesNewRef = false;
	for (const tileCode of Object.keys(tiles)) {
		const tile = tiles[tileCode];
		if (!tileConditionFn(tile)) {
			continue;
		}
		if (!tilesNewRef) {
			tiles = { ...tiles };
			tilesNewRef = true;
		}
		tiles[tileCode] = tileModFn({ ...tile });
	}
	return [tiles, tilesNewRef];
};

export const mapDefReducer = createReducer<MapDefState>(mapDefInitialState)
	.handleAction(
		getBootstrapDataAsync.success,
		(state: MapDefState, action: ActionType<typeof getBootstrapDataAsync.success>): MapDefState => {
			const bootstrapMapDefState = (action.payload ? action.payload.mapDef : undefined) || {};

			let newState = _.cloneDeep(state) as DeepWritable<MapDefState>;
			newState.availableMapDefsRefs = {};
			newState = _.merge(newState, bootstrapMapDefState) as DeepWritable<MapDefState>;

			return newState;
		}
	)
	.handleAction(
		saveAllSettingsAsync.request,
		(state: MapDefState, action: ActionType<typeof saveAllSettingsAsync.request>): MapDefState => {
			const {
				state: { mapDef },
			} = action.payload;

			return {
				...state,
				activeMapDef: mapDef.activeMapDef,
			};
		}
	)
	.handleAction(
		setActiveMapDef,
		(state: MapDefState, action: ActionType<typeof setActiveMapDef>): MapDefState => {
			const newState = { ...state } as DeepWritable<MapDefState>;
			safeAssign(newState, { activeMapDef: action.payload });
			return newState;
		}
	)
	.handleAction(
		deleteActiveMapDefTile,
		(state: MapDefState, action: ActionType<typeof deleteActiveMapDefTile>): MapDefState => {
			if (!state.activeMapDef) {
				return state;
			}
			const { payload: keys } = action;
			const newState = {
				...state,
				activeMapDef: { ...state.activeMapDef, planes: { ...state.activeMapDef.planes } },
			} as DeepWritable<MapDefState>;

			const newMapDef = newState.activeMapDef!;
			const planesMutated = new Set<number>();
			for (const key of keys) {
				const { plane, tileCode } = key;
				let planeData = newMapDef.planes[plane];
				if (!planesMutated.has(plane)) {
					planeData = _.cloneDeep(planeData);
					newMapDef.planes[plane] = planeData;
					planesMutated.add(plane);
				}
				delete planeData.tiles[tileCode];
				const idx = planeData.tilesOrdering.indexOf(tileCode);
				if (idx !== -1) {
					planeData.tilesOrdering.splice(idx, 1);
				}
			}

			return newState;
		}
	)
	.handleAction(
		updateActiveMapDefTile,
		(state: MapDefState, action: ActionType<typeof updateActiveMapDefTile>): MapDefState => {
			if (!state.activeMapDef) {
				return state;
			}
			const {
				payload: tile,
				meta: { code, plane },
			} = action;
			const newCode = tile.code ?? code;
			const newTile = _.cloneDeep(tile);
			const mergedTile: MapTileData = {
				...(state.activeMapDef.planes[plane].tiles?.[code] as DeepWritable<MapTileData>),
				...newTile,
			};
			if (mergedTile.customImage && (mergedTile.graphicMethod === GraphicMethod.GRID || mergedTile.graphicMethod === GraphicMethod.VSWAP)) {
				clearGlobalTexture(mergedTile.customImage);
				delete mergedTile.customImage;
			}

			const newState = {
				...state,
				activeMapDef: {
					...state.activeMapDef,
					planes: {
						...state.activeMapDef.planes,
						[plane]: {
							...state.activeMapDef.planes[plane],
							tiles: {
								...state.activeMapDef.planes[plane].tiles,
								[newCode]: mergedTile,
							},
						},
					},
				},
			} as DeepWritable<MapDefState>;

			const tilesOrdering = newState.activeMapDef!.planes[plane].tilesOrdering;
			let newTilesOrdering: number[] = tilesOrdering;
			if (newCode !== code) {
				delete newState.activeMapDef!.planes[plane].tiles[code];
				const orderIdx = tilesOrdering.findIndex((c) => c === code);
				if (orderIdx !== -1) {
					newTilesOrdering = [...tilesOrdering];
					newTilesOrdering.splice(orderIdx, 1, newCode);
				} else {
					newTilesOrdering = [...tilesOrdering, newCode];
				}
			}
			newState.activeMapDef!.planes[plane].tilesOrdering = newTilesOrdering ?? [];

			return newState;
		}
	)
	.handleAction(
		moveActiveMapDefTile,
		(state: MapDefState, action: ActionType<typeof moveActiveMapDefTile>): MapDefState => {
			if (!state.activeMapDef) {
				return state;
			}
			const {
				payload: { fromIndex, toIndex },
				meta: { plane },
			} = action;

			const newTilesOrdering = Arrays.moveTo([...state.activeMapDef.planes[plane].tilesOrdering], fromIndex, toIndex);

			return {
				...state,
				activeMapDef: {
					...state.activeMapDef,
					planes: {
						...state.activeMapDef.planes,
						[plane]: {
							...state.activeMapDef.planes[plane],
							tilesOrdering: newTilesOrdering,
						},
					},
				},
			};
		}
	)
	.handleAction(
		createActiveMapDefTile,
		(state: MapDefState, action: ActionType<typeof createActiveMapDefTile>): MapDefState => {
			if (!state.activeMapDef) {
				return state;
			}
			const { payload: tile } = action;
			const newTile: MapTileData = _.cloneDeep(tile);
			if (!newTile.graphicMethod) {
				newTile.graphicMethod = GraphicMethod.GRID;
			}
			const { code, plane } = tile;

			return {
				...state,
				activeMapDef: {
					...state.activeMapDef,
					planes: {
						...state.activeMapDef.planes,
						[plane]: {
							...state.activeMapDef.planes[plane],
							tiles: {
								...state.activeMapDef.planes[plane].tiles,
								[code]: newTile,
							},
							tilesOrdering: [...state.activeMapDef.planes[plane].tilesOrdering, code],
						},
					},
				},
			};
		}
	)
	.handleAction(
		createActiveMapDefPlane,
		(state: MapDefState, action: ActionType<typeof createActiveMapDefPlane>): MapDefState => {
			if (!state.activeMapDef) {
				return state;
			}
			const { payload: plane } = action;
			const newPlane = _.cloneDeep(plane);
			if (!newPlane.tiles[0]) {
				newPlane.tiles[0] = {
					code: 0,
					label: "Nothing",
					plane: newPlane.index,
				};
				newPlane.tilesOrdering = [0];
			}
			return {
				...state,
				activeMapDef: {
					...state.activeMapDef,
					planes: {
						...state.activeMapDef.planes,
						[plane.index]: newPlane,
					},
				},
			};
		}
	)
	.handleAction(
		updateActiveMapDefPlaneMeta,
		(state: MapDefState, action: ActionType<typeof updateActiveMapDefPlaneMeta>): MapDefState => {
			if (!state.activeMapDef) {
				return state;
			}
			const {
				payload: data,
				meta: { index, moveToIndex },
			} = action;
			let newPlanes: DeepReadonly<Dictionary<MapPlaneDefinition>>;
			if (moveToIndex != null && index !== moveToIndex) {
				newPlanes = Dictionaries.moveIndexed(state.activeMapDef.planes, index, moveToIndex, (plane, newIdx, oldIdx) => ({
					...plane,
					...(oldIdx === index ? data : undefined),
					tiles: doTileOperation(
						plane.tiles,
						(t) => t.plane === oldIdx,
						(t) => {
							t.plane = newIdx;
							return t;
						}
					)[0],
					index: newIdx,
				}));
			} else {
				newPlanes = {
					...state.activeMapDef.planes,
					[index]: {
						...state.activeMapDef.planes[index],
						...data,
					},
				};
			}

			return {
				...state,
				activeMapDef: {
					...state.activeMapDef,
					planes: newPlanes,
				},
			};
		}
	)
	.handleAction(
		deleteActiveMapDefPlane,
		(state: MapDefState, action: ActionType<typeof deleteActiveMapDefPlane>): MapDefState => {
			if (!state.activeMapDef) {
				return state;
			}
			const { payload: indexToDelete } = action;
			const currentPlanes = state.activeMapDef.planes;
			const maxIndex = Math.max(...(numberKeysOf(currentPlanes) ?? [NaN]));
			if (maxIndex !== indexToDelete) {
				return state;
			}
			const newPlanes = Dictionaries.deleteIndexed(currentPlanes, indexToDelete, (plane, newIdx) => ({ ...plane, index: newIdx }));

			return {
				...state,
				activeMapDef: {
					...state.activeMapDef,
					planes: newPlanes,
				},
			};
		}
	)
	.handleAction(
		createActiveMapDefDifficulty,
		(state: MapDefState, action: ActionType<typeof createActiveMapDefDifficulty>): MapDefState => {
			if (!state.activeMapDef) {
				return state;
			}
			return {
				...state,
				activeMapDef: {
					...state.activeMapDef,
					difficulties: [...(state.activeMapDef.difficulties ?? []), action.payload],
				},
			};
		}
	)
	.handleAction(
		updateActiveMapDefDifficulty,
		(state: MapDefState, action: ActionType<typeof updateActiveMapDefDifficulty>): MapDefState => {
			if (!state.activeMapDef?.difficulties) {
				return state;
			}
			const {
				payload: difficultyUpdates,
				meta: { index },
			} = action;

			const newDifficulties = [...state.activeMapDef.difficulties];
			const currentDifficulty = newDifficulties.splice(index, 1);
			newDifficulties.splice(index, 0, { ...currentDifficulty, ..._.cloneDeep(difficultyUpdates) } as MapDefinitionDifficulty);
			return {
				...state,
				activeMapDef: {
					...state.activeMapDef,
					difficulties: newDifficulties,
				},
			};
		}
	)
	.handleAction(
		deleteActiveMapDefDifficulty,
		(state: MapDefState, action: ActionType<typeof deleteActiveMapDefDifficulty>): MapDefState => {
			if (!state.activeMapDef?.difficulties) {
				return state;
			}
			const { payload: index } = action;
			const newDifficulties = [...state.activeMapDef.difficulties];
			newDifficulties.splice(index, 1);

			const planes = doTileOperationForAllPlanes(
				state.activeMapDef.planes!,
				(t) => t.difficulty === index,
				(t) => {
					delete t.difficulty;
					return t;
				}
			);

			return {
				...state,
				activeMapDef: {
					...state.activeMapDef,
					planes,
					difficulties: newDifficulties,
				},
			};
		}
	);

export default mapDefReducer;
