import { ActionType, createReducer } from "typesafe-actions";
import { DeepWritable } from "ts-essentials";

import { getBootstrapDataAsync, updateKeysDownAsync } from "../actions/app";
import { AppState, appInitialState } from "../types/state";
import { AddRemove } from "../../../models/wade";

export const appReducer = createReducer<AppState>(appInitialState)
	.handleAction(getBootstrapDataAsync.success, (state: AppState, action: ActionType<typeof getBootstrapDataAsync.success>) => {
		return { ...state, isInitialLoadComplete: true };
	})
	.handleAction(
		updateKeysDownAsync.request,
		(state: AppState, action: ActionType<typeof updateKeysDownAsync.request>): AppState => {
			const newState = { ...state } as DeepWritable<AppState>;
			const { key, op } = action.payload;
			const isAlreadyDown = key in state.keysDown;
			if (op === AddRemove.ADD && !isAlreadyDown) {
				newState.keysDown = {
					...state.keysDown,
					[key]: true,
				};
			} else if (op === AddRemove.REMOVE && isAlreadyDown) {
				newState.keysDown = { ...state.keysDown };
				delete newState.keysDown[key];
			}
			return newState;
		}
	);

export default appReducer;
