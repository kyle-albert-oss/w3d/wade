import { ActionType, createReducer } from "typesafe-actions";

import { updateTheme } from "../actions/ui";
import { ThemeState, initialThemeState } from "../types/state";

export const themeReducer = createReducer<ThemeState>(initialThemeState).handleAction(
	updateTheme,
	(state: ThemeState, action: ActionType<typeof updateTheme>) => {
		return { ...action.payload };
	}
);

export default themeReducer;
