import { Store, Unsubscribe } from "redux";

import { SharedRootState, AppRootState } from "../types/state";
import {
	activeMapDefPlanesSelector,
	selectedEditTileSelector,
	selectedPlaneNumSelector,
	selectedMapIndexSelector,
	mapsSelector,
} from "../selectors";
import { uiUpdate } from "../actions/ui";
import { numberKeysOf } from "../../../util/objects";
import { AppDispatch } from "../types/action";
import { clearMapSelection } from "../actions/map";

type Subscriber<S, R = void> = (dispatch: AppDispatch, getState: () => S) => R;
const subscribe = <S extends SharedRootState>(store: Store<S>, subscriber: Subscriber<S>): Unsubscribe => {
	return store.subscribe(() => {
		subscriber(store.dispatch, store.getState);
	});
};
type ChangeSubscriber<S> = Subscriber<S, () => void>;
const changeSubscriber = <S extends SharedRootState>(store: Store<S>, subscriber: ChangeSubscriber<S>): Unsubscribe => {
	return store.subscribe(subscriber(store.dispatch, store.getState));
};

const selectedMapConstrainer: Subscriber<AppRootState> = (dispatch, getState) => {
	const state = getState();
	const selectedMapIndex = selectedMapIndexSelector(state);
	const maps = mapsSelector(state);
	if (selectedMapIndex < 0) {
		dispatch(uiUpdate({ selectedMapIndex: 0 }));
	} else if (selectedMapIndex >= maps.length && selectedMapIndex !== 0) {
		dispatch(uiUpdate({ selectedMapIndex: maps.length - 1 }));
	}
};

const selectedPlaneConstrainer: Subscriber<SharedRootState> = (dispatch, getState) => {
	const state = getState();
	const selectedPlaneNum = selectedPlaneNumSelector(state);
	const activeMapDefPlanes = activeMapDefPlanesSelector(state);
	if (activeMapDefPlanes && !activeMapDefPlanes[selectedPlaneNum]) {
		const nextPlaneNum = Math.max(0, ...numberKeysOf(activeMapDefPlanes).filter((p) => p < selectedPlaneNum));
		dispatch(uiUpdate({ selectedPlaneNum: nextPlaneNum }));
	}
};

const selectedTileConstrainer: Subscriber<SharedRootState> = (dispatch, getState) => {
	const state = getState();
	const tile = selectedEditTileSelector(state);
	if (!tile) {
		return;
	}
	const activeMapDefPlanes = activeMapDefPlanesSelector(state);
	const { plane, tileCode } = tile;
	if (activeMapDefPlanes?.[plane].tiles[tileCode] == null) {
		const newTileCode = Math.max(0, ...numberKeysOf(activeMapDefPlanes?.[plane].tiles ?? {}).filter((c) => c < tileCode));
		dispatch(uiUpdate({ selectedEditTile: { plane, tileCode: newTileCode } }));
	}
};

const mapSelectionClearer: ChangeSubscriber<SharedRootState> = (dispatch, getState) => {
	let lastSelectedMapIndex = selectedMapIndexSelector(getState());
	return () => {
		const selectedMapIndex = selectedMapIndexSelector(getState());
		if (selectedMapIndex !== lastSelectedMapIndex) {
			lastSelectedMapIndex = selectedMapIndex;
			dispatch(clearMapSelection());
		}
	};
};

export const attachSharedStoreListeners = <S extends SharedRootState>(store: Store<S>): Store<S> => {
	subscribe(store, selectedPlaneConstrainer);
	subscribe(store, selectedTileConstrainer);
	return store;
};

export const attachAppStoreListeners = (store: Store<AppRootState>): Store<AppRootState> => {
	subscribe(store, selectedMapConstrainer);
	changeSubscriber(store, mapSelectionClearer);
	return store;
};
