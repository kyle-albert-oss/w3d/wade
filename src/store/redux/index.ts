import { routerMiddleware } from "connected-react-router";
import { History } from "history";
import { applyMiddleware, compose, createStore, Middleware, Store } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { DeepPartial } from "ts-essentials";
import { useStore } from "react-redux";

import { AppRootState } from "./types/state";
import { createRootReducer, createSettingsRootReducer } from "./reducers";

interface IConfigureStoreOpts<S> {
	history?: History;
	initialState?: DeepPartial<S>;
}

export const configureStore = <S = AppRootState>(opts: IConfigureStoreOpts<S> = {}): Store<S> => {
	const { history, initialState } = opts;
	const middlewares: Middleware[] = [];
	if (history) {
		middlewares.push(routerMiddleware(history));
	}
	middlewares.push(thunk);

	const composeEnhancers = process.env.NODE_ENV !== "production" ? composeWithDevTools : compose;

	return createStore(
		// @ts-expect-error
		history ? createRootReducer(history) : createSettingsRootReducer(),
		// @ts-expect-error
		initialState as AppRootState,
		// @ts-expect-error
		composeEnhancers(applyMiddleware(...middlewares))
	);
};

export const useAppStore = () => useStore<AppRootState>();
