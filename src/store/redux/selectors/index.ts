import { createSelector } from "reselect";
import { DeepReadonly } from "ts-essentials";

import { GraphicMethod, MapPlaneDefinition, MapTileData, TileType } from "../../../models/map";
import MapCanvasTool, { brushTools, ISelectedMapCanvasTools, selectionTools } from "../../../models/map-canvas-tool";
import { ImgSrcs } from "../../../models/wade";
import { Arrays, Sets } from "../../../util/collections";
import { toNumber } from "../../../util/numbers";
import * as MoreObjects from "../../../util/objects";
import * as Strings from "../../../util/strings";
import { AppRootState, SettingsRootState, SharedRootState } from "../types/state";
import { WadeMapDefinition } from "../../../models/map-definition";

// TODO: double check there's no memoization caused issues.

export const appStateSelector = (state: AppRootState) => state.app;
export const formsDisabledSelector = (state: SharedRootState): boolean => {
	const arr = state.ui.formsDisabled;
	return arr && arr.length > 0;
};
export const graphicChunksSelector = (state: SharedRootState) => state.graphic.graphicChunks;
export const keysDownMapSelector = (state: AppRootState) => state.app.keysDown;
export const keysDownSelector = createSelector(keysDownMapSelector, (keysDownMap) =>
	keysDownMap ? new Set(Object.keys(keysDownMap)) : new Set<string>()
);
export const mapCanvasTileSizeSelector = (state: SharedRootState) => state.ui.mapCanvasTileSize;
export const mapDefTileSizeSelector = (state: SharedRootState) => state.ui.mapDefTileSize;
export const mapStateSelector = (state: AppRootState) => state.map;
export const mapSaveTargetSelector = (state: AppRootState) => state.map.mapSrc;
export const mapsSelector = (state: AppRootState) => state.map.maps;
export const mapSelectionDataSelector = (state: AppRootState) => state.map.mapSelectionData;
export const mapHasSelectedDataSelector = createSelector(
	mapSelectionDataSelector,
	(selectionData) => selectionData != null && Object.keys(selectionData).length > 0
);
export const mapTileZoomSelector = (state: SharedRootState) => state.ui.mapTileZoom;
export const pendingMapDataSelector = (state: AppRootState) => state.map.pendingMapDataToRender;
export const pendingMapSelectionDataSelector = (state: AppRootState) => state.map.pendingMapSelectionToRender;
export const colorPresetsSelector = (state: SharedRootState) => state.ui.colorPresets;
export const selectedGraphicChunkIndexSelector = (state: SharedRootState) => state.ui.selectedGraphicChunkIndex;
export const selectedMapIndexSelector = (state: SharedRootState) => state.ui.selectedMapIndex;
export const selectedPlaneNumSelector = (state: SharedRootState) => state.ui.selectedPlaneNum;
export const selectedTilesMapSelector = (state: SharedRootState) => state.ui.selectedTiles;
export const selectedEditTileSelector = (state: SharedRootState) => state.ui.selectedEditTile;
export const selectedTileMouse1Selector = (state: SharedRootState) => state.ui.selectedTiles.MOUSE1;
export const selectedTileMouse2Selector = (state: SharedRootState) => state.ui.selectedTiles.MOUSE2;
export const spriteStartIndexSelector = (state: SharedRootState) => state.graphic.spriteStartIndex;
export const uiStateSelector = (state: SharedRootState) => state.ui;
export const useLegacyMapTilesSelector = (state: SharedRootState) => state.ui.useLegacyTileGraphics;
export const selectedMapToolsSelector = (state: SharedRootState) => state.ui.selectedMapCanvasTools;
export const updateThemeSelector = (state: SharedRootState) => state.ui.updateTheme;
export const settingsEditorSelector = (state: SettingsRootState) => state.settingsEditor;
export const themeStateSelector = (state: SharedRootState) => state.theme;

export const activePk3StagingDirSelector = (state: SharedRootState) => state.project.activePk3StagingDir;
export const activeProjectSelector = (state: SharedRootState) => state.project.activeProject;
export const activeProjectIdSelector = createSelector(activeProjectSelector, (activeProject) => activeProject?.id);
export const activeProjectCompileStatusSelector = (state: SharedRootState) => state.project.activeProjectCompileResult;
export const availableProjectRefsSelector = (state: SharedRootState) => state.project.availableProjectRefs;
export const availableProjectRefsArraySelector = createSelector(
	availableProjectRefsSelector,
	MoreObjects.dictionaryToROArrayAllSorted((p1, p2) => Strings.compareIgnoreCase(p1.name, p2.name))
);
export const recentProjectIdsSelector = (state: SharedRootState) => state.project.recentProjectIds;
export const recentProjectsSelector = createSelector(
	availableProjectRefsSelector,
	recentProjectIdsSelector,
	MoreObjects.dictionaryToROArrayByKeys
);
export const selectedProjectIdSelector = (state: SharedRootState) => state.ui.selectedProjectId;
export const selectedProjectSelector = createSelector(
	availableProjectRefsSelector,
	selectedProjectIdSelector,
	MoreObjects.dictionaryLookup
);

export const activePaletteSelector = (state: SharedRootState) => state.graphic.activePalette;
export const availablePaletteRefsSelector = (state: SharedRootState) => state.graphic.availablePaletteRefs;
export const availablePaletteRefsArraySelector = createSelector(
	availablePaletteRefsSelector,
	MoreObjects.dictionaryToROArrayAllSorted((p1, p2) => Strings.compareIgnoreCase(p1.name, p2.name))
);

export const activeMapDefSelector = (state: SharedRootState) => state.mapDef.activeMapDef;
export const activeMapDefDifficultiesSelector = createSelector(activeMapDefSelector, (activeMapDef) => activeMapDef?.difficulties ?? []);
export const activeMapDefPlanesSelector = (state: SharedRootState) => state.mapDef.activeMapDef?.planes;
export const availableMapDefRefsSelector = (state: SharedRootState) => state.mapDef.availableMapDefsRefs;
export const availableMapDefRefsArraySelector = createSelector(
	availableMapDefRefsSelector,
	MoreObjects.dictionaryToROArrayAllSorted((md1, md2) => Strings.compareIgnoreCase(md1.name, md2.name))
);
export const activeMapDefSelectedPlaneSelector = createSelector(
	activeMapDefSelector,
	selectedPlaneNumSelector,
	(mapDef, selectedPlaneNum) => {
		if (!mapDef || selectedPlaneNum == null) {
			return undefined;
		}
		return mapDef.planes[selectedPlaneNum];
	}
);

const getMapTileDataFn = (mapDef?: DeepReadonly<WadeMapDefinition>) => (
	plane: number | undefined,
	tileCode: number | undefined
): DeepReadonly<MapTileData> | undefined => {
	if (plane == null || tileCode == null) {
		return undefined;
	}
	return mapDef?.planes?.[plane]?.tiles?.[tileCode ?? 0];
};
export const activeMapDefTileFnSelector = createSelector(activeMapDefSelector, (activeMapDef) => getMapTileDataFn(activeMapDef));
export const activeMapDefAllTileCodesForSelectedPlaneSelector = createSelector(activeMapDefSelectedPlaneSelector, (plane) =>
	plane == null ? undefined : new Set([...Object.values(plane.tiles).map((t) => t.code)])
);

export const activeMapDefMaxTileCodeForSelectedPlaneSelector = createSelector(
	activeMapDefAllTileCodesForSelectedPlaneSelector,
	(tileCodes) => (tileCodes && tileCodes.size > 0 ? Math.max(...tileCodes) : 0)
);

// ⚠️: this feeds individual tiles on the map canvas. It should be efficient as possible and never break referential equality other UI might freeze on updates
export const mapTileGraphicUrlForEditorSelector = createSelector(
	activeMapDefSelector,
	spriteStartIndexSelector,
	useLegacyMapTilesSelector,
	graphicChunksSelector,
	(projectMapDef, spriteStartIndex, useLegacyMapTiles, graphicChunks) => {
		return (tile: DeepReadonly<MapTileData> | undefined): ImgSrcs => {
			let result;
			if (!tile || !projectMapDef) {
				return undefined;
			}
			const { customImage, graphicMethod = GraphicMethod.CUSTOM_IMAGE, gridImage, plane } = tile;
			let { graphicIndex } = tile;
			if (graphicMethod === GraphicMethod.GRID || (useLegacyMapTiles && gridImage != null)) {
				return gridImage;
			} else if (graphicMethod === GraphicMethod.CUSTOM_IMAGE) {
				result = customImage;
			} else if (graphicMethod === GraphicMethod.VSWAP && graphicIndex != null) {
				if (projectMapDef.planes[plane].name === "Objects" && spriteStartIndex != null) {
					graphicIndex += spriteStartIndex;
				}
				result = graphicChunks?.[graphicIndex]?.url;
			}
			if (result == null) {
				// could happen when: vswap changes and index no longer is valid
				result = gridImage;
			}
			return (result || undefined) as ImgSrcs;
		};
	}
);

export const mapTileGraphicUrlsSelector = createSelector(
	activeMapDefSelector,
	graphicChunksSelector,
	spriteStartIndexSelector,
	(projectMapDef, graphicChunks, spriteStartIndex) => {
		return (tile: DeepReadonly<MapTileData> | undefined): ImgSrcs => {
			if (!tile || !projectMapDef || !graphicChunks) {
				return undefined;
			}
			let imgSrcs: string[] | undefined;
			if (tile.customImage) {
				imgSrcs = [tile.customImage];
			}
			if (tile.graphicIndex != null) {
				const index = tile.graphicIndex + (tile.plane === 1 ? spriteStartIndex || 0 : 0);
				let url = graphicChunks?.[index]?.url;
				if (url) {
					imgSrcs = imgSrcs || [];
					imgSrcs.push(url);
					if (tile.types && (tile.types[TileType.WALL] || tile.types[TileType.DOOR])) {
						url = graphicChunks?.[index + 1]?.url;
						if (url) {
							imgSrcs.push(url);
						}
					}
				}
			}
			return imgSrcs;
		};
	}
);

export const mapTilegridImageUrlSelector = (tile: MapTileData) => tile && tile.gridImage;

export const mapTileSecondaryGraphicUrl = createSelector(
	useLegacyMapTilesSelector,
	graphicChunksSelector,
	(useLegacyMapTiles, graphicChunks) => {
		return (tile: DeepReadonly<MapTileData>) => {
			if (!tile || tile.graphicIndex == null || tile.types == null || !tile.types[TileType.WALL]) {
				return undefined;
			}
			return graphicChunks?.[tile.graphicIndex + 1]?.url;
		};
	}
);

const MIN_FONT_SIZE = 12;
const MAX_FONT_SIZE = 16;
export const calcMapDefLabelSize = (tileSize: number) =>
	toNumber(tileSize / 3, { min: MIN_FONT_SIZE, max: MAX_FONT_SIZE, defaultValue: 14 });
export const mapDefTileLabelSizeSelector = createSelector(mapDefTileSizeSelector, calcMapDefLabelSize);

export const selectedMapSelector = createSelector(mapsSelector, selectedMapIndexSelector, (maps, selectedMapIndex) =>
	!maps || selectedMapIndex == null || selectedMapIndex >= maps.length ? undefined : maps[selectedMapIndex]
);

export const selectedMapDataSelector = createSelector(selectedMapSelector, (selectedMap) => (!selectedMap ? undefined : selectedMap.data));

export const selectedMapWidthSelector = createSelector(selectedMapDataSelector, (data) => data?.width);
export const selectedMapHeightSelector = createSelector(selectedMapDataSelector, (data) => data?.height);
export const selectedMapCanvasSizeSelector = createSelector(
	selectedMapDataSelector,
	mapCanvasTileSizeSelector,
	(selectedMap, mapDefTileSize) => (!selectedMap || !mapDefTileSize ? 32 : Math.max(selectedMap.width, selectedMap.height) * mapDefTileSize)
);

const getPlaneTileOrdering = (plainIndex?: number, mapDef?: DeepReadonly<WadeMapDefinition>) => {
	if (plainIndex == null) {
		return [];
	}
	return mapDef?.planes?.[plainIndex]?.tilesOrdering ?? [];
};

export const selectedPlaneTileOrderingSelector = createSelector(selectedPlaneNumSelector, activeMapDefSelector, getPlaneTileOrdering);

const getMapDefPlanes = (mapDef?: DeepReadonly<WadeMapDefinition>): DeepReadonly<MapPlaneDefinition[]> => {
	const planes = mapDef?.planes ?? {};
	return Object.keys(planes)
		.map((k) => toNumber(k))
		.sort((p1, p2) => p1 - p2)
		.map((num) => planes[num]);
};
export const activeMapDefPlaneIndexNameSelector = createSelector(activeMapDefSelector, getMapDefPlanes);

export const selectedMapNameSelector = createSelector(selectedMapSelector, (selectedMap) => selectedMap?.data?.name);
export const mapNamesSelector = createSelector(mapsSelector, (maps): readonly string[] =>
	maps ? maps.map((m) => m?.data?.name ?? "") : []
);
export const selectedMapPlanesSelector = createSelector(selectedMapDataSelector, (selectedMap) =>
	!selectedMap ? undefined : selectedMap.planes
);

const getAllPlaneItems = (
	mapDef?: DeepReadonly<WadeMapDefinition>,
	selectedPlaneNum?: number,
	selectedPlaneTileOrdering?: DeepReadonly<number[]>
): DeepReadonly<MapTileData[]> => {
	if (!mapDef || selectedPlaneNum == null || selectedPlaneTileOrdering == null || selectedPlaneTileOrdering.length === 0) {
		return [];
	}
	return selectedPlaneTileOrdering.map((code) => mapDef.planes[selectedPlaneNum].tiles[code]);
};

export const selectedPlaneAllItemsSelector = createSelector(
	activeMapDefSelector,
	selectedPlaneNumSelector,
	selectedPlaneTileOrderingSelector,
	getAllPlaneItems
);
export const activeMapDefAllTagsForSelectedPlaneSelector = createSelector(selectedPlaneAllItemsSelector, (tiles) => {
	if (!tiles) {
		return [];
	}
	const tags = new Set<string>();
	for (const tile of tiles) {
		if (tile.tags) {
			Sets.addAll(tags, tile.tags);
		}
	}
	return [...tags].sort(Strings.compareIgnoreCase);
});

export const selectedMapTilesTuplesSelector = createSelector(selectedTilesMapSelector, (selectedTilesMap) =>
	!selectedTilesMap ? [] : Object.values(selectedTilesMap)
);

export const selectedMapToolsArraySelector = createSelector(
	selectedMapToolsSelector,
	(selectedMapTools) => Object.keys(selectedMapTools || {}) as MapCanvasTool[]
);

export const selectedMapToolsSetSelector = createSelector(selectedMapToolsArraySelector, (selectedMapTools) => new Set(selectedMapTools));

const getSelectedMapToolOfGroup = (
	selectedMapTools: ISelectedMapCanvasTools,
	groupTools: readonly MapCanvasTool[]
): MapCanvasTool | undefined => {
	if (!selectedMapTools) {
		return undefined;
	}
	for (const tool of groupTools) {
		if (selectedMapTools[tool]) {
			return tool;
		}
	}
	return undefined;
};

export const selectedBrushMapToolSelector = createSelector(selectedMapToolsSelector, (selectedMapTools) =>
	getSelectedMapToolOfGroup(selectedMapTools as ISelectedMapCanvasTools, brushTools)
);

export const selectedSelectionMapToolSelector = createSelector(selectedMapToolsSelector, (selectedMapTools) =>
	getSelectedMapToolOfGroup(selectedMapTools as ISelectedMapCanvasTools, selectionTools)
);

export const projectHasUnsavedChangesSelector = createSelector(mapsSelector, (maps) => {
	if (maps && Arrays.anyMatch(maps, (em) => em.hasUnsavedChanges)) {
		return true;
	}
	return false;
});

export const selectedVswapGraphicIndexSelector = (state: SharedRootState) => state.ui.selectedGraphicChunkIndex;
export const selectedVswapGraphicSelector = createSelector(
	selectedVswapGraphicIndexSelector,
	graphicChunksSelector,
	(selectedVswapGraphicIndex, graphicChunks) => graphicChunks?.[selectedVswapGraphicIndex]
);

export const vswapEditorZoomSelector = createSelector(
	selectedVswapGraphicSelector,
	(state) => state.ui.vswapEditorZoom,
	(selectedVswapGraphic, vswapEditorZoom) => {
		return (opts: { maxHeight?: number; maxWidth?: number } = {}): { defaultValue: number; max: number; min: number; value: number } => {
			let defaultValue = 1;
			let max = 2;
			let min = 0.5;
			if (!selectedVswapGraphic) {
				return { defaultValue, max, min, value: toNumber(vswapEditorZoom, { defaultValue, max, min }) };
			}
			// TODO: calculate constraint based on graphic size and input
			defaultValue = 4;
			max = 7;
			min = 2;
			return { defaultValue, max, min, value: toNumber(vswapEditorZoom, { defaultValue, max, min, outOfRangeOrNaN: "defaultValue" }) };
		};
	}
);

export const hasUnsavedSettingsSelector = createSelector(
	settingsEditorSelector,
	(settings) => settings.hasUnsavedActiveMapDef || settings.hasUnsavedActiveProject
);

export const surroundingMapTileCodeFnSelector = createSelector(selectedMapSelector, (map) => (plane: number, x: number, y: number) => {
	const planeData = map?.data.planes?.[plane]?.data;
	if (!planeData) {
		return null;
	}
	const topRow = planeData?.[y - 1];
	const row = planeData?.[y];
	const bottomRow = planeData?.[y + 1];
	return {
		n: topRow?.[x] ?? null,
		ne: topRow?.[x + 1] ?? null,
		e: row?.[x + 1] ?? null,
		se: bottomRow?.[x + 1] ?? null,
		s: bottomRow?.[x] ?? null,
		sw: bottomRow?.[x - 1] ?? null,
		w: row?.[x - 1] ?? null,
		nw: topRow?.[x - 1] ?? null,
	};
});
