import { mapStackTrace } from "sourcemapped-stacktrace";
import * as ReactDOM from "react-dom";
import * as React from "react";
import { CssBaseline, ThemeProvider, createMuiTheme } from "@material-ui/core";

import { ErrorPage } from "./ui/components/core/ErrorPage/ErrorPage";

const renderError = (error: Error | string) => {
	ReactDOM.render(
		<ThemeProvider theme={createMuiTheme({ palette: { type: "dark" } })}>
			<CssBaseline />
			<ErrorPage error={error} />
		</ThemeProvider>,
		document.getElementById("root")
	);
};

window.addEventListener("error", (evt) => {
	if (evt.error && evt.error.stack) {
		mapStackTrace(evt.error.stack, (mappedStack: string[]) => {
			evt.error.mappedStack = mappedStack;
			renderError(evt.error);
		});
		return;
	}

	renderError(evt.error ?? evt.message);
});
