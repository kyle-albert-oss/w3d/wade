import path from "path";

import chroma from "chroma-js";
import { readWdcWmcFile } from "wolf3d-data";
import chalk from "chalk";
import { Dictionary, SafeDictionary } from "ts-essentials";

import { GraphicMethod, MapTileData, PlaneType, TileDirection, TileType, MapPlaneDefinition } from "../../../models/map";
import { WadeMapDefinition } from "../../../models/map-definition";
import { GameResourceRef } from "../../../models/resources";
import { toNumber } from "../../../util/numbers";
import { generateUUID } from "../../../util/strings";
import { MAP_EDIT_COLOR_PRESETS } from "../../../models/wade";
import { Arrays } from "../../../util/collections";
import { deleteNullAndUndefined } from "../../../util/objects";
import { getAppVersion } from "../../../util/env";

export type ToCSSColors<T> = (arg: T) => (string | null)[] | undefined;
export type ToCSSColor<T> = (arg: T) => string | null;
export type WDCColorConverter<T> = ToCSSColors<T> | { [key: string]: string | null };

export interface IWDCToWadeConversionOpts {
	colorConversion?: WDCColorConverter<string>;
	extensions?: readonly string[];
	id?: GameResourceRef["id"];
	name?: GameResourceRef["name"];
}

const SYMBOL_ORDER_PLANE_REGEX = /^Plane(\d+)$/i;

// TODO: this doesn't need to be a class.
export class WDCMapDefinitionFileConverter {
	static readonly WDC_GRAPHIC_SIZE = 7;
	static readonly WDC_GRAPHIC_STR_LENGTH = Math.pow(WDCMapDefinitionFileConverter.WDC_GRAPHIC_SIZE, 2);
	static readonly WDC_COLORS = MAP_EDIT_COLOR_PRESETS.reduce<Record<number, string | null>>((acc, color, i) => {
		acc[i] = chroma(color).num() === 0 ? null : color;
		return acc;
	}, {});

	static getColorConverter<T>(fn?: ToCSSColor<number>): ToCSSColors<string> {
		return (wdcColorStr) => {
			const wdcGfxSize = WDCMapDefinitionFileConverter.WDC_GRAPHIC_SIZE;
			if (wdcColorStr.length !== wdcGfxSize * wdcGfxSize) {
				return;
			}

			const result = [];
			for (let i = 0, x = 0; x < wdcGfxSize; x++) {
				for (let y = 0; y < wdcGfxSize; y++, i++) {
					const hexVal = toNumber(wdcColorStr.charAt(i), { radix: 16, min: 0, max: 0xf, outOfRangeOrNaN: "min", defaultValue: 0 });
					result.push(fn ? fn(hexVal) : WDCMapDefinitionFileConverter.WDC_COLORS[hexVal]);
				}
			}
			return result;
		};
	}

	protected static toTileDirection(num: number | string | undefined): TileDirection | undefined {
		return toNumber(num, { min: TileDirection.EAST, max: TileDirection.SOUTH_EAST });
	}

	convertToWadeFormat = async (filePath: string, opts: IWDCToWadeConversionOpts = {}): Promise<WadeMapDefinition> => {
		const result: WadeMapDefinition = {
			difficulties: [
				{ name: "Can I play, Daddy?" },
				{ name: "Don't hurt me." },
				{ name: "Bring 'em on!" },
				{ name: "I am Death incarnate!" },
			],
			extensions: [...(opts?.extensions ?? [])],
			id: opts?.id ?? generateUUID(),
			name: opts?.name ?? path.basename(filePath, path.extname(filePath)),
			planes: {},
			wadeVersion: getAppVersion(),
		};

		let [wmcNodes] = await readWdcWmcFile(filePath);
		wmcNodes = wmcNodes.filter((n: any) => n != null);

		const tileOrderingByPlaneIndex: Dictionary<number[]> = {};

		// find tile ordering
		// assumption: no numbers are missing
		let inOrderingSection = false;
		for (const node of wmcNodes) {
			if (node.type === "ini_section_header" && node.value?.toLowerCase() === "symbolorder") {
				inOrderingSection = true;
				continue;
			}
			if (!inOrderingSection || node.type !== "setting") {
				continue;
			}
			const settingName = node.name.value || "";
			const planeMatch = SYMBOL_ORDER_PLANE_REGEX.exec(settingName);
			const planeIndex = planeMatch?.[1];
			if (!planeMatch || planeIndex == null) {
				continue;
			}
			tileOrderingByPlaneIndex[planeIndex] = ((node.eq?.value || "") as string)
				.split(",")
				.map((c) => toNumber(c, { min: 1, outOfRangeOrNaN: Number.NaN }))
				.filter((c) => !isNaN(c));
		}

		let currentHeader: Maybe<string>;
		let planeIndex: Maybe<number>;
		let planeData: MapPlaneDefinition | undefined;
		let converted: MapTileData | undefined;
		for (const node of wmcNodes) {
			if (node.type === "ini_section_header") {
				currentHeader = node.value.toLowerCase();
				let planeName: Maybe<string>;
				let planeType: Maybe<PlaneType>;
				if (currentHeader === "walls") {
					planeIndex = 0;
					planeName = "Walls";
					planeType = PlaneType.WALL;
				} else if (currentHeader === "objects") {
					planeIndex = 1;
					planeName = "Objects";
					planeType = PlaneType.OBJECT;
				} else if (currentHeader === "plane 3") {
					planeIndex = 2;
					planeName = "Extra";
					planeType = PlaneType.DEFAULT;
				} else {
					planeIndex = planeName = planeType = planeData = undefined;
					continue;
				}
				result.planes[planeIndex] = planeData = {
					index: planeIndex,
					name: planeName || "UNKNOWN",
					tiles: {
						0: { code: 0, label: "Nothing", plane: planeIndex },
					},
					tilesOrdering: [0, ...(tileOrderingByPlaneIndex[planeIndex] ?? [])],
					type: planeType ?? PlaneType.DEFAULT,
				};
				continue;
			}

			if (node.type === "setting" && planeData) {
				const tileCode = toNumber(node.name.value, {
					min: 1,
					outOfRangeOrNaN: (num: number) => {
						throw new Error(`${num} is out of range (< 1) or not a number. Node: ${JSON.stringify(node)}`);
					},
				});
				const tilePropertiesStr = node.eq?.value;
				const { index: pIndex, type: planeType } = planeData;
				if (tilePropertiesStr) {
					if (planeType === PlaneType.WALL) {
						converted = this.convertWall(tileCode, tilePropertiesStr, pIndex, opts);
					} else if (planeType === PlaneType.OBJECT) {
						converted = this.convertObject(tileCode, tilePropertiesStr, pIndex, opts);
					} else {
						// TODO: plane 3
						// converted = this.convertGeneric(tileCode, tilePropertiesStr, pIndex, opts);
					}
				}
				if (converted && planeData) {
					if (converted.difficulty != null) {
						result.difficulties = Arrays.expandToLength(
							result.difficulties ?? [],
							Math.max(converted.difficulty, result.difficulties?.length ?? 0),
							(idx) => ({ name: `Difficulty ${idx + 1}` })
						);
					}
					planeData.tiles[converted.code] = converted;
					if (!tileOrderingByPlaneIndex[pIndex]) {
						planeData.tilesOrdering.push(converted.code);
					}
				}
				converted = undefined;
			}
		}

		return result;
	};

	protected convertWall = (code: number, data: string, plane: number, opts: IWDCToWadeConversionOpts = {}): MapTileData | undefined => {
		const { type, graphicIndex, gridImage, label } = this.parseTileLine(data, opts?.colorConversion);

		const isCodeEven = code % 2 === 0;
		const typesArr: TileType[] = [];
		let direction: TileDirection | undefined;
		let match;
		let graphicMethod: GraphicMethod = graphicIndex != null ? GraphicMethod.VSWAP : GraphicMethod.GRID;
		let keyId: string | undefined;

		if (type === "a") {
			typesArr.push(TileType.DEAF);
			graphicMethod = GraphicMethod.GRID;
		} else if (type === "d") {
			typesArr.push(TileType.DOOR, TileType.SOLID);
			direction = isCodeEven ? TileDirection.EAST_WEST : TileDirection.NORTH_SOUTH;
			graphicMethod = GraphicMethod.GRID;
		} else if (type === "e") {
			typesArr.push(TileType.ELEVATOR, TileType.SOLID, TileType.WALL);
			graphicMethod = GraphicMethod.GRID;
		} else if (type === "f") {
			typesArr.push(TileType.FLOOR_CODE);
			graphicMethod = GraphicMethod.GRID;
		} else if (type === "s") {
			typesArr.push(TileType.END_LEVEL, TileType.FLOOR_CODE, TileType.SECRET);
			graphicMethod = GraphicMethod.GRID;
		} else if (type === "w") {
			typesArr.push(TileType.SOLID, TileType.WALL);
		} else if ((match = type.match(/l(\d)/))) {
			typesArr.push(TileType.LOCKED_DOOR, TileType.SOLID);
			direction = isCodeEven ? TileDirection.EAST_WEST : TileDirection.NORTH_SOUTH;
			keyId = match[1];
			graphicMethod = GraphicMethod.GRID;
		} else {
			console.warn(chalk`⚠️ {yellow Unknown wall tile type '{bold.inverse ${type}}' in ${data}.}`);
		}

		const types = typesArr.reduce((acc, t) => {
			acc[t] = 1;
			return acc;
		}, {} as SafeDictionary<1, TileType>);

		return deleteNullAndUndefined({ code, direction, graphicIndex, graphicMethod, gridImage, keyId, label, plane, types });
	};

	protected convertObject = (code: number, data: string, plane: number, opts: IWDCToWadeConversionOpts = {}): MapTileData | undefined => {
		const { type, graphicIndex, gridImage, label } = this.parseTileLine(data, opts?.colorConversion);

		const typesArr: TileType[] = [];
		let direction: TileDirection | undefined;
		let difficulty;
		let match;
		let graphicMethod: GraphicMethod = graphicIndex != null ? GraphicMethod.VSWAP : GraphicMethod.GRID;
		let keyId: string | undefined;

		if (type === "o") {
			typesArr.push(TileType.STATIC);
		} else if (type === "s") {
			typesArr.push(TileType.SOLID, TileType.STATIC);
		} else if (type === "t") {
			typesArr.push(TileType.BONUS, TileType.STATIC);
		} else if (type === "d") {
			typesArr.push(TileType.SECRET);
			graphicMethod = GraphicMethod.GRID;
		} else if (type === "e") {
			typesArr.push(TileType.END_LEVEL);
			graphicMethod = GraphicMethod.GRID;
		} else if (type === "g") {
			typesArr.push(TileType.DEAD, TileType.ACTOR);
		} else if ((match = type.match(/b(\d)/))) {
			typesArr.push(TileType.BOSS, TileType.ACTOR);
			const bossFlag = match[1];
			if (bossFlag === "0") {
				typesArr.push(TileType.END_LEVEL); // inference leads to this (death cam)
			} else {
				keyId = bossFlag;
			}
		} else if ((match = type.match(/k(\d)/))) {
			typesArr.push(TileType.KEY, TileType.STATIC);
			keyId = match[1];
		} else if ((match = type.match(/p(\d)/))) {
			typesArr.push(TileType.PLAYER_START);
			direction = WDCMapDefinitionFileConverter.toTileDirection(match[1]);
			if (direction == null) {
				throw new Error(`Expected a direction for a directional tile but ${match[1]} could not be mapped to a direction.`);
			}
			graphicMethod = GraphicMethod.GRID;
		} else if ((match = type.match(/r(\d)/))) {
			typesArr.push(TileType.PATROL);
			direction = WDCMapDefinitionFileConverter.toTileDirection(match[1]);
			if (direction == null) {
				throw new Error(`Expected a direction for a directional tile but ${match[1]} could not be mapped to a direction.`);
			}
			graphicMethod = GraphicMethod.GRID;
		} else if ((match = type.match(/(\d)(\d)/))) {
			difficulty = toNumber(match[1]);
			direction = WDCMapDefinitionFileConverter.toTileDirection(match[2]);
			if (direction == null) {
				throw new Error(`Expected a direction for a directional tile but ${match[1]} could not be mapped to a direction.`);
			}
			typesArr.push(TileType.ACTOR);
			graphicMethod = GraphicMethod.GRID;
		} else if ((match = type.match(/(\d)m(\d)/))) {
			difficulty = toNumber(match[1]);
			direction = WDCMapDefinitionFileConverter.toTileDirection(match[2]);
			if (direction == null) {
				throw new Error(`Expected a direction for a directional tile but ${match[1]} could not be mapped to a direction.`);
			}
			typesArr.push(TileType.ACTOR, TileType.MOVING);
			graphicMethod = GraphicMethod.GRID;
		} else {
			console.warn(chalk`⚠️ {yellow Unknown object tile type '{bold.inverse ${type}}' in ${data}.}`);
		}

		difficulty = difficulty != null ? difficulty - 1 : undefined; // we are doing index based matching

		const types = typesArr.reduce((acc, t) => {
			acc[t] = 1;
			return acc;
		}, {} as SafeDictionary<1, TileType>);

		return deleteNullAndUndefined({ code, difficulty, direction, graphicIndex, gridImage, graphicMethod, keyId, label, plane, types });
	};

	protected getGridImage = (gridImageStr: string, colorConversion?: WDCColorConverter<string>) => {
		if (!colorConversion) {
			return;
		} else if (typeof colorConversion === "object") {
			return WDCMapDefinitionFileConverter.getColorConverter((hex) => colorConversion[hex])(gridImageStr);
		}
		return colorConversion(gridImageStr);
	};

	protected parseTileLine = (tileLine: string, colorConversion?: IWDCToWadeConversionOpts["colorConversion"]) => {
		const dataSplits = tileLine.split(",");

		let type: string;
		let graphicIndexStr = "";
		let gridImageStr: string;
		let nameSplits: string[];
		if (dataSplits[1].length === WDCMapDefinitionFileConverter.WDC_GRAPHIC_STR_LENGTH) {
			// graphicIndex missing
			[type, gridImageStr, ...nameSplits] = dataSplits;
		} else {
			[type, graphicIndexStr, gridImageStr, ...nameSplits] = dataSplits;
		}

		type = type.toLowerCase();
		let graphicIndex: number | undefined = toNumber(graphicIndexStr, { min: 0, outOfRangeOrNaN: Number.NaN });
		if (isNaN(graphicIndex)) {
			graphicIndex = undefined;
		}

		const gridImage = this.getGridImage(gridImageStr, colorConversion);
		const label = nameSplits.join(",");

		return { type, graphicIndex, gridImage, label };
	};
}
