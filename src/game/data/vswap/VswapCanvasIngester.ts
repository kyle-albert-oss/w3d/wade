import { Canvas } from "canvas";
import { IVswapGraphicChunk, IVswapHeader, IVswapIngester, PaletteColor, VswapChunkMeta } from "wolf3d-data";

import { AppCanvas, newCanvas } from "../../../util/canvases";

export abstract class VswapCanvasIngesterBase<R> implements IVswapIngester<R> {
	protected spritePageOffset?: number;
	protected canvas?: AppCanvas;
	protected twoD?: OffscreenCanvasRenderingContext2D;
	protected transparentColor: PaletteColor;

	constructor(protected palette: PaletteColor[]) {
		this.transparentColor = palette[palette.length - 1];
	}

	onInit(header: IVswapHeader): void {
		this.spritePageOffset = header.spritePageOffset;
		this.canvas = newCanvas(header.calculated.maxPageWidth, header.calculated.maxPageHeight);
		this.twoD = this.canvas.getContext("2d") as OffscreenCanvasRenderingContext2D;
	}

	accept = async (data: number[], pageIndex: number, meta: VswapChunkMeta): Promise<void> => {
		const { canvas, palette, transparentColor, twoD } = this;
		const { height, width } = meta;

		if (canvas == null || twoD == null || palette == null) {
			throw new Error("Cannot accept: not properly initialized.");
		}

		// map colors from palette
		const imgData = twoD.getImageData(0, 0, width, height);
		const imageRGBA = imgData.data;
		for (let col = 0, z = 0; col < width; col++) {
			for (let row = 0; row < height; row++, z += 4) {
				const palIdx = data[width * col + row];
				const [r, g, b] = palIdx != null ? palette[palIdx] : transparentColor;
				imageRGBA[z] = r;
				imageRGBA[z + 1] = g;
				imageRGBA[z + 2] = b;
				imageRGBA[z + 3] = palIdx != null ? 255 : 0;
			}
		}
		twoD.putImageData(imgData, 0, 0);
		await this.onCanvasReady(pageIndex, meta);
	};

	protected abstract onCanvasReady(pageIndex: number, meta: VswapChunkMeta): Promise<void>;
	abstract onComplete(): Promise<R>;
}

export interface IVswapWorkerResult {
	chunks: IVswapGraphicChunk[];
	spritePageOffset: number;
	transferables: any[];
}

export class VswapWorkerCanvasIngester extends VswapCanvasIngesterBase<IVswapWorkerResult> {
	protected chunks?: IVswapGraphicChunk[];
	protected transferables?: any[];

	onInit = (header: IVswapHeader) => {
		super.onInit(header);
		this.chunks = Array(header.soundPageOffset - 1);
		this.transferables = [];
	};

	onCanvasReady = async (pageIndex: number, meta: VswapChunkMeta) => {
		const { canvas, chunks, transferables } = this;
		if (canvas == null || chunks == null || transferables == null) {
			throw new Error("Cannot process canvas: not properly initialized.");
		}

		let rawData: ImageBitmap;
		if (typeof OffscreenCanvas !== "undefined" && canvas instanceof OffscreenCanvas) {
			// 🐌 for some reason, convertToBlob is extremely slow in worker context, so create image bitmap, then convert to blob in render thread.
			rawData = await (canvas as OffscreenCanvas).transferToImageBitmap();
		} else {
			rawData = await createImageBitmap((canvas as Canvas).getContext("2d").getImageData(0, 0, meta.width, meta.height));
		}

		chunks[pageIndex] = { rawData, meta, type: "graphic" };
		transferables.push(rawData);
	};

	onComplete = async (): Promise<IVswapWorkerResult> => {
		const { chunks, spritePageOffset, transferables } = this;

		if (chunks == null || spritePageOffset == null || transferables == null) {
			throw new Error("Cannot complete: not properly initialized.");
		}

		return {
			chunks,
			spritePageOffset,
			transferables,
		};
	};
}
