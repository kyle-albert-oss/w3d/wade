/**
 * Types specific to this project. This file should not import or export types so that they can remain usable without importing.
 */

// Base Types
type Maybe<T> = undefined | null | T;

type NullableKeys<T, K extends keyof T> = K extends unknown ? (undefined extends T[K] ? K : never) : never;
type NullableKeysOf<T> = NullableKeys<T, keyof T>;
type NonNullableKeysOf<T> = Exclude<keyof T, NullableKeysOf<T>>;

type NullableSlice<T> = Pick<T, NullableKeysOf<T>>;
type NonNullableSlice<T> = Pick<T, NonNullableKeysOf<T>>;

type OneOrMany<T> = T | T[];
type OneOrManyRO<T> = T | readonly T[];
type Supplier<T> = () => T;
type Consumer<T> = (item: T) => void;
type Fn<T, R> = (input: T) => R;

declare module "bigbuffer" {
	const BigBuffer: any;
	export default BigBuffer;
}
