import * as el from "./electron/electron";

/**
 * The main file exports can be used on the render thread via electron.remote so we're creating a type signature here to be use on instance without manually maintaining an interface to it.
 */
export type ElectronMainHandle = typeof el;
