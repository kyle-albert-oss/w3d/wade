import "@storybook/addon-actions/register";
import "@storybook/addon-knobs/register";
import "@storybook/addon-links/register";

import { STORIES_CONFIGURED } from "@storybook/core-events";
import AddonAPI, { addons } from "@storybook/addons";
import { themes } from "@storybook/theming";

addons.setConfig({
	enableShortcuts: false,
	theme: themes.dark,
});

AddonAPI.register("nav-to-default-story", (storybookApi) => {
	storybookApi.on(STORIES_CONFIGURED, (kind, story) => {
		storybookApi.selectStory("routes-mapeditor", "Default");
	});
});
