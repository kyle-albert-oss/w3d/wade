const overrides = require("../webpack.overrides.config");

module.exports = ({ config }) => {
	config = overrides.storybookWebpack(config);
	return config;
};
