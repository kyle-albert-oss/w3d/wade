# dev-scripts

## Creating a script

Because of how ts-node works and how these scripts pull in pieces of app code, add line

```typescript
/// <reference types="../src/global" />
```

to the top of the script file in order to prevent errors. See [ts-node docs](https://github.com/TypeStrong/ts-node#help-my-types-are-missing) for an explanation.

## Running a script

Execute

```shell script
npm run script -- dev-scripts/<script-name>.ts
```

### Generating an ID for a base resource

A base resource is a resource that will be available when WADE is installed (or updated). To generate a fixed ID, use the `generate-id` script in this directory. Base resources are stored in the `assets` folder.
