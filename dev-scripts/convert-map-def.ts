/// <reference types="../src/global" />
import * as path from "path";

import { WDCMapDefinitionFileConverter } from "../src/game/data/maps/MapDefinitionFiles";
import { writeMapDefinition } from "../src/workers/resource";
import { WL1_MAP_DEF_ID, WL6_MAP_DEF_ID, SOD_MAP_DEF_ID } from "../src/models/map-definition";

const BASELINE_MAP_DEFS = [
	{
		id: WL1_MAP_DEF_ID,
		name: "Wolfenstein 3D Shareware",
		extensions: ["WL1"],
		wdcFile: path.resolve(__dirname, "../assets-test/wdc/WL1.wmc"),
	},
	{
		id: WL6_MAP_DEF_ID,
		name: "Wolfenstein 3D",
		extensions: ["WL6"],
		wdcFile: path.resolve(__dirname, "../assets-test/wdc/WL6.wmc"),
	},
	{
		id: SOD_MAP_DEF_ID,
		name: "Spear of Destiny",
		extensions: ["SOD"],
		wdcFile: path.resolve(__dirname, "../assets-test/wdc/SOD.wmc"),
	},
];

(async () => {
	const converter = new WDCMapDefinitionFileConverter();
	const defaultColorConversion = WDCMapDefinitionFileConverter.getColorConverter((hex) => WDCMapDefinitionFileConverter.WDC_COLORS[hex]);

	for (const baseline of BASELINE_MAP_DEFS) {
		const { id, name, extensions, wdcFile } = baseline;
		const md = await converter.convertToWadeFormat(wdcFile, {
			id,
			name,
			extensions,
			colorConversion: defaultColorConversion,
		});
		await writeMapDefinition({ ...md, isTemplate: true }, path.resolve(__dirname, `../assets`));
	}
})().catch((e) => {
	console.error(e);
	process.exit(1);
});
