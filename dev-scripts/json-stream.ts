/// <reference types="../src/global" />

import { readAvailableResourcesInDir } from "../src/workers/resource";

(async () => {
	const result = await readAvailableResourcesInDir("./tmp");
	console.log(result);
})().catch((e) => {
	console.error(e);
	process.exit(1);
});
