/// <reference types="../src/global" />

import * as path from "path";

import { GameMapsReader, IVswapHeader, PaletteColor, VswapChunkMeta, VswapFileReader } from "wolf3d-data";
import * as fs from "fs-extra";
import chalk from "chalk";

import { IEditableMap } from "../src/models/map";
import { readPalette } from "../src/workers/resource";
import { VswapCanvasIngesterBase } from "../src/game/data/vswap/VswapCanvasIngester";
import { IWadeVswapGraphicChunk } from "../src/models/vswap";
import { toDataUrl } from "../src/util/canvases";

const WL6_PALETTE_ID = "eb9e1e60-c1f4-11e9-a7d2-13e2d0a6422c";

//
// COPY FILES TO THIS DIRECTORY TO USE
//
const gameDataBasePath = path.resolve(process.cwd(), "assets-test/game-files/wl6");
const wadeResourceBasePath = path.resolve(process.cwd(), "assets");
const outputPath = path.resolve(process.cwd(), "assets-test/stories");

interface INodeCanvasIngesterResult {
	graphicChunks: IWadeVswapGraphicChunk[];
	spriteStartIndex: number;
}

class VswapNodeCanvasIngester extends VswapCanvasIngesterBase<INodeCanvasIngesterResult> {
	protected chunks!: IWadeVswapGraphicChunk[];

	constructor(palette: PaletteColor[]) {
		super(palette);
	}

	onInit = (header: IVswapHeader): void => {
		super.onInit(header);
		this.chunks = [];
	};

	onCanvasReady = async (pageIndex: number, meta: VswapChunkMeta): Promise<void> => {
		const { canvas, chunks } = this;
		const dataUrl = await toDataUrl(canvas!);
		chunks.push({ url: dataUrl, meta, type: "graphic" });
	};

	onComplete = (): Promise<INodeCanvasIngesterResult> => {
		return Promise.resolve({ graphicChunks: this.chunks, spriteStartIndex: this.spritePageOffset! });
	};
}

(async () => {
	fs.mkdirpSync(outputPath);

	// write maps
	const gmr = new GameMapsReader(path.resolve(gameDataBasePath, "MAPHEAD.WL6"), path.resolve(gameDataBasePath, "GAMEMAPS.WL6"), {
		numPlanes: 3,
	});
	const maps = (await gmr.readAllMaps()).reduce((acc, mapData, index) => {
		acc.push({ data: mapData, index });
		return acc;
	}, [] as IEditableMap[]);

	console.log(chalk`✅ {green Successfully read maps.}`);
	console.log(chalk`✏️ {blue Writing maps.json...}`);

	fs.writeFileSync(path.resolve(outputPath, "maps.json"), JSON.stringify(maps));

	console.log(chalk`✅ {green Wrote {bold ${path.resolve(outputPath, "maps.json")}}}`);

	// write graphics data
	const palette = await readPalette(WL6_PALETTE_ID, wadeResourceBasePath);
	const vfr = new VswapFileReader(path.resolve(gameDataBasePath, "VSWAP.WL6"));
	const partialGraphicState = await vfr.getData(new VswapNodeCanvasIngester(palette.data.colors));

	console.log(chalk`✅ {green Successfully read vswap.}`);
	console.log(chalk`✏️ {blue Writing vswap.json...}`);

	fs.writeFileSync(path.resolve(outputPath, "vswap.json"), JSON.stringify(partialGraphicState));

	console.log(chalk`✅ {green Wrote {bold ${path.resolve(outputPath, "vswap.json")}}}`);
})().catch((e) => {
	console.error(e);
	process.exit(1);
});
