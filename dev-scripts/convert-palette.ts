/// <reference types="../src/global" />
import * as path from "path";

import { readJascPaletteFileColors } from "wolf3d-data";

import { writePalette } from "../src/workers/resource";
import { SOD_PAL_ID, WL6_PAL_ID } from "../src/models/palette";

const BASELINE_PALETTES = [
	{
		id: WL6_PAL_ID,
		name: "Wolfenstein 3D",
		extensions: ["WL1", "WL6"],
		jascPalFile: path.resolve(__dirname, "../assets-test/wdc/Wolf3D.pal"),
	},
	{
		id: SOD_PAL_ID,
		name: "Spear of Destiny",
		extensions: ["SOD"],
		jascPalFile: path.resolve(__dirname, "../assets-test/wdc/Spear.pal"),
	},
];

(async () => {
	for (const baseline of BASELINE_PALETTES) {
		const { id, name, extensions, jascPalFile } = baseline;
		const colors = await readJascPaletteFileColors(jascPalFile);

		await writePalette({ id, name, extensions, data: { colors }, isTemplate: true }, path.resolve(__dirname, "../assets"));
	}
})().catch((e) => {
	console.error(e);
	process.exit(1);
});
